<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Searchprojectcru_model extends CI_Model {
    /*  var $table = 'bd_tenderdetails as a';
      var $genertedid_table = 'tender_generated_id as b';
      var $bid_table = 'bid_project as e';
      var $project_description_table = 'project_description as c';

      var $countries_table = 'countries as f';
      var $states_table = 'states as g';
      var $main_services_table = 'main_services as h';
      var $sector_table = 'sector as i'; */

    var $table = 'bd_tenderdetail as a';
    var $genertedid_table = 'tender_generated_id as b';
    var $bid_table = 'bdproject_status as e';
    var $project_description_table = 'project_description as c';
    var $countries_table = 'countries as f';
    var $states_table = 'states as g';
    var $main_services_table = 'main_services as h';
    var $sector_table = 'sector as i';
    var $scope_table = 'bdtender_scope as j';
    var $column_order = array(null, 'a.TenderDetails', 'a.Location', 'a.Organization', 'a.keyword_phase'); //set column field database for datatable orderable
    var $column_search = array('b.generated_tenderid', 'a.keyword_phase', 'a.TenderDetails', 'a.Location', 'a.Organization'); //set column field database for datatable searchable 
    var $order = array('a.fld_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        if (!empty($this->input->post('date_cond')) == '1') {
            $date_cond = ' AND ';
        } elseif (!empty($this->input->post('date_cond')) == '2') {
            $date_cond = ' OR ';
        } else {
            $date_cond = '';
        }

        if (!empty($this->input->post('service_cond')) == '1') {
            $service_cond = ' AND ';
        } elseif (!empty($this->input->post('service_cond')) == '2') {
            $service_cond = ' OR ';
        } else {
            $service_cond = '';
        }

        if (!empty($this->input->post('sector_cond')) == '1') {
            $sector_cond = ' AND ';
        } elseif (!empty($this->input->post('sector_cond')) == '2') {
            $sector_cond = ' OR ';
        } else {
            $sector_cond = '';
        }

        if (!empty($this->input->post('country_cond')) == '1') {
            $country_cond = ' AND ';
        } elseif (!empty($this->input->post('country_cond')) == '2') {
            $country_cond = ' OR ';
        } else {
            $country_cond = '';
        }
        /*
          if(!empty($this->input->post('state_cond')) == '1'){
          $state_cond = ' AND ';
          }elseif(!empty($this->input->post('state_cond')) == '2'){
          $state_cond = ' OR ';
          }else{
          $state_cond = '';
          } */


        if ($this->input->post('start_dates') and ( $this->input->post('end_dates'))) {
            $start_date = $this->input->post('start_dates');
            $end_date = $this->input->post('end_dates');
            $dates = "(c.submission_date >= '$start_date' AND c.submission_date <= '$end_date')";
        } else {
            $dates = '';
        }

        if (!empty($this->input->post('services'))) {
            $services = " c.service_id = " . $this->input->post('services');
        } else {
            $services = '';
        }

        if (!empty($this->input->post('sector'))) {
            $sector = " c.sector_id = " . $this->input->post('sector');
        } else {
            $sector = '';
        }

        if (!empty($this->input->post('country'))) {
            $country = " c.country_id = " . $this->input->post('country');
            //$this->db->where($where);
        } else {
            $country = '';
        }

        if (!empty($this->input->post('state'))) {
            $state = " AND c.state_id = " . $this->input->post('state');
            //$this->db->where($where);
        } else {
            $state = '';
        }

        if (!empty($this->input->post('status'))) {
            $status = " e.project_status = " . $this->input->post('status');
        } else {
            $status = '';
        }



        if (!empty($date_cond) != '0' || !empty($service_cond) != '0' || !empty($sector_cond) != '0' || !empty($country_cond) != '0' ||
                !empty($dates) || !empty($services) || !empty($sector) || !empty($country) || !empty($state) || !empty($status)
        ) {
            $wheres = $dates . $date_cond . $services . $service_cond . $sector . $sector_cond . $country . $state . $country_cond . $status;
            $this->db->where($wheres);
        }

        $this->db->select('a.fld_id,a.TenderDetails,a.Organization,j.visible_scope,b.generated_tenderid,b.generate_type,e.project_status,h.service_name,f.country_name,g.state_name,i.sectName');
        $this->db->from($this->table);
        $this->db->join($this->scope_table, 'a.fld_id = j.project_id', 'left');
        $this->db->join($this->bid_table, 'a.fld_id = e.project_id', 'left');
        $this->db->join($this->genertedid_table, 'a.fld_id = b.project_id', 'left');
        $this->db->join($this->project_description_table, 'a.fld_id = c.project_id', 'left');
        $this->db->join($this->countries_table, 'c.country_id = f.country_id', 'left');
        $this->db->join($this->states_table, 'c.state_id = g.state_id', 'left');
        $this->db->join($this->main_services_table, 'c.service_id = h.id', 'left');
        $this->db->join($this->sector_table, 'c.sector_id = i.fld_id', 'left');

        // $this->db->where('a.is_active', '1');
        $scope = array('Bid_project', 'To_Go_project');
        $this->db->where_in('j.visible_scope', $scope);
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        //$this->db->where('a.is_active', '1');
        $this->db->where('j.visible_scope', 'Bid_project');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        // $this->db->from($this->table);
        // $this->db->where('a.is_active', '1');
        // $this->db->where('a.visible_scope', 'Bid_project');
        //  return $this->db->count_all_results();
        return '0';
    }

}
