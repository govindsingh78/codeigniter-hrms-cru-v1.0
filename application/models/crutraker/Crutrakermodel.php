<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crutrakermodel extends CI_Model {

    // table for add project for cru traker
    var $table = 'bdcruvacant_project as a';
    var $order = array('project_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bd_tenderdetail.TenderDetails,$db1.bdcegexp_proj_summery.project_id,$db1.bdcegexp_proj_summery.project_numberid,$db2.tm_projects.project_name");
        $this->db->from("$db1.bdcruvacant_project");
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id = $db1.bdcruvacant_project.project_id", 'left');
        $this->db->join("$db1.bdcegexp_proj_summery", "$db1.bdcruvacant_project.project_id = $db1.bdcegexp_proj_summery.project_numberid", 'left');
        $this->db->join("$db1.bd_tenderdetail", "$db1.bd_tenderdetail.fld_id = $db1.bdcegexp_proj_summery.project_id", 'inner	');

        $this->db->where(array("$db1.bdcruvacant_project.status" => '1'));
        $this->db->order_by("$db1.bdcruvacant_project.project_id", 'ASC');
        $this->db->group_by("$db1.bdcruvacant_project.project_id");
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $ResultRec = $query->result();

        $recordArr = array();
        if ($ResultRec) {
            foreach ($ResultRec as $recR) {
                $projId = $recR->project_id;
            }
        }
        return $ResultRec;
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    //code by durgesh Fetch Details of project..
    public function GetProjectList() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
        $this->db->from("$db2.tm_projects");
        $this->db->where(array("$db2.tm_projects.is_active" => '1'));
        $this->db->where("($db2.tm_projects.project_category='ie' OR $db2.tm_projects.project_category='ae')", NULL, FALSE);
        $this->db->order_by("$db2.tm_projects.project_name", 'ASC');
        $result = $this->db->get()->result();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    //code by durgesh Fetch Details of users..
    public function GetUserList() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_users.id,$db2.main_users.userfullname,$db2.main_users.employeeId");
        $this->db->from("$db2.main_users");
        $this->db->where(array("$db2.main_users.isactive" => '1'));
        $this->db->where("$db2.main_users.id > ", '190');
        $this->db->order_by("$db2.main_users.id", 'ASC');
        $result = $this->db->get()->result();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    /// Get staff by project id code by durgesh
    public function GetStaffByProjID($project_id, $keyid) {
        $db2 = $this->db1->database;
        $db1 = $this->db2->database;
        $this->db->select("$db2.designation_master_requisition.fld_id,$db2.assign_finalteam.id,$db2.assign_finalteam.srno,$db2.assign_finalteam.designation_id,$db2.assign_finalteam.empname,$db2.assign_finalteam.project_id,$db1.main_employees_summary.userfullname,$db2.designation_master_requisition.designation_name,$db2.assign_finalteam.man_months,$db2.assign_finalteam.rate");
        $this->db->from("$db2.assign_finalteam");
        $this->db->join("$db1.main_employees_summary", "$db2.assign_finalteam.empname = $db1.main_employees_summary.user_id", 'left');
        $this->db->join("$db2.designation_master_requisition", "$db2.assign_finalteam.designation_id = $db2.designation_master_requisition.fld_id", 'left');
        $this->db->where("$db2.assign_finalteam.project_id", $project_id);
        $this->db->where("$db2.assign_finalteam.key_id", $keyid);
        $this->db->order_by("$db2.assign_finalteam.srno", 'asc');
        $recArr = $this->db->get()->result_array();
        $pluginArr = array();
        if ($recArr) {
            foreach ($recArr as $Rowarr) {
                $projID = $Rowarr['project_id'];
                $empId = $Rowarr['empname'];
                $Rowarr['BalanceMm'] = $this->Gettotalcumulativemm($projID, $empId);
                array_push($pluginArr, $Rowarr);
            }
        }
        return ($pluginArr) ? $pluginArr : null;
    }

    /// Get invoice data by project id and employee id code by durgesh
    public function Gettotalcumulativemm($projID, $empId) {
        //Get Lastest invc details for blnv mm...
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db1.invoicesave.*");
        $this->db->FROM("$db1.invoicesave");
        $this->db->WHERE(array("$db1.invoicesave.project_id" => $projID, "$db1.invoicesave.emp_id" => $empId));
        $this->db->ORDER_BY("$db1.invoicesave.id", "DESC");
        $recRowArr = $this->db->get()->row();
        $BalanceMm = null;
        if ($recRowArr) {
            $BalanceMm = ($recRowArr->mm - $recRowArr->totalcumulativemm);
        }
        return $BalanceMm;
    }

    // Code by durgesh For Get project Name or code......
    public function Get_project_name_or_code_record($projid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcegexp_proj_summery.project_id,$db1.bd_tenderdetail.TenderDetails,$db2.tm_projects.project_name");
        $this->db->from("$db1.bdcegexp_proj_summery");
        $this->db->join("$db1.bd_tenderdetail", "$db1.bdcegexp_proj_summery.project_id=$db1.bd_tenderdetail.fld_id", "left");
        $this->db->join("$db2.tm_projects", "$db1.bdcegexp_proj_summery.project_numberid=$db2.tm_projects.id", "left");
        $this->db->where(array("$db1.bdcegexp_proj_summery.project_id" => $projid));
        $this->db->order_by("$db1.bdcegexp_proj_summery.project_id", "ASC");
        $result = $this->db->get()->row_array();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    // Code by durgesh
    public function designation_name($desiid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.invc_vacant_resign_date.last_date_resign,$db1.assign_finalteam.designation_id,$db1.designation_master_requisition.designation_name");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id", "inner");
        $this->db->join("$db1.invc_vacant_resign_date", "$db1.invc_vacant_resign_date.designation_id=$db1.designation_master_requisition.fld_id", "inner");
        $this->db->where(array("$db1.assign_finalteam.designation_id" => $desiid));
        $this->db->order_by("$db1.assign_finalteam.project_id", "ASC");
        $result = $this->db->get()->row_array();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    // Code by durgesh
    public function Get_project_name_or_code_designation_record($projid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcegexp_proj_summery.project_id,$db1.bd_tenderdetail.TenderDetails,$db2.tm_projects.project_name,$db1.project_coordinator.emp_id as proj_coordinator_empid");
        $this->db->from("$db1.bdcegexp_proj_summery");
        $this->db->join("$db1.assign_finalteam", "$db1.assign_finalteam.project_id=$db1.bdcegexp_proj_summery.project_id", "left");
        $this->db->join("$db1.bd_tenderdetail", "$db1.bdcegexp_proj_summery.project_id=$db1.bd_tenderdetail.fld_id", "left");
        $this->db->join("$db2.tm_projects", "$db1.bdcegexp_proj_summery.project_numberid=$db2.tm_projects.id", "left");
        $this->db->join("$db1.project_coordinator", "$db1.bdcegexp_proj_summery.project_id=$db1.project_coordinator.bd_project_id", "LEFT");
        
        $this->db->where(array("$db1.bdcegexp_proj_summery.project_id" => $projid));
        $this->db->order_by("$db1.bdcegexp_proj_summery.project_id", "ASC");
        $result = $this->db->get()->row_array();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    // Code by durgesh For Get vacant position......
    public function Get_replacement($replc_id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.replacementteam.*");
        $this->db->from("$db1.replacementteam");
        $this->db->where(array("$db1.replacementteam.replacement_id" => $replc_id));
        $this->db->order_by("$db1.replacementteam.replacement_id", "ASC");
        $result = $this->db->get()->row_array();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    // Code by durgesh For Get Project Coordinator List......
    public function GetProjectCoordinatorlist($projid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcruvacant_crupanel.*,$db1.designation_master_requisition.fld_id,$db1.designation_master_requisition.designation_name,$db1.bd_tenderdetail.TenderDetails,$db2.tm_projects.project_name");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db1.assign_finalteam", "$db1.assign_finalteam.project_id=$db1.bdcruvacant_crupanel.project_id", "inner");
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id", "inner");
        $this->db->join("$db1.bdcegexp_proj_summery", "$db1.bdcegexp_proj_summery.project_id=$db1.assign_finalteam.project_id", "inner");
        $this->db->join("$db1.bd_tenderdetail", "$db1.bdcegexp_proj_summery.project_id=$db1.bd_tenderdetail.fld_id", "inner");
        $this->db->join("$db2.tm_projects", "$db1.bdcegexp_proj_summery.project_numberid=$db2.tm_projects.id", "inner");
        $this->db->where(array("$db1.bdcruvacant_crupanel.project_id" => $projid));
        $this->db->order_by("$db1.bdcruvacant_crupanel.project_id", "ASC");
        $result = $this->db->get()->result();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    //Get Bd Team LIst Arr..
    public function GetTeamEmplistByDeptID($deptID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_employees_summary.userfullname,$db2.main_employees_summary.user_id,$db2.main_employees_summary.emailaddress");
        $this->db->from("$db2.main_employees_summary");
        $this->db->where(array("$db2.main_employees_summary.department_id" => $deptID, "$db2.main_employees_summary.isactive" => "1"));
        $this->db->order_by("$db2.main_employees_summary.userfullname", "ASC");
        $result = $this->db->get()->result();
        if ($result) {
            return ($result) ? $result : null;
        }
    }

    public function GetEmpDetailsID($empID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_employees_summary.userfullname,$db2.main_employees_summary.user_id,$db2.main_employees_summary.emailaddress");
        $this->db->from("$db2.main_employees_summary");
        $this->db->where(array("$db2.main_employees_summary.user_id" => $empID, "$db2.main_employees_summary.isactive" => "1"));
        $this->db->order_by("$db2.main_employees_summary.userfullname", "ASC");
        $result = $this->db->get()->row();
        if ($result) {
            return ($result) ? $result : null;
        }
    }

    //Get Bd Team IDES Arr..
    public function GetEmpIDslistByDeptID($deptID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_employees_summary.user_id");
        $this->db->from("$db2.main_employees_summary");
        $this->db->where(array("$db2.main_employees_summary.department_id" => $deptID, "$db2.main_employees_summary.isactive" => "1"));
        $this->db->order_by("$db2.main_employees_summary.userfullname", "ASC");
        $result = $this->db->get()->result();
        $retun = array();
        if ($result) {
            foreach ($result as $kEyy => $roWs) {
                $retun[] = $roWs->user_id;
            }
        }
        return ($retun) ? $retun : "";
    }

    public function GetCvFormattetingDataDetailsbyid($rowId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.bdcruvacant_crupanel.id");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.curr_status" => "2"));



//        $this->db->select("$db2.reqtobduser.emailaddress as reqtobduser_emailaddress,$db2.reqtobduser.userfullname as reqtobduser_userfullname,$db2.reqtobduser.userfullname as reqtobduser_userfullname,$db2.bdteamuser.userfullname as userfullname_bdteamuser,$db1.bdcruvacant_crupanel.aftercv_formatting_bd,$db1.bdcruvacant_crupanel.cv_formating_lock,$db1.bdcruvacant_crupanel.cv_formatting_bdteam,$db1.bdcruvacant_crupanel.id,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.resourceuser_inhouse.userfullname as resourceuser_inhouse_userfullname,$db2.resourceuser_inhouse.emailaddress as resourceuser_inhouse_emailaddress,$db2.resourceuser_inhouse.contactnumber as resourceuser_inhouse_contactnumber,$db2.tm_projects.project_name,$db1.designation_master_requisition.designation_name,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.entry_date,$db2.main_employees_summary.userfullname as entryby,$db2.coord.userfullname as coord_userfullname,$db1.bdcruvacant_crupanel.approval_status_chng_by,$db1.bdcruvacant_crupanel.approval_status_chng_date,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.step_by_cru,$db1.bdcruvacant_crupanel.cv_sent_to_client,$db2.cvsentclientby.userfullname as cvsentclientby_userfullname,$db2.clntcvappryesornoentryby.userfullname as clntcvappryesornoentryby_userfullname,$db1.bdcruvacant_crupanel.client_cvappr_yesorno,$db1.bdcruvacant_crupanel.interaction_date,$db1.bdcruvacant_crupanel.final_cvappr_yesorno,$db1.bdcruvacant_crupanel.mobilization_date,$db1.bdcruvacant_crupanel.inhouse_or_other,$db2.main_employees_summary.emailaddress as entrybyemail");
//        $this->db->from("$db1.bdcruvacant_crupanel");
//        $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_employees_summary.user_id", "LEFT");
//        $this->db->join("$db2.main_employees_summary as coord", "$db1.bdcruvacant_crupanel.approval_status_chng_by=$db2.coord.user_id", "LEFT");
//        $this->db->join("$db2.main_employees_summary as cvsentclientby", "$db1.bdcruvacant_crupanel.cv_sent_to_client_by=$db2.cvsentclientby.user_id", "LEFT");
//        $this->db->join("$db2.main_employees_summary as clntcvappryesornoentryby", "$db1.bdcruvacant_crupanel.client_cvappr_entryby=$db2.clntcvappryesornoentryby.user_id", "LEFT");
//        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.bdcruvacant_crupanel.designation_id", 'LEFT');
//        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.bdcruvacant_crupanel.project_id", 'LEFT');
//        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
//        $this->db->join("$db2.main_employees_summary as resourceuser_inhouse", "$db1.bdcruvacant_crupanel.user_id=$db2.resourceuser_inhouse.user_id", "LEFT");
//        $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
//        $this->db->join("$db2.main_employees_summary as bdteamuser", "$db1.bdcruvacant_crupanel.cv_formatteby=$db2.bdteamuser.user_id", "LEFT");
//        $this->db->join("$db2.main_employees_summary as reqtobduser", "$db1.bdcruvacant_crupanel.cv_formatting_reqbyid=$db2.reqtobduser.user_id", "LEFT");
//
//        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.id" => $rowId));
        $resultRec = $this->db->get()->result();
        return ($resultRec) ? $resultRec : null;
    }

    public function GetAllOtherEmployeeList() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        //Get All Other Employee..
        $this->db->select("$db1.team_req_otheremp.*");
        $this->db->from("$db1.team_req_otheremp");
        $this->db->where(array("$db1.team_req_otheremp.status" => "1"));
        $otherEmprec = $this->db->get()->result();
        return ($otherEmprec) ? $otherEmprec : null;
    }

    public function GetPositionRecDetail($bdProjId, $positionID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        //Get All Other Employee..
        $this->db->select("$db1.team_req_otheremp.*");
        $this->db->from("$db1.team_req_otheremp");
        $this->db->where(array("$db1.team_req_otheremp.status" => "1"));
        $otherEmprec = $this->db->get()->result();
        return ($otherEmprec) ? $otherEmprec : null;
    }

    //#############################################################################################################################
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$      Function Call   After 17-09-2019     $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    //#############################################################################################################################
    public function GetRecCvonProject($bdProjId, $positionID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcruvacant_crupanel.id,$db1.bdcruvacant_crupanel.project_id,$db1.bdcruvacant_crupanel.designation_id,$db1.bdcruvacant_crupanel.user_id,$db1.bdcruvacant_crupanel.inhouse_or_other,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.entry_date,$db2.entrybyEMP.userfullname as entryby_userfullname,$db2.hrmsempl.userfullname as userfullname_ihr,$db2.hrmsempl.contactnumber as contactnumber_ihr,$db2.hrmsempl.emailaddress as emailaddress_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.apprvbypcuser.prefix_name as apprvbypcuser_prefix_name,$db2.apprvbypcuser.userfullname as apprvbypcuser_userfullname,$db1.bdcruvacant_crupanel.apprv_reject_datebypc,$db2.bdteamuser.prefix_name as bdteamuser_prefix_name,$db2.bdteamuser.userfullname as bdteamuser_userfullname,$db1.bdcruvacant_crupanel.cvformt_statuschng_date,$db1.bdcruvacant_crupanel.formatted_cv_resume");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary as entrybyEMP", "$db1.bdcruvacant_crupanel.entry_by=$db2.entrybyEMP.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as hrmsempl", "$db1.bdcruvacant_crupanel.user_id=$db2.hrmsempl.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as apprvbypcuser", "$db1.bdcruvacant_crupanel.pcid_apprv_reject=$db2.apprvbypcuser.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as bdteamuser", "$db1.bdcruvacant_crupanel.bdempid_cvform_yes=$db2.bdteamuser.user_id", "LEFT");

        $this->db->where(array("$db1.bdcruvacant_crupanel.project_id" => $bdProjId, "$db1.bdcruvacant_crupanel.designation_id" => $positionID, "$db1.bdcruvacant_crupanel.status" => "1"));
        $CvRec = $this->db->get()->result();
        return ($CvRec) ? $CvRec : null;
    }

    public function ActiveRecSingleCv($bdProjId, $positionID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcruvacant_crupanel.*,$db1.bdcruvacant_crupanel.designation_id,$db1.bdcruvacant_crupanel.user_id,$db1.bdcruvacant_crupanel.inhouse_or_other,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.entry_date,$db2.entrybyEMP.userfullname as entryby_userfullname,$db2.hrmsempl.userfullname as userfullname_ihr,$db2.hrmsempl.contactnumber as contactnumber_ihr,$db2.hrmsempl.emailaddress as emailaddress_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.apprvbypcuser.prefix_name as apprvbypcuser_prefix_name,$db2.apprvbypcuser.userfullname as apprvbypcuser_userfullname,$db2.bdteamuser.prefix_name as bdteamuser_prefix_name,$db2.bdteamuser.userfullname as bdteamuser_userfullname,$db2.cvsentclientuser.prefix_name as cvsentclientuser_prefix_name,$db2.cvsentclientuser.userfullname as cvsentclientuser_userfullname,$db2.finalapprvsetuser.prefix_name as finalapprvsetuser_prefix_name,$db2.finalapprvsetuser.userfullname as finalapprvsetuser_userfullname");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary as entrybyEMP", "$db1.bdcruvacant_crupanel.entry_by=$db2.entrybyEMP.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as hrmsempl", "$db1.bdcruvacant_crupanel.user_id=$db2.hrmsempl.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as apprvbypcuser", "$db1.bdcruvacant_crupanel.pcid_apprv_reject=$db2.apprvbypcuser.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as bdteamuser", "$db1.bdcruvacant_crupanel.bdempid_cvform_yes=$db2.bdteamuser.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as cvsentclientuser", "$db1.bdcruvacant_crupanel.cv_sent_toclient_byuid=$db2.cvsentclientuser.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as finalapprvsetuser", "$db1.bdcruvacant_crupanel.mobil_interdate_setby=$db2.finalapprvsetuser.user_id", "LEFT");

        $this->db->where(array("$db1.bdcruvacant_crupanel.project_id" => $bdProjId, "$db1.bdcruvacant_crupanel.designation_id" => $positionID, "$db1.bdcruvacant_crupanel.status" => "1"));
        $this->db->where("$db1.bdcruvacant_crupanel.curr_status!=", "8", NULL, FALSE);
        $this->db->where("$db1.bdcruvacant_crupanel.curr_status!=", "3", NULL, FALSE);
        $CvRec = $this->db->get()->row();
        return ($CvRec) ? $CvRec : null;
    }

    public function GetCvFormattetingData() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcruvacant_crupanel.id,$db1.bdcruvacant_crupanel.project_id,$db1.bdcruvacant_crupanel.designation_id,$db1.bdcruvacant_crupanel.user_id,$db1.bdcruvacant_crupanel.inhouse_or_other,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.entry_date,$db2.entrybyEMP.userfullname as entryby_userfullname,$db2.hrmsempl.userfullname as userfullname_ihr,$db2.hrmsempl.contactnumber as contactnumber_ihr,$db2.hrmsempl.emailaddress as emailaddress_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db1.designation_master_requisition.designation_name,$db2.tm_projects.project_name,$db2.apprvbypcuser.prefix_name as apprvbypcuser_prefix_name,$db2.apprvbypcuser.userfullname as apprvbypcuser_userfullname");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary as entrybyEMP", "$db1.bdcruvacant_crupanel.entry_by=$db2.entrybyEMP.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as hrmsempl", "$db1.bdcruvacant_crupanel.user_id=$db2.hrmsempl.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.bdcruvacant_crupanel.designation_id", 'LEFT');
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.bdcruvacant_crupanel.project_id", 'LEFT');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
        $this->db->join("$db2.main_employees_summary as apprvbypcuser", "$db1.bdcruvacant_crupanel.pcid_apprv_reject=$db2.apprvbypcuser.user_id", "LEFT");
        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.curr_status" => "2"));
        $this->db->group_by("$db1.bdcruvacant_crupanel.id");
        $resultRec = $this->db->get()->result();
        return ($resultRec) ? $resultRec : null;
    }

    // Code by durgesh For Get vacant position......
    public function GetTblRecByIDSingleRow($tblName, $whereArr) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("*");
        $this->db->from($tblName);
        $this->db->where($whereArr);
        $result = $this->db->get()->row();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    public function GetTblNumRowsByCond($tblName, $whereArr) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("*");
        $this->db->from($tblName);
        $this->db->where($whereArr);
        $result = $this->db->get()->num_rows();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    //Get Details For Email..
    public function GetRecDetailsForMail($rowID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.bdcruvacant_crupanel.*,$db1.bdcruvacant_crupanel.designation_id,$db1.bdcruvacant_crupanel.user_id,$db1.bdcruvacant_crupanel.inhouse_or_other,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.entry_date,$db2.entrybyEMP.userfullname as entryby_userfullname,$db2.entrybyEMP.emailaddress as entryby_emailaddress,$db2.hrmsempl.userfullname as userfullname_ihr,$db2.hrmsempl.contactnumber as contactnumber_ihr,$db2.hrmsempl.emailaddress as emailaddress_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.apprvbypcuser.prefix_name as apprvbypcuser_prefix_name,$db2.apprvbypcuser.userfullname as apprvbypcuser_userfullname,$db2.bdteamuser.prefix_name as bdteamuser_prefix_name,$db2.bdteamuser.userfullname as bdteamuser_userfullname,$db2.cvsentclientuser.prefix_name as cvsentclientuser_prefix_name,$db2.cvsentclientuser.userfullname as cvsentclientuser_userfullname,$db2.finalapprvsetuser.prefix_name as finalapprvsetuser_prefix_name,$db2.finalapprvsetuser.userfullname as finalapprvsetuser_userfullname,$db2.projcoorduser.prefix_name as projcoorduser_prefix_name,$db2.projcoorduser.userfullname as projcoorduser_userfullname,$db2.projcoorduser.emailaddress as projcoorduser_emailaddress,$db1.project_coordinator.emp_id as proj_coordinator_user_id,$db1.designation_master_requisition.designation_name,$db2.tm_projects.project_name");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary as entrybyEMP", "$db1.bdcruvacant_crupanel.entry_by=$db2.entrybyEMP.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as hrmsempl", "$db1.bdcruvacant_crupanel.user_id=$db2.hrmsempl.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as apprvbypcuser", "$db1.bdcruvacant_crupanel.pcid_apprv_reject=$db2.apprvbypcuser.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as bdteamuser", "$db1.bdcruvacant_crupanel.bdempid_cvform_yes=$db2.bdteamuser.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as cvsentclientuser", "$db1.bdcruvacant_crupanel.cv_sent_toclient_byuid=$db2.cvsentclientuser.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as finalapprvsetuser", "$db1.bdcruvacant_crupanel.mobil_interdate_setby=$db2.finalapprvsetuser.user_id", "LEFT");
        $this->db->join("$db1.project_coordinator", "$db1.bdcruvacant_crupanel.project_id=$db1.project_coordinator.bd_project_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as projcoorduser", "$db1.project_coordinator.emp_id=$db2.projcoorduser.user_id", "LEFT");

        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.bdcruvacant_crupanel.designation_id", 'LEFT');
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.bdcruvacant_crupanel.project_id", 'LEFT');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
        
        $this->db->where(["$db1.bdcruvacant_crupanel.id" => $rowID, "$db1.bdcruvacant_crupanel.status" => "1"]);
        $this->db->where("$db1.bdcruvacant_crupanel.curr_status!=", "8", NULL, FALSE);
//        $this->db->where("$db1.bdcruvacant_crupanel.curr_status!=", "3", NULL, FALSE);
        $recDetails = $this->db->get()->row();
        return ($recDetails) ? $recDetails : null;
    }

}

?>