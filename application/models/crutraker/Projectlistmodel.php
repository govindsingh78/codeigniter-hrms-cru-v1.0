<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Projectlistmodel extends CI_Model {
    // table for add project for cru traker
    var $table = 'tm_projects';
    //var $order = array('id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
        $this->db->from("$db2.tm_projects");
        $this->db->where(array("$db2.tm_projects.is_active" => '1'));
        $this->db->where("($db2.tm_projects.project_category='ie' OR $db2.tm_projects.project_category='ae' OR $db2.tm_projects.project_category='pmc' OR $db2.tm_projects.project_category='IE_O&M')", NULL, FALSE);
        $this->db->order_by("$db2.tm_projects.project_name", 'ASC');
        $this->db->group_by("$db2.tm_projects.project_name");
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $ResultRec = $query->result();
        return $ResultRec;
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    //code by durgesh Fetch Details of project..
    public function GetProjectList() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
        $this->db->from("$db2.tm_projects");
        $this->db->where(array("$db2.tm_projects.is_active" => '1'));
        $this->db->where("($db2.tm_projects.project_category='ie' OR $db2.tm_projects.project_category='ae')", NULL, FALSE);
        $this->db->order_by("$db2.tm_projects.project_name", 'ASC');
        $result = $this->db->get()->result();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    //code by durgesh Fetch Details of users..
    public function GetUserList() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_users.id,$db2.main_users.userfullname,$db2.main_users.employeeId");
        $this->db->from("$db2.main_users");
        $this->db->where(array("$db2.main_users.isactive" => '1'));
        $this->db->where("$db2.main_users.id > ", '190');
        $this->db->order_by("$db2.main_users.id", 'ASC');
        $result = $this->db->get()->result();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

}
?>