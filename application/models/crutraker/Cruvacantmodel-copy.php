<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cruvacantmodel extends CI_Model {
    // table for add project for cru traker
    var $table = 'bdcruvacant_project as a';	
	var $order = array('project_id' => 'DESC'); // default order 
	 
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }
	
	 private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bd_tenderdetail.TenderDetails,$db1.bdcegexp_proj_summery.project_id,$db1.bdcegexp_proj_summery.project_numberid,$db2.tm_projects.project_name");
        $this->db->from("$db1.bdcruvacant_project");
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id = $db1.bdcruvacant_project.project_id", 'left');
	    $this->db->join("$db1.bdcegexp_proj_summery", "$db1.bdcruvacant_project.project_id = $db1.bdcegexp_proj_summery.project_numberid", 'left');
        $this->db->join("$db1.bd_tenderdetail", "$db1.bd_tenderdetail.fld_id = $db1.bdcegexp_proj_summery.project_id", 'inner	');
        
		$this->db->where(array("$db1.bdcruvacant_project.status" => '1'));
        $this->db->order_by("$db1.bdcruvacant_project.project_id", 'ASC');
		$this->db->group_by("$db1.bdcruvacant_project.project_id");
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $ResultRec = $query->result();

        $recordArr = array();
        if ($ResultRec) {
            foreach ($ResultRec as $recR) {
                $projId = $recR->project_id;
            }
        }
        return $ResultRec;
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	
   //code by durgesh Fetch Details of project..
    public function GetProjectList() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
		$this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
		$this->db->from("$db2.tm_projects");
		$this->db->where(array("$db2.tm_projects.is_active" => '1'));
		$this->db->where("($db2.tm_projects.project_category='ie' OR $db2.tm_projects.project_category='ae')", NULL, FALSE);
		$this->db->order_by("$db2.tm_projects.project_name", 'ASC');
		$result = $this->db->get()->result();
		if($result)
		{
			return ($result)? $result:'';
		}
    }
	
    //code by durgesh Fetch Details of users..
    public function GetUserList() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
		$this->db->select("$db2.main_users.id,$db2.main_users.userfullname");
		$this->db->from("$db2.main_users");
		$this->db->where(array("$db2.main_users.isactive" => '1'));
		$this->db->order_by("$db2.main_users.id", 'ASC');
		$result = $this->db->get()->result();
		if($result)
		{
			return ($result)? $result:'';
		}
    }
	
	/// Get staff by project id code by durgesh
	public function GetStaffByProjID($project_id, $keyid) {
        $db2 = $this->db1->database;
        $db1 = $this->db2->database;
        $this->db->select("$db2.designation_master_requisition.fld_id,$db2.assign_finalteam.id,$db2.assign_finalteam.srno,$db2.assign_finalteam.designation_id,$db2.assign_finalteam.empname,$db2.assign_finalteam.project_id,$db1.main_employees_summary.userfullname,$db2.designation_master_requisition.designation_name,$db2.assign_finalteam.man_months,$db2.assign_finalteam.rate");
        $this->db->from("$db2.assign_finalteam");
        $this->db->join("$db1.main_employees_summary", "$db2.assign_finalteam.empname = $db1.main_employees_summary.user_id", 'left');
        $this->db->join("$db2.designation_master_requisition", "$db2.assign_finalteam.designation_id = $db2.designation_master_requisition.fld_id", 'left');
        $this->db->where("$db2.assign_finalteam.project_id", $project_id);
        $this->db->where("$db2.assign_finalteam.key_id", $keyid);
        $this->db->order_by("$db2.assign_finalteam.srno", 'asc');
        $recArr = $this->db->get()->result_array();
        $pluginArr = array();
        if ($recArr) {
            foreach ($recArr as $Rowarr) {
                $projID = $Rowarr['project_id'];
                $empId = $Rowarr['empname'];
                $Rowarr['BalanceMm'] = $this->Gettotalcumulativemm($projID, $empId);
                array_push($pluginArr, $Rowarr);
            }
        }
        return ($pluginArr) ? $pluginArr : null;
    }
	
	/// Get invoice data by project id and employee id code by durgesh
	public function Gettotalcumulativemm($projID, $empId) {
        //Get Lastest invc details for blnv mm...
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db1.invoicesave.*");
        $this->db->FROM("$db1.invoicesave");
        $this->db->WHERE(array("$db1.invoicesave.project_id" => $projID, "$db1.invoicesave.emp_id" => $empId));
        $this->db->ORDER_BY("$db1.invoicesave.id", "DESC");
        $recRowArr = $this->db->get()->row();
        $BalanceMm = null;
        if ($recRowArr) {
            $BalanceMm = ($recRowArr->mm - $recRowArr->totalcumulativemm);
        }
        return $BalanceMm;
    }
    
	// Code by durgesh For Get project Name or code......
	public function Get_project_name_or_code_record($projid)
	{
	    $db1 = $this->db1->database;
        $db2 = $this->db2->database;
		$this->db->select("$db1.bdcegexp_proj_summery.project_id,$db1.bd_tenderdetail.TenderDetails,$db2.tm_projects.project_name");
		$this->db->from("$db1.bdcegexp_proj_summery");
		$this->db->join("$db1.bd_tenderdetail","$db1.bdcegexp_proj_summery.project_id=$db1.bd_tenderdetail.fld_id","left");
		$this->db->join("$db2.tm_projects","$db1.bdcegexp_proj_summery.project_numberid=$db2.tm_projects.id","left");
		$this->db->where(array("$db1.bdcegexp_proj_summery.project_id"=>$projid));
		$this->db->order_by("$db1.bdcegexp_proj_summery.project_id", "ASC");
	    $result = $this->db->get()->row_array();
        if($result){
			return ($result) ? $result:'';
		}		
	}
	// Code by durgesh
	public function designation_name($desiid)
	{
	    $db1 = $this->db1->database;
        $db2 = $this->db2->database;		
	    $this->db->select("$db1.assign_finalteam.designation_id,$db1.designation_master_requisition.designation_name");
		$this->db->from("$db1.assign_finalteam");
		$this->db->join("$db1.designation_master_requisition","$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id","inner");	
		$this->db->where(array("$db1.assign_finalteam.designation_id"=>$desiid));
		$this->db->order_by("$db1.assign_finalteam.project_id", "ASC");
	    $result = $this->db->get()->row_array();
        if($result){
			return ($result) ? $result:'';
		}	
	}
	// Code by durgesh
     public function Get_project_name_or_code_designation_record($projid)
	{
	    $db1 = $this->db1->database;
        $db2 = $this->db2->database;
		$this->db->select("$db1.bdcegexp_proj_summery.project_id,$db1.bd_tenderdetail.TenderDetails,$db2.tm_projects.project_name");
		$this->db->from("$db1.bdcegexp_proj_summery");
		$this->db->join("$db1.assign_finalteam","$db1.assign_finalteam.project_id=$db1.bdcegexp_proj_summery.project_id","left");			
		$this->db->join("$db1.bd_tenderdetail","$db1.bdcegexp_proj_summery.project_id=$db1.bd_tenderdetail.fld_id","left");
		$this->db->join("$db2.tm_projects","$db1.bdcegexp_proj_summery.project_numberid=$db2.tm_projects.id","left");
		$this->db->where(array("$db1.bdcegexp_proj_summery.project_id"=>$projid));
		$this->db->order_by("$db1.bdcegexp_proj_summery.project_id", "ASC");
	    $result = $this->db->get()->row_array();
        if($result){
			return ($result) ? $result:'';
		}		
	}
	
	// Code by durgesh For Get vacant position......
	public function Get_replacement($replc_id){
	    $db1 = $this->db1->database;
        $db2 = $this->db2->database;
		$this->db->select("$db1.replacementteam.*");
		$this->db->from("$db1.replacementteam");
		$this->db->where(array("$db1.replacementteam.replacement_id"=>$replc_id));
		$this->db->order_by("$db1.replacementteam.replacement_id","ASC");
		$result= $this->db->get()->row_array();
		if($result)
		{
			return ($result) ? $result:'';
		}
		
	}
	// Code by durgesh For Get Project Coordinator List......
	public function GetProjectCoordinatorlist($projid)
	{
	  	$db1 = $this->db1->database;
        $db2 = $this->db2->database;
		$this->db->select("$db1.bdcruvacant_crupanel.*,$db1.designation_master_requisition.fld_id,$db1.designation_master_requisition.designation_name,$db1.bd_tenderdetail.TenderDetails,$db2.tm_projects.project_name");
		$this->db->from("$db1.bdcruvacant_crupanel");	
		$this->db->join("$db1.assign_finalteam","$db1.assign_finalteam.project_id=$db1.bdcruvacant_crupanel.project_id","inner");
		$this->db->join("$db1.designation_master_requisition","$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id","inner");
		$this->db->join("$db1.bdcegexp_proj_summery","$db1.bdcegexp_proj_summery.project_id=$db1.assign_finalteam.project_id","inner");
		$this->db->join("$db1.bd_tenderdetail","$db1.bdcegexp_proj_summery.project_id=$db1.bd_tenderdetail.fld_id","inner");
		$this->db->join("$db2.tm_projects","$db1.bdcegexp_proj_summery.project_numberid=$db2.tm_projects.id","inner");
		$this->db->where(array("$db1.bdcruvacant_crupanel.project_id"=>$projid));
		$this->db->order_by("$db1.bdcruvacant_crupanel.project_id", "ASC");
	    $result = $this->db->get()->row_array();
        if($result){
			return ($result) ? $result:'';
		}			
	}	
	
}
?>