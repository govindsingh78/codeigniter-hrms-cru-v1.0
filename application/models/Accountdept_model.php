<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accountdept_model extends CI_Model {

    var $table = 'accountinfo as a';

    public function __construct() {
        parent::__construct();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    public function checkexist($tableName, $whereArr) {
        $this->db->where($whereArr);
        $num = $this->db->count_all_results($tableName);
        return ($num) ? $num : null;
    }

    public function getallprojaccountinfo() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        // $loginUser = $this->session->userdata('uid');
        $this->db->select("$db1.accountinfo.project_id,$db2.tm_projects.project_name,$db2.tm_projects.id");
        $this->db->from("$db1.accountinfo");
        $this->db->join("$db2.tm_projects", "$db1.accountinfo.project_numberid=$db2.tm_projects.id", "LEFT");
        $this->db->where(array("$db1.accountinfo.status" => '1'));
        $this->db->order_by("$db2.tm_projects.project_name", "ASC");
        $this->db->group_by("$db2.tm_projects.id");
        $projListArr = $this->db->get()->result();
        return ($projListArr) ? $projListArr : null;
    }

    public function getall_reimbursablelist() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        // $loginUser = $this->session->userdata('uid');
        $this->db->select("$db1.acc_master.id,$db1.acc_master.type");
        $this->db->from("$db1.acc_master");
        $this->db->where(array("$db1.acc_master.status" => '1'));
        $this->db->order_by("$db1.acc_master.type", "ASC");
        $this->db->group_by("$db1.acc_master.id");
        $recListArr = $this->db->get()->result();
        return ($recListArr) ? $recListArr : null;
    }

    //Get All Designation Category Master..
    public function getall_designationcatlist() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.designationcat_master.k_id,$db1.designationcat_master.ktype_name");
        $this->db->from("$db1.designationcat_master");
        $this->db->where(array("$db1.designationcat_master.status" => '1'));
        $this->db->order_by("$db1.designationcat_master.ktype_name", "ASC");
        $this->db->group_by("$db1.designationcat_master.k_id");
        $recListArr = $this->db->get()->result();
        return ($recListArr) ? $recListArr : null;
    }

    //Get All reimbursable on Project..
    public function getallreimbursableonproject($tsProjID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.acc_master.type");
        $this->db->from("$db1.acc2_reimbursable_assign");
        $this->db->join("$db1.acc_master", "$db1.acc2_reimbursable_assign.reim_type_id=$db1.acc_master.id", "LEFT");

        $this->db->where(array("$db1.acc2_reimbursable_assign.ts_projid" => $tsProjID, "$db1.acc2_reimbursable_assign.status" => '1'));
        $recListArr = $this->db->get()->result();
        $arrRecd = array();
        if ($recListArr) {
            foreach ($recListArr as $recList) {
                $arrRecd[] = $recList->type;
            }
        }
        return ($arrRecd) ? implode(" ,<br> ", $arrRecd) : null;
    }

    //Designation Category String 
    public function getalldesignationcategonproject($tsProjID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.designationcat_master.ktype_name");
        $this->db->from("$db1.acc2_designcateg_assign");
        $this->db->join("$db1.designationcat_master", "$db1.acc2_designcateg_assign.desigcat_id=$db1.designationcat_master.k_id", "LEFT");
        $this->db->where(array("$db1.acc2_designcateg_assign.ts_projid" => $tsProjID, "$db1.acc2_designcateg_assign.status" => '1'));
        $recListArr = $this->db->get()->result();

        $arrRecd = array();
        if ($recListArr) {
            foreach ($recListArr as $recList) {
                $arrRecd[] = $recList->ktype_name;
            }
        }
        return ($arrRecd) ? implode(" ,<br> ", $arrRecd) : null;
    }

    public function getalldesigncategontsprojid($tsProjID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.acc2_designcateg_assign.desigcat_id,$db1.designationcat_master.ktype_name");
        $this->db->from("$db1.acc2_designcateg_assign");
        $this->db->join("$db1.designationcat_master", "$db1.acc2_designcateg_assign.desigcat_id=$db1.designationcat_master.k_id", "LEFT");
        $this->db->where(array("$db1.acc2_designcateg_assign.bd_projid" => $tsProjID, "$db1.acc2_designcateg_assign.status" => '1'));
        $recListArr = $this->db->get()->result();
        return ($recListArr) ? $recListArr : null;
    }

    //Get Designation Category Name By Id .. 
    public function getDesignCategNameById($desCategid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.designationcat_master.ktype_name");
        $this->db->from("$db1.designationcat_master");
        $this->db->where(array("$db1.designationcat_master.k_id" => $desCategid, "$db1.designationcat_master.status" => '1'));
        $recListArr = $this->db->get()->row();
        return ($recListArr->ktype_name) ? $recListArr->ktype_name : null;
    }

    //GetAll DEsignation List By Designation Category
    public function GetAllDesignListByDesigCategID($desigCategId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.designation_master_requisition.designation_name,$db1.designation_master_requisition.fld_id");
        $this->db->from("$db1.designation_master_requisition");
        $this->db->where(array("$db1.designation_master_requisition.cat_id" => $desigCategId, "$db1.designation_master_requisition.is_active" => '1'));
        $this->db->order_by("$db1.designation_master_requisition.designation_name", "ASC");
        $this->db->group_by("$db1.designation_master_requisition.designation_name");
        $recListArr = $this->db->get()->result();
        return ($recListArr) ? $recListArr : null;
    }

    //Company List On A particular Project
    public function GetAllCompanyonProj($bdProjId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.jv_cegexp.base_comp_id,$db1.jv_cegexp.lead_comp_id,$db1.jv_cegexp.joint_venture,$db1.jv_cegexp.asso_comp");
        $this->db->from("$db1.jv_cegexp");
        $this->db->where(array("$db1.jv_cegexp.project_id" => $bdProjId, "$db1.jv_cegexp.status" => '1'));
        $recListArr = $this->db->get()->row();
        $base_comp_id = explode(",", $recListArr->base_comp_id);
        $leadSarr = explode(",", $recListArr->lead_comp_id);
        $jvarr = explode(",", $recListArr->joint_venture);
        $asscomparr = explode(",", $recListArr->asso_comp);
        $CompnArray = array_unique(array_filter(array_merge($leadSarr, $jvarr, $asscomparr, $base_comp_id)));
        $compnArr = array();
        if ($CompnArray) {
            foreach ($CompnArray as $rEcd):
                $compnArr[$rEcd] = $this->getCompanyNameByIdcegExp($rEcd);
            endforeach;
        }
        return ($compnArr) ? $compnArr : null;
    }

    //Company Name By Id..
    public function getCompanyNameByIdcegExp($compID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db1.main_company.company_name");
        $this->db->FROM("$db1.main_company");
        $this->db->where(array("$db1.main_company.fld_id" => $compID, "$db1.main_company.status" => "1"));
        $recdArr = $this->db->get()->row();
        return ($recdArr) ? $recdArr->company_name : null;
    }

    public function GetAccountInfoData($bdProjId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db2.tm_projects.project_name,$db1.accountinfo.*");
        $this->db->FROM("$db1.accountinfo");
        $this->db->join("$db2.tm_projects", "$db1.accountinfo.project_numberid=$db2.tm_projects.id", "LEFT");

        $this->db->where(array("$db1.accountinfo.status" => "1", "$db1.accountinfo.project_id" => $bdProjId));
        $recdArr = $this->db->get()->row();
        return ($recdArr) ? $recdArr : null;
    }

    public function GetProjectInfobyID($bdProjId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->SELECT("$db1.accountinfo.*");
        $this->db->FROM("$db1.accountinfo");
        $this->db->where(array("$db1.accountinfo.status" => "1", "$db1.accountinfo.project_id" => $bdProjId));
        $recdArr = $this->db->get()->row();
        return ($recdArr) ? $recdArr : null;
    }

    public function GetTsProjectID($bdProjId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->SELECT("$db1.accountinfo.project_numberid");
        $this->db->FROM("$db1.accountinfo");
        $this->db->where(array("$db1.accountinfo.status" => "1", "$db1.accountinfo.project_id" => $bdProjId));
        $recdArr = $this->db->get()->row();
        return ($recdArr) ? $recdArr->project_numberid : null;
    }

    public function getDesignationTeamDetails($projId, $designatin_categid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        //$projId = '89209';
        //$designatin_categid = '1';

        $this->db->SELECT("$db1.assign_finalteam.rate,$db1.assign_finalteam.empname,$db1.assign_finalteam.proj_positioid_metro,$db1.assign_finalteam.id,$db1.designation_master_requisition.designation_name,$db1.main_company.company_name,$db1.assign_finalteam.maintenance_months,$db1.assign_finalteam.construction_months,$db1.assign_finalteam.employee_type,$db1.assign_finalteam.account_date,$db1.assign_finalteam.construction_months,$db1.assign_finalteam.maintenance_rate,$db1.assign_finalteam.construction_rate,$db1.assign_finalteam.man_months");
        $this->db->FROM("$db1.assign_finalteam");
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id", "LEFT");
        $this->db->join("$db1.main_company", "$db1.main_company.fld_id=$db1.assign_finalteam.company_id", "LEFT");

        $this->db->where(array("$db1.assign_finalteam.status" => "1", "$db1.assign_finalteam.project_id" => $projId, "$db1.assign_finalteam.designatin_categid" => $designatin_categid));
        $recdArr = $this->db->get()->result();
        return ($recdArr) ? $recdArr : null;
    }

    public function getall_hrmsEmplist() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db2.main_users.id,$db2.main_users.userfullname,$db2.main_users.employeeId");
        $this->db->from("$db2.main_users");
        $this->db->where(array("$db2.main_users.isactive" => '1'));
        $this->db->order_by("$db2.main_users.userfullname", "ASC");
        $this->db->group_by("$db2.main_users.id");
        $recListArr = $this->db->get()->result();
        return ($recListArr) ? $recListArr : null;
    }

    public function getall_otherEmplist() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.team_req_otheremp.fld_id,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email");
        $this->db->from("$db1.team_req_otheremp");
        $this->db->where(array("$db1.team_req_otheremp.status" => '1'));
        $this->db->order_by("$db1.team_req_otheremp.emp_name", "ASC");
        $this->db->group_by("$db1.team_req_otheremp.emp_email");
        $recListArr = $this->db->get()->result();
        return ($recListArr) ? $recListArr : null;
    }

    public function getAssignFinalSingleRow($rOwId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db1.accountinfo.project_numberid,$db1.assign_finalteam.teamstatus,$db1.assign_finalteam.project_id,$db1.assign_finalteam.designatin_categid,$db1.assign_finalteam.status,$db1.assign_finalteam.designation_id,$db1.assign_finalteam.empname,$db1.assign_finalteam.company_id");
        $this->db->FROM("$db1.assign_finalteam");
        $this->db->join("$db1.accountinfo", "$db1.assign_finalteam.project_id=$db1.accountinfo.project_id", "LEFT");
        $this->db->where(array("$db1.assign_finalteam.status" => "1", "$db1.assign_finalteam.id" => $rOwId));
        $recdArr = $this->db->get()->row();
        return ($recdArr) ? $recdArr : null;
    }

    public function GetAllDesignCategAndTeamByProjID($bdProjID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.acc2_designcateg_assign.desigcat_id,$db1.designationcat_master.ktype_name");
        $this->db->from("$db1.acc2_designcateg_assign");
        $this->db->join("$db1.designationcat_master", "$db1.acc2_designcateg_assign.desigcat_id=$db1.designationcat_master.k_id", "LEFT");
        $this->db->where(array("$db1.acc2_designcateg_assign.bd_projid" => $bdProjID, "$db1.acc2_designcateg_assign.status" => '1'));
        $recListArr = $this->db->get()->result();
        return ($recListArr) ? $recListArr : null;
    }

    public function GetKeyTeamList($projId, $keyID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.assign_finalteam.*,$db1.designation_master_requisition.designation_name,$db2.main_employees_summary.prefix_name,$db2.main_employees_summary.userfullname,$db1.team_req_otheremp.emp_name,$db2.tm_project_employees.is_intermittent");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db2.main_employees_summary", "$db1.assign_finalteam.empname=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "$db1.team_req_otheremp.fld_id=$db1.assign_finalteam.empname", "LEFT");
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id", "LEFT");
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id=$db1.assign_finalteam.project_id", "LEFT");
        $this->db->join("$db2.tm_project_employees", "$db1.accountinfo.project_numberid=$db2.tm_project_employees.project_id AND $db2.tm_project_employees.emp_id=$db1.assign_finalteam.empname AND $db2.tm_project_employees.is_active='1'", "LEFT");
        $this->db->where(array("$db1.assign_finalteam.status" => "1", "$db1.assign_finalteam.designatin_categid" => $keyID, "$db1.assign_finalteam.project_id" => $projId));
        $this->db->group_by("$db1.assign_finalteam.designation_id");
        $this->db->order_by("$db1.assign_finalteam.srno", "ASC");
        $recListArr = $this->db->get()->result();


        $returnResp = array();
        if ($recListArr) {
            foreach ($recListArr as $recDr) {
                $recDr->repl_reducation_tot_rate = "";
                if ($RdRate = $recDr->replacement_reducationrate > 0) {
                    $recDr->repl_reducation_tot_rate = $recDr->rate - (($recDr->rate * $recDr->replacement_reducationrate) / 100);
                }
                $returnResp[] = $recDr;
            }
        }
        return ($returnResp) ? $returnResp : null;
    }

    public function reimbursable_setproj($projId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.acc2_reimbursable_assign.reim_type_id,$db1.acc_master.type");
        $this->db->from("$db1.acc2_reimbursable_assign");
        $this->db->join("$db1.acc_master", "$db1.acc_master.id=$db1.acc2_reimbursable_assign.reim_type_id", "LEFT");
        $this->db->where(array("$db1.acc2_reimbursable_assign.status" => "1", "$db1.acc2_reimbursable_assign.bd_projid" => $projId));
        $recListArr = $this->db->get()->result();
        if ($recListArr) {
            foreach ($recListArr as $recD) {
                $this->db->where(["project_id" => $projId, "type_id" => $recD->reim_type_id, "status" => "1"]);
                $Num = $this->db->count_all_results('acc_msterdetail');
                //Insert Rows..
                if ($Num < 1) {
                    $inserRecArr = array("project_id" => $projId,
                        "type_id" => $recD->reim_type_id,
                        "description" => $recD->type,
                        "status" => "1",
                        "createdby" => $this->session->userdata('uid'));
                    $this->db->insert("acc_msterdetail", $inserRecArr);
                }
            }
        }
        //Layer 2...
        $this->db->where(["project_id" => $projId, "status" => "1"]);
        $RecArr = $this->db->get('acc_msterdetail')->result();
        return ($RecArr) ? $RecArr : null;
    }

    public function GetReimbursable_Byproj($projId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.acc2_reimbursable_assign.reim_type_id,$db1.acc_master.type");
        $this->db->from("$db1.acc2_reimbursable_assign");
        $this->db->join("$db1.acc_master", "$db1.acc_master.id=$db1.acc2_reimbursable_assign.reim_type_id", "LEFT");
       // $this->db->join("$db1.acc2_designcateg_assign", "($db1.acc2_reimbursable_assign.reim_type_id=$db1.acc2_designcateg_assign.desigcat_id AND $db1.acc2_designcateg_assign.bd_projid=$db1.acc2_reimbursable_assign.bd_projid)", "RIGHT");

        $this->db->where(array("$db1.acc2_reimbursable_assign.status" => "1", "$db1.acc2_reimbursable_assign.bd_projid" => $projId));
        $this->db->order_by("$db1.acc2_reimbursable_assign.reim_type_id", "ASC");
        $recListArr = $this->db->get()->result();

        return ($recListArr) ? $recListArr : null;
    }

    //GetReimbursableList
    public function GetReimbursableList($projId, $keyID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.acc_msterdetail.*");
        $this->db->from("$db1.acc_msterdetail");
        // $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id", "LEFT");
        $this->db->where(array("$db1.acc_msterdetail.status" => "1", "$db1.acc_msterdetail.project_id" => $projId, "$db1.acc_msterdetail.type_id" => $keyID));
        $recListArr = $this->db->get()->result();
        return ($recListArr) ? $recListArr : null;
    }

    //Invoice Project ..
    public function invoiceProjectList() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        
        $this->db->select("$db1.assign_finalteam.project_id,$db2.tm_projects.project_name,$db2.tm_projects.id");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db1.accountinfo", "$db1.assign_finalteam.project_id=$db1.accountinfo.project_id", "Inner");
        $this->db->join("$db2.tm_projects", "$db1.accountinfo.project_numberid=$db2.tm_projects.id", "LEFT");
        $this->db->where("$db1.assign_finalteam.status", '1');
        $this->db->group_by("$db1.assign_finalteam.project_id");
        $recResults = $this->db->get()->result();
        return ($recResults) ? $recResults : null;
        
        
        
        
        
//        $this->db->select("$db1.timeshet_fill.project_id,$db2.tm_projects.project_name,$db2.tm_projects.id");
//        $this->db->from("$db1.timeshet_fill");
//        $this->db->join("$db1.accountinfo", "$db1.timeshet_fill.project_id=$db1.accountinfo.project_id", "Inner");
//        $this->db->join("$db2.tm_projects", "$db1.accountinfo.project_numberid=$db2.tm_projects.id", "LEFT");
//        $this->db->where("$db1.timeshet_fill.is_active", '1');
//        $this->db->group_by("$db1.timeshet_fill.project_id");
//        $recResults = $this->db->get()->result();
//        return ($recResults) ? $recResults : null;
    }

    //Invoice Project ..
    public function GetAttendRec($bdProjId, $year, $month) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.timeshet_fill.*");
        $this->db->from("$db1.timeshet_fill");
        $this->db->where("$db1.timeshet_fill.is_active", '1');
        $this->db->where(array("$db1.timeshet_fill.project_id" => $bdProjId, "$db1.timeshet_fill.year" => $year, "$db1.timeshet_fill.month" => $month));
        $recResults = $this->db->get()->result_array();
        $returnArr = array();
        if ($recResults) {
            foreach ($recResults as $recd) {
                $returnArr[$recd['designation_id']] = $recd;
            }
        }
        return ($returnArr) ? $returnArr : null;
    }

    //Get Supporting Staff or Admin Staff..
    public function GetStaffRateMMAmnt_Total($bdProjID, $keyID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $maintenance_monthsTot = 0;
        $maintenance_rateTot = 0;
        $construction_monthsTot = 0;
        $construction_rateTot = 0;
        $MMTot = 0;
        $RateAmountTot = 0;

        $this->db->select("$db1.assign_finalteam.id,$db1.assign_finalteam.maintenance_months,$db1.assign_finalteam.maintenance_rate,$db1.assign_finalteam.construction_months,$db1.assign_finalteam.construction_rate,$db1.assign_finalteam.man_months,$db1.assign_finalteam.rate,$db1.assign_finalteam.key_id,$db1.assign_finalteam.designation_id,$db1.assign_finalteam.empname");
        $this->db->from("$db1.assign_finalteam");
        $this->db->where("$db1.assign_finalteam.status", '1');
        $this->db->where(array("$db1.assign_finalteam.project_id" => $bdProjID, "$db1.assign_finalteam.designatin_categid" => $keyID));
        $recResults = $this->db->get()->result_array();

        if ($recResults) {
            foreach ($recResults as $recd) {
                //Maintenance
                if ($recd['maintenance_months']) {
                    $maintenance_monthsTot += $recd['maintenance_months'];
                }
                if ($recd['maintenance_months'] and $recd['rate']) {
                    $maintenance_rateTot += ($recd['maintenance_months'] * $recd['rate']);
                }
                //Construction
                if ($recd['construction_months']) {
                    $construction_monthsTot += $recd['construction_months'];
                }
                if ($recd['construction_months'] and $recd['rate']) {
                    $construction_rateTot += ($recd['construction_months'] * $recd['rate']);
                }
                //Total MM Rate..
                if ($recd['man_months']) {
                    $MMTot += $recd['man_months'];
                }
                if ($recd['man_months'] and $recd['rate']) {
                    $RateAmountTot += ($recd['man_months'] * $recd['rate']);
                }
            }
        }
        return array("maintenance_monthsTot" => $maintenance_monthsTot, "maintenance_rateTot" => $maintenance_rateTot, "construction_monthsTot" => $construction_monthsTot, "construction_rateTot" => $construction_rateTot, "MMTot" => $MMTot, "RateAmountTot" => $RateAmountTot);
    }

    //Get Project Remuneration Total Amount & MM Details..
    public function GetRembursableTotAmntDetail($bdProjID, $typeID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $maintenance_monthsTot = 0;
        $maintenance_rateTot = 0;
        $construction_monthsTot = 0;
        $construction_rateTot = 0;
        $MMTot = 0;
        $RateAmountTot = 0;

        $this->db->select("$db1.acc_msterdetail.id,$db1.acc_msterdetail.type_id,$db1.acc_msterdetail.unit,$db1.acc_msterdetail.qty,$db1.acc_msterdetail.construction_months,$db1.acc_msterdetail.construction_rate,$db1.acc_msterdetail.maintenance_months,$db1.acc_msterdetail.maintenance_rate,$db1.acc_msterdetail.no_month,$db1.acc_msterdetail.per_month");
        $this->db->from("$db1.acc_msterdetail");
        $this->db->where("$db1.acc_msterdetail.status", '1');
        $this->db->where(array("$db1.acc_msterdetail.project_id" => $bdProjID, "$db1.acc_msterdetail.type_id" => $typeID));
        $recResults = $this->db->get()->result_array();


        if ($recResults) {
            foreach ($recResults as $recd) {
                $quanty = ($recd['qty'] > 0) ? $recd['qty'] : 1;
                $unit = ($recd['unit'] > 0) ? $recd['unit'] : 1;
                //Maintenance
                if ($recd['maintenance_months']) {
                    $maintenance_monthsTot += $recd['maintenance_months'];
                }
                if ($recd['maintenance_months']) {
                    $maintenance_rateTot += ($recd['maintenance_months'] * $recd['per_month'] * $quanty * $unit);
                }
                //Construction
                if ($recd['construction_months']) {
                    $construction_monthsTot += $recd['construction_months'];
                }
                if ($recd['construction_months']) {
                    $construction_rateTot += ($recd['construction_months'] * $recd['per_month'] * $quanty * $unit);
                }
                //Total MM Rate..
                if ($recd['no_month']) {
                    $MMTot += $recd['no_month'];
                }
                if ($recd['no_month'] and $recd['per_month']) {
                    if ($recd['qty'] > 0) {
                        $RateAmountTot += ($recd['qty'] * $recd['no_month'] * $recd['per_month'] * $unit);
                    } else {
                        $RateAmountTot += ($recd['no_month'] * $recd['per_month'] * $unit);
                    }
                }
            }
        }
        return array("maintenance_monthsTot" => $maintenance_monthsTot, "maintenance_rateTot" => $maintenance_rateTot, "construction_monthsTot" => $construction_monthsTot, "construction_rateTot" => $construction_rateTot, "MMTot" => $MMTot, "RateAmountTot" => $RateAmountTot);
    }

    //Get Reimbursable Cumulative up to Previous Bill..
    public function ReimbursableCumulativePreviousBill($bdprojId, $typerId) {
        $TsProjID = $this->GetTsProjectID($bdprojId);
        $masterdetail_idArr = $this->getIdsArr($bdprojId, $typerId);
        $resultArr = 0;
        if (($TsProjID) and ( $masterdetail_idArr)) {
            $this->db->select("$db1.invoicedetail.id,$db1.acc_othermonthdate.id as othermonthdateID");
            $this->db->from("$db1.invoicedetail");
            $this->db->join("$db1.acc_othermonthdate", "$db1.invoicedetail.id=$db1.acc_othermonthdate.invoice_id", "LEFT");
            $this->db->where("$db1.invoicedetail.project_numberid", $TsProjID);
            $this->db->order_by("$db1.invoicedetail.invoice_date", "DESC");
            $this->db->order_by("$db1.acc_othermonthdate.id", "DESC");
            //acc_othermonthdate
            $this->db->limit(1, 1);
            $recRow = $this->db->get()->row();

            if ($recRow->othermonthdateID and count($masterdetail_idArr) > 0) {
                $this->db->select_sum("$db1.acc_othermonthdetail.cumulative_pre_amount");
                $this->db->from("$db1.acc_othermonthdetail");
                $this->db->where("$db1.acc_othermonthdetail.month_id", $recRow->othermonthdateID);
                $this->db->where_in("$db1.acc_othermonthdetail.masterdetail_id", $masterdetail_idArr);
                $resultArr = $this->db->get()->row();
            }
        }
        return ($resultArr) ? $resultArr->cumulative_pre_amount : "0";
    }

    public function getIdsArr($bdprojId, $typerId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $recDDArr = array();
        if ($typerId) {
            $this->db->select("$db1.acc_msterdetail.id");
            $this->db->from("$db1.acc_msterdetail");
            $this->db->where("$db1.acc_msterdetail.project_id", $bdprojId);
            $this->db->where("$db1.acc_msterdetail.type_id", $typerId);
            $recordArr = $this->db->get()->result();
            if ($recordArr) {
                foreach ($recordArr as $RoWWs) {
                    $recDDArr[] = $RoWWs->id;
                }
            }
        }
        return $recDDArr;
    }

    //Get Reimbursable Cumulative up to Previous Bill..
    public function ReimbursableCumulativeCurrentBill($bdprojId, $typerId) {
        $TsProjID = $this->GetTsProjectID($bdprojId);
        $masterdetail_idArr = $this->getIdsArr($bdprojId, $typerId);
        $resultArr = 0;
        if (($TsProjID) and ( $masterdetail_idArr)) {
            $this->db->select("$db1.invoicedetail.id,$db1.acc_othermonthdate.id as othermonthdateID");
            $this->db->from("$db1.invoicedetail");
            $this->db->join("$db1.acc_othermonthdate", "$db1.invoicedetail.id=$db1.acc_othermonthdate.invoice_id", "LEFT");
            $this->db->where("$db1.invoicedetail.project_numberid", $TsProjID);
            $this->db->order_by("$db1.invoicedetail.invoice_date", "DESC");
            $this->db->limit(0, 1);
            $recRow = $this->db->get()->row();

            if ($recRow->othermonthdateID and count($masterdetail_idArr) > 0) {
                $this->db->select_sum("$db1.acc_othermonthdetail.cumulative_pre_amount");
                $this->db->from("$db1.acc_othermonthdetail");
                $this->db->where("$db1.acc_othermonthdetail.month_id", $recRow->othermonthdateID);
                $this->db->where_in("$db1.acc_othermonthdetail.masterdetail_id", $masterdetail_idArr);
                $resultArr = $this->db->get()->row();
            }
        }
        return ($resultArr) ? $resultArr->cumulative_pre_amount : "0";
    }

    //Single Row Holidays Arr...
    public function getSunHolidaysArr($month, $year, $projID) {
        $sundays = array();
        $StartDate = $year . "-" . $month . "-" . "01";
        $EndDate = $year . "-" . $month . "-" . "31";
        $startDate = new DateTime($StartDate);
        $endDate = new DateTime($EndDate);
        while ($startDate <= $endDate) {
            if ($startDate->format('w') == 0) {
                $sundays[] = $startDate->format('d');
            }
            $startDate->modify('+1 day');
        }
        return ($sundays) ? $sundays : null;
    }

    //Get Holidays Recd..
    public function getHolidaysArr($month, $year, $projID) {
        $HolidaysRecds = $this->db->select('holiday_date')->where(["is_active" => "1", "proj_id" => $projID, "year" => $year, "month" => $month])->get('siteoffice_holiday')->result();
        if ($HolidaysRecds) {
            foreach ($HolidaysRecds as $reCdS):
                $Holidays[] = date("d", strtotime($reCdS->holiday_date));
            endforeach;
        }
        return ($Holidays) ? $Holidays : null;
    }

    //Get Single Reimbursable Cumulative up to Previous Bill..
    public function SingleReimbursableCumulativeCurrentBill($bdprojId, $typerId, $masterdetail_id) {
        $TsProjID = $this->GetTsProjectID($bdprojId);
        $resultArr = 0;
        if ($TsProjID) {
            $this->db->select("$db1.invoicedetail.id,$db1.acc_othermonthdate.id as othermonthdateID");
            $this->db->from("$db1.invoicedetail");
            $this->db->join("$db1.acc_othermonthdate", "$db1.invoicedetail.id=$db1.acc_othermonthdate.invoice_id", "LEFT");
            $this->db->where("$db1.invoicedetail.project_numberid", $TsProjID);
            $this->db->order_by("$db1.invoicedetail.invoice_date", "DESC");
            // $this->db->order_by("$db1.acc_othermonthdate.id", "DESC");
            $this->db->limit(0, 1);
            $recRow = $this->db->get()->row();
            if ($recRow->othermonthdateID and $masterdetail_id) {
                $this->db->select("$db1.acc_othermonthdetail.cumulative_pre_amount,$db1.acc_othermonthdetail.cumulative_pre_mm,$db1.acc_othermonthdetail.other_mm");
                $this->db->from("$db1.acc_othermonthdetail");
                $this->db->where("$db1.acc_othermonthdetail.month_id", $recRow->othermonthdateID);
                $this->db->where("$db1.acc_othermonthdetail.masterdetail_id", $masterdetail_id);
                $resultArr = $this->db->get()->row();
            }
        }
        return ($resultArr) ? $resultArr : "0";
    }

    //Cumulative up to Previous Bill.. Func Code By 
    public function GetKeyProf_SubProf_CurrentBillAmnt($Project_id, $invoice_no) {
        $cumulativeamount = 0;
        $totalcumulativeamount = 0;

        if ($Project_id and $invoice_no) {
            $this->db->select("$db1.invoicesave.cumulativeamount,$db1.invoicesave.totalcumulativeamount");
            $this->db->from("$db1.invoicesave");
            $this->db->where("$db1.invoicesave.project_id", $Project_id);
            $this->db->where("$db1.invoicesave.invoiceno_id", $invoice_no);
            $this->db->where("($db1.invoicesave.key_id='1' OR $db1.invoicesave.key_id='2')", NULL, FALSE);
            $recRow = $this->db->get()->result_array();

            if ($recRow) {
                foreach ($recRow as $recd) {
                    if ($recd['cumulativeamount']):
                        $cumulativeamount += $recd['cumulativeamount'];
                    endif;
                    if ($recd['totalcumulativeamount']):
                        $totalcumulativeamount += $recd['totalcumulativeamount'];
                    endif;
                }
            }
        }
        return array("cumulativeamount" => $cumulativeamount, "totalcumulativeamount" => $totalcumulativeamount);
    }

    public function GetTotReimbursableCurrentMM($bdProjID, $reim_type_id) {
        $TransportationRecArr = GetReimbursableList($bdProjID, $reim_type_id);
        $recdToT4 = 0;
        if ($TransportationRecArr) {
            foreach ($TransportationRecArr as $Tran_key => $Tran_row) {

                $quanty = ($Tran_row->qty > 0) ? $Tran_row->qty : 1;
                $unit = ($Tran_row->unit > 0) ? $Tran_row->unit : 1;

                $cumulative_pre_amount = 0;
                $userRateForCal = 0;
                $snglCurrentMMAmount = 0;

                $cumulative_pre_amount = SingleReimbursableCumulativeCurrentBill($Tran_row->project_id, $Tran_row->type_id, $Tran_row->id);

                if ($Tran_row->maintenance_rate > 0) {
                    $userRateForCal = $Tran_row->maintenance_rate;
                }
                if ($Tran_row->construction_rate > 0) {
                    $userRateForCal = $Tran_row->construction_rate;
                }
                if (($Tran_row->construction_rate < 1) and ( $Tran_row->maintenance_rate < 1)) {
                    $userRateForCal = $Tran_row->per_month;
                }
                $snglCurrentMMAmount = ($cumulative_pre_amount->other_mm * $userRateForCal * $quanty * $unit);

                if (($bdProjID == "89224") and ( ($reim_type_id == "3") or ( $reim_type_id == "4") or ( $reim_type_id == "5"))) {
                    $snglCurrentMMAmount = (($snglCurrentMMAmount * 75) / 100);
//                    if ($Tran_row->type_id == "1" or $Tran_row->type_id == "2" or $Tran_row->type_id == "7" or $Tran_row->type_id == "8" or $Tran_row->type_id == "9" or $Tran_row->type_id == "10") {
//                        $snglCurrentMMAmount = (($snglCurrentMMAmount * 100) / 100);
//                    }
                }
                $recdToT4 += $snglCurrentMMAmount;
            }
        }
        return ($recdToT4) ? $recdToT4 : "0";
    }

//     public function GetKey_SubTeamList($projId) {
//        $db1 = $this->db1->database;
//        $db2 = $this->db2->database;
//        $this->db->select("$db1.assign_finalteam.*,$db1.designation_master_requisition.designation_name,$db2.main_employees_summary.prefix_name,$db2.main_employees_summary.userfullname,$db1.team_req_otheremp.emp_name,$db2.tm_project_employees.is_intermittent");
//        $this->db->from("$db1.assign_finalteam");
//        $this->db->join("$db2.main_employees_summary", "$db1.assign_finalteam.empname=$db2.main_employees_summary.user_id", "LEFT");
//        $this->db->join("$db1.team_req_otheremp", "$db1.team_req_otheremp.fld_id=$db1.assign_finalteam.empname", "LEFT");
//        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id", "LEFT");
//        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id=$db1.assign_finalteam.project_id", "LEFT");
//        $this->db->join("$db2.tm_project_employees", "$db1.accountinfo.project_numberid=$db2.tm_project_employees.project_id AND $db2.tm_project_employees.emp_id=$db1.assign_finalteam.empname AND $db2.tm_project_employees.is_active='1'", "LEFT");
//        $this->db->where(array("$db1.assign_finalteam.status" => "1", "$db1.assign_finalteam.project_id" => $projId));
//        $this->db->where("($db1.assign_finalteam.designatin_categid='1' OR $db1.assign_finalteam.designatin_categid='2')", NULL, FALSE);
//        
//        //"$db1.assign_finalteam.designatin_categid" => $keyID,
//        
//        $this->db->order_by("$db1.assign_finalteam.srno", "ASC");
//        $recListArr = $this->db->get()->result();
//        return ($recListArr) ? $recListArr : null;
//    }
//    
//    
    //Function By Asheesh..
    public function getCurrentMonthAmount($bdProjectId, $invoice_id) {
        $this->load->model("Front_model");
        $keyProfRecDataArr = GetKeyTeamList($bdProjectId, "1");
        $AccountInfoDataArr = $this->GetAccountInfoData($bdProjectId);
        if ($keyProfRecDataArr and $AccountInfoDataArr and $invoice_id) {
            $invoicedata = $this->Front_model->getaccountdetailforexcel($invoice_id);
            if ($invoicedata->invoice_date) {
                $Invcprev_month = date("d-m-Y", strtotime("$invoicedata->invoice_date -1 month"));
                $InvcDateAtten = date('M Y', strtotime($Invcprev_month));
                $InvcYearAtten = date('Y', strtotime($Invcprev_month));
                $InvcMonthAtten = date('m', strtotime($Invcprev_month));
            }
            //No of Days Global Variable Define..
            if ($invoicedata->no_days == "") {
                $GLB_no_days = "30";
            }
            if ($invoicedata->no_days) {
                $GLB_no_days = $invoicedata->no_days;
            }
//Decimal Place Global..
            if ($invoicedata->decimal_place == "") {
                $GLB_decimal_place = "2";
            }
            if ($invoicedata->decimal_place) {
                $GLB_decimal_place = $invoicedata->decimal_place;
            }

            $keyProfRecDataArr = GetKeyTeamList($invoicedata->project_id, "1");
            $subProfRecDataArr = GetKeyTeamList($invoicedata->project_id, "2");
            $AdminSupportingStaffArr = GetKeyTeamList($invoicedata->project_id, "3");

            if ($keyProfRecDataArr) {
                foreach ($keyProfRecDataArr as $rOws) {
                    if ($rOws->maintenance_rate > 0) {
                        $userRateForCal = $rOws->maintenance_rate;
                    }
                    if ($rOws->construction_rate > 0) {
                        $userRateForCal = $rOws->construction_rate;
                    }
                    if (($rOws->construction_rate < 1) and ( $rOws->maintenance_rate < 1)) {
                        $userRateForCal = $rOws->rate;
                    }
                    if ($rOws->key_id == "1") {
                        if ($rOws->is_intermittent == "1") {
                            $TotPresentDays_up = getintermittentattd($invoicedata->project_id, $rOws->empname, $rOws->designation_id, $InvcMonthAtten, $InvcYearAtten);
                            $intermMM = ($TotPresentDays_up->present / $GLB_no_days);
                            $intermMM2 = number_format($intermMM, $GLB_decimal_place);
                        }
                        if ($rOws->is_intermittent != "1") {
                            $TotPresentDays_up = gettimesheetdetail($invoicedata->project_id, $rOws->designation_id, $rOws->empname, $InvcMonthAtten, $InvcYearAtten);
                            $intermMM = ($TotPresentDays_up->leave + $TotPresentDays_up->absent);
                            $intermMM1 = 1 - ($intermMM / $GLB_no_days);
                            $intermMM2 = number_format($intermMM1, $GLB_decimal_place);
                        }

                        $recdToT1 += ($userRateForCal * $intermMM2);
                    }
                }
            }

//Sub Prof...
            if ($subProfRecDataArr) {
                foreach ($subProfRecDataArr as $recD) {
                    if ($recD->is_intermittent == "1") {
                        $TotPresentDays_up = getintermittentattd($invoicedata->project_id, $recD->empname, $recD->designation_id, $InvcMonthAtten, $InvcYearAtten);
                        $intermMM = ($TotPresentDays_up->present / $GLB_no_days);
                        $intermMM2 = number_format($intermMM, $GLB_decimal_place);
                    }
                    if ($rOws->is_intermittent != "1") {
                        $TotPresentDays_up = gettimesheetdetail($invoicedata->project_id, $recD->designation_id, $recD->empname, $InvcMonthAtten, $InvcYearAtten);
                        $intermMM = ($TotPresentDays_up->leave + $TotPresentDays_up->absent);
                        $intermMM1 = 1 - ($intermMM / $GLB_no_days);
                        $intermMM2 = number_format($intermMM1, $GLB_decimal_place);
                    }
                    if ($recD->maintenance_rate > 0) {
                        $userRateForCal = $recD->maintenance_rate;
                    }
                    if ($recD->construction_rate > 0) {
                        $userRateForCal = $recD->construction_rate;
                    }
                    if (($recD->construction_rate < 1) and ( $recD->maintenance_rate < 1)) {
                        $userRateForCal = $recD->rate;
                    }
                    $recdToT2 += ($userRateForCal * $intermMM2);
                }
            }

//Supporting Staff or Admin Staff...
            if ($AdminSupportingStaffArr) {
                foreach ($AdminSupportingStaffArr as $AdmkEy => $AdmRow) {
                    if ($AdmRow->is_intermittent == "1") {
                        $TotPresentDays_up = getintermittentattd($invoicedata->project_id, $AdmRow->empname, $AdmRow->designation_id, $InvcMonthAtten, $InvcYearAtten);
                        $intermMM = ($TotPresentDays_up->present / $GLB_no_days);
                        $intermMM2 = number_format($intermMM, $GLB_decimal_place);
                    }
                    if ($AdmRow->is_intermittent != "1") {
                        $TotPresentDays_up = gettimesheetdetail($invoicedata->project_id, $AdmRow->designation_id, $AdmRow->empname, $InvcMonthAtten, $InvcYearAtten);
                        $intermMM = ($TotPresentDays_up->leave + $TotPresentDays_up->absent);
                        $intermMM1 = 1 - ($intermMM / $GLB_no_days);
                        $intermMM2 = number_format($intermMM1, $GLB_decimal_place);
                    }
                    if ($AdmRow->maintenance_rate > 0) {
                        $userRateForCal = $AdmRow->maintenance_rate;
                    }
                    if ($AdmRow->construction_rate > 0) {
                        $userRateForCal = $AdmRow->construction_rate;
                    }
                    if (($AdmRow->construction_rate < 1) and ( $AdmRow->maintenance_rate < 1)) {
                        $userRateForCal = $AdmRow->rate;
                    }
                    $recdToT3 += ($userRateForCal * $intermMM2);
                }
            }
        }

        $returNarr = array("keyProfTota" => round($recdToT1), "subProfTota" => round($recdToT2), "adminstaffProfTota" => round($recdToT3));
        return ($returNarr) ? $returNarr : null;
    }

    //Get getCurrentMonthAmount Reimbursable..
    public function getCurrentMonthAmountReimbursable($bdProjectId, $invoice_id) {
        $invoicedata->project_id = $bdProjectId;
        //Transportation..
        $TransportationRecArr = GetReimbursableList($invoicedata->project_id, "1");
        //Duty Travel to Site (Fixed Costs): Professional and Sub - Professional Staff..
        $DutyTravelRecArr = GetReimbursableList($invoicedata->project_id, "2");
        //Office Rent..
        $OfficeRentRecArr = GetReimbursableList($invoicedata->project_id, "3");
        //Start Section(6) Office Supplies, Utilities and Communication (Fixed Costs)..
        $OfficeSupplieRecArr = GetReimbursableList($invoicedata->project_id, "4");
        // Start (7) Office Furniture and Equipment (Rental) (Fixed Monthly Cost)..
        $OfcFurnitureRecArr = GetReimbursableList($invoicedata->project_id, "5");
        //Office Equipment (Rental / Hire)
        $OfcEquipmentRecArr = GetReimbursableList($invoicedata->project_id, "6");
        //Reports and Documents..
        $RepDocsRecArr = GetReimbursableList($invoicedata->project_id, "7");
        //Survey Equipment with Survey Party and Vehicle etc
        $SurveyEqpRecArr = GetReimbursableList($invoicedata->project_id, "8");
        // Road Survey Equipment
        $RoadSurveyEquipRecArr = GetReimbursableList($invoicedata->project_id, "9");
        //Contingencies
        $ContingenciesRecArr = GetReimbursableList($invoicedata->project_id, "10");

        //(1) Transportation..
        $userRateForCal = 0;
        $Transp_Tot = 0;
        foreach ($TransportationRecArr as $Tran_key => $Tran_row) {
            $cumulative_pre_amount = SingleReimbursableCumulativeCurrentBill($Tran_row->project_id, $Tran_row->type_id, $Tran_row->id);
            if ($Tran_row->maintenance_rate > 0) {
                $userRateForCal = $Tran_row->maintenance_rate;
            }
            if ($Tran_row->construction_rate > 0) {
                $userRateForCal = $Tran_row->construction_rate;
            }
            if (($Tran_row->construction_rate < 1) and ( $Tran_row->maintenance_rate < 1)) {
                $userRateForCal = $Tran_row->rate;
            }
            $Transp_Tot += ($cumulative_pre_amount->other_mm * $userRateForCal);
        }
        //(2) DutyTravelRec
        $userRateForCal2 = 0;
        $DutyTravelRec_Tot = 0;
        foreach ($DutyTravelRecArr as $row2) {
            $cumulative_pre_amount2 = SingleReimbursableCumulativeCurrentBill($row2->project_id, $row2->type_id, $row2->id);
            if ($row2->maintenance_rate > 0) {
                $userRateForCal2 = $row2->maintenance_rate;
            }
            if ($row2->construction_rate > 0) {
                $userRateForCal2 = $row2->construction_rate;
            }
            if (($row2->construction_rate < 1) and ( $row2->maintenance_rate < 1)) {
                $userRateForCal2 = $row2->rate;
            }
            $DutyTravelRec_Tot += ($cumulative_pre_amount2->other_mm * $userRateForCal2);
        }
        //(3) OfficeRentRec
        $userRateForCal3 = 0;
        $OfficeRentRec_Tot = 0;
        foreach ($OfficeRentRecArr as $row3) {
            $cumulative_pre_amount3 = SingleReimbursableCumulativeCurrentBill($row3->project_id, $row3->type_id, $row3->id);
            if ($row3->maintenance_rate > 0) {
                $userRateForCal3 = $row3->maintenance_rate;
            }
            if ($row3->construction_rate > 0) {
                $userRateForCal3 = $row3->construction_rate;
            }
            if (($row3->construction_rate < 1) and ( $row3->maintenance_rate < 1)) {
                $userRateForCal3 = $row2->rate;
            }
            $OfficeRentRec_Tot += ($cumulative_pre_amount3->other_mm * $userRateForCal3);
        }
        //(4) OfficeSupplieRecArr..
        $userRateForCal4 = 0;
        $OfficeSupplie_Tot = 0;
        foreach ($OfficeSupplieRecArr as $row4) {
            $cumulative_pre_amount4 = SingleReimbursableCumulativeCurrentBill($row4->project_id, $row4->type_id, $row4->id);
            if ($row4->maintenance_rate > 0) {
                $userRateForCal4 = $row4->maintenance_rate;
            }
            if ($row4->construction_rate > 0) {
                $userRateForCal4 = $row4->construction_rate;
            }
            if (($row4->construction_rate < 1) and ( $row4->maintenance_rate < 1)) {
                $userRateForCal4 = $row4->rate;
            }
            $OfficeSupplie_Tot += ($cumulative_pre_amount4->other_mm * $userRateForCal4);
        }
        //(5) OfcFurniture
        $userRateForCal5 = 0;
        $OfcFurniture_Tot = 0;
        foreach ($OfcFurnitureRecArr as $row5) {
            $cumulative_pre_amount5 = SingleReimbursableCumulativeCurrentBill($row5->project_id, $row5->type_id, $row5->id);
            if ($row5->maintenance_rate > 0) {
                $userRateForCal5 = $row5->maintenance_rate;
            }
            if ($row5->construction_rate > 0) {
                $userRateForCal5 = $row5->construction_rate;
            }
            if (($row5->construction_rate < 1) and ( $row5->maintenance_rate < 1)) {
                $userRateForCal5 = $row5->rate;
            }
            $OfcFurniture_Tot += ($cumulative_pre_amount5->other_mm * $userRateForCal5);
        }
        //(6) Ofc Equipment Record Array..
        $userRateForCal6 = 0;
        $OfcEquipment_Tot = 0;
        foreach ($OfcEquipmentRecArr as $row6) {
            $cumulative_pre_amount6 = SingleReimbursableCumulativeCurrentBill($row6->project_id, $row6->type_id, $row6->id);
            if ($row6->maintenance_rate > 0) {
                $userRateForCal6 = $row6->maintenance_rate;
            }
            if ($row6->construction_rate > 0) {
                $userRateForCal6 = $row6->construction_rate;
            }
            if (($row6->construction_rate < 1) and ( $row6->maintenance_rate < 1)) {
                $userRateForCal6 = $row6->rate;
            }
            $OfcEquipment_Tot += ($cumulative_pre_amount6->other_mm * $userRateForCal6);
        }
        //(7) RepDocsRecArr..
        $userRateForCal7 = 0;
        $RepDocsRec_Tot = 0;
        foreach ($RepDocsRecArr as $row7) {
            $cumulative_pre_amount7 = SingleReimbursableCumulativeCurrentBill($row7->project_id, $row7->type_id, $row7->id);
            if ($row7->maintenance_rate > 0) {
                $userRateForCal7 = $row7->maintenance_rate;
            }
            if ($row7->construction_rate > 0) {
                $userRateForCal7 = $row7->construction_rate;
            }
            if (($row7->construction_rate < 1) and ( $row7->maintenance_rate < 1)) {
                $userRateForCal7 = $row7->rate;
            }
            $RepDocsRec_Tot += ($cumulative_pre_amount7->other_mm * $userRateForCal7);
        }
        //(8) SurveyEqpRecArr..
        $userRateForCal8 = 0;
        $SurveyEqpRec_Tot = 0;
        foreach ($SurveyEqpRecArr as $row8) {
            $cumulative_pre_amount8 = SingleReimbursableCumulativeCurrentBill($row8->project_id, $row8->type_id, $row8->id);
            if ($row8->maintenance_rate > 0) {
                $userRateForCal8 = $row8->maintenance_rate;
            }
            if ($row8->construction_rate > 0) {
                $userRateForCal8 = $row8->construction_rate;
            }
            if (($row8->construction_rate < 1) and ( $row8->maintenance_rate < 1)) {
                $userRateForCal8 = $row8->rate;
            }
            $SurveyEqpRec_Tot += ($cumulative_pre_amount8->other_mm * $userRateForCal8);
        }
        //(9) RoadSurveyEquip
        $userRateForCal9 = 0;
        $RoadSurveyEquip_Tot = 0;
        foreach ($RoadSurveyEquipRecArr as $row9) {
            $cumulative_pre_amount9 = SingleReimbursableCumulativeCurrentBill($row9->project_id, $row9->type_id, $row9->id);
            if ($row9->maintenance_rate > 0) {
                $userRateForCal9 = $row9->maintenance_rate;
            }
            if ($row9->construction_rate > 0) {
                $userRateForCal9 = $row9->construction_rate;
            }
            if (($row9->construction_rate < 1) and ( $row9->maintenance_rate < 1)) {
                $userRateForCal9 = $row9->rate;
            }
            $RoadSurveyEquip_Tot += ($cumulative_pre_amount9->other_mm * $userRateForCal9);
        }
        //(10) Contingencies Record..
        $userRateForCal10 = 0;
        $Contingencies_Tot = 0;
        foreach ($ContingenciesRecArr as $row10) {
            $cumulative_pre_amount10 = SingleReimbursableCumulativeCurrentBill($row10->project_id, $row10->type_id, $row10->id);
            if ($row10->maintenance_rate > 0) {
                $userRateForCal10 = $row10->maintenance_rate;
            }
            if ($row10->construction_rate > 0) {
                $userRateForCal10 = $row10->construction_rate;
            }
            if (($row10->construction_rate < 1) and ( $row10->maintenance_rate < 1)) {
                $userRateForCal10 = $row10->rate;
            }
            $Contingencies_Tot += ($cumulative_pre_amount10->other_mm * $userRateForCal10);
        }
        return array("transportation" => $Transp_Tot, "DutyTravel" => $DutyTravelRec_Tot, "OfficeRent" => $OfficeRentRec_Tot, "OfficeSupplie" => $OfficeSupplie_Tot, "OfcFurniture" => $OfcFurniture_Tot, "OfcEquipment" => $OfcEquipment_Tot, "ReportDocuments" => $RepDocsRec_Tot, "SurveyEqpRec" => $SurveyEqpRec_Tot, "RoadSurveyEquip" => $RoadSurveyEquip_Tot, "Contingencies" => $Contingencies_Tot);
    }

    //Get Escalation
    public function GetEscalationRecArr($project_id, $invoiceID) {
        $this->db->select("*");
        $this->db->from("escalation_tax");
        $this->db->where("project_id", $project_id);
        $this->db->where("invoice_id", $invoiceID);
        $recRow = $this->db->get()->result();
        return ($recRow) ? $recRow : null;
    }

    //Get EOT Single Empl.
    public function GetEOTRecArr($project_id, $designationID, $emplID) {
        $this->db->select("eot_mm,eot_rate");
        $this->db->from("saveeotteam");
        $this->db->where(["project_id" => $project_id, "designation_id" => $designationID, "emp_id" => $emplID]);
        $this->db->order_by("eot_id", "DESC");
        $this->db->group_by("eot_id");
        $this->db->limit("1");
        $recRow = $this->db->get()->row();
        return ($recRow) ? $recRow : null;
    }

    public function GetEOTRecReimburRow($project_id, $id) {
        $this->db->select("id");
        $this->db->from("acceot_date");
        $this->db->where(["project_id" => $project_id]);
        $this->db->order_by("id", "DESC");
        $this->db->limit("1");
        $recRow = $this->db->get()->row();
        if ($recRow->id) {
            $this->db->select("eot_mm,eot_rate");
            $this->db->from("eot_detail");
            $this->db->where(["project_id" => $project_id, "masterdetail_id" => $id, "eot_id" => $recRow->id]);
            $recResult = $this->db->get()->row();
        }
        return ($recResult) ? $recResult : null;
    }

    //Get Tax on Invoice.. Code Asheesh..
    public function GetTaxesRecArr($project_id, $invoiceID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.project_tax.eot_tax,$db1.project_tax.status,$db1.project_tax.amount,$db1.project_tax.contractamount,$db1.project_tax.currenttaxtotal,$db1.project_tax.totaltaxcum,$db1.tax_master.tax_name,$db1.tax_master.percentage,$db1.project_tax.construction_tax,$db1.project_tax.maintenance_tax");
        $this->db->from("$db1.project_tax");
        $this->db->join("$db1.tax_master", "$db1.project_tax.tax_id=$db1.tax_master.id", "LEFT");
        $this->db->where(["$db1.project_tax.project_id" => $project_id, "$db1.project_tax.invoice_id" => $invoiceID]);
        $this->db->order_by("$db1.project_tax.id", "ASC");
        $recResult = $this->db->get()->result();
        return ($recResult) ? $recResult : null;
    }

    //Get Total Reimbursable Value EOT-Total And REVISED CONTRACT VALUE As Per...
    public function GetToReimbursableEOT_RvContVal($project_id, $typeID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $TransportationRecArr = GetReimbursableList($project_id, $typeID);

        $TOTSaving_trans = 0;
        $TOTRv_trans = 0;

        if ($TransportationRecArr) {
            foreach ($TransportationRecArr as $Tran_key => $row) {
                $EotRate_Tran = 0;
                $TransRVamount = 0;

                $eotRecdReimb = GetEOTRecReimburRow($project_id, $row->id);
                if (($eotRecdReimb) and ( $eotRecdReimb->eot_mm)) {
                    $EotRate_Tran = ($eotRecdReimb->eot_rate) ? $eotRecdReimb->eot_rate : ($row->per_month * $eotRecdReimb->eot_mm);
                }
                if ($row->qty > 0) {
                    $TransRVamount = ($row->qty * $row->per_month * $row->no_month) + $EotRate_Tran;
                } else {
                    $TransRVamount = ($row->per_month * $row->no_month) + $EotRate_Tran;
                }
                //######################################################################
                $TOTSaving_trans += $EotRate_Tran;
                $TOTRv_trans += $TransRVamount;
            }
        }
        return array("TotEOT" => $TOTSaving_trans, "TotRV" => $TOTRv_trans);
    }

    //Cumulative up to Previous Bill.. Func Code By Asheesh
    public function GetKeyProf_SubProf_CumulativuptoPrevAmnt($Project_id, $invoice_no) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $cumulativeamount = 0;
        $totalcumulativeamount = 0;

        if ($Project_id and $invoice_no) {
            //Key Prof..
            $recDdata = $this->GetProjDesignEmpIDs($Project_id, "1");
            if ($recDdata) {
                foreach ($recDdata as $rEdRow) {
                    //$cumulativeamount = 0;
                    // $totalcumulativeamount = 0;
                    $RecdCumulativuptoPrev = $this->Emp_CumulativuptoPrevAmnt($Project_id, $invoice_no, $rEdRow->designation_id, $rEdRow->empname);
                    if ($RecdCumulativuptoPrev->cumulativeamount):
                        $cumulativeamount += $RecdCumulativuptoPrev->cumulativeamount;
                    endif;
                    if ($RecdCumulativuptoPrev->totalcumulativeamount):
                        $totalcumulativeamount += $RecdCumulativuptoPrev->totalcumulativeamount;
                    endif;
                }
            }
            //Sub Prof..
            $recDdata2 = $this->GetProjDesignEmpIDs($Project_id, "2");
            if ($recDdata2) {
                foreach ($recDdata2 as $rEdRow) {
                    $RecdCumulativuptoPrev = $this->Emp_CumulativuptoPrevAmnt($Project_id, $invoice_no, $rEdRow->designation_id, $rEdRow->empname);
                    if ($RecdCumulativuptoPrev->cumulativeamount):
                        $cumulativeamount += $RecdCumulativuptoPrev->cumulativeamount;
                    endif;
                    if ($RecdCumulativuptoPrev->totalcumulativeamount):
                        $totalcumulativeamount += $RecdCumulativuptoPrev->totalcumulativeamount;
                    endif;
                }
            }
        }
        return array("cumulativeamount" => $cumulativeamount, "totalcumulativeamount" => $totalcumulativeamount);
    }

    //Supporting Staff or Admin Staff Cumulative up to Previous Bill.. Func Code By Asheesh
    public function GetAdminSupportingStaffCumulativAmnt($Project_id, $invoice_no) {
        $cumulativeamount = 0;
        $totalcumulativeamount = 0;

        if ($Project_id and $invoice_no) {

            $recDdata = $this->GetProjDesignEmpIDs($Project_id, "3");
            if ($recDdata) {
                foreach ($recDdata as $rEdRow) {
                    $RecdCumulativuptoPrev = $this->Emp_CumulativuptoPrevAmnt($Project_id, $invoice_no, $rEdRow->designation_id, $rEdRow->empname);
                    if ($RecdCumulativuptoPrev->cumulativeamount):
                        $cumulativeamount += $RecdCumulativuptoPrev->cumulativeamount;
                    endif;
                    if ($RecdCumulativuptoPrev->totalcumulativeamount):
                        $totalcumulativeamount += $RecdCumulativuptoPrev->totalcumulativeamount;
                    endif;
                }
            }

//            $this->db->select("$db1.invoicesave.cumulativeamount,$db1.invoicesave.totalcumulativeamount");
//            $this->db->from("$db1.invoicesave");
//            $this->db->where("$db1.invoicesave.project_id", $Project_id);
//            $this->db->where("$db1.invoicesave.invoiceno_id", $invoice_no);
//            $this->db->where("$db1.invoicesave.key_id", "3");
//            $recRow = $this->db->get()->result_array();
//
//            if ($recRow) {
//                foreach ($recRow as $recd) {
//                    if ($recd['cumulativeamount']):
//                        $cumulativeamount += $recd['cumulativeamount'];
//                    endif;
//                    if ($recd['totalcumulativeamount']):
//                        $totalcumulativeamount += $recd['totalcumulativeamount'];
//                    endif;
//                }
//            }
        }
        return array("cumulativeamount" => $cumulativeamount, "totalcumulativeamount" => $totalcumulativeamount);
    }

    public function GetProjDesignEmpIDs($projID, $key) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.assign_finalteam.designation_id,$db1.assign_finalteam.empname");
        $this->db->from("$db1.assign_finalteam");
        $this->db->where(array("$db1.assign_finalteam.project_id" => $projID, "$db1.assign_finalteam.designatin_categid" => $key, "$db1.assign_finalteam.status" => "1"));
        $recResult = $this->db->get()->result();
        return ($recResult) ? $recResult : null;
    }

    //Cumulative up to Previous Bill.. Single Employee Wise..
    public function Emp_CumulativuptoPrevAmnt($Project_id, $invoice_no, $empDesignationID, $emplID) {
        if ($Project_id and $invoice_no and $empDesignationID) {
            $this->db->select("$db1.invoicesave.cumulativeamount,$db1.invoicesave.totalcumulativeamount,$db1.invoicesave.cumulativemm,$db1.invoicesave.totalcumulativemm");
            $this->db->from("$db1.invoicesave");
            $this->db->where("$db1.invoicesave.project_id", $Project_id);
            $this->db->where("$db1.invoicesave.invoiceno_id", $invoice_no);
            $this->db->where("$db1.invoicesave.designation_id", $empDesignationID);
            $this->db->where("$db1.invoicesave.emp_id", $emplID);
            $recRow = $this->db->get()->row();
        }
        return ($recRow) ? $recRow : "0";
    }

    //Get Key Professional Staff & Sub Professional Staff Total RateAmount.. Code By Asheesh
    public function GetKeyProf_SubProfStaffTotRateAmnt($bdProjID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $maintenance_monthsTot = 0;
        $maintenance_rateTot = 0;
        $construction_monthsTot = 0;
        $construction_rateTot = 0;
        $MMTot = 0;
        $RateAmountTot = 0;

        $this->db->select("$db1.assign_finalteam.id,$db1.assign_finalteam.maintenance_months,$db1.assign_finalteam.maintenance_rate,$db1.assign_finalteam.construction_months,$db1.assign_finalteam.construction_rate,$db1.assign_finalteam.man_months,$db1.assign_finalteam.rate,$db1.assign_finalteam.key_id,$db1.assign_finalteam.designation_id,$db1.assign_finalteam.empname");
        $this->db->from("$db1.assign_finalteam");
        $this->db->where("$db1.assign_finalteam.status", '1');
        $this->db->where(array("$db1.assign_finalteam.project_id" => $bdProjID));
        $this->db->where("($db1.assign_finalteam.designatin_categid='1' OR $db1.assign_finalteam.designatin_categid='2')", NULL, FALSE);
        $recResults = $this->db->get()->result_array();

        if ($recResults) {
            foreach ($recResults as $recd) {

                //Maintenance
                if ($recd['maintenance_months']) {
                    $maintenance_monthsTot += $recd['maintenance_months'];
                }
                if ($recd['maintenance_months'] and $recd['rate']) {
                    $maintenance_rateTot += ($recd['maintenance_months'] * $recd['rate']);
                }
                //Construction
                if ($recd['construction_months']) {
                    $construction_monthsTot += $recd['construction_months'];
                }
                if ($recd['construction_months'] and $recd['rate']) {
                    $construction_rateTot += ($recd['construction_months'] * $recd['rate']);
                }
                //Total MM Rate..
                if ($recd['man_months']) {
                    $MMTot += $recd['man_months'];
                }
                if ($recd['man_months'] and $recd['rate']) {
                    $RateAmountTot += ($recd['rate'] * $recd['man_months']);
                }
            }
        }
        return array("maintenance_monthsTot" => $maintenance_monthsTot, "maintenance_rateTot" => $maintenance_rateTot, "construction_monthsTot" => $construction_monthsTot, "construction_rateTot" => $construction_rateTot, "MMTot" => $MMTot, "RateAmountTot" => $RateAmountTot);
    }

    //Get Project Remuneration Total Amount & MM Details..
    public function GetRembursableTotAmntDetail_new($bdProjID, $typeID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $maintenance_monthsTot = 0;
        $maintenance_rateTot = 0;
        $construction_monthsTot = 0;
        $construction_rateTot = 0;
        $MMTot = 0;
        $RateAmountTot = 0;

        $this->db->select("$db1.acc_msterdetail.id,$db1.acc_msterdetail.type_id,$db1.acc_msterdetail.unit,$db1.acc_msterdetail.qty,$db1.acc_msterdetail.construction_months,$db1.acc_msterdetail.construction_rate,$db1.acc_msterdetail.maintenance_months,$db1.acc_msterdetail.maintenance_rate,$db1.acc_msterdetail.no_month,$db1.acc_msterdetail.per_month");
        $this->db->from("$db1.acc_msterdetail");
        $this->db->where("$db1.acc_msterdetail.status", '1');
        $this->db->where(array("$db1.acc_msterdetail.project_id" => $bdProjID, "$db1.acc_msterdetail.type_id" => $typeID));
        $recResults = $this->db->get()->result_array();


        if ($recResults) {
            foreach ($recResults as $recd) {
                $quanty = ($recd['qty'] > 0) ? $recd['qty'] : 1;
                $unit = ($recd['unit'] > 0) ? $recd['unit'] : 1;
                //Maintenance
                if ($recd['maintenance_months']) {
                    $maintenance_monthsTot += $recd['maintenance_months'];
                }
                if ($recd['maintenance_months']) {
                    $maintenance_rateTot += ($recd['maintenance_months'] * $recd['maintenance_rate'] * $quanty * $unit);
                }
                //Construction
                if ($recd['construction_months']) {
                    $construction_monthsTot += $recd['construction_months'];
                }
                if ($recd['construction_months']) {
                    $construction_rateTot += ($recd['construction_months'] * $recd['construction_rate'] * $quanty * $unit);
                }
                //Total MM Rate..
                if ($recd['no_month']) {
                    $MMTot += $recd['no_month'];
                }
                if ($recd['no_month'] and $recd['per_month']) {
                    if ($recd['qty'] > 0) {
                        $RateAmountTot += ($recd['qty'] * $recd['no_month'] * $recd['per_month'] * $unit);
                    } else {
                        $RateAmountTot += ($recd['no_month'] * $recd['per_month'] * $unit);
                    }
                }
            }
        }
        return array("maintenance_monthsTot" => $maintenance_monthsTot, "maintenance_rateTot" => $maintenance_rateTot, "construction_monthsTot" => $construction_monthsTot, "construction_rateTot" => $construction_rateTot, "MMTot" => $MMTot, "RateAmountTot" => $RateAmountTot);
    }

    //Get Single Reimbursable Cumulative up to Previous Bill 26-12-2019 new Function By Asheesh..
    public function SingleReimbursableCumulativePrev_Bill($bdprojId, $invoiceId, $masterdetail_id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.acc_othermonthdate.id as othermonthdateID");
        $this->db->from("$db1.acc_othermonthdate");
        $this->db->where("$db1.acc_othermonthdate.project_id", $bdprojId);
        $this->db->where("$db1.acc_othermonthdate.invoice_id <= ", $invoiceId);
        $this->db->order_by("$db1.acc_othermonthdate.id", "DESC");
        $this->db->limit(1, 1);
        $recRow = $this->db->get()->row();
        if ($recRow->othermonthdateID and $masterdetail_id) {
            $this->db->select("$db1.acc_othermonthdetail.cumulative_pre_amount,$db1.acc_othermonthdetail.cumulative_pre_mm,$db1.acc_othermonthdetail.other_mm");
            $this->db->from("$db1.acc_othermonthdetail");
            $this->db->where("$db1.acc_othermonthdetail.month_id", $recRow->othermonthdateID);
            $this->db->where("$db1.acc_othermonthdetail.masterdetail_id", $masterdetail_id);
            $resultArr = $this->db->get()->row();
        }
        return ($resultArr) ? $resultArr : "0";
    }

    public function SingleReimbursableCumulativeCurrent_Bill($bdprojId, $invoiceId, $masterdetail_id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.acc_othermonthdate.id as othermonthdateID");
        $this->db->from("$db1.acc_othermonthdate");
        $this->db->where("$db1.acc_othermonthdate.project_id", $bdprojId);
        $this->db->where("$db1.acc_othermonthdate.invoice_id <= ", $invoiceId);
        $this->db->order_by("$db1.acc_othermonthdate.id", "DESC");
        $this->db->limit(0, 1);
        $recRow = $this->db->get()->row();
        if ($recRow->othermonthdateID and $masterdetail_id) {
            $this->db->select("$db1.acc_othermonthdetail.cumulative_pre_amount,$db1.acc_othermonthdetail.cumulative_pre_mm,$db1.acc_othermonthdetail.other_mm");
            $this->db->from("$db1.acc_othermonthdetail");
            $this->db->where("$db1.acc_othermonthdetail.month_id", $recRow->othermonthdateID);
            $this->db->where("$db1.acc_othermonthdetail.masterdetail_id", $masterdetail_id);
            $resultArr = $this->db->get()->row();
        }
        return ($resultArr) ? $resultArr : "0";
    }

    //Get Reimbursable Cumulative up to Previous Bill..
    public function ReimbursableCumulative_PrevBill($bdprojId, $invoiceId, $typerId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $masterdetail_idArr = $this->getIdsArr($bdprojId, $typerId);
        $resultArr = 0;
        if (($bdprojId) and ( $masterdetail_idArr)) {
            $this->db->select("$db1.acc_othermonthdate.id as othermonthdateID");
            $this->db->from("$db1.acc_othermonthdate");
            $this->db->where("$db1.acc_othermonthdate.project_id", $bdprojId);
            $this->db->where("$db1.acc_othermonthdate.invoice_id <= ", $invoiceId);
            $this->db->order_by("$db1.acc_othermonthdate.id", "DESC");
            $this->db->limit(1, 1);
            $recRow = $this->db->get()->row();
            if ($recRow) {
                $othermonthdateID = $recRow->othermonthdateID;
                $this->db->select_sum("$db1.acc_othermonthdetail.cumulative_pre_amount");
                $this->db->from("$db1.acc_othermonthdetail");
                $this->db->where("$db1.acc_othermonthdetail.month_id", $othermonthdateID);
                $this->db->where_in("$db1.acc_othermonthdetail.masterdetail_id", $masterdetail_idArr);
                $resultArr = $this->db->get()->row();
            }
        }
        return ($resultArr) ? $resultArr->cumulative_pre_amount : "0";
    }

    //Get Reimbursable Cumulative up to Current Bill..
    public function ReimbursableCumulative_CurrentBill($bdprojId, $invoiceId, $typerId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $resulrSumAmount = 0;
        $masterdetail_idArr = $this->getIdsArr($bdprojId, $typerId);

        if (($bdprojId) and ( $masterdetail_idArr)) {
            $this->db->select("$db1.acc_othermonthdate.id as othermonthdateID");
            $this->db->from("$db1.acc_othermonthdate");
            $this->db->where("$db1.acc_othermonthdate.project_id", $bdprojId);
            $this->db->where("$db1.acc_othermonthdate.invoice_id <= ", $invoiceId);
            $this->db->order_by("$db1.acc_othermonthdate.id", "DESC");
            $this->db->limit(0, 1);
            $recRow = $this->db->get()->row();

            if ($recRow) {
                $othermonthdateID = $recRow->othermonthdateID;
                $this->db->select("$db1.acc_othermonthdetail.*,$db1.acc_msterdetail.description,$db1.acc_msterdetail.per_month,$db1.acc_msterdetail.qty");
                $this->db->from("$db1.acc_othermonthdetail");
                $this->db->join("$db1.acc_msterdetail", "$db1.acc_othermonthdetail.masterdetail_id=$db1.acc_msterdetail.id", "LEFT");
                $this->db->where("$db1.acc_othermonthdetail.month_id", $othermonthdateID);
                $this->db->where_in("$db1.acc_othermonthdetail.masterdetail_id", $masterdetail_idArr);
                $resultArr = $this->db->get()->result();

                if ($resultArr) {
                    foreach ($resultArr as $roWs) {
                        $snglCurrentMMAmount = 0;
                        $quanty = ($roWs->qty > 0) ? $roWs->qty : "1";
                        $snglCurrentMMAmount = ($roWs->other_mm * $roWs->per_month * $quanty);
                        if ($bdprojId == "89224") {
                            $snglCurrentMMAmount = (($snglCurrentMMAmount * 75) / 100);
                        }
                        $resulrSumAmount += $snglCurrentMMAmount;
                    }
                }
            }
        }
        return ($resulrSumAmount) ? $resulrSumAmount : "0";
    }

    //Get Key Professional Staff Total RateAmount.. Code By Asheesh
    public function GetKeyProf_staffTotRateAmnt($bdProjID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $maintenance_monthsTot = 0;
        $maintenance_rateTot = 0;
        $construction_monthsTot = 0;
        $construction_rateTot = 0;
        $MMTot = 0;
        $RateAmountTot = 0;

        $this->db->select("$db1.assign_finalteam.id,$db1.assign_finalteam.maintenance_months,$db1.assign_finalteam.maintenance_rate,$db1.assign_finalteam.construction_months,$db1.assign_finalteam.construction_rate,$db1.assign_finalteam.man_months,$db1.assign_finalteam.rate,$db1.assign_finalteam.key_id,$db1.assign_finalteam.designation_id,$db1.assign_finalteam.empname");
        $this->db->from("$db1.assign_finalteam");
        $this->db->where("$db1.assign_finalteam.status", '1');
        $this->db->where(array("$db1.assign_finalteam.project_id" => $bdProjID));
        $this->db->where("($db1.assign_finalteam.designatin_categid='1')", NULL, FALSE);
        $recResults = $this->db->get()->result_array();

        if ($recResults) {
            foreach ($recResults as $recd) {

                //Maintenance
                if ($recd['maintenance_months']) {
                    $maintenance_monthsTot += $recd['maintenance_months'];
                }
                if ($recd['maintenance_months'] and $recd['rate']) {
                    $maintenance_rateTot += ($recd['maintenance_months'] * $recd['rate']);
                }
                //Construction
                if ($recd['construction_months']) {
                    $construction_monthsTot += $recd['construction_months'];
                }
                if ($recd['construction_months'] and $recd['rate']) {
                    $construction_rateTot += ($recd['construction_months'] * $recd['rate']);
                }
                //Total MM Rate..
                if ($recd['man_months']) {
                    $MMTot += $recd['man_months'];
                }
                if ($recd['man_months'] and $recd['rate']) {
                    $RateAmountTot += ($recd['rate'] * $recd['man_months']);
                }
            }
        }
        return array("maintenance_monthsTot" => $maintenance_monthsTot, "maintenance_rateTot" => $maintenance_rateTot, "construction_monthsTot" => $construction_monthsTot, "construction_rateTot" => $construction_rateTot, "MMTot" => $MMTot, "RateAmountTot" => $RateAmountTot);
    }

    //Get Key Professional Staff Total RateAmount.. Code By Asheesh
    public function GetSubProf_staffTotRateAmnt($bdProjID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $maintenance_monthsTot = 0;
        $maintenance_rateTot = 0;
        $construction_monthsTot = 0;
        $construction_rateTot = 0;
        $MMTot = 0;
        $RateAmountTot = 0;

        $this->db->select("$db1.assign_finalteam.id,$db1.assign_finalteam.maintenance_months,$db1.assign_finalteam.maintenance_rate,$db1.assign_finalteam.construction_months,$db1.assign_finalteam.construction_rate,$db1.assign_finalteam.man_months,$db1.assign_finalteam.rate,$db1.assign_finalteam.key_id,$db1.assign_finalteam.designation_id,$db1.assign_finalteam.empname");
        $this->db->from("$db1.assign_finalteam");
        $this->db->where("$db1.assign_finalteam.status", '1');
        $this->db->where(array("$db1.assign_finalteam.project_id" => $bdProjID));
        $this->db->where("($db1.assign_finalteam.designatin_categid='2')", NULL, FALSE);
        $recResults = $this->db->get()->result_array();

        if ($recResults) {
            foreach ($recResults as $recd) {

                //Maintenance
                if ($recd['maintenance_months']) {
                    $maintenance_monthsTot += $recd['maintenance_months'];
                }
                if ($recd['maintenance_months'] and $recd['rate']) {
                    $maintenance_rateTot += ($recd['maintenance_months'] * $recd['rate']);
                }
                //Construction
                if ($recd['construction_months']) {
                    $construction_monthsTot += $recd['construction_months'];
                }
                if ($recd['construction_months'] and $recd['rate']) {
                    $construction_rateTot += ($recd['construction_months'] * $recd['rate']);
                }
                //Total MM Rate..
                if ($recd['man_months']) {
                    $MMTot += $recd['man_months'];
                }
                if ($recd['man_months'] and $recd['rate']) {
                    $RateAmountTot += ($recd['rate'] * $recd['man_months']);
                }
            }
        }
        return array("maintenance_monthsTot" => $maintenance_monthsTot, "maintenance_rateTot" => $maintenance_rateTot, "construction_monthsTot" => $construction_monthsTot, "construction_rateTot" => $construction_rateTot, "MMTot" => $MMTot, "RateAmountTot" => $RateAmountTot);
    }

    //Cumulative up to Previous Bill.. Func Code By Asheesh
    public function GetKeyProf_CumulativuptoPrevAmnt($Project_id, $invoice_no) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $cumulativeamount = 0;
        $totalcumulativeamount = 0;

        if ($Project_id and $invoice_no) {
            //Key Prof..
            $recDdata = $this->GetProjDesignEmpIDs($Project_id, "1");
            if ($recDdata) {
                foreach ($recDdata as $rEdRow) {
                    //$cumulativeamount = 0;
                    // $totalcumulativeamount = 0;
                    $RecdCumulativuptoPrev = $this->Emp_CumulativuptoPrevAmnt($Project_id, $invoice_no, $rEdRow->designation_id, $rEdRow->empname);
                    if ($RecdCumulativuptoPrev->cumulativeamount):
                        $cumulativeamount += $RecdCumulativuptoPrev->cumulativeamount;
                    endif;
                    if ($RecdCumulativuptoPrev->totalcumulativeamount):
                        $totalcumulativeamount += $RecdCumulativuptoPrev->totalcumulativeamount;
                    endif;
                }
            }
        }
        return array("cumulativeamount" => $cumulativeamount, "totalcumulativeamount" => $totalcumulativeamount);
    }

    //Cumulative up to Previous Bill.. Func Code By Asheesh
    public function GetSubProf_CumulativuptoPrevAmnt($Project_id, $invoice_no) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $cumulativeamount = 0;
        $totalcumulativeamount = 0;

        if ($Project_id and $invoice_no) {
            //Sub Prof..
            $recDdata2 = $this->GetProjDesignEmpIDs($Project_id, "2");
            if ($recDdata2) {
                foreach ($recDdata2 as $rEdRow) {
                    $RecdCumulativuptoPrev = $this->Emp_CumulativuptoPrevAmnt($Project_id, $invoice_no, $rEdRow->designation_id, $rEdRow->empname);
                    if ($RecdCumulativuptoPrev->cumulativeamount):
                        $cumulativeamount += $RecdCumulativuptoPrev->cumulativeamount;
                    endif;
                    if ($RecdCumulativuptoPrev->totalcumulativeamount):
                        $totalcumulativeamount += $RecdCumulativuptoPrev->totalcumulativeamount;
                    endif;
                }
            }
        }
        return array("cumulativeamount" => $cumulativeamount, "totalcumulativeamount" => $totalcumulativeamount);
    }

}
