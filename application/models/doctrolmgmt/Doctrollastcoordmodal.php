<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Doctrollastcoordmodal extends CI_Model {

    var $table = 'project_coordinator as a';
    var $column_order = array(null, 'a.emp_id',); //set column field database for datatable orderable
    var $column_search = array('a.emp_id'); //set column field database for datatable searchable 

    // var $order = array("a.fld_id" => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();

        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $projcoordid = $this->session->userdata('uid');

        if ($this->input->post('doc_from') and $this->input->post('doc_to')) {
            $fromdate = date("Y-m-d", strtotime($this->input->post('doc_from')));
            $todate = date("Y-m-d", strtotime($this->input->post('doc_to')));
            $this->db->where("$db1.dbo_pwd_doctrol_metadata.entry_date >=", $fromdate);
            $this->db->where("$db1.dbo_pwd_doctrol_metadata.entry_date <=", $todate);
        }
        if ($this->input->post('doc_from') and $this->input->post('doc_to')) {
            $fromdate = date("Y-m-d", strtotime($this->input->post('doc_from')));
            $todate = date("Y-m-d", strtotime($this->input->post('doc_to')));
            $this->db->where("$db1.dbo_doctrol_upload_report.entry_date >=", $fromdate);
            $this->db->where("$db1.dbo_doctrol_upload_report.entry_date <=", $todate);
        }

        $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name,$db1.project_coordinator.bd_project_id");
        $this->db->from("$db1.project_coordinator");
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id = $db1.project_coordinator.hrms_projid", 'left');
        $this->db->join("$db1.dbo_pwd_doctrol_metadata", "$db1.dbo_pwd_doctrol_metadata.project_id = $db1.project_coordinator.hrms_projid", 'left');
        $this->db->join("$db1.dbo_doctrol_upload_report", "$db1.dbo_doctrol_upload_report.rep_project_id = $db1.project_coordinator.hrms_projid", 'left');
        $this->db->where("$db1.project_coordinator.emp_id",$projcoordid);
        $this->db->group_by("$db1.project_coordinator.hrms_projid");
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    //last date document date by project id
    public function get_lastdaterecordByprojid($projid) {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $this->db->select("$db1.dbo_pwd_doctrol_metadata.entry_date");
        $this->db->from("$db1.dbo_pwd_doctrol_metadata");
        $this->db->where_in("$db1.dbo_pwd_doctrol_metadata.project_id", $projid);
        $this->db->order_by("$db1.dbo_pwd_doctrol_metadata.entry_date", "DESC");
        $ProjArr = $this->db->get()->row_array();
        if ($ProjArr) {
            return $ProjArr;
        }
    }

    //code by durgesh(16-01-2020) for last date of report by project id
    public function get_lastdatereprecordByprojid($projid) {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $this->db->select("$db1.dbo_doctrol_upload_report.entry_date");
        $this->db->from("$db1.dbo_doctrol_upload_report");
        $this->db->where_in("$db1.dbo_doctrol_upload_report.rep_project_id", $projid);
        $this->db->order_by("$db1.dbo_doctrol_upload_report.entry_date", "DESC");
        $ProjArr = $this->db->get()->row_array();
        if ($ProjArr) {
            return $ProjArr;
        }
    }

    //current date letter count code by durgesh(16-01-2020)
    public function count_record_current_date($projid) {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $this->db->select("$db1.dbo_pwd_doctrol_metadata.project_id");
        $this->db->from("$db1.dbo_pwd_doctrol_metadata");
        $this->db->where_in("$db1.dbo_pwd_doctrol_metadata.project_id", $projid);
        $this->db->where(array("$db1.dbo_pwd_doctrol_metadata.entry_date" => date('Y-m-d'), "$db1.dbo_pwd_doctrol_metadata.status" => '1'));
        $ProjArr = $this->db->get()->num_rows();
        if ($ProjArr) {
            return ($ProjArr) ? $ProjArr : '';
        }
    }

    //current date report count code by durgesh(16-01-2020)
    public function count_record_report_current_date($projid) {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $this->db->select("$db1.dbo_doctrol_upload_report.rep_project_id");
        $this->db->from("$db1.dbo_doctrol_upload_report");
        $this->db->where_in("$db1.dbo_doctrol_upload_report.rep_project_id", $projid);
        $this->db->where(array("$db1.dbo_doctrol_upload_report.entry_date" => date('Y-m-d'), "$db1.dbo_doctrol_upload_report.status" => '1'));
        $ProjArr = $this->db->get()->num_rows();
        if ($ProjArr) {
            return ($ProjArr) ? $ProjArr : '';
        }
    }

}
