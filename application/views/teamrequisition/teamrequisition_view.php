<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
#table_length {
    position: absolute;
    margin-left: 100px !important;
}
.table tbody tr td, .table tbody th td {
    vertical-align: middle;
    white-space: normal !important;
}
</style>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                <div class="col-md-12">
                   

                         
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs"> Success ! </strong> <?= $this->session->flashdata('msg'); ?></div>
                        </div>
                    <?php } ?>

                    <?php if ($this->session->flashdata('error_msg')) { ?>
                        <div class="alert alert-danger alert-dismissable" style="">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs"> Error ! </strong> <?= $this->session->flashdata('error_msg'); ?></div>
                        </div>
                    <?php } ?>

                    <div class="card">
                        <div class="body">
                            <div class="tab-content">
                                <div id="home" class="tab-pane active">

                                    <div class="row">
                                        <div class="col-md-10 ml-0 pl-0">


                                            <div class="alert alert-success" ><?php echo $resulstArr[0]['generated_tenderid']; ?></div>
                                            
                                            
                                        </div>
                                        <div class="col-md-2">
                                            
                                            <div id="colvis" style="float:right">
                                                <form action="<?= base_url('teamrequisition/exportTeam'); ?>" method="post" enctype="">
                                                    <input type="hidden" name="project_id" value ="<?php echo $resulstArr[0]['project_id']; ?>" />
                                                    <button class="ColVis_Button ColVis_MasterButton export_team btn btn-info btn-sm mt-0" onclick="form.submit()">												
                                                        <span>Export</span>
                                                    </button>
                                                </form>
                                            </div>


                                        </div>
                                    </div>
                                    

                                    <div class="row">
                                        <div class=" mb-3">
                                            <?php echo $resulstArr[0]['TenderDetails']; ?>
                                        </div>
                                        <div class="">
                                            <form name="req_form" action="<?= base_url('teamrequisition/savereqprojectdata'); ?>" method="post" >



                                            <div class="table-responsive">
                                <table id="table" class="display table center-aligned-table">
                                    <thead>
                                    <tr>
                                    <th>Designation</th>
                                                            <th>Age Limit</th>
                                                            <th>Total Man Months</th>
                                                            <th>Weightage Marks (RFP)</th>
                                                            <th>Comment</th>
                                                            <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                                    foreach ($resulstArr as $rows) {
                                                        ?>
                                                       <tr>
                                                        <input type="hidden" id="team_id" name="team_id[]" value="<?php echo $rows['id']; ?>">
                                                        <td><?php echo $rows['designation_name']; ?></td>
                                                        <td><input type="number" id="age_limit" name="age_limit[]" value="<?php echo $rows['age_limit']; ?>" class="form-control"></td>
                                                        <td><input type="number" step="any" id="man_months" name="man_months[]" value="<?php echo $rows['man_months']; ?>" class="form-control"></td>
                                                        <td><input type="number" step="any" id="weightage_marks_rfp" name="weightage_marks_rfp[]" value="<?= $rows['weightage_marks_rfp']; ?>" class="form-control"></td>
                                                        <td><input type="text" id="single_comment" name="single_comment[]" value="<?= $rows['single_comment']; ?>" class="form-control"></td>
                                                        <td> <a href="<?= base_url('teamrequisition/deleteassignteam/' . $rows['id'] . '/' . $rows['project_id']); ?>" title="Delete" class="btn btn-danger js-sweetalert" ><i class="fa fa-trash-o"></i></a></td>
                                                       </tr>
                                                    <?php } ?>
									 
                                    </tbody>
                                </table>
                             
                </div>



                                                
                                                <div class="form-group has-success  pt-3"> 
                                        
                                                    <div class="btn btn-info btn-sm p-2 pull-left" data-toggle="modal" data-target="#Designation"><i class="fa fa-plus" aria-hidden="true"></i> Add More Designation</div>
                                                    <div class="btn btn-info btn-sm p-2 pull-right"  data-toggle="modal" data-target="#assignteam"><i class="fa fa-plus" aria-hidden="true"></i> Add More Team</div>
                                                </div>
                                                <div class="form-group has-successpt-5 mt-5"> 
                                                    <textarea name="comment" class="form-control" rows="4" cols="50"><?php echo $resulstArr[0]['comment']; ?></textarea>
                                                    <input type="hidden" name="url_chk" value="1">
                                                    <input type="hidden" name="project_id" value="<?php echo $resulstArr[0]['project_id']; ?>">
                                                </div>
                                                
                                                    <input class="btn btn-info btn-sm pull-left" type="submit" name="submit" value="Submit">
                                                
                                                
                                            </form>
                                            <form name="req_form" action="<?= thisurl(); ?>" method="post" >
                                                 
                                                    <i class="glyphicon glyphicon-envelope pull-right mt-2 ml-2"></i> &nbsp;
                                                    <input class="btn btn-info btn-sm pull-right" type="submit" name="sendsubmit_empbd" value="Send Mail & Lock Data" />
                                                     &nbsp;<small class="mt-2 ml-2 mr-3" style="color:red; float: right">  <?= ($LastUpdSendMail) ? " Sent Email : " . $LastUpdSendMail : ""; ?></small>
                                                </div>
                                                <input type="hidden" name="sendmail" value="1">
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>	
        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>
        <hr>



        <!-- Add  Designation -->
        <div class="modal fade" id="Designation" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Add Designation </h4>
                        <button type="button" class="close" id="reqrd" data-dismiss="modal">&times;</button>
                      
                    </div>
                    <div class="modal-body">
                        <form method="post" id="designations_add" action="<?= base_url("teamrequisition/addnewdesignation"); ?>">
                            <div class="box-content">
                                <div class="form-group">
                                    <label class="email"> Select Sector : <span id="reqrd"> * </span></label>
                                    <select required="required" onchange="categsetbysector()" class="form-control" name="sector_id" id="sector_id">
                                        <option value=""> --select-- </option>
                                        <?php foreach ($SectorDesigArr as $rowrec) { ?>
                                            <option value="<?= $rowrec->id; ?>"><?= $rowrec->sector_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group" id="common_categ">
                                    <label class="email" > Select Category : <span id="reqrd"> * </span></label>
                                    <select required="required" class="form-control" name="addcat_ids" id="addcat_ids"></select>
                                </div>
                                <div class="form-group">
                                    <label for="email"> Designation Name : <span id="reqrd"> * </span></label>
                                    <input required="required" type="text" autocomplete="off" class="form-control" name="designation" id="designation">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-info btn-sm pull-right" id="submit" name="Add" value="Add">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
 


        <!-- Assign Project Manager Modal Popup -->
        <div class="modal fade" id="assignteam" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 id="tender_title" class="modal-title"> Add New Team  </h4>
                        <button type="button" class="close" id="reqrd" data-dismiss="modal">&times;</button>
                       
                    </div>

                    <div class="modal-body">
                        <form method="post" id="assignTeamForm" >
                            <div class="box-content">
                                <div class="box-content">
                                    <div class="form-group">
                                        <label class="email"> Select Sector : <span id="reqrd"> * </span> </label>
                                        <select required="required" onchange="categsetbysector_team()" class="form-control sector_ids"  name="sector_ids" id="sector_ids">
                                            <option value="">--select--</option>
                                            <?php foreach ($SectorDesigArr as $rowrec) { ?>
                                                <option value="<?php echo $rowrec->id; ?>"><?php echo $rowrec->sector_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="email"> Select Category : <span id="reqrd"> * </span> </label>
                                        <select required="required" class="form-control" onchange="setdesignationdropdown()" name="cat_ids" id="cat_ids"></select>
                                    </div>

                                    <div class="form-group">
                                        <label class="email"> Select Designation : <span id="reqrd"> * </span></label>
                                        <select required="required" class="form-control designation_id" onchange="displayextra_attribute()" name="designation_ofsec" id="designation_ofsec">
                                            <option value="">--select--</option>
                                        </select>
                                    </div>

                                    <div class="form-group" id="metroproj_position_iddiv" style="display:none">
                                        <label class="email"> Project position ID : <span id="reqrd"> * </span></label>
                                        <input type="text" name="proj_position_idmetro" id="proj_position_idmetro" class="form-control">
                                    </div>


                                    <div class="box-content designation_detail">
                                        <div class="form-group">
                                            <label class="email" > Age Limit : </label>
                                            <input type="number" id="age_limit" name="age_limit" value="65" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="email"> Total Man Months : </label>
                                            <input type="number" step="any" id="man_months" name="man_months" value="" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="email"> Weightage Marks (RFP) : </label>
                                            <input type="number" step="any" id="weightage_marks_rfp" name="weightage_marks_rfp" value="" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="email"> Comment : </label>
                                            <textarea class="form-control" rows="2" cols="10" id="single_comment" name="single_comment"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                                <div class="col-md-12">
                                    <input type="hidden" id="assignteamprojid" name="project_id" value="<?= $BdProjectID; ?>" class="form-control">
                                    <input type="hidden" id="urlchk" name="url_chk" value="1">
                                    <input type="hidden" id="assignpropmangridSEC" name="assignpropmangrid" value="<?= $resulstArr[0]['proposal_mngr_id']; ?>" class="form-control">
                                    <!-- <input type="submit"  value="Submit" class="btn btn-info btn-sm pull-right" >	 -->
                                    <div class="btn btn-info btn-sm pull-right" onclick="ajaxAssignTeam()">Submit</div>	
                                </div>
                             
                            <div style="clear:both;"></div>
                        </form>
                    </div>
               
             


        </div>
    </div>
    <?php $this->load->view('admin/includes/footer'); ?>
    </div>
    <style>
        #table_length {
            margin-left: 20px;
        }

        #table_filter {
            margin-right: 2%;
        }

        .designation_detail {
            display: none;
        }
    </style>

       


        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            .designation_detail{display:none;}
            #reqrd{color:red}
        </style>
        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $("li#teamrequisition").addClass('active');
                                            });

                                            $(document).ready(function () {
                                                $("#checkAll").click(function () {
                                                    $('input:checkbox').not(this).prop('checked', this.checked);
                                                });
                                            });

                                            function categsetbysector() {
                                                var sectID = $("#sector_id").val();
                                                if (sectID) {
                                                    $('select[name="addcat_ids"]').empty();
                                                    $.ajax({
                                                        url: '<?= base_url('teamrequisition/setdesignationcateg_ajax'); ?>/' + sectID,
                                                        type: "GET",
                                                        dataType: "json",
                                                        success: function (data) {
                                                            $('select[name="addcat_ids"]').empty();
                                                            $('select[name="addcat_ids"]').append('<option value="">-- Select Category --</option>');
                                                            $.each(data, function (key, value) {
                                                                $('select[name="addcat_ids"]').append('<option value="' + value.k_id + '">' + value.ktype_name + '</option>');
                                                            });
                                                        }
                                                    });
                                                }
                                            }


                                            function categsetbysector_team() {
                                                var sectID = $("#sector_ids").val();
                                                if (sectID) {
                                                    $('select[name="cat_ids"]').empty();
                                                    $.ajax({
                                                        url: '<?= base_url('teamrequisition/setdesignationcateg_ajax'); ?>/' + sectID,
                                                        type: "GET",
                                                        dataType: "json",
                                                        success: function (data) {
                                                            $('select[name="cat_ids"]').empty();
                                                            $('select[name="cat_ids"]').append('<option value="">-- Select Category --</option>');
                                                            $.each(data, function (key, value) {
                                                                $('select[name="cat_ids"]').append('<option value="' + value.k_id + '">' + value.ktype_name + '</option>');
                                                            });
                                                        }
                                                    });
                                                }
                                            }

//Set Designation By Sector ID..
                                            function setdesignationdropdown() {
                                                var sectrID = $("#sector_ids").val();
                                                var categrID = $("#cat_ids").val();
                                                $('select[name="designation_ofsec"]').empty();
                                                if (sectrID && categrID) {
                                                    $.ajax({
                                                        url: '<?= base_url('teamrequisition/setdesignationcateg_teamaddajax'); ?>',
                                                        type: "POST",
                                                        dataType: "json",
                                                        data: {'sectrID_id': sectrID, 'categrID_id': categrID},
                                                        success: function (data) {
                                                            $('select[name="designation_ofsec"]').append('<option value="">-- Select Designation --</option>');
                                                            $.each(data, function (key, value) {
                                                                $('select[name="designation_ofsec"]').append('<option value="' + value.fld_id + '">' + value.designation_name + '</option>');
                                                            });
                                                        }
                                                    });
                                                }
                                            }

                                            //Display Extra Div..
                                            function displayextra_attribute() {
                                                var sectrID = $("#sector_ids").val();
                                                var categrID = $("#cat_ids").val();
                                                if (sectrID && categrID) {
                                                    $('.designation_detail').css('display', 'block');
                                                    if (sectrID == 3) {
                                                        $("#metroproj_position_iddiv").show();
                                                        $("#proj_position_idmetro").attr("required", "required");
                                                    } else {
                                                        $("#metroproj_position_iddiv").hide();
                                                        $("#proj_position_idmetro").removeAttr("required");
                                                    }
                                                }
                                            }


                function ajaxAssignTeam(){
                var data = $("#assignTeamForm").serialize();
                $.ajax({
                type: 'POST',
                url: "<?= base_url('teamrequisition/assignteamreq'); ?>",
                data: data,
                dataType: "json",
                success: function(responData) {
                $('#alert').css('display', 'block');
                $('#assignteam').modal('toggle');
                location.reload();
                toastr.success(responData.msg , 'Message', {timeOut: 5000});
               
                },
                });
                }

        </script>

    </div>
</body>
</html>