<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>

				
				<div class="row clearfix">
                    <div class="col-lg-12">
						<div class="card">
                        <div class="header">
                            <form id="form-filter"> 
                                        <!--<div class="card-header" id="headingOne">-->
                                            <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>Sector :</b>
                                                        <div class="input-group mb-3">
                                                            <select name="sectorinput" id="sectorinput"  class="form-control">
                                                    <option <?= ($secId == "") ? 'Selected' : ''; ?> value="">--All--</option>
                                                    <?php
                                                    if ($sectorArr):
                                                        foreach ($sectorArr as $row) {
                                                            ?>
                                                            <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
                                                            <?php
                                                        }
                                                    endif;
                                                    ?>
                                                </select>
                                                        </div>
                                                    </div>
													<div class="col-lg-3 col-md-6">
                                                        <b>Phase :</b>
                                                        <div class="input-group mb-3">
                                                           <select id="company_name" class="form-control"><option value="" selected="selected">--All--</option>
															<option value="Design">Design</option>
															<option value="Construction">Construction</option>
															<option value="Electrical">Electrical</option>
															<option value="Maintenance">Maintenance</option>
															<option value="Other">Other</option>
														</select>
                                                        </div>
                                                    </div>
													
													
                                                   
                                                    <div class="col-lg-1 col-md-6">
                                                        <div class="mb-2">
                                                            <b></b>
															<button type="button" id="btn-filter" class="btn btn-success btn-block" style="margin-top:20px;"> Filter </button>
                                                        </div>
                                                    </div>
													<div class="col-lg-1 col-md-6">
                                                        
														<div class="mb-2">
                                                            <b></b>
															<button type="button" id="btn-reset" class="btn btn-primary btn-block" style="margin-top:20px;"> Reset </button>
                                                        </div>
                                                    </div>
													
													<div class="col-lg-1 col-md-6">
                                                        
														<div class="mb-2">
                                                            <b></b>
															<a class="btn btn-success" style="margin-top:20px;" href="<?= base_url('tender/traffic'); ?>">Filter Traffic Project </a>
                                                        </div>
                                                    </div>
													
													
                                                </div>
                                        <!--</div> -->                               
                                        
										</form>
										
								
                        </div>
                        <div class="body">
                            <ul class="nav nav-tabs-new">
								<li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#all-new">All</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Home-new">National</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Profile-new">International</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Contact-new">Other</a></li>
                                
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane show active" id="all-new">
                                    <div class="table-responsive">
								<form action="<?= base_url('activeproject/importantprojectbycheckbox'); ?>" method="post">
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Sr. No</th>
											<th style="width:11%">Exp-Date</th>
											<th style="width:40%">Tender Details</th>
											<th>Location</th>
											<th>Client</th>
											<th style="width:15%">Actions </th>
										</tr> 
									</thead>
									<tbody>
									</tbody>
									<tfoot>
										<tr>
											<th>Sr. No</th>
											<th style="width:10%">Exp-Date</th>
											<th style="width:10%">Tender Details</th>
											<th>Location</th>
											<th>Client</th>
											<th style="width:15%">Actions </th>
										</tr> 
									</tfoot>
                
									<tfoot>
										<tr>
											<td>&nbsp;</td>
											<td> <select name="act_type">
													<option value="Active"> Active </option>
													<option value="InActive"> InActive </option>
												</select>
											</td>
						
											<td>
												<button type="submit" class="btn btn-success">Submit</button>&emsp;All <input type="checkbox" id="checkAll">
											</td>
											<td>&nbsp; </td>
											
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
									</tfoot>
									<hr>
									</table>
								</form>
                            </div>
                            </div>
								<div class="tab-pane" id="Home-new">
                                    <div class="table-responsive">
								<form action="<?= base_url('activeproject/importantprojectbycheckbox'); ?>" method="post">
                                    <table id="table1" class="table table-striped display" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Sr. No</th>
											<th style="width:11%">Exp-Date</th>
											<th style="width:40%">Tender Details</th>
											<th>Location</th>
											<th>Client</th>
											<th style="width:15%">Actions </th>
										</tr> 
									</thead>
									<tbody>
									</tbody>
									<tfoot>
										<tr>
											<th>Sr. No</th>
											<th style="width:10%">Exp-Date</th>
											<th style="width:40%">Tender Details</th>
											<th>Location</th>
											<th>Client</th>
											<th style="width:15%">Actions </th>
										</tr> 
									</tfoot>
                
									<tfoot>
										<tr>
											<td>&nbsp;</td>
											<td> <select name="act_type">
													<option value="Active"> Active </option>
													<option value="InActive"> InActive </option>
												</select>
											</td>
						
											<td>
												<button type="submit" class="btn btn-success">Submit</button>&emsp;All <input type="checkbox" id="checkAll">
											</td>
											<td>&nbsp; </td>
											
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
									</tfoot>
									<hr>
									</table>
								</form>
                            </div>
                                </div>
                                <div class="tab-pane" id="Profile-new">
                                    <div class="table-responsive">
								<form action="<?= base_url('activeproject/importantprojectbycheckbox'); ?>" method="post">
                                    <table id="table2" class="table table-striped display" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Sr. No</th>
											<th style="width:11%">Exp-Date</th>
											<th style="width:40%">Tender Details</th>
											<th>Location</th>
											<th>Client</th>
											<th style="width:15%">Actions </th>
										</tr> 
									</thead>
									<tbody>
									</tbody>
									<tfoot>
										<tr>
											<th>Sr. No</th>
											<th style="width:10%">Exp-Date</th>
											<th style="width:40%">Tender Details</th>
											<th>Location</th>
											<th>Client</th>
											<th style="width:15%">Actions </th>
										</tr> 
									</tfoot>
                
									<tfoot>
										<tr>
											<td>&nbsp;</td>
											<td> <select name="act_type">
													<option value="Active"> Active </option>
													<option value="InActive"> InActive </option>
												</select>
											</td>
						
											<td>
												<button type="submit" class="btn btn-success">Submit</button>&emsp;All <input type="checkbox" id="checkAll">
											</td>
											<td>&nbsp; </td>
											
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
									</tfoot>
									<hr>
									</table>
								</form>
                            </div>
                                </div>
                                <div class="tab-pane" id="Contact-new">
                                    <div class="table-responsive">
								<form action="<?= base_url('activeproject/importantprojectbycheckbox'); ?>" method="post">
                                    <table id="table3" class="table table-striped display" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Sr. No</th>
											<th style="width:11%">Exp-Date</th>
											<th style="width:40%">Tender Details</th>
											<th>Location</th>
											<th>Client</th>
											<th style="width:15%">Actions </th>
										</tr> 
									</thead>
									<tbody>
									</tbody>
									<tfoot>
										<tr>
											<th>Sr. No</th>
											<th style="width:10%">Exp-Date</th>
											<th style="width:40%">Tender Details</th>
											<th>Location</th>
											<th>Client</th>
											<th style="width:15%">Actions </th>
										</tr> 
									</tfoot>
                
									<tfoot>
										<tr>
											<td>&nbsp;</td>
											<td> <select name="act_type">
													<option value="Active"> Active </option>
													<option value="InActive"> InActive </option>
												</select>
											</td>
						
											<td>
												<button type="submit" class="btn btn-success">Submit</button>&emsp;All <input type="checkbox" id="checkAll">
											</td>
											<td>&nbsp; </td>
											
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
									</tfoot>
									<hr>
									</table>
								</form>
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
					</div>
				</div>
				
                
				
				
				<div class="row clearfix">
                    <div class="col-lg-12">
                        <!--<div class="card">
                            <div class="body">
                                <div class="accordion" id="accordion">
                                    <div>
									<form id="form-filter"> 
                                            <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>Sector :</b>
                                                        <div class="input-group mb-3">
                                                            <select name="sectorinput" id="sectorinput"  class="form-control">
                                                    <option <?= ($secId == "") ? 'Selected' : ''; ?> value="">--All--</option>
                                                    <?php
                                                    if ($sectorArr):
                                                        foreach ($sectorArr as $row) {
                                                            ?>
                                                            <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
                                                            <?php
                                                        }
                                                    endif;
                                                    ?>
                                                </select>
                                                        </div>
                                                    </div>
													<div class="col-lg-3 col-md-6">
                                                        <b>Phase :</b>
                                                        <div class="input-group mb-3">
                                                           <select id="company_name" class="form-control"><option value="" selected="selected">--All--</option>
															<option value="Design">Design</option>
															<option value="Construction">Construction</option>
															<option value="Electrical">Electrical</option>
															<option value="Maintenance">Maintenance</option>
															<option value="Other">Other</option>
														</select>
                                                        </div>
                                                    </div>
													
													
                                                   
                                                    <div class="col-lg-1 col-md-6">
                                                        <div class="mb-2">
                                                            <b></b>
															<button type="button" id="btn-filter" class="btn btn-success btn-block" style="margin-top:20px;"> Filter </button>
                                                        </div>
                                                    </div>
													<div class="col-lg-1 col-md-6">
                                                        
														<div class="mb-2">
                                                            <b></b>
															<button type="button" id="btn-reset" class="btn btn-primary btn-block" style="margin-top:20px;"> Reset </button>
                                                        </div>
                                                    </div>
													
													
                                                </div>                         
                                        
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="card">
                            <div class="body">
                                <div class="table-responsive">
								<form action="<?= base_url('activeproject/importantprojectbycheckbox'); ?>" method="post">
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                <thead>
						<tr>
							<th>Sr. No</th>
							<th style="width:11%">Exp-Date</th>
							<th style="width:40%">Tender Details</th>
							<th>Location</th>
							<th>Client</th>
							<th style="width:15%">Actions </th>
						</tr> 
                </thead>
				<tbody>
                </tbody>
                <tfoot>
						<tr>
							<th>Sr. No</th>
							<th style="width:10%">Exp-Date</th>
							<th style="width:40%">Tender Details</th>
							<th>Location</th>
							<th>Client</th>
							<th style="width:15%">Actions </th>
						</tr> 
                </tfoot>
                
				<tfoot>
					<tr>
						<td>&nbsp;</td>
						<td> <select name="act_type">
								<option value="Togo"> To Go Review  </option>
								<option value="Nogo"> No Go /Not important </option>
							</select>
						</td>
						
						<td>
							<button type="submit" class="btn btn-success">Submit</button>&emsp;All <input type="checkbox" id="checkAll">
						</td>
						<td>&nbsp; </td>
						
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</tfoot>
				<hr>
            </table>
			 </form>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
<style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $("li#tender_search").addClass('active');
                $("li#newprojects").addClass('active');
            });
            $(document).ready(function () {
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            });
            function gocomment(tndrid) {
                //Put ProjId And Secot Id..
                $("#projId").val(tndrid);
                $.ajax({
                    type: 'POST',
                    dataType: "text",
                    data: {'tndrid': tndrid},
                    url: "<?php echo base_url('newproject/commentset'); ?>",
                    success: function (responData) {
                        $("#chatbox").html(responData);
                    },
                });
            }
            //Comment Submitted...
            $(document).ready(function () {
                $('#commentmsg').keypress(function (e) {
                    var key = e.which;
                    if (key == 13) {
                        var comnt = $("#commentmsg").val();
                        var projId = $("#projId").val();
                        //Comment Submit..
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?php echo base_url('newproject/commentsubmit'); ?>",
                            data: {'projId': projId, 'comnt': comnt},
                            success: function (responData) {
                                $("#commentmsg").val('');
                                return gocomment(projId);
                            },
                        });
                    }
                });
            });

            function deletethisrecord(delid, sectid) {
                if (confirm("Are You Sure Inactive/Delete This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url('newproject/gototrashbyuser'); ?>",
                        data: {'delid': delid},
                        success: function (responData) {
                            $('#alert').css('display', 'block');
                            $('#msgs').html('Tender Inactivated.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);
                            console.log(responData);
                        },
                    });
                }
            }

            function activethisrecord(actid, sectid) {
				// alert(test)
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url('newproject/activeproject'); ?>",
                    data: {'actid': actid},
                    success: function (responData) {
                        $('#alert').css('display', 'block');
                        $('#msgs').html('Tender Activated.');
                        setTimeout(function () {
                            location.reload(1);
                        }, 1000);
                    },
                });
            }
            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('newProject/newProjectAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.project_name = $('#company_name').val();
                            data.sectorinput = $('#sectorinput').val();
                            data.national_intern = $('#national_intern').val();
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{
                            extend: 'collection',
                            text: 'Export',
                            buttons: [
                                'copy',
                                'excel',
                                'csv',
                                'pdf',
                                'print'
                            ]
                        }
                    ],

                    //Set column definition initialisation properties.
                    "columnDefs": [{
                            "targets": [0], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],

                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });

                // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });

            });
        </script>
<script type="text/javascript">
            $(document).ready(function () {
                $("li#tender_search").addClass('active');
                $("li#newprojects").addClass('active');
            });
            $(document).ready(function () {
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            });
            function gocomment(tndrid) {
                //Put ProjId And Secot Id..
                $("#projId").val(tndrid);
                $.ajax({
                    type: 'POST',
                    dataType: "text",
                    data: {'tndrid': tndrid},
                    url: "<?= base_url('newproject/commentset'); ?>",
                    success: function (responData) {
                        $("#chatbox").html(responData);
                    },
                });
            }
            //Comment Submitted...
            $(document).ready(function () {
                $('#commentmsg').keypress(function (e) {
                    var key = e.which;
                    if (key == 13) {
                        var comnt = $("#commentmsg").val();
                        var projId = $("#projId").val();
                        //Comment Submit..
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?= base_url('newproject/commentsubmit'); ?>",
                            data: {'projId': projId, 'comnt': comnt},
                            success: function (responData) {
                                $("#commentmsg").val('');
                                return gocomment(projId);
                            },
                        });
                    }
                });
            });

            function deletethisrecord(delid, sectid) {
                if (confirm("Are You Sure Inactive/Delete This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('newproject/gototrashbyuser'); ?>",
                        data: {'delid': delid},
                        success: function (responData) {
                            $('#alert').css('display', 'block');
                            $('#msgs').html('Tender Inactivated.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);
                            console.log(responData);
                        },
                    });
                }
            }

            function activethisrecord(actid, sectid) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('newproject/activeproject'); ?>",
                    data: {'actid': actid},
                    success: function (responData) {
                        $('#alert').css('display', 'block');
                        $('#msgs').html('Tender Activated.');
                        setTimeout(function () {
                            location.reload(1);
                        }, 1000);
                    },
                });
            }
            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table1').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('newProject/newProjectnational') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.project_name = $('#company_name').val();
                            data.sectorinput = $('#sectorinput').val();
                            data.national_intern = $('#national_intern').val();
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{
                            extend: 'collection',
                            text: 'Export',
                            buttons: [
                                'copy',
                                'excel',
                                'csv',
                                'pdf',
                                'print'
                            ]
                        }
                    ],

                    //Set column definition initialisation properties.
                    "columnDefs": [{
                            "targets": [0], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });

                // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });

            });
        </script>
		 </script>
<script type="text/javascript">
            $(document).ready(function () {
                $("li#tender_search").addClass('active');
                $("li#newprojects").addClass('active');
            });
            $(document).ready(function () {
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            });
            function gocomment(tndrid) {
                //Put ProjId And Secot Id..
                $("#projId").val(tndrid);
                $.ajax({
                    type: 'POST',
                    dataType: "text",
                    data: {'tndrid': tndrid},
                    url: "<?= base_url('newproject/commentset'); ?>",
                    success: function (responData) {
                        $("#chatbox").html(responData);
                    },
                });
            }
            //Comment Submitted...
            $(document).ready(function () {
                $('#commentmsg').keypress(function (e) {
                    var key = e.which;
                    if (key == 13) {
                        var comnt = $("#commentmsg").val();
                        var projId = $("#projId").val();
                        //Comment Submit..
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?= base_url('newproject/commentsubmit'); ?>",
                            data: {'projId': projId, 'comnt': comnt},
                            success: function (responData) {
                                $("#commentmsg").val('');
                                return gocomment(projId);
                            },
                        });
                    }
                });
            });

            function deletethisrecord(delid, sectid) {
                if (confirm("Are You Sure Inactive/Delete This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('newproject/gototrashbyuser'); ?>",
                        data: {'delid': delid},
                        success: function (responData) {
                            $('#alert').css('display', 'block');
                            $('#msgs').html('Tender Inactivated.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);
                            console.log(responData);
                        },
                    });
                }
            }

            function activethisrecord(actid, sectid) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('newproject/activeproject'); ?>",
                    data: {'actid': actid},
                    success: function (responData) {
                        $('#alert').css('display', 'block');
                        $('#msgs').html('Tender Activated.');
                        setTimeout(function () {
                            location.reload(1);
                        }, 1000);
                    },
                });
            }
            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table2').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('newProject/newProjectInterNational') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.project_name = $('#company_name').val();
                            data.sectorinput = $('#sectorinput').val();
                            data.national_intern = $('#national_intern').val();
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{
                            extend: 'collection',
                            text: 'Export',
                            buttons: [
                                'copy',
                                'excel',
                                'csv',
                                'pdf',
                                'print'
                            ]
                        }
                    ],

                    //Set column definition initialisation properties.
                    "columnDefs": [{
                            "targets": [0], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });

                // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });

            });
        </script>
		
		<script type="text/javascript">
            $(document).ready(function () {
                $("li#tender_search").addClass('active');
                $("li#newprojects").addClass('active');
            });
            $(document).ready(function () {
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            });
            function gocomment(tndrid) {
                //Put ProjId And Secot Id..
                $("#projId").val(tndrid);
                $.ajax({
                    type: 'POST',
                    dataType: "text",
                    data: {'tndrid': tndrid},
                    url: "<?= base_url('newproject/commentset'); ?>",
                    success: function (responData) {
                        $("#chatbox").html(responData);
                    },
                });
            }
            //Comment Submitted...
            $(document).ready(function () {
                $('#commentmsg').keypress(function (e) {
                    var key = e.which;
                    if (key == 13) {
                        var comnt = $("#commentmsg").val();
                        var projId = $("#projId").val();
                        //Comment Submit..
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?= base_url('newproject/commentsubmit'); ?>",
                            data: {'projId': projId, 'comnt': comnt},
                            success: function (responData) {
                                $("#commentmsg").val('');
                                return gocomment(projId);
                            },
                        });
                    }
                });
            });

            function deletethisrecord(delid, sectid) {
                if (confirm("Are You Sure Inactive/Delete This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('newproject/gototrashbyuser'); ?>",
                        data: {'delid': delid},
                        success: function (responData) {
                            $('#alert').css('display', 'block');
                            $('#msgs').html('Tender Inactivated.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);
                            console.log(responData);
                        },
                    });
                }
            }

            function activethisrecord(actid, sectid) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('newproject/activeproject'); ?>",
                    data: {'actid': actid},
                    success: function (responData) {
                        $('#alert').css('display', 'block');
                        $('#msgs').html('Tender Activated.');
                        setTimeout(function () {
                            location.reload(1);
                        }, 1000);
                    },
                });
            }
            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table3').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('newProject/newProjectMultiLocation') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.project_name = $('#company_name').val();
                            data.sectorinput = $('#sectorinput').val();
                            data.national_intern = $('#national_intern').val();
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{
                            extend: 'collection',
                            text: 'Export',
                            buttons: [
                                'copy',
                                'excel',
                                'csv',
                                'pdf',
                                'print'
                            ]
                        }
                    ],

                    //Set column definition initialisation properties.
                    "columnDefs": [{
                            "targets": [0], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });

                // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });

            });
        </script>
</body>