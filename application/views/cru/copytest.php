<?php
// error_reporting(E_ALL);
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$projNm = ProjNameById($resulstArr[0]['project_id']);

?>
<title><?php echo substr(strip_tags($projNm), 0, 77); ?> </title>


<style>
    #table_length {
        position: absolute;
        margin-left: 100px !important;
    }

    .table tbody tr td,
    .table tbody th td {
        vertical-align: middle;
        white-space: normal !important;
    }
    div.ColVis {
    float: right;
    
}
</style>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : "CRU Team Detail"; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : "CRU Team Detail"; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                

                   

                    <!-- open Phase Section-->
                     <div class="card">
                                    
                                        <div class="body">

                                             <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable" >
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Success ! </div></strong> <?= $this->session->flashdata('msg'); ?>
                        </div>
                    <?php } ?>
                                           
                            <div class="tab-content">
                                <div id="home" class="tab-pane  in active">
                                    <div class="row">
                                         
                                            <div class="col-md-12  ml-2 mr-2" style="color: green; margin: 0px auto;text-align: center;font-size: 20px;"><?php echo $resulstArr[0]['project_no']; ?></div>
                                            <div class="highlight m-2 p-2">
                                                <p><?php echo strip_tags($projNm); ?></p>
                                            </div>
                                        
                                    </div>
                                    <br>
                                    <hr>




 <div class="table-responsive">
                                    <div class="highlight mt-2 mb-4" id="formsection" style="display:none;">
                                        <form name="frmm" id="frmm" method="post" action="" enctype="">
                                            <div class="row"><div class="col-md-12"><h5>Add Detail</h5></div></div>
                                            <div class="row">

                                                 <div class="col-md-3">
                                                    <div class="form-group required">
                                                        <label class="control-label" for="usr">Position </label> &nbsp;&nbsp;&nbsp;
                                                        <input type="text" id="designation_name" class="form-control" >
                                                        <input type="hidden" id="designation_id" name="designation_id" class="form-control" >
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group required">
                                                        <label class="control-label" for="usr">Select Name (In House) </label> &nbsp;&nbsp;&nbsp;
                                                        <select  class="form-control" id="empid" name="empname" onchange="setdetailsemp()">
                                                            <option value="">Select Employee</option>
                                                            <?php
                                                            if ($userData):
                                                                foreach ($userData as $row) {
                                                                    ?>
                                                                    <option value="<?= $row->id; ?>"><?= $row->userfullname; ?></option>
                                                                    <?php
                                                                }
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group required">
                                                        <label class="control-label" for="usr">Select Name (Other) </label> &nbsp;&nbsp;&nbsp;
                                                        <span style="cursor:pointer; font-size: 10px;" class="btn btn-success btn-sm" title="Add New Other Employee" data-toggle="modal" data-target="#myModalotherEmp"> <i class="fa fa-plus"></i> </span>
                                                        <select  class="form-control" id="empidother" name="empnameother" onchange="setdetailsempother()">
                                                            <option value="">Select Employee Other</option>
                                                            <?php
                                                            $recArr = GetAllOtherUserRec();
                                                            foreach ($recArr as $row) {
                                                                ?>
                                                                <option value="<?= $row->fld_id; ?>"><?= $row->emp_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group required">
                                                        <label class="control-label" for="usr">Email </label>
                                                        <input type="email" name="emailaddress" required id="emailaddress"  class="form-control" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group required">
                                                        <label class="control-label" for="usr">Contact No </label>
                                                        <input type="number" min="1" name="contactnumber" required class="form-control" id="contactnumber">
                                                    </div>
                                                </div>
                                             
                                            
                                                <div class="col-md-3">
                                                    <div class="form-group required">
                                                        <label class="control-label" for="usr">Marks </label>
                                                        <input type="text" required class="form-control" name="marks" id="remarks_bd">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label" for="usr">Firm</label>
                                                        <input type="text" class="form-control" name="firm" id="remarks_bd">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label" for="usr">Certifcate </label>
                                                        <select class="form-control" name="certificate" id="remarks_bd">
                                                            <option>Select</option>
                                                            <option value="1"> Yes </option>
                                                            <option value="2"> No </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label" for="usr">Undertaking </label>
                                                        <select class="form-control" name="undertaking" id="remarks_bd">
                                                            <option>Select</option>
                                                            <option value="1"> Yes </option>
                                                            <option value="2"> No </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label" for="usr">Salary</label>
                                                        <input type="text" class="form-control" name="salary" id="remarks_bd">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label" for="usr">Remarks (CRU) </label>
                                                        <input type="text" class="form-control" name="remarks_cru" id="remarks_bd">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label" for="usr">Remarks (BD)</label>
                                                        <input type="text" class="form-control" name="remarks_bd" id="remarks_bd" >
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                       
                                                        <input type="hidden" name="project_id" value="<?= $resulstArr[0]['project_id']; ?>">
                                                        <a href="javascript:void(0)" class="btn btn-info pull-right"   onclick="addDetail()">Save</a>
                                                        <input type="hidden" id="fld_id" name="fld_id" class="form-control" >
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

 

                                   
                                         

                                        <table id="table" class="table table-striped display">                                                         
                                            <thead>
                                                <tr>
                                                    <th>Sr. no.</th>
                                                    <th>Position</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Contact</th>
                                                    <th>Age Limit</th>
                                                    <th>Certifcate</th>
                                                    <th>Firm</th>
                                                    <th>Marks</th>
                                                    <th>Total MM</th>
                                                    <th>RFP Marks</th>
                                                    <th>Weightage Marks</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                $fixArr = array('1' => 'Yes', '2' => 'No');
                                                $srn = 0;
                                                $totalmmcount = $rfpcount = $weightagemmcount = 0;

                                                //echo "<pre>"; print_r($tqRecArr);

                                                if ($tqRecArr):
                                                    foreach ($tqRecArr as $roRec) {
                                                        ?>
                                                        <tr>
                                                            <?php
                                                            $empNM = '';
                                                            $empNM2 = '';
                                                            $desiname = DesignationNameById($roRec->designation_id);
                                                            if ($roRec->empname):
                                                                $empNMs = UserNameById($roRec->empname);
                                                                $empNM = $empNMs->userfullname;
                                                            else:
                                                                $empNM2 = UserOtherNameById($roRec->empnameother);
                                                            endif;
                                                            ?>
                                                            <td>
                                                                <?= $srn = $srn + 1; ?>.
                                                                
                                                            </td>
                                                            <td>
                                                                <?= $desiname; ?>
                                                                <!-- ########################### Delete Position Code By Asheesh 28-06-2019 -->

                                                            </td>
                                                            <td><?= $empNM . " " . $empNM2; ?></td>
                                                            <td><?= $roRec->emailaddress; ?></td>
                                                            <td><?= $roRec->contactnumber; ?></td>
                                                            <td><?= $roRec->age_limit; ?></td>
                                                            <td><?= @$fixArr[$roRec->certificate]; ?></td>
                                                            <td><?= $roRec->firm; ?></td>
                                                            <td><?= $roRec->marks; ?></td>
                                                            <td><?= $roRec->man_months; ?><?php $totalmmcount += $roRec->man_months; ?></td>
                                                            <td>
                                                                <?php
                                                                $realrfp = GetRfpByPid_desId($resulstArr[0]['project_id'], $roRec->designation_id);
                                                                echo @$realrfp;
                                                                $rfpcount += $realrfp;
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $realWei = ($roRec->marks / 100) * $realrfp;
                                                                echo $realWei;
                                                                $weightagemmcount += $realWei;
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php if (($empNM == '') AND ( $empNM2 == '')) { ?>
                                                                    <a href="#formsection" onclick="setnewadd('<?= preg_replace("/\r|\n/", "", $desiname); ?>', '<?= $roRec->designation_id; ?>', '<?= $roRec->id; ?>')" title="Add Details" class="btn btn-info btn-sm mt-1"><i class="fa fa-user"></i> &nbsp; Add Details</a>
                                                                <?php } ?>
                                                                <?php if (($empNM == "" and $empNM2 == "") and ( $roRec->emailaddress == "") and ( $roRec->contactnumber == "")) { ?>
                                                                    &nbsp; <a href="javascript:void(0)" onclick="delpositionbycru('<?= $roRec->id; ?>', '<?= $roRec->team_assign_id; ?>','<?= $roRec->project_id; ?>')"   class="btn btn-danger btn-sm mt-1" title="Delete Position"><i class="fa fa-trash"> </i>&nbsp; Delete Position</a>
                                                                <?php } ?>
                                                                <?php if ($roRec->locks == '') : ?>
                                                                    <a href="javascript:void(0)" title="Lock " id="lock<?= $roRec->id; ?>" onclick="lockteamreq('<?= $roRec->id; ?>')"   class="btn btn-warning btn-sm mt-1"><i class="fa fa-lock"></i> &nbsp;Lock </a>  
                                                                <?php endif; ?>  
                                                                <!--  Emp Lock Cond-->
                                                                <?php if (($roRec->locks == '') and ( $loginRoleEmp->role_name == 'employee')): ?>
                                                                    <a href="javascript:void(0)" title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')"   class="btn btn-success btn-sm mt-1"><i class="fa fa-edit"></i> &nbsp;Edit / View </a>  
                                                                <?php endif; ?>
                                                                <!--  cru Lock Cond-->
                                                                <?php if (($roRec->locks == '' or $roRec->locks == 'employee') and ( $loginRoleEmp->role_name == 'cru_manager')): ?>
                                                                    <!-- <i title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')" style="cursor:pointer" class="fa fa-edit"></i> &nbsp; -->
                                                                     <a href="javascript:void(0)" title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')"   class="btn btn-success btn-sm mt-1"><i class="fa fa-edit"></i> &nbsp;Edit / View </a> 
                                                                <?php endif; ?>
                                                                <!--Perpos Manager Cond-->
                                                                <?php if (($roRec->locks == '' or $roRec->locks == 'employee' or $roRec->locks == 'cru_manager') and ( $loginRoleEmp->role_name == 'proposal_manager')): ?>
                                                                   <!--  <i title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')" style="cursor:pointer" class="fa fa-edit"></i> &nbsp; -->

                                                                    <a href="javascript:void(0)" title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')"   class="btn btn-success btn-sm mt-1"><i class="fa fa-edit"></i> &nbsp;Edit / View </a> &nbsp;
                                                                <?php endif; ?>
                                                                <!--Top Manager Cond-->
                                                                <?php if ($loginRoleEmp->role_name == 'top_management'): ?>
                                                                    <!-- <i title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')" style="cursor:pointer" class="fa fa-edit"></i> &nbsp; -->

                                                                     <a href="javascript:void(0)" title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')"   class="btn btn-success btn-sm mt-1"><i class="fa fa-edit"></i> &nbsp;Edit / View </a> &nbsp;
                                                                <?php endif; ?>
                                                                 
                                                                <?php if ($roRec->resume_file == '') { ?>
                                                                    <a href="javascript:void(0)" title="Upload Resume" data-toggle="modal" data-target="#modeUpdResume" onclick="uploadresume('<?= $roRec->id; ?>', '<?= $empNM . " " . $empNM2; ?>')" class="btn btn-info btn-sm mt-1">   <i class="fa fa-paperclip"></i>&nbsp; Upload Resume</a>
                                                                <?php } else { ?>
                                                                     
                                                                    <a href="<?= HOSTNAME . "uploads/resume/" . $roRec->resume_file; ?>" target="_blank" class="btn btn-default mt-1"> <i title="Resume" style="cursor:pointer" class="fa fa-download"></i> &nbsp; Download Resume </a>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>

                                                    <?php } else: ?>
                                                    <tr>
                                                        <td colspan="13">
                                                            <p align="center" style="color:red">No Any Entry for this Project</p>
                                                        </td>
                                                    <?php endif; ?>         
                                            </tbody>

                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th ><div class="alert alert-success m-2"><?php echo $totalmmcount; ?></div></th>
                                                    <th ><div class="alert alert-warning m-2"><?php echo $rfpcount; ?></div></th>
                                                    <th><div  class="alert alert-info m-2"><?php echo $weightagemmcount; ?></div></th>
                                                    <th></th>
                                                </tr>
                                            </tfoot>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             
        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>	

</div>  
        <!-- close Model -->
       <?php $this->load->view('admin/includes/footer'); ?>
    </div>
    </body>

    <style>
        #table_length{margin-left:20px;}
        #table_filter{margin-right:2%;}
        .designation_detail{display:none;}
        #selectuser_chosen{width:251px!important;}
        .hidden1{display:none;}

    </style>
    <script type="text/javascript">
                                                            $(document).ready(function () {
                                                                $("li#cruproject").addClass('active');
                                                            });

                                                            function setdetailsemp() {
                                                                var userID = $("#empid").val();
                                                                $("#empidother").val('');
                                                                $("#contactnumber").val('');
                                                                $("#emailaddress").val('');
                                                                if (userID) {
                                                                    $.ajax({
                                                                        type: 'POST',
                                                                        url: '<?= base_url('cru/userDatas'); ?>',
                                                                        data: {'userID': userID},
                                                                        success: function (response) {
                                                                            var data = JSON.parse(response);
                                                                            $('#contactnumber').val(data[0]);
                                                                            $('#emailaddress').val(data[1]);
                                                                        }
                                                                    });
                                                                }
                                                            }

                                                            function setdetailsempother() {
                                                                var userIDOther = $("#empidother").val();
                                                                $("#empid").val('');
                                                                $("#contactnumber").val('');
                                                                $("#emailaddress").val('');
                                                                $.ajax({
                                                                    type: 'POST',
                                                                    url: '<?= base_url('cru/userDatasOther'); ?>',
                                                                    data: {'userIDOther': userIDOther},
                                                                    success: function (response) {
                                                                        var data = JSON.parse(response);
                                                                        $('#contactnumber').val(data[1]);
                                                                        $('#emailaddress').val(data[0]);
                                                                    }
                                                                });
                                                            }

                                                            //Edit Process By Asheesh,,
                                                            function editteamreq(edtid) {
                                                                $.ajax({
                                                                    type: 'post',
                                                                    url: "<?= base_url('cru/tqDetailsByID'); ?>",
                                                                    data: {edtid: edtid},
                                                                    success: function (response) {
                                                                        var obj = jQuery.parseJSON(response);
                                                                        $("#emailaddressUPD").val(obj.emailaddress);
                                                                        $("#contactnumberUPD").val(obj.contactnumber);
                                                                        $("#marksUPD").val(obj.marks);
                                                                        $("#firmUPD").val(obj.firm);
                                                                        $("#man_monthsUPD").val(obj.man_months);
                                                                        $("#age_limitUPD").val(obj.age_limit);
                                                                        $("#salaryUPD").val(obj.salary);
                                                                        $("#remarks_cruUPD").val(obj.remarks_cru);
                                                                        $("#remarks_bdUPD").val(obj.remarks_bd);
                                                                        $("#editid").val(obj.id);
                                                                        $("#project_idUPD").val(obj.project_id);
                                                                        $("#certificateUPD").val(obj.certificate);
                                                                        $("#undertakingUPD").val(obj.undertaking);
                                                                        $("#designation_idUPD").val(obj.designation_id);
                                                                        $("#empnameUPD").html("Employee Name : " + obj.empname);
                                                                    }
                                                                });
                                                            }

                                                            //Delete Process
                                                            function deleteteamreq(delid) {
                                                                if (confirm("Are You Sure Delete This ? ") == true) {
                                                                    window.location = "<?= base_url('cru/deleteam_treq?delid='); ?>" + delid;
                                                                }
                                                            }
                                                            //Delete Position By Cru Before Entry User Details Asheesh..
                                                            function delpositionbycru(tassignmemberid,tassignid,bdproj_id) {
                                                                if (confirm("Are You Sure Delete This ? ") == true) {
                                                                    


                                                                    deletePosition(tassignmemberid,tassignid,bdproj_id);
                                                                }
                                                            }

                                                            //Lock Proc..
                                                            function lockteamreq(lockid) {
                                                                $.ajax({
                                                                    type: 'POST',
                                                                    url: '<?= base_url('cru/lockbylbl'); ?>',
                                                                    data: {'lockid': lockid},
                                                                    success: function (response) {
                                                                        $('#lock' + lockid).hide();
                                                                    }
                                                                });
                                                            }


                                                            //Upload Resume Ajax Section..
                                                            function uploadresume(actid, empname) {
                                                                $("#emplnname").html(" Resume of : " + empname);
                                                                $("#uploaderror2").hide();
                                                                $("#actionid").val(actid);
                                                            }

                                                            //Resume Upload Validation Code By Asheesh..
                                                            $(document).ready(function () {
                                                                $("#uploaderror2").hide();
                                                                $('INPUT[type="file"]').change(function () {
                                                                    var ext = this.value.match(/\.(.+)$/)[1];
                                                                    switch (ext) {

                                                                        case 'doc':
                                                                        case 'docx':
                                                                        case 'pdf':
                                                                            $('#uploadButton').attr('disabled', false);
                                                                            $("#uploaderror2").hide();
                                                                            break;
                                                                        default:
                                                                            $("#uploaderror2").show();
                                                                            $("#uploaderror2").html('This is not an allowed file type.');
                                                                            this.value = '';
                                                                    }
                                                                });
                                                                $('INPUT[type="file"]').bind('change', function () {
                                                                    var imgfilesize = (this.files[0].size / 17024);
                                                                    if (imgfilesize > 100) {
                                                                        $("#uploaderror2").show();
                                                                        $("#uploaderror2").html('Reducing File Size Max Uploads (17MB). ');
                                                                        this.value = '';
                                                                    } else {
                                                                        $("#uploaderror2").hide();
                                                                    }
                                                                });
                                                            });
    </script>


    <!-- Modal For Add New Other EMP -->



    <div class="modal fade" id="modelTQedit" role="dialog">
        <div class="modal-dialog modal-lg">
            <form name="othrfrmID" id="othrfrmID" method="post" action="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="empnameUPD" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <!-- <h4 class="modal-title">Add New Other Employee</h4> -->
                        
                    </div>
                    <div class="modal-body">
                        <div class="row">
                        <div class="form-group required col-md-4">
                            <label class="control-label" for="usr">Position </label>
                            <select required="" name="designation_id" id="designation_idUPD" class="form-control" >
                                <option value=""> Select </option>
                                <?php foreach ($resulstArr as $rows) { ?>
                                    <option value="<?= $rows['designation_id']; ?>"><?= $rows['designation_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group required col-md-4">
                            <label class="control-label" for="usr">Email </label>
                            <input type="email" name="emailaddress" required id="emailaddressUPD"  class="form-control" >
                        </div>
                        <div class="form-group required col-md-4">
                            <label class="control-label" for="usr">Contact No </label>
                            <input type="number" min="1" name="contactnumber" required class="form-control" id="contactnumberUPD">
                        </div>
                        <div class="form-group required col-md-4">
                            <label class="control-label" for="usr">Marks </label>
                            <input type="text" required class="form-control" name="marks" id="marksUPD">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label" for="usr"> Firm </label>
                            <input type="text" class="form-control" name="firm" id="firmUPD">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label" for="usr"> Total MM </label>
                            <input type="text" class="form-control" name="man_months" id="man_monthsUPD">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label" for="usr">Age Limit </label>
                            <input type="number" min="1" class="form-control" name="age_limit" id="age_limitUPD">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label" for="usr">Certifcate </label>
                            <select class="form-control" name="certificate" id="certificateUPD">
                                <option value="">Select</option>
                                <option value="1"> Yes </option>
                                <option value="2"> No </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label" for="usr">Undertaking </label>
                            <select class="form-control" name="undertaking" id="undertakingUPD">
                                <option value="">Select</option>
                                <option value="1"> Yes </option>
                                <option value="2"> No </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label" for="usr">Salary </label>
                            <input type="text" class="form-control" name="salary" id="salaryUPD">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label" for="usr">Remarks (CRU) </label>
                            <input type="text" class="form-control" name="remarks_cru" id="remarks_cruUPD">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label" for="usr">Remarks (BD) </label>
                            <input type="text" class="form-control" name="remarks_bd" id="remarks_bdUPD" >
                        </div>

                        <div class="form-group col-md-12">
                            <br>
                            <input type="hidden" id="project_idUPD" name="project_id" value="" />
                            <input type="hidden" id="editid" name="editid" value="" />
                            <a href="javascript:void(0)"  class="btn btn-info pull-right" onclick="teamEdit()">Update & Save </a>
                        </div>

                    </div>
                         
                    </div>
                </div>
            </form>
        </div>
    </div>



    <!-- Modal  Edit Team Req Mode Pop UP-->
    <div class="modal fade" id="myModalotherEmp" role="dialog">
        <div class="modal-dialog">
            <form name="othrfrm" id="othrfrm" method="post" action="">
                <div class="modal-content">
                    <div class="modal-header">
                          <h4 class="modal-title">Edit / Update Team ReQ</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      
                    </div>
                    <div class="modal-body">
                        <div class="form-group required">
                            <label class="control-label" for="usr">Name </label>
                            <input type="text" required id="oname" name="oname" class="form-control" >
                        </div>
                        <div class="form-group required">
                            <label class="control-label" for="usr">Email </label>
                            <input type="email" required id="oemail" name="oemail" class="form-control" >
                        </div>
                        <div class="form-group required">
                            <label class="control-label" for="usr">Contact No </label>
                            <input type="number" min="1" required id="ocontact" name="ocontact" class="form-control" >
                        </div>
                        <div class="form-group required">
                            <input type="hidden" name="project_id" value ="<?= $resulstArr[0]['project_id']; ?>" />
                            <a href="javascript:void(0)" class="btn btn-info pull-right" onclick="saveTeamReq()">Add</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal  Cand Resume Upload Mode Pop UP-->





    <div class="modal fade" id="modeUpdResume" role="dialog">
        <div class="modal-dialog">
            <form name="othrfrmres" id="othrfrmres" method="post" enctype="multipart/form-data" action="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="emplnname" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <h4 id="uploaderror2" style="color:red" class="modal-title"></h4>
                    <div class="modal-body">
                        <div class="form-group required">
                            <label class="control-label" for="usr">Upload File (<small> .pdf .doc </small>) </label>
                            <input type="file" required id="resume" name="resume" class="form-control" >
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="usr">Extra Contact No </label>
                            <input type="number" maxlength="12" min="1" required id="ocontact" name="ocontact" class="form-control" >
                        </div>
                        <div class="form-group required">
                            <input type="hidden" name="project_id" id="projid_resume" value ="<?= $resulstArr[0]['project_id']; ?>" />
                            <input type="hidden" name="act_id" id="actionid" value ="" />
                            <a href="javascript:void(0)"  class="btn btn-primary pull-right" onclick="uploadFile()">Upload & Save</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


</body>
<style>
    .form-group.required .control-label:after {
        content:"*";
        color:red;
    }
</style>

<link href="<?= FRONTASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
<script src="<?= FRONTASSETS; ?>fm.selectator.jquery.js"></script>


<script>
                                                            $('#empid').selectator({
                                                                showAllOptionsOnFocus: true,
                                                                searchFields: 'value text subtitle right'
                                                            });


                                                            function setnewadd(designame, designid, fldid) {
                                                                $('#designation_name').val(designame);
                                                                $('#designation_id').val(designid);
                                                                $('#fld_id').val(fldid);
                                                                $('#formsection').show();
                                                            }
</script>

<style>
    table{
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-size: 14px;
    }
</style>
<script>

    $(document).ready(function () {
        $('#table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [0, ':visible']
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [0, 1, 2, 5]
                    }
                },
                'colvis'
            ]
        });
    });






    
  


     function saveTeamReq() {
           
            var data = $("#othrfrm").serialize();
            $.ajax({
                type: 'POST',
                url: "<?= base_url('cru/saveotherempdata'); ?>",
                data: data,
                dataType: "json",
                success: function(responData) {
                if(responData.status == 0){
                    toastr.success(responData.msg , 'Message', {timeOut: 5000});
                    $('#myModalotherEmp').modal('toggle');
                }else{
                    toastr.error(responData.msg , 'Message', {timeOut: 5000});
                    
                }
                   

                },
            });
        }

 function addDetail() {
           
            var data = $("#frmm").serialize();
            $.ajax({
                type: 'POST',
                url: "<?= base_url('cru/save_cruteam_req'); ?>",
                data: data,
                dataType: "json",
                success: function(responData) {
                if(responData.status == 0){
                    toastr.success(responData.msg , 'Message', {timeOut: 5000});
                    $('#formsection').hide();
                }else{
                    toastr.error(responData.msg , 'Message', {timeOut: 5000});
                    
                }
                   

                },
            });
        }


        function deletePosition(tassignmemberid,tassignid,bdproj_id) {
           
            var data = $("#frmm").serialize();
            $.ajax({
                type: 'POST',
                url: "<?= base_url('cru/delpositionby_cru'); ?>",
                data: { tassignmemberid: tassignmemberid, tassignid : tassignid, bdproj_id : bdproj_id},
                dataType: "json",
                success: function(responData) {
                if(responData.status == 0){
                    toastr.success(responData.msg , 'Message', {timeOut: 5000});
                    $('#formsection').hide();
                }else{
                    toastr.error(responData.msg , 'Message', {timeOut: 5000});
                    
                }
                   

                },
            });
        }

 
 
    
    
   

      function teamEdit() {
           
            var data = $("#othrfrmID").serialize();
            $.ajax({
                type: 'POST',
                url: "<?= base_url('cru/updaterqdata'); ?>",
                data: data,
                dataType: "json",
                success: function(responData) {
                if(responData.status == 0){
                    toastr.success(responData.msg , 'Message', {timeOut: 5000});
                    $('#modelTQedit').modal('toggle');
                }else{
                    toastr.error(responData.msg , 'Message', {timeOut: 5000});
                    
                }
                   

                },
            });
        }

        function uploadFile() {
           
           // var data = $("#othrfrmres").serialize();
            var form = $('#othrfrmres')[0];
            var data = new FormData(form);
            $.ajax({
                type: 'POST',
                url: "<?= base_url('cru/saveupld_resume'); ?>",
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                success: function(responData) {
                if(responData.status == 0){
                    toastr.success(responData.msg , 'Message', {timeOut: 5000});
                    $('#modeUpdResume').modal('toggle');
                }else{
                    toastr.error(responData.msg , 'Message', {timeOut: 5000});
                    
                }
                   

                },
            });
        }

  
         
  
</script>