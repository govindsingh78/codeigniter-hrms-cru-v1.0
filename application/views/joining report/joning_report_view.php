<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="accordion" id="accordion">
                                    <div>
									<form method="post" action="<?= base_url('joining_report_data'); ?>" id="visitingcard_form">
                                        <!--<div class="card-header" id="headingOne">-->
                                            <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>From Date</b>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                                                            </div>
                                                            <input type="date" name="from_date" class="form-control date" value="<?= @$from_date;?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>To Date</b>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                                                            </div>
                                                            <input type="date" name="to_date" class="form-control date" value="<?= @$to_date;?>">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-1 col-md-6">
                                                        <div class="mb-2">
                                                            <b></b>
                                                            <input type="submit" style="margin-top: 20px;" id="" value="Filter" name="filter" class="btn btn-round btn-primary">
                                                        </div>
                                                    </div>
													<div class="col-lg-1 col-md-6">
                                                        
														<div class="mb-2">
                                                            <b></b>
                                                            <input type="reset" style="margin-top: 20px;" id="" value="Reset" name="submit" class="btn btn-round btn-primary">
                                                        </div>
                                                    </div>
													
													
                                                </div>
											<h5 class="mb-0">
                                                
                                                
                                            </h5>
                                        <!--</div> -->                               
                                        
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Name</th>
                                                <th>Emp code</th>
                                                <th>DOJ</th>
                                                <th>Designation</th>
                                                <th>grade</th>
                                                <th>Department</th>
                                                <th>Sub Dpt</th>
                                                <th>Proj. Name</th>
                                                <th>Comp. Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if ($joiningDetailArr) {
                                                foreach ($joiningDetailArr as $kEy => $data) { ?>
                                                    <tr style="">
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($data->userfullname) ? $data->userfullname : ""; ?></td>
                                                        <td><?= ($data->employeeId) ? $data->employeeId : ""; ?></td>
                                                        <td><?= ($data->date_of_joining) ? $data->date_of_joining : ""; ?></td>
                                                        <td><?= ($data->position_name) ? $data->position_name : ""; ?></td>
                                                        <td><?= ($data->jobtitle_name) ? $data->jobtitle_name : ""; ?></td>
                                                        <td><?= ($data->department_name) ? $data->department_name : ""; ?></td>
                                                        <td><?= ($data->subdepartment) ? $data->subdepartment : ""; ?></td>
                                                        <td><?= ($data->project_name) ? $data->project_name : ""; ?></td>
                                                        <td><?= ($data->company_name) ? $data->company_name : ""; ?></td>
                                                    <?php
											}
											}
											
											else {  ?>
                                                <tr>
                                                    <td style="color:red;text-align:center;" colspan="9"> Record Not Found(Connect Live DB). </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Emp code</th>
                                                <th>Name</th>
                                                <th>DOJ</th>
                                                <th>Designation</th>
                                                <th>grade</th>
                                                <th>Department</th>
                                                <th>Sub Dpt</th>
                                                <th>Proj. Name</th>
                                                <th>Comp. Name</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
	
<script>
    function approvevisiting(approveid) {
		// alert(approveid)
            window.location = "<?= base_url("update_visiting_card_rqst"); ?>/" + approveid;
    }
	 function rejectvisiting(rejectid) {
		// alert(rejectid)
            window.location = "<?= base_url("update_visiting_card_rqst_reject"); ?>/" + rejectid;
    }
	function approvevisitinghr(approveid) {
		// alert(approveid)
            window.location = "<?= base_url("update_visiting_card_rqst_hr"); ?>/" + approveid;
    }
	 function rejectvisitinghr(rejectid) {
		// alert(rejectid)
            window.location = "<?= base_url("update_visiting_card_rqst_reject"); ?>/" + rejectid;
    }
	
	
</script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title">Preview Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		
      </div>
      <div class="modal-body">
       <form method="post" action="<?= base_url('reissue_id_card'); ?>" id="visitingcard_form"> 
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Apply Date</b>
															<input readonly="readonly" name="date_of_initiation" type="text" value="<?php echo date("d-m-Y");?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Name</b>
                                                            <input readonly="readonly" name="userfullname" type="text" value="<?= (@$idcardDetailArr->userfullname) ? $idcardDetailArr->userfullname : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Deparatment</b>
                                                            <input readonly="readonly" type="text" step="any" value="<?= (@$idcardDetailArr->department_name) ? $idcardDetailArr->department_name : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Designation</b>
                                                            <input readonly="readonly" name="position_name" type="text" step="any" value="<?= (@$idcardDetailArr->position_name) ? $idcardDetailArr->position_name : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <b>Company</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="emailaddress" type="text" step="any" value="<?= (@$idcardDetailArr->company_name) ? $idcardDetailArr->company_name : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
													 <div class="col-lg-4 col-md-6">
                                                        <b>Date of Joining</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="joining_date" type="text" step="any" value="<?= (@$idcardDetailArr->date_of_joining) ? $idcardDetailArr->date_of_joining : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <b>Reason for Re-issue</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="reason_reissue" id="reason_reissue" type="text" step="any" value="" id="" class="form-control">
                                                        </div>
                                                    </div>
													<div class="col-lg-4 col-md-6">
                                                        <div class="mb-4">
                                                            <b></b>
                                                            <input  type="submit" name="submit" class="btn btn-round btn-primary">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</body>
<script>
$('#modal_click').on('click', function (e) {
	
    var value = $('#select_reason').val();
    $('#reason_reissue').val(value);
});
</script>