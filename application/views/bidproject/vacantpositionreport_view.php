<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>

        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->

        <link href="<?= FRONTASSETS; ?>css/jquery.multiselect.css" rel="stylesheet"></link>
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                <!-- content starts -->
                <div>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?= base_url(); ?>">Home</a>
                        </li>
                        <li>
                            <a href="#"><?= $title; ?> </a>
                        </li>
                    </ul>


                </div>
                <div class="row">
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                        </div>
                    <?php } ?>

                    <!-- open Phase Section-->
                    <div class="box-inner">
                        <div class="box-content">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <form id="form-filter" class="form-horizontal">
                                        <div class="row">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12" for="selectError2"> Project : </label>
                                                <div class="col-sm-12">
                                                    <select id="projarr" name="projarr" class="form-control col-md-4 col-xs-12">
                                                        <?php
                                                        if ($AllprojArr):
                                                            foreach ($AllprojArr as $rowr) {
                                                                ?>
                                                                <option value="<?= $rowr->id; ?>"> <?= $rowr->project_name; ?></option>
                                                            <?php } endif; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-2" >
                                                <label for="Company Nature" class="col-sm-12"> Type : </label>
                                                <div class="col-sm-12">
                                                    <select id="company_name_nature" name="company_name_nature" class="form-control">
                                                        <option value="" > -- Select -- </option>
                                                        <option value="tbn" > TBN </option>
                                                        <option value="all" > ALL </option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3" style="margin: 1%;">
                                                <div class="col-sm-12">
                                                    <button type="button" id="btn-filter" class="btn btn-primary">Filter</button> &nbsp;&nbsp;&nbsp;
                                                    <button type="button" id="btn-reset" class="btn btn-primary">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="colvis"></div>
                                        </div>
                                    </div>


                                    <table id="table" class="display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Position</th>
                                                <th>Position Type</th>
                                                <th style="width:10px">Emp Name</th>
                                                <th>Balance MM</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Position</th>
                                                <th>Position Type</th>
                                                <th style="width:10px">Emp Name</th>
                                                <th>Balance MM</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                        <hr>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Close phase Section-->
            </div>

        </div>	

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>
        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
        <?php $this->load->view('include/datatablejs'); ?>


        <script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
            .ms-options >ul >li{list-style-type:none;}
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $("li#company").addClass('active');
            });



            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    "ajax": {
                        "url": "<?php echo site_url('company/companyListAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.sectorinput = $('#sectorinput').val();
                            data.company_name_nature = $('#company_name_nature').val();
                            data.country_name = $('#country_name').val();
                            data.opcountry_name = $('#opcountry_name').val();
                            console.log(data);
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}],
                    "columnDefs": [{"targets": [0], "orderable": false,
                        },
                    ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });

                var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });
            });
        </script>

    </div>
</body>
</html>
