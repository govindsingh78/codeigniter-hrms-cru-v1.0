<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
    #table_length,
    #table1_length,
    #table2_length,
    #table3_length {
        position: absolute;
        margin-left: 100px !important;
    }

    .table tbody tr td,
    .table tbody th td {
        vertical-align: middle;
        white-space: normal !important;
    }

    ul.nav.nav-tabs-new {
        float: right !important;
        margin: auto;
    }
</style>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Body Part of This page...-->

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                        <div class="header">
                        
                            <div class="alert alert-success"><?php echo $dataArr[0]->generated_tenderid; ?></div>
                             
                        </div>

                                <!-- <div id="home" class="tab-pane fade in active"> -->
                                <div class="body pt-0 mt-0">
                                 
                                   
                                            <?php echo $dataArr[0]->TenderDetails; ?>
 
                                            

                                        
                                    </div>


                                    <div class="footer col-md-12 pb-5 mb-4">

                                                <a href="<?= base_url('bidproject/tenderteam_view?pn=' . $dataArr[0]->fld_id); ?>" class="btn btn-info pull-right ml-2" target="_blank"><i class="fa fa-users"></i>&nbsp;&nbsp;Tender Team</a>
                                                <a href="<?= base_url('bidproject/negotiationteam_view?pn=' . $dataArr[0]->fld_id); ?>" class="btn btn-success ml-2 pull-right" target="_blank"><i class="fa fa-users"></i>&nbsp;&nbsp;Negotiation Team</a>
                                                <a href="<?= base_url('bidproject/contractteam_view?pn=' . $dataArr[0]->fld_id); ?>" class="btn btn-warning ml-2 pull-right" target="_blank"><i class="fa fa-users"></i>&nbsp;&nbsp;After Contract Team</a>


                                    </div>

                                <!-- </div> -->
                           </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php $this->load->view('admin/includes/footer'); ?>
        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
            #table1_wrapper {
                width: 94em;
                /*overflow-x: auto;*/
                white-space: nowrap;
            }
        </style>
    
<script type="text/javascript">
    $(document).ready(function () {
        $("li#bidprojects").addClass('active');
    });
</script>