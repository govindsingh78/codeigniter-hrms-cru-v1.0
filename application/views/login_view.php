<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Login : C-Link </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?= FRONTASSETS; ?>login/fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?= FRONTASSETS; ?>login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" type="text/css" href="<?= FRONTASSETS; ?>login/fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?= FRONTASSETS; ?>login/css/util.css">
        <link rel="stylesheet" type="text/css" href="<?= FRONTASSETS; ?>login/css/main.css">
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100" style="background-image: url('<?= FRONTASSETS; ?>img/bg-01.jpg');">
                <div class="wrap-login100">
                    <?php if ($this->session->flashdata('errormsg')) { ?>
                        <div class="alert alert-danger" style="text-align:center;">
                            <?php echo '<strong>Error! </strong>' . $this->session->flashdata('errormsg'); ?></span>
                        </div>
                    <?php } ?>
                    <form class="login100-form validate-form" action="<?= base_url('welcome/login'); ?>" method="post">
                            <!--<span class="login100-form-logo">
                                    <i class="zmdi zmdi-landscape"></i>
                            </span>
                        -->
                        <img src="<?php echo HOSTNAME . "assets/bdlogo.png"; ?>" height="110" style="margin-bottom: 26px;">

                        <div class="wrap-input100 validate-input" data-validate = "Enter username">
                            <input class="input100" type="text" required name="userID" placeholder="Username">
                            <span class="focus-input100" data-placeholder="&#xf207;"></span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <input class="input100" type="password" required name="userPWD" placeholder="Password">
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        </div>

                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn" type="submit">
                                Login
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
