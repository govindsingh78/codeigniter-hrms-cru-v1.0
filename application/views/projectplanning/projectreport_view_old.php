<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <!--<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.css" rel="stylesheet" />-->
            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
            <link href="<?= FRONTASSETS; ?>css/jquery.multiselect.css" rel="stylesheet"></link>
            <div id="content" class="col-lg-10 col-sm-10">
                <!-- content starts -->
                <div>
                    <ul class="breadcrumb">
                        <li> <a href="<?= base_url(); ?>">Home</a></li>
                        <li><a href="#"><?= $title; ?></a></li>
                    </ul>
                </div>
                <div class="row">
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable" >
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                        </div>
                    <?php } ?>

                    <form id="form-filter" class="form-horizontal">
                        <div class="box col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">


                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="email"> Select Employee </label>
                                            <select id="emp_id" name="emp_id" class="form-control col-md-4 col-xs-12 chosen-select" >
                                                <option value=""> -- Select Employee -- </option>
                                                <?php
                                                if ($employee):
                                                    foreach ($employee as $value) {
                                                        ?>
                                                        <option value="<?= $value->id; ?>"><?= $value->userfullname; ?></option>
                                                    <?php } endif; ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-2">
                                            <label class="email"> Operator </label>
                                            <select id="emp_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Email </label>
                                            <input placeholder="Email" type="text" id="email_id" name="email_id" value="" class="form-control"/>
                                        </div>

                                        <div class="col-sm-2" >
                                            <label class="email"> Operator </label>
                                            <select id="email_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="row">	
                                        <div class="col-sm-3">
                                            <label class="email"> Select Designation </label>
                                            <select id="designation_id" name="designation_id[]" multiple class="form-control col-md-4 col-xs-12">
                                                <!--<option value=""> -- Select Designation -- </option>-->
                                                <?php
                                                if ($designation):
                                                    foreach ($designation as $rowr) {
                                                        ?>
                                                        <option value="<?= $rowr->fld_id; ?>"> <?= $rowr->designation_name; ?></option>
                                                    <?php } endif; ?>
                                            </select>

                                        </div>
                                        <div class="col-sm-2" >
                                            <label class="email"> Operator </label>
                                            <select id="designation_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>

                                        </div>
                                        <div class="col-sm-3" >
                                            <label class="email"> Mobile No. </label>
                                            <input placeholder="Mobile" type="text" id="contact_no" name="contact_no" value="" class="form-control"/>
                                        </div>
                                        <div class="col-sm-2" >
                                            <label class="email"> Operator </label>
                                            <select id="contact_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>

                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">	
                                        <div class="col-sm-3">
                                            <label class="email"> Total MM </label>
                                            <input placeholder="Total MM" type="text" class="form-control" name="max_mm" id="max_mm" />
                                        </div>

                                        <div class="col-sm-2" >
                                            <label class="email"> Operator </label>
                                            <select id="balance_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>

                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Balance MM </label>
                                            <input placeholder="Balance MM" type="text" id="balance_mm" name="balance_mm" value="" class="form-control"/>
                                        </div>

                                        <div class="col-sm-2" >
                                            <label class="email"> Operator </label>
                                            <select id="project_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>
                                        </div>	
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="email"> Project </label>
                                            <select name="projectinput" id="projectinput"  class="form-control">
                                                <option value="">--select Project--</option>
                                                <?php
                                                if ($tmProject):
                                                    foreach ($tmProject as $value) {
                                                        ?>
                                                        <option value="<?= $value->id; ?>"><?= $value->project_name; ?></option>
                                                    <?php } endif; ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-7"> <br>
                                            <button type="button" id="btn-filter" class="btn btn-primary"> Filter </button> &nbsp;
                                            <button type="button" id="btn-reset" class="btn btn-default"> Reset </button> &nbsp;
    <!--                                        <a class="btn btn-primary" href="<?= base_url('projectplanning/mmupdateemp'); ?>">Update Data </a>-->
                                            <?= isset($list[0]->inventrydate) ? date('j F Y', strtotime($list[0]->inventrydate)) : ''; ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>


                <!-- open Phase Section-->
                <div class="box-inner">
                    <div class="box-content">
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="colvis"></div>
                                    </div>
                                </div>
                                <table id="table" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Sr. No</th>
                                            <th>Project Name</th>
                                            <th>Employee Name</th>
                                            <th>Email ID</th>
                                            <th>Mobile Number</th>
                                            <th>Designation</th>
                                            <th>Marks</th>
                                            <th>Firm</th>
                                            <th>Total MM</th>
                                            <th>Balance MM</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Sr. No</th>
                                            <th>Project Name</th>
                                            <th>Employee Name</th>
                                            <th>Email ID</th>
                                            <th>Mobile Number</th>
                                            <th>Designation</th>
                                            <th>Marks</th>
                                            <th>Firm</th>
                                            <th>Total MM</th>
                                            <th>Balance MM</th>
                                        </tr>
                                    </tfoot>
                                    <hr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Close phase Section-->
        </div>

    </div>	
    <div class="row">
        <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
        </div>
    </div>
    <hr>
    <!-- BootStrap Model For Comments.. -->
    <?php $this->load->view('include/commenthtml'); ?>
    <!-- close Model -->
    <?php $this->load->view('include/footer'); ?>
    <?php $this->load->view('include/datatablejs'); ?>
    <style>
        #table_length{margin-left:20px;}
        #table_filter{margin-right:2%;} 
        #chatbox {padding: 15px;overflow: scroll;height: 300px;}
        .ms-options >ul >li{list-style-type:none;}
    </style>
            <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.js"></script>-->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
    <link href="<?= FRONTASSETS; ?>bower_components/chosen/chosen.css" rel="stylesheet"></link>
    <script src="<?= FRONTASSETS; ?>bower_components/chosen/chosen.jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("li#submitted_search").addClass('active');
            $("li#bidprojects").addClass('active');
        });
        function assignpm(projid) {
            $("#assigntenderid").val(projid);
            $("#projectstatus").val(projid);
        }

        $("#emp_id").chosen();
        $("#projectinput").chosen();
        $('#designation_id').multiselect({
            columns: 1,
            placeholder: 'Select Designation',
            search: true
        });

        var table;
        $(document).ready(function () {
            //datatables
            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo site_url('projectplanning/projectreportData') ?>",
                    "type": "POST",
                    "data": function (data) {
                        data.projectinput = $('#projectinput').val();
                        data.max_mm = $('#max_mm').val();
                        data.balance_mm = $('#balance_mm').val();
                        data.emp_id = $('#emp_id').val();
                        data.emp_cond = $('#emp_cond').val();
                        data.email_id = $('#email_id').val();
                        data.email_cond = $('#email_cond').val();
                        data.designation_id = $('#designation_id').val();
                        data.designation_cond = $('#designation_cond').val();
                        data.contact_no = $('#contact_no').val();
                        data.contact_cond = $('#contact_cond').val();
                        data.balance_cond = $('#balance_cond').val();
                        data.project_cond = $('#project_cond').val();
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}
                ],
                "columnDefs": [{"targets": [0], "orderable": false, }, ],
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            });
            var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
            $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
            $('#btn-filter').click(function () { //button filter event click
                table.ajax.reload();  //just reload table
            });
            $('#btn-reset').click(function () { //button reset event click
                $('#form-filter')[0].reset();
                table.ajax.reload();  //just reload table
            });
        });
    </script>
</div>
</body>
</html>
