<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
#table_length {
    position: absolute;
    margin-left: 100px !important;
}
.table tbody tr td, .table tbody th td {
    vertical-align: middle;
    white-space: normal !important;
}
</style>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">

                        <?php if ($this->session->flashdata('msg')) { ?>

                            <!-- <script>
                            toastr.success(<?//= //$this->session->flashdata('msg'); ?> , 'Message', {timeOut: 5000});
                            </script> -->
                            <div class="alert alert-success alert-dismissable" style="">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>
                                    <div id="msgs"> Success ! 
                                </strong> <?= $this->session->flashdata('msg'); ?></div>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('error_msg')) { ?>
                            <div class="alert alert-danger alert-dismissable" style="">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>
                                    <div id="msgs"> Error !
                                </strong> <?= $this->session->flashdata('error_msg'); ?> </div>
                            </div>
                        <?php } ?>


                        <div class="card">
                            <div class="header">
                                <form id="form-filter" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="email"> Business Unit : </label>
                                    <select id="bussn_unit" name="bussn_unit" class="form-control" >
                                        <option value="1"> CEG </option>
                                        <option value="2"> CEGTH </option>
                                        <option value="3"> CEG Project </option>
                                    </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Education Level : </label>
                                            <select id="education_level" name="education_level" class="form-control" >
                                                <option value=""> -- Select -- </option>
                                                <option value="3"> Graduate </option>
                                                <option value="4"> Post Graduate </option>
                                                <option value="5"> Doctorate </option>
                                                <option value="6"> Diploma </option>
                                                <option value="7"> Post Doctorate </option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Employee Name : </label>
                                            <input type="text" name="empl_name" id="empl_name" value="" class="form-control">
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Specialization : </label>
                                            <input type="text" name="specialization" id="specialization" value="" class="form-control">
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Tags : </label>
                                            <input type="text" name="rectags" id="rectags" value="" class="form-control">
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> &nbsp; </label> <br>
                                            <button type="button" id="btn-filter" class="btn btn-primary pull-right ml-2"> Filter </button> &nbsp;&nbsp;&nbsp;
                                            <button type="button" id="btn-reset" class="btn btn-default pull-right"> Reset </button>
                                        </div>
                                    </div>
                                </form>


                            </div>



                            






                            <div class="row">
                                
                            </div>


                            <div class="row clearfix">
                                <div class="col-lg-12">
                    <div class="card">
                        
                        <div class="body">
						
                        <div class="table-responsive"> 
                                   
                        <div class="col-md-12">
                                    <div id="colvis"></div>
                                    <div class="form-group has-success pull-right"> 
                                        
                                    </div>
                                </div>
                                    
                               
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Sr. No</th>
                                    <th>EMPCODE</th>
                                    <th>Employee Name</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Exp. With CEG</th>
                                    <th>Other Exp.</th>
                                    <th>Educational Level</th>
                                    <th>Institution Name </th>
                                    <th>Course</th>
                                    <th>Location</th>
                                    <th>Specialization</th>
                                    <th>Pass Year</th>
                                    <th>Educational Level</th>
                                    <th>Institution Name </th>
                                    <th>Course</th>
                                    <th>Location</th>
                                    <th>Specialization</th>
                                    <th>Pass Year</th>
                                    <th>Educational Level</th>
                                    <th>Institution Name </th>
                                    <th>Course</th>
                                    <th>Location</th>
                                    <th>Specialization</th>
                                    <th>Pass Year</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        
                                        <th>Sr. No</th>
                                    <th>EMPCODE</th>
                                    <th>Employee Name</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Exp. With CEG</th>
                                    <th>Other Exp.</th>
                                    <th>Educational Level</th>
                                    <th>Institution Name </th>
                                    <th>Course</th>
                                    <th>Location</th>
                                    <th>Specialization</th>
                                    <th>Pass Year</th>
                                    <th>Educational Level</th>
                                    <th>Institution Name </th>
                                    <th>Course</th>
                                    <th>Location</th>
                                    <th>Specialization</th>
                                    <th>Pass Year</th>
                                    <th>Educational Level</th>
                                    <th>Institution Name </th>
                                    <th>Course</th>
                                    <th>Location</th>
                                    <th>Specialization</th>
                                    <th>Pass Year</th>
                                    </tr>
                                </tfoot>
                                <hr>
                            </table>
                                    
                                    
                                    
                                    
							</div>
                        </div>
                    </div>
                </div>
            </div>




                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('admin/includes/footer'); ?>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
    <link href="<?= FRONTASSETS; ?>bower_components/chosen/chosen.css" rel="stylesheet"></link>
    <script src="<?= FRONTASSETS; ?>bower_components/chosen/chosen.jquery.js"></script>


    
    </div>
   <style>
        #table_length {
            margin-left: 20px;
        }

        #table_filter {
            margin-right: 2%;
        }

        .designation_detail {
            display: none;
        }
    </style>

    <script type="text/javascript">
         $(function () {
            $("#tags input").on({
                focusout: function () {
                    var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig, ''); // allowed characters
                    if (txt)
                        $("<span/>", {text: txt.toLowerCase(), insertBefore: this});
                    this.value = "";
                },
                keyup: function (ev) {
                    if (/(188|13)/.test(ev.which))
                        $(this).focusout();
                }
            });
            $('#tags').on('click', 'span', function () {
                $(this).remove();
            });
        });
        function assignpm(projid) {
            $("#assigntenderid").val(projid);
            $("#projectstatus").val(projid);
        }
        //$("#education_level").chosen();
        var table;
        $(document).ready(function() {
            //datatables
            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [],
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('projectplanning/educationalreport_ajax') ?>",
                    "type": "POST",
                    "data": function(data) {
                        var tags = $('#tags span').map(function () {
                            return $(this).text();
                        });
                        tags = Array.prototype.join.call(tags, ",");
                        // console.log(tags);
                        data.education_level = $('#education_level').val();
                        data.empl_name = $('#empl_name').val();
                        data.bussn_unit = $('#bussn_unit').val();
                        data.specialization = $('#specialization').val();
                        data.tagsrec = tags;
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: ['copy', 'excel', 'csv', 'pdf', 'print']
                }],
                //Set column definition initialisation properties.
                "columnDefs": [{
                    "targets": [0],
                    "orderable": false,
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });

            var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
            $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
            $('#btn-filter').click(function() { //button filter event click
                table.ajax.reload(); //just reload table
            });
            $('#btn-reset').click(function() { //button reset event click
                $('#form-filter')[0].reset();
                table.ajax.reload(); //just reload table
            });
        });

        
    </script>
    <style>
        #tags{
            float:left;
            border:1px solid #ccc;
            padding:5px;
            font-family:Arial;
        }
        #tags > span{
            cursor:pointer;
            display:block;
            float:left;
            color:#fff;
            background:#789;
            padding:5px;
            padding-right:25px;
            margin:4px;
        }
        #tags > span:hover{
            opacity:0.7;
        }
        #tags > span:after{
            position:absolute;
            content:"×";
            border:1px solid;
            padding:2px 5px;
            margin-left:3px;
            font-size:11px;
        }
        #tags > input{
            background:#eee;
            border:0;
            margin:4px;
            padding:7px;
            width:auto;
        }
    </style>
</body>