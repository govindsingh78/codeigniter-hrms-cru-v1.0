<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
			<?php $this->load->view('include/sidebar'); ?>
			
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.css" />
            <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li><a href="#"><?= $title; ?></a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable" >
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>
                        


                        <!-- open Phase Section-->
                        <div class="box-inner">
							
							<div class="box-content">
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
									<form name="frm1" method="post" action="<?= base_url('projectplanning/viewteamproject'); ?>">
                                        <div class="row">
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Project:</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <select onchange="this.form.submit()" required name="project_id" id="project_id"  class="form-control selectpicker" data-live-search="true">
                                                        <option Selected value="">--All--</option>
                                                        <?php
                                                        if ($tmProject):
                                                            foreach ($tmProject as $value) {
                                                                ?>
                                                                <option <?= ($id == $value->id) ? 'selected' : ''; ?> value="<?= $value->id; ?>"><?= $value->project_name; ?></option>
                                                            <?php } endif; ?>
                                                    </select>
                                                </div>
                                            </div>
										</div>
									</form>
										<br/><br/>
										<?php echo $userdata['project'][0]->TenderDetails; ?>
										<br/><br/>
										<?php //echo '<pre>'; print_r($userdata); ?>
										<form name="frm1" method="post" action="<?= base_url('projectplanning/saveviewteamproject'); ?>">
										<?php 
											if(!empty($userdata)){
											?>
											<table>
												<tr>
													<th>Designation</th>
													<th>Employee Name</th>
													<th>Designation Change</th>
													<th>Category</th>
												</tr>
  

											
											<?php
											if(!empty($userdata['users'])){
												foreach($userdata['users'] as $val){
											?>
												<tr>
													<td>
														<input type="text" value="<?= ($val->userfullname) ? $val->userfullname : '' ?>" id="userfullname" class="form-control"/>
														<input type="hidden" value="<?= ($val->id) ? $val->id : '' ?>" id="user_id" name="user_id[]" class="form-control"/>
													</td>
													<td>
														<input type="text" id="designation" value="<?= ($val->PositionName) ? $val->PositionName : '' ?>" class="form-control"/>
													</td>
													<td>
														<select id="designation_id" name="designation_id[]" class="form-control selectpicker" data-live-search="true">
															<option value="">Select Designation</option>
															<?php 
																foreach($userdata['designation'] as $vals){
															?>		
																<option <?= ($val->designation_id == $vals->fld_id) ? 'selected' : ''; ?> value="<?= $vals->fld_id; ?>"><?= $vals->designation_name .'('.$vals->sector_name.')'; ?></option>	
																	
															<?php 
																}
															?>
														</select>
													</td>
													<td>
														<select id="cat_id" name="cat_id[]" class="form-control selectpicker" data-live-search="true">
															<option value="">Select category</option>
															<option <?= ($val->key_id == 1) ? 'selected' : ''; ?> value="1">Key Professional</option>
															<option <?= ($val->key_id == 2) ? 'selected' : ''; ?> value="2">Sub Professional</option>
															<option <?= ($val->key_id == 3) ? 'selected' : ''; ?> value="3">Administration Staff</option>
														</select>
													</td>
												</tr>
											
										<?php 
												}	
											}
										?>
										</table>
										<br/><br/>
										<div class="row">
											<div class="col-sm-5">
                                                <div class="col-sm-10">
													<input type="hidden" value="<?= $id ?>" name="project_id"/>
													<input type="submit" value="Submit" class="btn btn-primary"/>
                                                </div>
                                            </div>
											
										</div>
										
										<?php 
											} 
										?>
										</form>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                    </div>
                    <!--Close phase Section-->

                </div>
          
        </div>	
        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>

        


        



        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
		<?php $this->load->view('include/datatablejs'); ?>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.js"  type="text/JavaScript" />
		
		<style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
			
			table {
				font-family: arial, sans-serif;
				border-collapse: collapse;
				width: 100%;
			}

			td, th {
				border: 1px solid #dddddd;
				text-align: left;
				padding: 8px;
			}

			tr:nth-child(even) {
				background-color: #dddddd;
			}
        </style>
		<script>
			$('document').ready(function(){
				$('#emp_id').on("change",function(){
					var userID = $(this).val();
					$.ajax({
						"url": "<?php echo site_url('projectplanning/getuserdetailByID') ?>",
                        "type": "POST",
						"data":{'id':userID},
						success:function(res){
							var result = JSON.parse(res);
							console.log(result[0]);
							$('#email_id').val(result[0].emailaddress);
							$('#contact_no').val(result[0].contactnumber);
						}
					});
				});
			});
			
			/* $('document').ready(function(){
				$('#project_id').on("change",function(){
					var projectId = $(this).val();
					$.ajax({
						"url": "<?php echo site_url('projectplanning/getuserdetailByprojectID') ?>",
                        "type": "POST",
						"data":{'projectId':projectId},
						success:function(res){
							var result = JSON.parse(res);
							console.log(result);
							$.each(result,function(index,val){
								//console.log(index + ' ' + val.userfullname);	
								
							});
						}
					});
				});
			}); */
		</script>
    </div>
</body>
</html>
