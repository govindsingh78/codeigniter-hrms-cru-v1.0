<html lang="en">
    <?php
    error_reporting(E_ALL); //error_reporting(0);
    $statusArr = array('won' => '1', 'loose' => '2', 'awaiting' => '0', 'cancel' => '3');
    ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <?php $this->load->view('include/tabbar'); ?>
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                <div>
                    <ul class="breadcrumb">
                        <li> <a href="<?= base_url(); ?>">Home</a></li>
                        <li><a href="#">Project User Detail </a></li>
                    </ul>
                </div>
                <div class="row">
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                        </div>
                    <?php } ?>
                    <div class="box col-md-12">
                        <div class="panel panel-default">
                            <?php if (!empty($data)) { ?>
                                <div class="panel-body">
                                    <b> Project Name: <?= $projName[0]->TenderDetails; ?> </b>

                                    <div class="row">
                                        <div class="well well-sm"> <?= $data[0]->project_name; ?> </div>
                                        <div class="box">
                                            <div class="box-body">
                                                <table>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Position</th>
                                                        <th>Work Category</th>
                                                        <th>Total MM</th>
                                                        <th>Balance MM</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    <?php
                                                    foreach ($data as $val) {
                                                        if (!empty($val->userfullname)) {
                                                            $invoicecum = getCumulativemm($val->project_id, $val->designation_id);
                                                            if (!empty($invoicecum)) {
                                                                $totalcumulativemm = $invoicecum['totalcumulativemm'];
                                                                $balancemm = $val->man_months - $totalcumulativemm;
                                                            } else {
                                                                $currentdate = date('Y-m-d', strtotime($invoice[0]->invoice_date . "-1 month"));
                                                                $cruyears = date('Y', strtotime($currentdate));
                                                                $crumonths = date('n', strtotime($currentdate));
                                                                $manmonthscrukey = getuseattd($val->project_id, $val->empname, $crumonths, $cruyears);
                                                                $mmcountscrukey = 1 - ($manmonthscrukey->leave + $manmonthscrukey->absent) / 30;
                                                                $totalcumulativemm = $val->cumulative_pre_mm + $mmcountscrukey;
                                                                $balancemm = $val->man_months - $totalcumulativemm;
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td><a href="<?= base_url('/projectplanning/userprofile/' . $val->empname); ?>">
                                                                        <?= $val->userfullname; ?></a></td>
                                                                <td><?= $val->finaldesignation_name; ?></td>
                                                                <td><?= ($val->is_intermittent > 0) ? 'Intermittent' : 'Regular'; ?></td>
                                                                <td><?= ($val->man_months + $val->eot_mm); ?></td>
                                                                <td>
                                                                    <?php
                                                                    $MmToT = ($val->man_months + $val->eot_mm);
                                                                    echo ($MmToT - $val->totalcumulativemm);
                                                                    ?>
                                                                </td>
                                                                <td><b><a href="<?= base_url('/projectplanning/userview/' . $val->project_id . '/' . $val->empname); ?>">View</a></b></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } else { ?>	
                                <div class="panel-body"><p align="center">Detail Not Found</p> </div>
                            <?php } ?>
                        </div>
                    </div>

                </div> 
            </div>
        </div>
        <?php $this->load->view('include/commenthtml'); ?>
        <?php $this->load->view('include/footer'); ?>
        <?php $this->load->view('include/datatablejs'); ?>
        <link href="<?= FRONTASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
        <script src="<?= FRONTASSETS; ?>fm.selectator.jquery.js"></script> 
        <script src="<?= FRONTASSETS; ?>ckeditor/ckeditor.js"></script> 
        <script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
    </body>
</html>
<style>
    table{
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-size: 14px;
    }
</style>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>