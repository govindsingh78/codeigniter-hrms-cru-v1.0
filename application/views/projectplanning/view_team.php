<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                <!-- content starts -->
                <div>
                    <ul class="breadcrumb">
                        <li> <a href="<?= base_url(); ?>">Home</a></li>
                        <li><a href="#"><?= $title; ?></a></li>
                    </ul>
                </div>
                <div class="row">
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable" >
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                        </div>
                    <?php } ?>



                    <!-- open Phase Section-->
                    <div class="box-inner">

                        <div class="box-content">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <form name="frm1" method="post" action="<?= base_url('projectplanning/viewteam'); ?>">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Project:</label>
                                                <br/>
                                                <div class="col-sm-10">
                                                    <select onchange="this.form.submit()" required name="project_id" id="project_id"  class="form-control">
                                                        <option Selected value="">--All--</option>
                                                        <?php
                                                        if ($tmProject):
                                                            foreach ($tmProject as $value) {
                                                                ?>
                                                                <option <?= ($id == $value->id) ? 'selected' : ''; ?> value="<?= $value->id; ?>"><?= $value->project_name; ?></option>
                                                            <?php } endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <br/><br/>
                                    <form name="frm1" method="post" action="<?= base_url('projectplanning/savemultipleprojectteam'); ?>">
                                        <?php
                                        if (!empty($userdata)) {
                                            ?>
                                            <table>
                                                <tr>
                                                    <th>Designation</th>
                                                    <th>Employee Name</th>
                                                    <th>Email ID</th>
                                                    <th>Contact Number</th>
                                                    <th>Age Limit</th>
                                                    <th>Firm</th>
                                                    <th>Marks</th>
                                                    <th>Total MM</th>
                                                    <th>Used MM</th>
                                                </tr>



                                                <?php
                                                foreach ($userdata as $val) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <input type="text" id="designation_name" value="<?= ($val->position_name) ? $val->position_name : '' ?>" name="designation_name[]" class="form-control"/>
                                                            <input type="hidden" id="designation_id" value="<?= ($val->position_id) ? $val->position_id : '' ?>" name="designation_id[]" class="form-control"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="emp_name" value="<?= ($val->userfullname) ? $val->userfullname : '' ?>" name="emp_name[]" class="form-control"/>
                                                            <input type="hidden" id="emp_id" value="<?= ($val->id) ? $val->id : '' ?>" name="emp_id[]" class="form-control"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="email_id" value="<?= ($val->emailaddress) ? $val->emailaddress : '' ?>" name="email_id[]" class="form-control" />
                                                        </td>
                                                        <td>
                                                            <input type="number" id="contact_no" value="<?= ($val->contactnumber) ? $val->contactnumber : '' ?>" name="contact_no[]" class="form-control" />
                                                        </td>
                                                        <td>
                                                            <input type="number" id="age_limit" name="age_limit[]" class="form-control" value="65"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="firm" name="firm[]" class="form-control" value="CEG"/>
                                                        </td>
                                                        <td>
                                                            <input type="number" step="any" id="marks" name="marks[]" class="form-control"/>
                                                        </td>
                                                        <td>
                                                            <input type="number" step="any" id="total_mm" name="total_mm[]" class="form-control"/>
                                                        </td>
                                                        <td>
                                                            <input type="number" step="any" id="used_mm" name="used_mm[]" class="form-control"/>
                                                        </td>
                                                    </tr>
                                                    <!--<div class="row">
                                                            <div class="col-sm-5">
                                                                    <label class="col-sm-10 control-label" for="selectError2">Designation:</label>
                                                                    <br/>
                                                                    <div class="col-sm-10">
                                                                            <input type="text" id="designation_name" value="<?= ($val->position_name) ? $val->position_name : '' ?>" name="designation_name[]" class="form-control"/>
                                                                            <input type="hidden" id="designation_id" value="<?= ($val->position_id) ? $val->position_id : '' ?>" name="designation_id[]" class="form-control"/>
                                                                    </div>
                                                            </div>
                                                            
                                                            <div class="col-sm-5">
                                                                    <label class="col-sm-10 control-label" for="selectError2">Employee Name:</label>
                                                                    <br/>
                                                                    <div class="col-sm-10">
                                                                            <input type="text" id="emp_name" value="<?= ($val->userfullname) ? $val->userfullname : '' ?>" name="emp_name[]" class="form-control"/>
                                                                            <input type="hidden" id="emp_id" value="<?= ($val->id) ? $val->id : '' ?>" name="emp_id[]" class="form-control"/>
                                                                    </div>
                                                            </div>
                                                    
                                                            <div class="col-sm-5">
                                                                    <label class="col-sm-10 control-label" for="selectError2">Email ID:</label>
                                                                    <br/>
                                                                    <div class="col-sm-10">
                                                                            <input type="text" id="email_id" value="<?= ($val->emailaddress) ? $val->emailaddress : '' ?>" name="email_id[]" class="form-control" />
                                                                    </div>
                                                            </div>
                                                            
                                                            <div class="col-sm-5">
                                                                    <label class="col-sm-10 control-label" for="selectError2">Contact Number:</label>
                                                                    <br/>
                                                                    <div class="col-sm-10">
                                                                            <input type="number" id="contact_no" value="<?= ($val->contactnumber) ? $val->contactnumber : '' ?>" name="contact_no[]" class="form-control" />
                                                                    </div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                    <label class="col-sm-10 control-label" for="selectError2">Age Limit:</label>
                                                                    <br/>
                                                                    <div class="col-sm-10">
                                                                            <input type="number" id="age_limit" name="age_limit[]" class="form-control" value="65"/>
                                                                    </div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                    <label class="col-sm-10 control-label" for="selectError2">Firm:</label>
                                                                    <br/>
                                                                    <div class="col-sm-10">
                                                                            <input type="text" id="firm" name="firm[]" class="form-control" value="CEG"/>
                                                                    </div>
                                                            </div>
                                                            
                                                            <div class="col-sm-5">
                                                                    <label class="col-sm-10 control-label" for="selectError2">Marks</label>
                                                                    <br/>
                                                                    <div class="col-sm-10">
                                                                            <input type="number" step="any" id="marks" name="marks[]" class="form-control"/>
                                                                    </div>
                                                            </div>
                                                            
                                                            <div class="col-sm-5">
                                                                    <label class="col-sm-10 control-label" for="selectError2">Total MM:</label>
                                                                    <br/>
                                                                    <div class="col-sm-10">
                                                                            <input type="number" step="any" id="total_mm" name="total_mm[]" class="form-control"/>
                                                                    </div>
                                                            </div>
                                                            
                                                            <div class="col-sm-5">
                                                                    <label class="col-sm-10 control-label" for="selectError2">Used MM</label>
                                                                    <br/>
                                                                    <div class="col-sm-10">
                                                                            <input type="number" step="any" id="used_mm" name="used_mm[]" class="form-control"/>
                                                            
                                                                    </div>
                                                            </div>
                                                    </div>
                                                    <br/><br/>
                                                    -->
                                                    <?php
                                                }
                                                ?>
                                            </table>
                                            <br/><br/>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="col-sm-10">
                                                        <input type="hidden" value="<?= $id ?>" name="project_id"/>
                                                        <input type="submit" value="Submit" class="btn btn-primary"/>
                                                    </div>
                                                </div>

                                            </div>

    <?php
}
?>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!--Close phase Section-->

            </div>

        </div>	
        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>








        <!-- BootStrap Model For Comments.. -->
<?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
        <?php $this->load->view('include/datatablejs'); ?>
        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}

            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>
        <script>
            $('document').ready(function () {
                $('#emp_id').on("change", function () {
                    var userID = $(this).val();
                    $.ajax({
                        "url": "<?php echo site_url('projectplanning/getuserdetailByID') ?>",
                        "type": "POST",
                        "data": {'id': userID},
                        success: function (res) {
                            var result = JSON.parse(res);
                            console.log(result[0]);
                            $('#email_id').val(result[0].emailaddress);
                            $('#contact_no').val(result[0].contactnumber);
                        }
                    });
                });
            });

            /* $('document').ready(function(){
             $('#project_id').on("change",function(){
             var projectId = $(this).val();
             $.ajax({
             "url": "<?php echo site_url('projectplanning/getuserdetailByprojectID') ?>",
             "type": "POST",
             "data":{'projectId':projectId},
             success:function(res){
             var result = JSON.parse(res);
             console.log(result);
             $.each(result,function(index,val){
             //console.log(index + ' ' + val.userfullname);	
             
             });
             }
             });
             });
             }); */
        </script>
    </div>
</body>
</html>
