<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
			<?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li><a href="#"><?= $title; ?></a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable" >
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>
                        


                        <!-- open Phase Section-->
                        <div class="box-inner">
							<form name="frm1" method="post" action="<?= base_url('projectplanning/saveprojectteam'); ?>">
                            <div class="box-content">
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Project:</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <select required name="project_id" id="project_id"  class="form-control">
                                                        <option Selected value="">--All--</option>
                                                        <?php
                                                        if ($tmProject):
                                                            foreach ($tmProject as $value) {
                                                                ?>
                                                                <option value="<?= $value->id; ?>"><?= $value->project_name; ?></option>
                                                            <?php } endif; ?>
                                                    </select>
                                                </div>
                                            </div>
											
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Designation:</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <select required name="designation_id" id="designation_id"  class="form-control">
                                                        <option Selected value="">--All--</option>
                                                        <?php
                                                        if ($designation):
                                                            foreach ($designation as $value) {
                                                                ?>
                                                                <option value="<?= $value->fld_id; ?>"><?= $value->designation_name; ?></option>
                                                            <?php } endif; ?>
                                                    </select>
                                                </div>
											</div>
										</div>
										<br/><br/>
										<div class="row">
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Employee:</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <select required name="emp_id" id="emp_id"  class="form-control">
                                                        <option Selected value="">--All--</option>
                                                        <?php
                                                        if ($employee):
                                                            foreach ($employee as $value) {
                                                                ?>
                                                                <option value="<?= $value->id; ?>"><?= $value->userfullname; ?></option>
                                                            <?php } endif; ?>
                                                    </select>
                                                </div>
                                            </div>
											
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Email ID:</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <input type="text" id="email_id" name="email_id" class="form-control" />
                                                </div>
                                            </div>
											
										</div>
                                        
										<br/><br/>
										<div class="row">
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Contact Number:</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <input type="number" id="contact_no" name="contact_no" class="form-control" />
                                                </div>
                                            </div>
											
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Age Limit:</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <input type="number" id="age_limit" name="age_limit" class="form-control" value="65"/>
                                                </div>
                                            </div>
										</div>
										
										<br/><br/>
										<div class="row">
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Firm:</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <input type="text" id="firm" name="firm" class="form-control" value="CEG"/>
                                                </div>
                                            </div>
											
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Marks</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <input type="number" step="any" id="marks" name="marks" class="form-control"/>
                                                </div>
                                            </div>
										</div>
										<br/><br/>
										<div class="row">
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Total MM:</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <input type="number" step="any" id="total_mm" name="total_mm" class="form-control"/>
                                                </div>
                                            </div>
											
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Used MM</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <input type="number" step="any" id="used_mm" name="used_mm" class="form-control"/>
                                                </div>
                                            </div>
										</div>
										<br/><br/>
										<div class="row">
											<div class="col-sm-5">
                                                <div class="col-sm-10">
                                                    <input type="submit" value="Submit" class="btn btn-primary"/>
                                                </div>
                                            </div>
											
										</div>
										
                                    </div>
                                </div>
                            </div>
							</form>
                        </div>
                    </div>
                    <!--Close phase Section-->

                </div>
          
        </div>	
        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>

        


        



        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
		<?php $this->load->view('include/datatablejs'); ?>
		<style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
        </style>
		<script>
			$('document').ready(function(){
				$('#emp_id').on("change",function(){
					var userID = $(this).val();
					$.ajax({
						"url": "<?php echo site_url('projectplanning/getuserdetailByID') ?>",
                        "type": "POST",
						"data":{'id':userID},
						success:function(res){
							var result = JSON.parse(res);
							console.log(result[0]);
							$('#email_id').val(result[0].emailaddress);
							$('#contact_no').val(result[0].contactnumber);
						}
					});
				});
			});
		</script>
    </div>
</body>
</html>
