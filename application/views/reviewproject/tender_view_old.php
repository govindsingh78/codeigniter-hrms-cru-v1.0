<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
			<?php $this->load->view('include/sidebar'); ?>
                <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li>
                                <a href="#"><?= $title; ?> </a>
                            </li>
                        </ul>
                    </div>

                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>


                        <div class="box col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form id="form-filter" class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-4" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label class="col-sm-3 control-label" for="selectError2">Sector:</label>
                                                <div class="col-sm-8">
                                                    <select name="sectorinput" id="sectorinput"  class="form-control">
                                                        <option <?= ($secId == "") ? 'Selected' : ''; ?> value="">--All--</option>
                                                        <?php
                                                        if ($sectorArr):
                                                            foreach ($sectorArr as $row) {
                                                                ?>
                                                                <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
                                                                <?php
                                                            }
                                                        endif;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-sm-4" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label for="Business Unit" class="col-sm-2 control-label">Filter</label>
                                                <div class="col-sm-10">
                                                    <select id="company_name" class="form-control">
                                                        <option value="" selected="selected">--All--</option>
                                                        <option value="Design">Design</option>
                                                        <option value="Construction">Construction</option>
                                                        <option value="Electrical">Electrical</option>
                                                        <option value="Maintenance">Maintenance</option>
                                                        <option value="Other">Other</option>

                                                    </select>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-sm-4" style="margin: 1%;">
                                            <div class="col-sm-12">
                                                <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                                <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>


                        <!-- open Phase Section-->
                        <div class="box-inner">
                            <div class="box-content">
                                <div class="tab-content">

                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="colvis"></div>
                                            </div>
                                        </div>
                                        <form action="<?= base_url('reviewproject/activeprojectbycheckbox_projectreview'); ?>" method="post">
                                            <table id="table" class="display" cellspacing="0" width="100%">

                                                <thead>
                                                    <tr>
                                                        <th>Sr. No</th>
                                                        <th style="width:10%">Exp-Date</th>
                                                        <th>Tender Details</th>
                                                        <th>Location</th>
                                                        <th>Client</th>
                                                        <th style="width:15%">Actions </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>

                                                <tfoot>
                                                    <tr>
                                                        <th>Sr. No</th>
                                                        <th>Exp-Date</th>
                                                        <th>Tender Details</th>
                                                        <th>Location</th>
                                                        <th>Client</th>
                                                        <th>Actions </th>
                                                    </tr>
                                                </tfoot>

                                                <tfoot>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>



                                                        <td> 	<select name="act_type">
                                                                <option value="delete"> Delete </option>
                                                                <option value="togo"> To Be Submitted </option>
                                                                <option value="nogo" > No Go </option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <button type="submit" class="btn btn-success">Submit</button>
                                                        </td>
                                                        <td>All <input type="checkbox" id="checkAll"> </td>
                                                    </tr>
                                                </tfoot>


                                                <hr>

                                            </table>
                                        </form>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Close phase Section-->

                </div>

            
        </div>	

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>
        <!-- Assign Project Manager Modal Popup -->
        <div class="modal fade" id="assignmanager" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="tender_title" class="modal-title"></h4>
                    </div>

                    <div class="modal-body">
                        <form action="<?= base_url('reviewproject/assign_proposal_manager'); ?>" method="post">
                            <div class="box-content">
                                <div class="controls">
                                    <?php // $userAll = GetAllUserRec();        ?>
                                    <label>Select Proposal Manager..</label>
                                    <select required="required" class="form-control"  name="proposal_manager">
                                        <option value="">--select--</option>
                                        <?php foreach ($proposalManager as $rowrec) { ?>
                                            <option value="<?php echo $rowrec->id; ?>"><?php echo $rowrec->userfullname; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="box-content">
                                <input type="hidden" id="assigntendid" name="project_id" value="">
                                <input type="submit"  value="Assign" class="btn btn-success glyphicon glyphicon-user" >
                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal">X</button>
                            </div>

                        </form>
                    </div>

                </div>
            </div>

        </div>
		
		
		 <!-- Generate Project ID Modal -->
        <div class="modal fade" id="generateprojid" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="tender_title" class="modal-title">Generate Project No. </h4>
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url('togoproject/generate_project_no'); ?>" method="post">
                            <div class="box-content">
                                <div class="controls">
                                    <?php $userAll = GetAllPrefixRec(); ?>
                                    <label>Prefix</label>
                                    <select required="required" class="form-control" id="prefx_name" name="prefx_name">
                                        <option value="">--select--</option>
                                        <?php
                                        foreach ($userAll as $rowrec) {
                                            ?>
                                            <option value="<?= $rowrec->prefix_id; ?>"><?= $rowrec->prefix_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="projno" style="text-align: center;">&nbsp;</div>
                            <div class="box-content">
                                <?php foreach ($userAll as $rowrec) { ?>
                                    <input type="hidden" id="<?= $rowrec->prefix_id; ?>" name="<?= $rowrec->prefix_id; ?>" value="<?= $rowrec->last_generate_id; ?>">
                                <?php } ?>
                                <input type="hidden" id="assigntenderid" name="project_id" value="">
                                <input type="submit"  value="Generate" class="btn btn-success glyphicon glyphicon-user" >
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal">X</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->

        <?php $this->load->view('include/footer'); ?>
		<?php $this->load->view('include/datatablejs'); ?>
		<style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
				$("li#tender_search").addClass('active');
                $("li#inreviewproject").addClass('active');
            });


            $(document).ready(function () {
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            });

            function assignpm(projid) {
                $("#assigntenderid").val(projid);
                $("#assigntendid").val(projid);
            }
            function gocomment(tndrid) {
                //Put ProjId And Secot Id..
                $("#projId").val(tndrid);
                $.ajax({
                    type: 'POST',
                    dataType: "text",
                    data: {'tndrid': tndrid},
                    url: "<?= base_url('reviewproject/commentset'); ?>",
                    success: function (responData) {
                        $("#chatbox").html(responData);
                    },
                });
            }

            //Comment Submitted...
            $(document).ready(function () {
                $('#commentmsg').keypress(function (e) {
                    var key = e.which;

                    if (key == 13)  // the enter key code
                    {
                        var comnt = $("#commentmsg").val();
                        var projId = $("#projId").val();
                        //Comment Submit..
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?= base_url('reviewproject/commentsubmit'); ?>",
                            data: {'projId': projId, 'comnt': comnt},
                            success: function (responData) {
                                $("#commentmsg").val('');
                                return gocomment(projId);
                                //location.reload(1);
                            },
                        });
                    }
                });
            });

            function setnotimportant(delid, sectid) {
                if (confirm("Are You Sure Not Important This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('reviewproject/projNotImportantMark'); ?>",
                        data: {'actid': delid},
                        success: function (responData) {
                            $('#alert').css('display', 'block');
                            $('#msgs').html('Tender Not Important.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);
                            console.log(responData);
                            //location.reload(1);
                        },
                    });
                    //window.location = '<?= base_url('newproject/gototrashbyuser?delid='); ?>' + delid
                }
            }

            //To Go Set..
            function  togoproj(tndrid, sectid) {
                $.ajax({
                    type: 'post',
                    url: '<?= base_url('reviewproject/projtogoset'); ?>',
                    data: {'actid': tndrid},
                    success: function (responData) {
                        location.reload(1);
                    },
                });
                //window.location = '<?= base_url('dashboard/projtogoset?actid='); ?>' + tndrid + '&sectid=' + sectid + '&contName=projectreview';
            }

            //Project No Go..
            function  nogoproj(tndrid, sectid) {
                $.ajax({
                    type: 'post',
                    url: '<?= base_url('reviewproject/projnogoset'); ?>',
                    data: {'actid': tndrid},
                    success: function (responData) {
                        location.reload(1);
                    },
                });
            }
            var table;

            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.

                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo site_url('reviewproject/newProjectAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.project_name = $('#company_name').val();
                            data.sectorinput = $('#sectorinput').val();

                            // data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                        },
                        //"data": {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>','employeeId' : $('#employeeId').val()},
                    },
                    "dom": 'lBfrtip',
                    "buttons": [
                        {
                            extend: 'collection',
                            text: 'Export',
                            buttons: [
                                'copy',
                                'excel',
                                'csv',
                                'pdf',
                                'print'
                            ]

                        }
                    ],

                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

                });

                var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"

                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });

                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });

            });

        </script>

    </div>
</body>
</html>
