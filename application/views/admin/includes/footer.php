
<!--Start Section Form All Element Included  --> 
<script src="<?= FRONTASSETS; ?>bundles/libscripts.bundle.js"></script> 
<script src="<?= FRONTASSETS; ?>bundles/vendorscripts.bundle.js"></script>

<script src="<?= ASSETSVENDOR; ?>bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery.maskedinput/jquery.maskedinput.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>multi-select/js/jquery.multi-select.js"></script>
<script src="<?= ASSETSVENDOR; ?>bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="<?= ASSETSVENDOR; ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?= ASSETSVENDOR; ?>nouislider/nouislider.js"></script> 
<script src="<?= ASSETSVENDOR; ?>select2/select2.min.js"></script>
<script src="<?= FRONTASSETS; ?>bundles/mainscripts.bundle.js"></script>
<script src="<?= FRONTASSETS; ?>js/pages/forms/advanced-form-elements.js"></script>
<!-- Close Section For Data Table  -->

<!-- Start Javascript -->
<script src="<?= FRONTASSETS; ?>bundles/datatablescripts.bundle.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>sweetalert/sweetalert.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>toastr/toastr.js"></script>
<script src="<?= FRONTASSETS; ?>js/pages/tables/jquery-datatable.js"></script>

<script src="<?= FRONTASSETS; ?>bundles/knob.bundle.js"></script>
<script src="<?= FRONTASSETS; ?>bundles/mainscripts.bundle.js"></script>
<script src="<?= FRONTASSETS; ?>bundles/vendorscripts.bundle.js"></script>
<script src="<?= FRONTASSETS; ?>js/index.js"></script>
<script src="<?= FRONTASSETS; ?>js/index8.js"></script>
<!-- <script src="<?= FRONTASSETS; ?>js/scroll-startstop.events.jquery.js"></script> -->

<script src="//cdn.datatables.net/colvis/1.1.2/js/dataTables.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

<script>


		$('.activeMenu').click(function() {
			 
			localStorage.setItem("someVarKey", "menu");

			var id = $(this).parents("li").attr("id");
			console.log(">>> " + id);
			localStorage.setItem("aStore", id);


			var trid = $(this).parents("li").attr("id");
			console.log(">>> " + trid);
			localStorage.setItem("trStore", trid);




					 	
		});

		$('.activeMasterMenu').click(function() {
			 
			localStorage.setItem("someVarKey", "master_menu");

			var id = $(this).parents("li").attr("id");
			console.log(">>> " + id);
			localStorage.setItem("aStore", id);
			 
		}); 



			$(function() {
				if($("#dashboardVal").val() == 1){
						console.log('dashboardVal');
						localStorage.removeItem("someVarKey");
						localStorage.removeItem("aStore");
						localStorage.removeItem("trStore");
					
				}
				
				

				var someVarName = localStorage.getItem("someVarKey");
				var bStore = localStorage.getItem("aStore");
				var trStore = localStorage.getItem("trStore");

				if(someVarName == "menu"){
						$('[href="#'+someVarName+'"]').trigger('click');
						
						$("#"+ bStore).addClass(" highlightMenu p-0");
						$("#"+ bStore).parents("ul").parents("li").addClass(" active");
						$("#"+ bStore).parents("ul").addClass(" in");
						$("#"+ bStore).parents("ul").attr("aria-expanded", "true");

						$("#"+ bStore).parents("ul").siblings('ul').addClass(" in");
						$("#"+ bStore).parents("ul").siblings('ul').attr("aria-expanded", "true");


						if(trStore == "tr_61"){
							$("#"+ trStore).addClass(" highlightMenu p-0");
						}

						

				}else if(someVarName == "master_menu"){
						$('[href="#'+someVarName+'"]').trigger('click');

						$("#"+ bStore).addClass(" highlightMenu p-0");
						

						

				}else{
						$('[href="#menu"]').trigger('click');
				}
				

				
				
				

				var $elem = $('#wrapper');
				
				$('#nav_up').fadeIn('slow');
				$('#nav_down').fadeIn('slow');  
				
				$(window).bind('scrollstart', function(){
					$('#nav_up,#nav_down').stop().animate({'opacity':'0.2'});
				});
				$(window).bind('scrollstop', function(){
					$('#nav_up,#nav_down').stop().animate({'opacity':'1'});
				});
				
				$('#nav_down').click(
                     
					function (e) {
						$('html, body').animate({scrollTop: $(document).height()}, 800);
					}
				);
				$('#nav_up').click(
					function (e) {
						$('html, body').animate({scrollTop: '0px'}, 800);
					}
				);
            });

            function menu(val){
            	console.log(val);
            	$('[href="#'+val+'"]').trigger('click');
            }
        </script>


<!-- Close Javascript -->


<style>
    #reqd{color:red}
</style>

</body>
</html>