<nav class="navbar navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
        </div>
        <div class="navbar-brand">
            <a href="<?= base_url(""); ?>">
                <img src="http://www.cegindia.com/images/logo.png" style="width:200px!important;" alt="CEG" class="img-responsive logo">
            </a>                
        </div>
        <div class="navbar-right">
            <form id="navbar-search" class="navbar-form search-form">
                <input value="" class="form-control" placeholder="Search here" type="text">
                <button type="button" class="btn btn-default"><i class="icon-magnifier"></i></button>
            </form>                
            <div id="navbar-menu">

                 <div style="display:block;" class="nav_up" id="nav_up"></div>
<div style="display:block;" class="nav_down" id="nav_down"></div>
                
               
                &nbsp;
                <ul class="nav navbar-nav">
               


                    <li>
                        <a href="#" class="icon-menu d-none d-sm-block d-md-none d-lg-block"><i class="fa fa-folder-open-o"></i></a>
                    </li>
                    <li>
                        <a href="<?= base_url('logout'); ?>" class="icon-menu 123" title="Logout"><i class="icon-logout"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>