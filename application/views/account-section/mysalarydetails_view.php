<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$disAbl = "disabled";
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content" class="profilepage_1">
            <div class="container-fluid">
                <div class="block-header">

                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>

                    </div>
                </div>

                <br>
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">

                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="text-muted">Salary Currency : </label> <br>
                                            <input type="text" value="<?= (@$empSalDetailArr->currencyname) ? $empSalDetailArr->currencyname : ""; ?>" name="currencyname" id="currencyname" <?= $disAbl; ?> class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="text-muted">Pay Frequency : </label> <br>
                                            <input type="text" value="<?= (@$empSalDetailArr->freqtype) ? $empSalDetailArr->freqtype : ""; ?>" name="freqtype" id="freqtype" <?= $disAbl; ?> class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="text-muted">Gross Salary : </label> <br>
                                            <input type="text" value="<?= (@$empSalDetailArr->salary) ? $empSalDetailArr->salary : ""; ?>" name="salary" id="salary" <?= $disAbl; ?> class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="text-muted">Bank Name : </label> <br>
                                            <input type="text" value="<?= (@$empSalDetailArr->bankname) ? $empSalDetailArr->bankname : ""; ?>" name="bankname" id="bankname" <?= $disAbl; ?> class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="text-muted">Account Holder Name : </label> <br>
                                            <input type="text" value="<?= (@$empSalDetailArr->accountholder_name) ? $empSalDetailArr->accountholder_name : ""; ?>" name="accountholder_name" id="accountholder_name" <?= $disAbl; ?> class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="text-muted">Account Number : </label> <br>
                                            <input type="text" value="<?= (@$empSalDetailArr->accountnumber) ? $empSalDetailArr->accountnumber : ""; ?>" name="accountnumber" id="accountnumber" <?= $disAbl; ?> class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="text-muted">IFSC Code : </label> <br>
                                            <input type="text" value="<?= (@$empSalDetailArr->ifsc_code) ? $empSalDetailArr->ifsc_code : ""; ?>" name="ifsc_code" id="ifsc_code" <?= $disAbl; ?> class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="text-muted">Branch name of Bank : </label> <br>
                                            <input type="text" value="<?= (@$empSalDetailArr->branchname) ? $empSalDetailArr->branchname : ""; ?>" name="branchname" id="branchname" <?= $disAbl; ?> class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="text-muted">Appraisal Due Date : </label> <br>
                                            <input type="text" value="<?= (@$empSalDetailArr->appraisalduedate) ? $empSalDetailArr->appraisalduedate : ""; ?>" name="appraisalduedate" id="appraisalduedate" <?= $disAbl; ?> class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="text-muted">PAN Number : </label> <br>
                                            <input type="text" value="<?= (@$empSalDetailArr->pancard_no) ? $empSalDetailArr->pancard_no : ""; ?>" name="pancard_no" id="pancard_no" <?= $disAbl; ?> class="form-control">
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>