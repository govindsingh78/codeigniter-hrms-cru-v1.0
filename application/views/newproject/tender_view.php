<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
    #table_length,
    #table1_length,
    #table2_length,
    #table3_length {
        position: absolute;
        margin-left: 100px !important;
    }

    .table tbody tr td,
    .table tbody th td {
        vertical-align: middle;
        white-space: normal !important;
    }

    ul.nav.nav-tabs-new {
        float: right !important;
        margin: auto;
    }
</style>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="header">
                                <form id="form-filter">
                                    <!--<div class="card-header" id="headingOne">-->
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-6">
                                            <b>Sector :</b>
                                            <div class="input-group mb-3">
                                                <select name="sectorinput" id="sectorinput" class="form-control">
                                                    <option <?= ($secId == "") ? 'Selected' : ''; ?> value="">--All--</option>
                                                    <?php
                                                    if ($sectorArr) :
                                                        foreach ($sectorArr as $row) {
                                                    ?>
                                                            <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
                                                    <?php
                                                        }
                                                    endif;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <b>Phase :</b>
                                            <div class="input-group mb-3">
                                                <select id="company_name" class="form-control">
                                                    <option value="" selected="selected">--All--</option>
                                                    <option value="Design">Design</option>
                                                    <option value="Construction">Construction</option>
                                                    <option value="Electrical">Electrical</option>
                                                    <option value="Maintenance">Maintenance</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div>
                                        </div>





                                        <div class="col-lg-4 col-md-6">

                                            <div class="mb-2 mt-4">
                                                <b></b>
                                                <button type="button" id="btn-filter" class="btn btn-info  ml-2 pull-right"> Filter </button>



                                                <a class="btn btn-info ml-2 pull-right" href="<?= base_url('tender/traffic'); ?>">Filter Traffic Project </a>

                                                <button type="button" id="btn-reset" class="btn btn-default ml-2 pull-right"> Reset </button>
                                            </div>
                                        </div>





                                    </div>
                                    <!--</div> -->

                                </form>


                            </div>
                            <div class="body">


                                <div class="col-lg-12 col-md-12">

                                <form id="bulkDataForm">
                                    <input type="hidden" name="bulkentry" id="bulkentry" value="1"/>




                                    <div id="loading"  style="display: none; position: absolute; top: 10%; left: 45%; text-align: center"> <img src="<?= FRONTASSETS; ?>images/loader.gif" />
</div>
                                    <a class="btn btn-success btn-sm pull-left p-2 pr-3 pl-3" style="color: white; border: 1px solid #fd9449 !important;
    background: #fd9449;
    color: #fff; padding: 5px; border-radius: 20px" onclick="bulkUpdateNational()">Bulk Update (National/International)</a>
                                    </form>

                                    <ul class="nav nav-tabs-new">
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Home-new">National</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Profile-new">International</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Contact-new">Other</a></li>
                                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#all-new">All</a></li>



                                    </ul>
                                </div>

                                <div class="col-lg-12 col-md-12 mt-5">
                                    <div id="msgs"></div>
                                </div>


                                <div class="tab-content">






                                    <div class="tab-pane show active" id="all-new">
                                        <div class="table-responsive">
                                            <form action="" method="post" id="allForm">
                                                <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr. No</th>
                                                            <th style="width:11%">Exp-Date</th>
                                                            <th style="width:30%">Tender Details</th>
                                                            <th>Location</th>
                                                            <th>Client</th>
                                                            <th style="width:20%">Actions </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Sr. No</th>
                                                            <th style="width:10%">Exp-Date</th>
                                                            <th style="width:30%">Tender Details</th>
                                                            <th>Location</th>
                                                            <th>Client</th>
                                                            <th style="width:20%">Actions </th>
                                                        </tr>
                                                    </tfoot>

                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="2"><select class="form-control" name="act_type">
                                                                    <option value="Active"> Active </option>
                                                                    <option value="InActive"> InActive </option>
                                                                </select></td>


                                                            <td> &nbsp;

                                                            </td>
                                                            <td>&nbsp; </td>


                                                            <td colspan="2">
                                                                <div class="pull-right">
                                                                    <label class="fancy-checkbox">
                                                                        <input type="checkbox" id="checkAll">
                                                                        <span>All</span>
                                                                    </label>
                                                                    &nbsp;&nbsp;<button type="submit" class="btn btn-info" onclick="newActiveRecord('allForm')">Submit</button>&nbsp;
                                                            </td>

                                        </div>

                                        </tr>
                                        </tfoot>
                                        <hr>
                                        </table>
                                        </form>
                                    </div>
                                </div>







                                <div class="tab-pane" id="Home-new">
                                    <div class="table-responsive">
                                        <form action="" method="post" id="nationalForm">
                                            <table id="table1" class="table table-striped display" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sr. No</th>
                                                        <th style="width:11%">Exp-Date</th>
                                                        <th style="width:30%">Tender Details</th>
                                                        <th>Location</th>
                                                        <th>Client</th>
                                                        <th style="width:20%">Actions </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Sr. No</th>
                                                        <th style="width:10%">Exp-Date</th>
                                                        <th style="width:30%">Tender Details</th>
                                                        <th>Location</th>
                                                        <th>Client</th>
                                                        <th style="width:20%">Actions </th>
                                                    </tr>
                                                </tfoot>

                                                <!-- <tfoot>
										<tr>
											<td>&nbsp;</td>
											<td> <select name="act_type">
													<option value="Active"> Active </option>
													<option value="InActive"> InActive </option>
												</select>
											</td>
						
											<td>
												<button type="submit" class="btn btn-success">Submit</button>&emsp;All <input type="checkbox" id="checkAll">
											</td>
											<td>&nbsp; </td>
											
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
                                    </tfoot> -->

                                                <tfoot>
                                                    <tr>
                                                        <td colspan="2"><select class="form-control" name="act_type">
                                                                <option value="Active"> Active </option>
                                                                <option value="InActive"> InActive </option>
                                                            </select></td>


                                                        <td> &nbsp;

                                                        </td>
                                                        <td>&nbsp; </td>


                                                        <td colspan="2">
                                                            <div class="pull-right">
                                                                <label class="fancy-checkbox">
                                                                    <input type="checkbox" id="checkAll1">
                                                                    <span>All</span>
                                                                </label>
                                                                &nbsp;&nbsp;<button type="submit" class="btn btn-info" onclick="newActiveRecord('nationalForm')">Submit</button>&nbsp;
                                                        </td>

                                    </div>

                                    </tr>
                                    </tfoot>
                                    <hr>
                                    </table>
                                    </form>
                                </div>
                            </div>








                            <div class="tab-pane" id="Profile-new">
                                <div class="table-responsive">
                                    <form action="" method="post" id="internationalForm">
                                        <table id="table2" class="table table-striped display" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr. No</th>
                                                    <th style="width:11%">Exp-Date</th>
                                                    <th style="width:30%">Tender Details</th>
                                                    <th>Location</th>
                                                    <th>Client</th>
                                                    <th style="width:20%">Actions </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Sr. No</th>
                                                    <th style="width:10%">Exp-Date</th>
                                                    <th style="width:30%">Tender Details</th>
                                                    <th>Location</th>
                                                    <th>Client</th>
                                                    <th style="width:20%">Actions </th>
                                                </tr>
                                            </tfoot>

                                            <!-- <tfoot>
										<tr>
											<td>&nbsp;</td>
											<td> <select name="act_type">
													<option value="Active"> Active </option>
													<option value="InActive"> InActive </option>
												</select>
											</td>
						
											<td>
												<button type="submit" class="btn btn-success">Submit</button>&emsp;All <input type="checkbox" id="checkAll">
											</td>
											<td>&nbsp; </td>
											
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
                                    </tfoot> -->

                                            <tfoot>
                                                <tr>
                                                    <td colspan="2"><select class="form-control" name="act_type">
                                                            <option value="Active"> Active </option>
                                                            <option value="InActive"> InActive </option>
                                                        </select></td>


                                                    <td> &nbsp;

                                                    </td>
                                                    <td>&nbsp; </td>


                                                    <td colspan="2">
                                                        <div class="pull-right">
                                                            <label class="fancy-checkbox">
                                                                <input type="checkbox" id="checkAll2">
                                                                <span>All</span>
                                                            </label>
                                                            &nbsp;&nbsp;<button type="submit" class="btn btn-info" onclick="newActiveRecord('internationalForm')">Submit</button>&nbsp;
                                                    </td>

                                </div>

                                </tr>
                                </tfoot>
                                <hr>
                                </table>
                                </form>
                            </div>
                        </div>





                        <div class="tab-pane" id="Contact-new">
                            <div class="table-responsive">
                                <form action="" method="post" id="otherForm">
                                    <table id="table3" class="table table-striped display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th style="width:11%">Exp-Date</th>
                                                <th style="width:30%">Tender Details</th>
                                                <th>Location</th>
                                                <th>Client</th>
                                                <th style="width:20%">Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th style="width:10%">Exp-Date</th>
                                                <th style="width:30%">Tender Details</th>
                                                <th>Location</th>
                                                <th>Client</th>
                                                <th style="width:20%">Actions </th>
                                            </tr>
                                        </tfoot>

                                        <!-- <tfoot>
										<tr>
											<td>&nbsp;</td>
											<td> <select name="act_type">
													<option value="Active"> Active </option>
													<option value="InActive"> InActive </option>
												</select>
											</td>
						
											<td>
												<button type="submit" class="btn btn-success">Submit</button>&emsp;All <input type="checkbox" id="checkAll">
											</td>
											<td>&nbsp; </td>
											
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
                                    </tfoot> -->
                                        <tfoot>
                                            <tr>
                                                <td colspan="2"><select class="form-control" name="act_type">
                                                        <option value="ActiveNational"> Active for National</option>
                                                        <option value="ActiveInternational"> Active for Internation</option>
                                                        <option value="InActive"> InActive </option>
                                                    </select></td>


                                                <td> &nbsp;

                                                </td>
                                                <td>&nbsp; </td>


                                                <td colspan="2">
                                                    <div class="pull-right">
                                                        <label class="fancy-checkbox">
                                                            <input type="checkbox" id="checkAll3">
                                                            <span>All</span>
                                                        </label>
                                                        &nbsp;&nbsp;<button type="submit" class="btn btn-info" onclick="newActiveForNationProject('otherForm')">Submit</button>&nbsp;
                                                </td>

                            </div>

                            </tr>
                            </tfoot>
                            <hr>
                            </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>




    <div class="row clearfix">
        <div class="col-lg-12">
            <!--<div class="card">
                            <div class="body">
                                <div class="accordion" id="accordion">
                                    <div>
									<form id="form-filter"> 
                                            <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>Sector :</b>
                                                        <div class="input-group mb-3">
                                                            <select name="sectorinput" id="sectorinput"  class="form-control">
                                                    <option <?= ($secId == "") ? 'Selected' : ''; ?> value="">--All--</option>
                                                    <?php
                                                    if ($sectorArr) :
                                                        foreach ($sectorArr as $row) {
                                                    ?>
                                                            <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
                                                            <?php
                                                        }
                                                    endif;
                                                            ?>
                                                </select>
                                                        </div>
                                                    </div>
													<div class="col-lg-3 col-md-6">
                                                        <b>Phase :</b>
                                                        <div class="input-group mb-3">
                                                           <select id="company_name" class="form-control"><option value="" selected="selected">--All--</option>
															<option value="Design">Design</option>
															<option value="Construction">Construction</option>
															<option value="Electrical">Electrical</option>
															<option value="Maintenance">Maintenance</option>
															<option value="Other">Other</option>
														</select>
                                                        </div>
                                                    </div>
													
													
                                                   
                                                    <div class="col-lg-1 col-md-6">
                                                        <div class="mb-2">
                                                            <b></b>
															<button type="button" id="btn-filter" class="btn btn-success btn-block" style="margin-top:20px;"> Filter </button>
                                                        </div>
                                                    </div>
													<div class="col-lg-1 col-md-6">
                                                        
														<div class="mb-2">
                                                            <b></b>
															<button type="button" id="btn-reset" class="btn btn-primary btn-block" style="margin-top:20px;"> Reset </button>
                                                        </div>
                                                    </div>
													
													
                                                </div>                         
                                        
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="card">
                            <div class="body">
                                <div class="table-responsive">
								<form action="<?= base_url('activeproject/importantprojectbycheckbox'); ?>" method="post">
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                <thead>
						<tr>
							<th>Sr. No</th>
							<th style="width:11%">Exp-Date</th>
							<th style="width:40%">Tender Details</th>
							<th>Location</th>
							<th>Client</th>
							<th style="width:15%">Actions </th>
						</tr> 
                </thead>
				<tbody>
                </tbody>
                <tfoot>
						<tr>
							<th>Sr. No</th>
							<th style="width:10%">Exp-Date</th>
							<th style="width:40%">Tender Details</th>
							<th>Location</th>
							<th>Client</th>
							<th style="width:15%">Actions </th>
						</tr> 
                </tfoot>
                
				<tfoot>
					<tr>
						<td>&nbsp;</td>
						<td> <select name="act_type">
								<option value="Togo"> To Go Review  </option>
								<option value="Nogo"> No Go /Not important </option>
							</select>
						</td>
						
						<td>
							<button type="submit" class="btn btn-success">Submit</button>&emsp;All <input type="checkbox" id="checkAll">
						</td>
						<td>&nbsp; </td>
						
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</tfoot>
				<hr>
            </table>
			 </form>
                                </div>
                            </div>
                        </div>-->
        </div>
    </div>
    </div>
    </div>



     <!-- Generate Project ID Modal -->
     <div class="modal fade" id="projectLink" role="dialog">
            <div class="modal-dialog  modal-lg ">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 id="tender_title" class="modal-title">Welcome to BD Team </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <div class="modal-body">
                         
                            
                            <div class="box-content">
                                <iframe id="iframeLocation" width="100%" height="800px" src="">
                                <p>Your browser does not support iframes.</p>
                                </iframe>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal">X</button>
                            </div>
                       
                    </div>
                </div>
            </div>
        </div>


    
    <?php $this->load->view('admin/includes/footer'); ?>
    </div>
    <style>
        #table_length {
            margin-left: 20px;
        }

        #table_filter {
            margin-right: 2%;
        }

        #chatbox {
            padding: 15px;
            overflow: scroll;
            height: 300px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $("li#tender_search").addClass('active');
            $("li#newprojects").addClass('active');
        });
        $(document).ready(function() {
            
            $("#checkAll").click(function() {
                var tableId = $('table:visible').attr('id');
                $('#'+tableId+' input:checkbox').not(this).prop('checked', this.checked);
            });
            
            $("#checkAll1").click(function() {
                var tableId = $('table:visible').attr('id');
                $('#'+tableId+' input:checkbox').not(this).prop('checked', this.checked);
            });
            
            $("#checkAll2").click(function() {
                var tableId = $('table:visible').attr('id');
                $('#'+tableId+' input:checkbox').not(this).prop('checked', this.checked);
            });
            
            $("#checkAll3").click(function() {
                var tableId = $('table:visible').attr('id');
                $('#'+tableId+' input:checkbox').not(this).prop('checked', this.checked);
            });
        
            
        });

        function gocomment(tndrid) {
            //Put ProjId And Secot Id..
            $("#projId").val(tndrid);
            $.ajax({
                type: 'POST',
                dataType: "text",
                data: {
                    'tndrid': tndrid
                },
                url: "<?php echo base_url('newproject/commentset'); ?>",
                success: function(responData) {
                    $("#chatbox").html(responData);
                },
            });
        }
        //Comment Submitted...
        $(document).ready(function() {
            $('#commentmsg').keypress(function(e) {
                var key = e.which;
                if (key == 13) {
                    var comnt = $("#commentmsg").val();
                    var projId = $("#projId").val();
                    //Comment Submit..
                    $.ajax({
                        type: 'POST',
                        dataType: "text",
                        url: "<?php echo base_url('newproject/commentsubmit'); ?>",
                        data: {
                            'projId': projId,
                            'comnt': comnt
                        },
                        success: function(responData) {
                            $("#commentmsg").val('');
                            return gocomment(projId);
                        },
                    });
                }
            });
        });

        function deletethisrecord(delid, sectid) {
            var tableId = $('table:visible').attr('id');
            if (confirm("Are You Sure Inactive/Delete This.") == true) {
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url('newproject/gototrashbyuser'); ?>",
                    data: {
                        'delid': delid
                    },
                    dataType: "json",
                    success: function(responData) {
                        $('#alert').css('display', 'block');
                        //$('#msgs').html('<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Success!</strong> ' + responData.msg + '</div>');
                        toastr.info(responData.msg , 'Message', {timeOut: 5000});
                        console.log(responData);
                        reloadDataTable(tableId);
                    },
                });
            }
        }

        function activethisrecord(actid, sectid) {
            var tableId = $('table:visible').attr('id');
            // alert(test)
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url('newproject/activeproject'); ?>",
                data: {
                    'actid': actid
                },
                dataType: "json",
                success: function(responData) {
                    $('#alert').css('display', 'block');
                    //$('#msgs').html('<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Success!</strong> ' + responData.msg + '</div>');
                    toastr.info(responData.msg , 'Message', {timeOut: 5000});
                    console.log(responData);
                    reloadDataTable(tableId);
                },
            });
        }

        $("form").submit(function(e) {
            e.preventDefault();
        });

        function newActiveRecord(formId) {
            var tableId = $('table:visible').attr('id');
            var data = $("#" + formId).serialize();
            $('#msgs').html('');


            $.ajax({
                type: 'POST',
                url: "<?php echo base_url('newProject/newActiveProject'); ?>",
                data: data,
                dataType: "json",
                success: function(responData) {
                    $('#alert').css('display', 'block');
                    //$('#msgs').html('<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Success!</strong> ' + responData.msg + '</div>');
                    toastr.info(responData.msg , 'Message', {timeOut: 5000});
                    console.log(responData);
                    reloadDataTable(tableId);
                },
            });
        }


        function newprojurlview(newprojID) {
            $("#iframeLocation").attr("src", "");
            $('#projectLink').modal('toggle');

            $.ajax({
                type: 'POST',
                url: "<?php echo base_url('newProject/projurlview'); ?>",
                data: {
                    'projID': newprojID
                },
                dataType: "json",
                success: function(responData) {
                    
                    $("#iframeLocation").attr("src", responData.link);
                  
                    
                },
            });
        }

        

        function newActiveForNationProject(formId) {
            var tableId = $('table:visible').attr('id');
            var data = $("#" + formId).serialize();
            $('#msgs').html('');
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url('newProject/newActiveForNationProject'); ?>",
                data: data,
                dataType: "json",
                success: function(responData) {
                    $('#alert').css('display', 'block');
                   // $('#msgs').html('<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Success!</strong> ' + responData.msg + '</div>');
                   toastr.info(responData.msg , 'Message', {timeOut: 5000});
                    console.log(responData);
                    reloadDataTable(tableId);
                },
            });
        }


 

        function bulkUpdateNational() {
            var data = $("#bulkDataForm").serialize();
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url('newProject/bulkUpdateNational'); ?>",
                data: data,
                dataType: "json",
                beforeSend: function(){
                $("#loading").show();
                },
                complete: function(){
                $("#loading").hide();
                },
                success: function(responData) {
                    
                   toastr.success(responData.msg , 'Message', {timeOut: 5000});
 
                },
            });
        }



        var table;
        $(document).ready(function() {
            //datatables
            table = $('#table').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('newProject/newProjectAll') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.project_name = $('#company_name').val();
                        data.sectorinput = $('#sectorinput').val();
                        data.national_intern = 0;
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]
                }],

                //Set column definition initialisation properties.
                "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                }, ],

                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });

            // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
            // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
            $('#btn-filter').click(function() { //button filter event click
                table.ajax.reload(); //just reload table
            });
            $('#btn-reset').click(function() { //button reset event click
                $('#form-filter')[0].reset();
                table.ajax.reload(); //just reload table
            });

        });
    </script>
    <script type="text/javascript">
        var table;
        $(document).ready(function() {
            //datatables
            table = $('#table1').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('newProject/newProjectnational') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.project_name = $('#company_name').val();
                        data.sectorinput = $('#sectorinput').val();
                        data.national_intern = 1;
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]
                }],

                //Set column definition initialisation properties.
                "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });

            // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
            // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
            $('#btn-filter').click(function() { //button filter event click
                table.ajax.reload(); //just reload table
            });
            $('#btn-reset').click(function() { //button reset event click
                $('#form-filter')[0].reset();
                table.ajax.reload(); //just reload table
            });

        });
    </script>
    </script>
    <script type="text/javascript">
        var table;
        $(document).ready(function() {
            //datatables
            table = $('#table2').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('newProject/newProjectInterNational') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.project_name = $('#company_name').val();
                        data.sectorinput = $('#sectorinput').val();
                        data.national_intern = 2;
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]
                }],

                //Set column definition initialisation properties.
                "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });

            // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
            // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
            $('#btn-filter').click(function() { //button filter event click
                table.ajax.reload(); //just reload table
            });
            $('#btn-reset').click(function() { //button reset event click
                $('#form-filter')[0].reset();
                table.ajax.reload(); //just reload table
            });

        });
    </script>

    <script type="text/javascript">
        var table;
        $(document).ready(function() {
            //datatables
            table = $('#table3').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('newProject/newProjectMultiLocation') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.project_name = $('#company_name').val();
                        data.sectorinput = $('#sectorinput').val();
                        data.national_intern = 3;
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]
                }],

                //Set column definition initialisation properties.
                "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });

            // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
            // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
            $('#btn-filter').click(function() { //button filter event click
                table.ajax.reload(); //just reload table
            });
            $('#btn-reset').click(function() { //button reset event click
                $('#form-filter')[0].reset();
                table.ajax.reload(); //just reload table
            });

        });

        function reloadDataTable(tableId) {

            $('#' + tableId).DataTable().ajax.reload(null, false);

        }
    </script>
</body>