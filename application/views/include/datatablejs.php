<link href="<?= FRONTASSETS; ?>datatables/css/jquery.dataTables.min.css?>" rel="stylesheet">
<link href="<?= FRONTASSETS; ?>datatables/css/dataTables.colVis.css?>" rel="stylesheet">
<link href="<?= FRONTASSETS; ?>datatables/css/buttons.dataTables.min.css?>" rel="stylesheet"> 
<script src="<?= FRONTASSETS; ?>jquery/jquery-2.2.3.min.js?>"></script>
<script src="<?= FRONTASSETS; ?>datatables/js/jquery.dataTables.min.js?>" ></script>
<script src="<?= FRONTASSETS; ?>js/bootstrap.min.js?>" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/dataTables.bootstrap.min.js?>" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/dataTables.colVis.js?>" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/dataTables.buttons.min.js?>" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/buttons.flash.min.js?>" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/pdfmake.min.js?>" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/jszip.min.js?>" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/vfs_fonts.js?>" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/buttons.html5.min.js?>"></script>
<script src="<?= FRONTASSETS; ?>datatables/js/buttons.print.min.js?>"></script> 
<script src="<?= FRONTASSETS; ?>datatables/js/jquery.cookie.min.js?>"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>