<div class="navbar navbar-default" role="navigation" style="margin-bottom:0;">
    <div class="navbar-inner">
        <a class="navbar-brand" href="<?= base_url('dashboard/user_dashboard'); ?>"> 
            <img alt="Charisma Logo" src="<?= HOSTNAME; ?>assets/bdlogo.png" class=""/>	
        </a>
        <!-- user dropdown starts -->
        <div class="btn-group pull-right">
            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> <?php echo loginUserName(); ?> </span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#">Profile</a></li>
                <li class="divider"></li>
                <li><a href="<?= base_url('welcome/logout'); ?>">Logout</a></li>
            </ul>
        </div>
        <style>
            #nav{list-style:none;margin: 0px;
                 padding: 0px;}
            #nav li {
                float: left;
                margin-right: 20px;
                font-size: 14px;
                font-weight:bold;
            }
            #nav li a{color:#333333;text-decoration:none}
            #nav li a:hover{color:#006699;text-decoration:none}
            #notification_li{position:relative}
            #notificationContainer {
                background-color: #fff;
                border: 1px solid rgba(100, 100, 100, .4);
                -webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
                overflow: visible;
                position: absolute;
                top: 45px;
                margin-left: -170px;
                width: 400px;
                z-index: 1;
                display: none;
            }
            #notificationContainer:before {
                content: '';
                display: block;
                position: absolute;
                width: 0;
                height: 0;
                color: transparent;
                border: 10px solid black;
                border-color: transparent transparent white;
                margin-top: -20px;
                margin-left: 188px;
            }
            #notification_count {
                padding: 2px 2px 2px 2px;
                background: #cc0000;
                color: #ffffff;
                font-weight: bold;
                margin-left: 25px;
                /* border-radius: 31px; */
                position: absolute;
                margin-top: 13px;
                font-size: 11px;
                z-index: 10001;
                line-height: 7px;
            }
        </style>

        
        <!--<ul class="collapse navbar-collapse nav navbar-nav top-menu" style="margin-left:65px;">
            <?php
            //if(bd_rolecheck() == 8){
            $roleid = bd_rolecheck();
            $sidemainmenu = gettopMenubyrole($roleid);
            foreach ($sidemainmenu as $val) {
                $submenu = gettopsubmenu($val->menu_id, $roleid);
                if (!empty($submenu)) {
                    $attr = ' data-toggle="collapse" aria-expanded="false" ';
                } else {
                    $attr = '';
                }
                ?>			
                <li>
                    <a href="#" data-toggle="dropdown">
                        <i class="<?= $val->icon; ?>"></i> <?= $val->menuname; ?> 
                        <span class="caret"></span>
                    </a>
                        <?php if (!empty($submenu)) { ?>
                        <ul class="dropdown-menu" role="menu">
                            <?php
                            foreach ($submenu as $subval) { ?>
                                <li><a href="<?= base_url($subval->menulink); ?>"><?= $subval->menuname; ?> </a></li>
                            <?php } ?>
                        </ul>
                <?php } ?>
                </li>
                <?php
            }
            //}
            ?>			
        </ul>-->
    </div>
</div>

<style type="text/css">
    /* CSS used here will be applied after bootstrap.css */
    .dropdown {
        display:inline-block;
        margin-left:20px;
        padding:10px;
    }
    .glyphicon-bell {
        font-size:1.5rem;
    }
    .notifications {
        min-width:420px; 
    }
    .notifications-wrapper {
        overflow:auto;
        max-height:250px;
    }
    .menu-title {
        color:#ff7788;
        font-size:1.5rem;
        display:inline-block;
    }
    .glyphicon-circle-arrow-right {
        margin-left:10px;     
    }
    .notification-heading, .notification-footer  {
        padding:2px 10px;
    }
    .dropdown-menu.divider {
        margin:5px 0;          
    }
    .item-title {
        font-size:1.3rem;
        color:#000;
    }
    .notifications a.content {
        text-decoration:none;
        background:#ccc;
    }
    .notification-item {
        padding:10px;
        margin:5px;
        background:rgba(109, 109, 109, 0.05);
        border-radius:4px;
    }
    a#dLabel {
        color: white;
    }
</style>

<script>
    $(document).ready(function () {
        $("#dLabel").click(function () {
            truevar = true;
            $.ajax({type: 'POST',
                dataType: "text",
                url: "<?= base_url('dashboard/notificationseen'); ?>",
                data: {'projId': truevar},
                success: function (responData) {
                    $("#notification_count").html('0');
                },
            });
        });
    });
</script>