<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-comment"></span> Comment
                            <div class="btn-group pull-right">
                                <a style="padding: 5px 9px;font-size: 9px;"href="#" id="closemodel" class="btn btn-default" data-dismiss="modal">X</a>
                            </div>
                        </div>
                        <!-- Data Put With Ajax-->
                        <div class="panel-body" id="chatbox"></div>
                        <div class="panel-footer">
                            <div class="input-group">
                                <input id="commentmsg" type="text" style="height:35px;" class="form-control input-sm" placeholder="Type your message here...">
                                <span class="input-group-btn">
                                    <input type="hidden" name="projId" id="projId" value=""/>
                                    <input type="hidden" name="sectId" id="sectId" value="" />
                                    <span class="btn btn-sm" id="btn-chat">&nbsp;</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>