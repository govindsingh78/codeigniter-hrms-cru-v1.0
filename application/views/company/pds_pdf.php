<!DOCTYPE html>
<html lang="en">  
<title>How to export data in PDF format in Codeigniter using MPDF Library</title>
<body>
 
<div class="table-responsive">
    <table class="table table-hover tablesorter pdf_border">
        <thead>
            <tr class="pdf_border">
                <th class="header pdf_border">Model No.</th>
                
            </tr>
        </thead>
        <a class="pull-right btn btn-warning btn-large" style="margin-right:40px" href="<?= base_url('company/save_pdf'); ?>"><i class="fa fa-file-excel-o"></i> PDF Data</a>
        <tbody>
            <?php //print_r($sectArr_rec); die;
            if (isset($sectArr_rec) && !empty($sectArr_rec)) {
                foreach ($sectArr_rec as $key => $val) {
                    ?>
                    <tr class="pdf_border">
                        <td class="pdf_border"><?php echo $val->sector_name; ?></td>   
                       
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="5" class="alert alert-danger">No Records founds</td>    
                </tr>
            <?php } ?>
 
        </tbody>
    </table>
    
</div> 
</body>
</html>