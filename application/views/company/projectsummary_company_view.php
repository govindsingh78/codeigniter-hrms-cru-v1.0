<?php
error_reporting(0);
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
#table_length {
    position: absolute;
    margin-left: 100px !important;
}
.table tbody tr td, .table tbody th td {
    vertical-align: middle;
    white-space: normal !important;
}
</style>

 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


    <body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

 


                    <div class="row clearfix">
                     
                    <div class="card">

<?php if ($this->session->flashdata('msg')) { ?>
    <div class="alert alert-success alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
    </div>
<?php } ?>

<?php if ($this->session->flashdata('errormsg')) { ?>
    <div class="alert danger alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong><div id="msgs">Error ! </div></strong> <?= $this->session->flashdata('errormsg'); ?>
    </div>
<?php } ?>

 
 
<div class ="col-md-12">                            
    <div id="piechart" style="width: 700px; height: 500px; margin: auto"></div>
 <div class="responsive-table">
    <table class="table table-striped display ">
        <thead>
            <tr >
                <th>Total Project:</th>
                <th>As Competitor</th>
                <th>As Partner</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?= @$counttotalproject; ?></td>
                <td><?=$counttotalcompetitor;?></td>
                <td>

                    <div class="alert alert-success">Lead: <?=$counttotallead;?></div><div class="alert alert-warning">Jv: <?=$counttotaljv;?></div><div class="alert alert-info">Associate: <?=$counttotalassoc;?></div>

                    </td>
            </tr>
        </tbody>
    </table>
</div>
</div>  



<div class ="col-md-12">                            
<div id="piechart1" style="width: 700px; height: 500px; margin: auto"></div>
 <div class="responsive-table">
    <table class="table table-striped display">
        <thead>
            <tr>
                <th>Total Won Project :</th>
                <th>As Competitor</th>
                <th>As Partner</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>0</td>
                <td>0</td>
                <td><div class="alert alert-success">Lead: 0</div><div class="alert alert-warning">Jv: 0</div><div class="alert alert-info">Associate: 0</div></td>
            </tr>
        </tbody>
    </table>
</div>

</div>  

</div> 

</div>
</div>
</div>
</div>
</div>
 	


  
 <?php $this->load->view('admin/includes/footer'); ?>

<script type="text/javascript">
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['As Competitor', 1],
            ['As Lead Partner',2],
            ['As Jv Partner', 3],
            ['As Assoc Partner', 4]
        ]);

        var options = {
            title: 'Total Project: <?= @$total_project_count; ?>'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
</script>

<script type="text/javascript">
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Project Won', 1],
            ['As Competitor Won', 2],
            ['As Lead Partner Won', 3],
            ['As Jv Partner Won', 4],
            ['As Assoc Partner Won', 5]
        ]);

        var options = {
            title: 'Total Won Project:'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart1'));

        chart.draw(data, options);
    }
</script>

</body>
</html>






