<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
		 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
			<div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li>
                                <a href="#">CEG Experience List</a>
                            </li>
                        </ul>
                    </div>

                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>

						<div class="box col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form id="form-filter" class="form-horizontal">
                                        <div class="row">
											<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
												<label class="col-sm-4 control-label" for="selectError2">Start Date</label>
                                                <div class="col-sm-8">
													<input type="text" name="start_dates" id="start_dates" value=""  class="form-control">
												</div>
											</div>
											<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
												<label class="col-sm-4 control-label" for="selectError2">End Date</label>
                                                <div class="col-sm-8">
													<input type="text" name="end_dates" id="end_dates" value=""  class="form-control">
												</div>
											</div>

											<div class="col-sm-2" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <div class="col-sm-10">
                                                    <select id="date_cond" class="form-control">
														<option value="0">None</option>
                                                        <option value="1">AND</option>
                                                        <option value="2">OR</option>
                                                    </select>
                                                </div>
                                            </div>	
										
                                            <div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label class="col-sm-3 control-label" for="selectError2"> Service:</label>
                                                <div class="col-sm-9">
													<select id="serviceinput" class="form-control">
														<option value="">Select Service</option>
														<?php 
															if(!empty($service)){
																foreach($service as $vals){
															?>
																	<option value="<?= $vals->id ?>"><?= $vals->service_name ?></option>
															<?php		
																}
																
															}
														?>
													</select>
												</div>
                                            </div>
										</div>
										<div class="row"></div>
										<div class="row">
                                            <div class="col-sm-2" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <div class="col-sm-10">
                                                    <select id="service_cond" class="form-control">
														<option value="0">None</option>
                                                        <option value="1">AND</option>
                                                        <option value="2">OR</option>
                                                    </select>
                                                </div>
                                            </div>
											
											<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label class="col-sm-3 control-label" for="selectError2"> Client:</label>
                                                <div class="col-sm-9">
													<input type="text" id="clientnput" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-2" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <div class="col-sm-10">
                                                    <select id="client_cond" class="form-control">
														<option value="0">None</option>
                                                        <option value="1">AND</option>
                                                        <option value="2">OR</option>
                                                    </select>
                                                </div>
                                            </div>
											
											<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label class="col-sm-3 control-label" for="selectError2"> Year:</label>
                                                <div class="col-sm-9">
													<input type="text" id="yearinput" class="form-control">
                                                </div>
                                            </div>
										</div>
										<div class="row"></div>
										<div class="row">
                                            <div class="col-sm-2" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <div class="col-sm-10">
                                                    <select id="year_cond" class="form-control">
														<option value="0">None</option>
                                                        <option value="1">AND</option>
                                                        <option value="2">OR</option>
                                                    </select>
                                                </div>
                                            </div>
											
											<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label class="col-sm-3 control-label" for="selectError2"> Sector:</label>
                                                <div class="col-sm-9">
													<input type="text" id="sectorinput" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-2" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <div class="col-sm-10">
                                                    <select id="sector_cond" class="form-control">
														<option value="0">None</option>
                                                        <option value="1">AND</option>
                                                        <option value="2">OR</option>
                                                    </select>
                                                </div>
                                            </div>
											
											
											<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label class="col-sm-3 control-label" for="selectError2"> State:</label>
                                                <div class="col-sm-9">
													<input type="text" id="stateinput" class="form-control">
                                                </div>
                                            </div>
										</div>
										<div class="row"></div>
										<div class="row">
											<div class="col-sm-2" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <div class="col-sm-10">
                                                    <select id="state_cond" class="form-control">
														<option value="0">None</option>
                                                        <option value="1">AND</option>
                                                        <option value="2">OR</option>
                                                    </select>
                                                </div>
                                            </div>
											
											<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label class="col-sm-3 control-label" for="selectError2"> Project Cost:</label>
                                                <div class="col-sm-9">
													<input type="text" id="projectcost" class="form-control">
                                                </div>
                                            </div>
											<div class="col-sm-2" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <div class="col-sm-10">
                                                    <select id="cost_cond" class="form-control">
														<option value="0">None</option>
                                                        <option value="1">AND</option>
                                                        <option value="2">OR</option>
                                                    </select>
                                                </div>
                                            </div>
											
											<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label class="col-sm-3 control-label" for="selectError2"> Funding:</label>
                                                <div class="col-sm-9">
													<input type="text" id="funding" class="form-control">
                                                </div>
                                            </div>

                                            
                                        </div>
										<div class="row"></div>
										<div class="row">
											<div class="col-sm-4" style="margin: 1%;">
												<div class="col-sm-12">
													<button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
													<button type="button" id="btn-reset" class="btn btn-default">Reset</button>
												</div>
											</div>
										</div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- open Phase Section-->
                        <div class="box-inner">
                            <div class="box-content">
                                <div class="tab-content">

                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="colvis"></div>
                                            </div>
                                        </div>
                                        <table id="table" class="display" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sr. No</th>
                                                        <th>Name</th>
														<th>Project Name</th>
                                                        <th>Service</th>
                                                        <th>client</th>
                                                        <th>Start Year</th>
                                                        <th>Sector</th>
                                                        <th>State</th>
                                                        <th>Funding</th>
                                                        <th>Act</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>

                                                <tfoot>
                                                    <tr>
                                                        <th>Sr. No</th>
                                                        <th>Name</th>
														<th>Project Name</th>
                                                        <th>Service</th>
                                                        <th>client</th>
                                                        <th>Start Year</th>
                                                        <th>Sector</th>
                                                        <th>State</th>
                                                        <th>Funding</th>
                                                        <th>Act</th>
                                                    </tr>
                                                </tfoot>

                                                <hr>

                                            </table>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Close phase Section-->

                </div>

            
        </div>	

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>
        
<!-- BootStrap Model For Comments.. -->
<?php $this->load->view('include/commenthtml'); ?>
<!-- close Model -->

<?php $this->load->view('include/footer'); ?>
<?php $this->load->view('include/datatablejs'); ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                
<style>
#table_length{margin-left:20px;}
#table_filter{margin-right:2%;} 
#chatbox {padding: 15px;overflow: scroll;height: 300px;}
</style>
<script type="text/javascript">
$(document).ready(function () {
    $("li#cegexp").addClass('active');
});

            $(function () {
                    $("#start_dates").datepicker({dateFormat: "yy-mm-dd"}).val();
                    $("#end_dates").datepicker({dateFormat: "yy-mm-dd"}).val();
            });

$(document).ready(function () {
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
});
           
var table;
$(document).ready(function () {
    //datatables
    table = $('#table').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('company/expListAll') ?>",
            "type": "POST",
            "data": function (data) {
                data.start_dates = $('#start_dates').val();
                        data.end_dates = $('#end_dates').val();
                        data.date_cond = $('#date_cond').val();
                        data.serviceinput = $('#serviceinput').val();
                        data.service_cond = $('#service_cond').val();
                        data.clientnput = $('#clientnput').val();
                        data.client_cond = $('#client_cond').val();
                        data.yearinput = $('#yearinput').val();
                        data.year_cond = $('#year_cond').val();
                        data.sectorinput = $('#sectorinput').val();
                        data.sector_cond = $('#sector_cond').val();
                        data.stateinput = $('#stateinput').val();
                        data.state_cond = $('#state_cond').val();
                        data.projectcost = $('#projectcost').val();
                        data.cost_cond = $('#cost_cond').val();
                        data.funding = $('#funding').val();
                },
        },
        "dom": 'lBfrtip',
        "buttons": [
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]

            }
        ],

        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    });

    var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
    $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"

    $('#btn-filter').click(function () { //button filter event click
        table.ajax.reload();  //just reload table
    });

    $('#btn-reset').click(function () { //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload();  //just reload table
    });

});

</script>

</div>
</body>
</html>
