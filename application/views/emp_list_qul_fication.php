<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> Employee Educational  Qualification Report</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="accordion" id="accordion">
                                    <div>
									<form id="form-filter"> 
                                        <!--<div class="card-header" id="headingOne">-->
                                            <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>Business Unit :</b>
                                                        <div class="input-group mb-3">
                                                            
                                                           <select id="businessunit_name" class="select floating form-control">
															<option value="" selected="selected"> Select Business </option>
															<?php 
															$all_businessunit = get_businessunits();
															if ($all_businessunit):
															foreach ($all_businessunit as $unitrow) {
															?>
															<option value="<?= $unitrow->id; ?>"><?= $unitrow->unitname; ?></option>
															<?php
															}
															endif;
															 ?>
															</select>
                                                        </div>
                                                    </div>
													<div class="col-lg-3 col-md-6">
                                                        <b>Company Name :</b>
                                                        <div class="input-group mb-3">
                                                            
                                                           <select id="company_name" class="select floating form-control">
															<option value="" selected="selected">Select Company</option>
															<?php
															$all_company = get_companyname();
															if ($all_company):
															foreach ($all_company as $companyrow) {
															?>
															<option value="<?= $companyrow->id; ?>"><?= $companyrow->company_name; ?></option>
															<?php
															}
															endif;
															?>
															</select>
                                                        </div>
                                                    </div>
													<div class="col-lg-3 col-md-6">
                                                        <b>Select Department :</b>
                                                        <div class="input-group mb-3">
                                                            
                                                           <select id="department_id" class="select floating form-control">
															<option value="" selected="selected"> Select Department </option>
															<?php
															$all_Activedepartment = get_departments();
															if ($all_Activedepartment):
															foreach ($all_Activedepartment as $deprow) {
															?>
															<option value="<?= $deprow->id; ?>"><?= $deprow->deptname; ?></option>
															<?php
															}
															endif;
															?>
															</select>
                                                        </div>
                                                    </div>
													<div class="col-lg-3 col-md-6">
                                                        <b>Select Designation :</b>
                                                        <div class="input-group mb-3">
                                                            
                                                           <select id="potion_design_id" class="select floating form-control">
														<option value="" selected="selected"> Select Designation </option>
														<?php
														$all_Activedesignation = get_designation();
														if ($all_Activedesignation):
														foreach ($all_Activedesignation as $desigrow) {
														?>
														<option value="<?= $desigrow->id; ?>"><?= $desigrow->positionname; ?></option>
														<?php
														}
														endif;
														?>
														</select>
                                                        </div>
                                                    </div>
													
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>From Date (DOJ) :</b>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                                                            </div>
                                                            <input type="date" name="" class="form-control datetimepicker" id="from_date">
                                                        </div>
                                                    </div>
													
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>To Date (DOJ) :</b>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                                                            </div>
                                                            <input type="date" name="" class="form-control datetimepicker" id="to_date">
                                                        </div>
                                                    </div>
													
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>Employee Name :</b>
                                                        <div class="input-group mb-3">
														<input type="text" class="form-control floating" id="userfullname">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-1 col-md-6">
                                                        <div class="mb-2">
                                                            <b></b>
															<button type="button" id="btn-filter" class="btn btn-success btn-block" style="margin-top:20px;"> Filter </button>
                                                        </div>
                                                    </div>
													<div class="col-lg-1 col-md-6">
                                                        
														<div class="mb-2">
                                                            <b></b>
															<button type="button" id="btn-reset" class="btn btn-primary btn-block" style="margin-top:20px;"> Reset </button>
                                                        </div>
                                                    </div>
													
													
                                                </div>
                                        <!--</div> -->                               
                                        
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Emp Code</th>
                        <th>Employee Name</th>
                        <th>Designation</th>
                        <th>Department</th>
                        <th>Education Level</th>
                        <th>Institution Name</th>
                        <th>Course</th>
                        <th>Location</th>
                        <th>Specialization</th>
                        <th>Passing Year</th>
                        <th>Percentage</th>   
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                       <th>Sr. No</th>
                        <th>Emp Code</th>
                        <th>Employee Name</th>
                        <th>Designation</th>
                        <th>Department</th>
                        <th>Education Level</th>
                        <th>Institution Name</th>
                        <th>Course</th>
                        <th>Location</th>
                        <th>Specialization</th>
                        <th>Passing Year</th>
                        <th>Percentage</th>   
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>

<script type="text/javascript">
     var table;
    $(document).ready(function () {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?= base_url('ajax_list_empeducational') ?>",
                "type": "POST",
                "data": function (data) {
                    data.businessunit_name = $('#businessunit_name').val();
                    data.company_name = $('#company_name').val();
                    data.department_id = $('#department_id').val();
                    data.potion_design_id = $('#potion_design_id').val();
                    data.from_date = $('#from_date').val();
                    data.to_date = $('#to_date').val();
                    data.userfullname = $('#userfullname').val();					
					
			  },
            },
            "dom": 'lBfrtip',
            "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]
                }
            ],
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
        var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
        $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
        $('#btn-filter').click(function () { //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function () { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });
    });
</script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title">Preview Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		
      </div>
      <div class="modal-body">
       <form method="post" action="<?= base_url('reissue_id_card'); ?>" id="visitingcard_form"> 
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Apply Date</b>
															<input readonly="readonly" name="date_of_initiation" type="text" value="<?php echo date("d-m-Y");?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Name</b>
                                                            <input readonly="readonly" name="userfullname" type="text" value="<?= (@$idcardDetailArr->userfullname) ? $idcardDetailArr->userfullname : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Deparatment</b>
                                                            <input readonly="readonly" type="text" step="any" value="<?= (@$idcardDetailArr->department_name) ? $idcardDetailArr->department_name : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Designation</b>
                                                            <input readonly="readonly" name="position_name" type="text" step="any" value="<?= (@$idcardDetailArr->position_name) ? $idcardDetailArr->position_name : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <b>Company</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="emailaddress" type="text" step="any" value="<?= (@$idcardDetailArr->company_name) ? $idcardDetailArr->company_name : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
													 <div class="col-lg-4 col-md-6">
                                                        <b>Date of Joining</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="joining_date" type="text" step="any" value="<?= (@$idcardDetailArr->date_of_joining) ? $idcardDetailArr->date_of_joining : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <b>Reason for Re-issue</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="reason_reissue" id="reason_reissue" type="text" step="any" value="" id="" class="form-control">
                                                        </div>
                                                    </div>
													<div class="col-lg-4 col-md-6">
                                                        <div class="mb-4">
                                                            <b></b>
                                                            <input  type="submit" name="submit" class="btn btn-round btn-primary">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</body>
<script>
$('#modal_click').on('click', function (e) {
	
    var value = $('#select_reason').val();
    $('#reason_reissue').val(value);
});
</script>