<?php
error_reporting(0);
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
    #table_length {
        position: absolute;
        margin-left: 100px !important;
    }

    .table tbody tr td,
    .table tbody th td {
        vertical-align: middle;
        white-space: normal !important;
    }


    #financial_year_checklist{
        width: 100% !important;
    }
</style>

            
            <link rel="stylesheet" href="<?= FRONTASSETS; ?>css/jquery.multiselect.css" />
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

            <?php
            
            if (@$years == '') {
                @$years = date('Y');
            }
            ?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">

                    

                        <div class="card">
                            <div class="header">

                               
                            <form method="post" action="<?= base_url('report/financial_yearwisereport'); ?>">
                                <div class="row">

                                   

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="email"> Financial Year :  </label>
                                                                      
                                            <?php
                                            $year = '2016';
                                            ?>
                                            <select id="financial_year" name="financial_year[]" multiple class="form-control">
                                            <?php
                                            $j = 0;
                                            for ($i = $year; $i <= 2022; $i++) {
                                            ?>
                                            <option <?= (@$years == $i) ? "selected" : ''; ?> value=<?= $i; ?>><?= $i; ?></option>
                                            <?php
                                            $j++;
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <input type="submit" id="btn-filter" name="submit" class="btn btn-primary pull-right ml-2 mt-4" value="Filter" />
                                             
                                            <button type="button" id="btn-reset" class="btn btn-default pull-right mt-4"> Reset </button>
                                        </div>
                                    </div>
                                </div>
                                </form>


                            </div>







 

                            <div class="row clearfix">
                                <div class="col-lg-12">
                                    <div class="">
                                     
                                        
                
                                        <div class="body">


                                           
                                           
                                            
                                            <?php if (!empty($RecreturnArr) && isset($RecreturnArr)) { ?>
                                                 <h4 class="box-title">Financial Year Wise Order Booking :</h4> 


                                                  <div id="columnchart_values" style="width: 900px; margin: 0 auto;"></div>


                     <div class="table-responsive">

                                <table class="table  table-striped display">
                                    <thead>
                                        <tr>
                                            <th>Sr.no.</th>
                                            <th>Year</th>
                                            <th>Won</th>
                                            <th>Order Booking</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (($RecreturnArr) and ( $years)) {
                                            foreach ($years as $kKey => $rEcd) {
                                                ?>
                                                


                                                <tr>
                                                    <td><?= $kKey + 1; ?></td>
                                                    <td><?= $rEcd; ?></td>
                                                    <td><?= count($RecreturnArr[$rEcd]); ?></td>

                                                    <?php
                                                    $sno = 0;
                                                    $value_loa = 0;
                                                    $total_share = 0;

                                                    foreach ($RecFinancArr[$rEcd] as $rrow) {
                                                        $sno++;

                                                        $value_loa += $rrow->value_as_per_loa;
                                                        ?>

                                                        <?php $share = get_LeadjvAssocById($rrow->bd_projid); ?>

                                                        <?php
                                                        $else_value_loa = 0;
                                                        $lead1 = 0;
                                                        $jv1 = 0;
                                                        $assoc1 = 0;

                                                        if ($share) {

                                                            foreach ($share as $shrrow) {
                                                                $leadArr = explode(',', $shrrow->lead_comp_id);
                                                                $jvArr = explode(',', $shrrow->joint_venture);
                                                                $assocArr = explode(',', $shrrow->asso_comp);
                                                                $json_decode = json_decode($shrrow->financial_detail);
                                                                if (in_array(74, $leadArr)) {
                                                                    $lead1 = $json_decode->lead_amount74;
                                                                }

                                                                if (in_array(74, $jvArr)) {
                                                                    $jv1 = $json_decode->jv_amount74;
                                                                }

                                                                if (in_array(74, $assocArr)) {
                                                                    $assoc1 = $json_decode->assoc_amount74;
                                                                }
                                                            }
                                                        } else {
                                                            $else_value_loa = $rrow->value_as_per_loa;
                                                        }

                                                        $total_share += (($else_value_loa) + ($lead1) + ($jv1) + ($assoc1));
                                                        ?>


                                                    <?php } ?>

                                                    <td><b><?= number_format(($total_share), 2) ?></b></td>

                                                    <td><a href="<?= base_url('report/yearly_financial_report'); ?>/<?= $rEcd; ?>" class="btn btn-info btn-sm">View All</a></td>
                                                </tr>
            <?php
        }
    }
    ?>
                                    </tbody>
                                </table>

                                 

                                  

                                 
                             
<?php } ?>
                                            


                                               


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>





                        </div>

                    </div>
                </div>

            
            </div>




        
        </div>
    </div>
    <?php $this->load->view('admin/includes/footer'); ?>
     <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" rel="stylesheet"></link>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
    </div>
    <script type="text/javascript">
         $(document).ready(function() {
        <?php 

        if($years){
        foreach ($years as $kKey => $rEcd) {
        ?>
        
        var checkThis = '<?php echo $rEcd; ?>';
        console.log(checkThis);
        $('#financial_year_' + checkThis).trigger('click');
        $('#financial_year_' + checkThis).prop('checked', true);
       
        <?php
        }

        }

        ?>

    });

         </script>
                                                
    <script>
        $(document).ready(function() {
            $("li#dashboard").addClass('active');


        });

        $('#financial_year').multiselect({
            columns: 1,
            placeholder: 'Select Year',
            search: true
        });
    </script>

    <style>
        .ms-options>ul>li {
            list-style-type: none;
        }

        .box {
            position: relative;
            border-radius: 3px;
            background: #ffffff;
            border-top: 3px solid #ffffff;
            margin-bottom: 20px;
            width: 100%;
            box-shadow: 0 2px 2px rgba(0, 0, 0, 1.0);
        }

        .box-header.with-border {
            border-bottom: 1px solid #f4f4f4;
        }

        .box-header.with-border {
            border-bottom: 1px solid #d2d6de;
        }

        .box-header {
            color: #444;
            display: block;
            padding: 10px;
            position: relative;
        }

        .box-header {
            color: #444;
            display: block;
            padding: 10px;
            position: relative;
        }

        .box-header .box-title {
            display: inline-block;
            font-size: 18px;
            margin: 0;
            line-height: 1;
        }

        .box-header>.box-tools {
            position: absolute;
            right: 10px;
            top: 5px;
        }

        .box-body {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
            padding: 10px;
        }

        .box-header:before,
        .box-body:before,
        .box-footer:before,
        .box-header:after,
        .box-body:after,
        .box-footer:after {
            content: " ";
            display: table;
        }

        .box-body>.table {
            margin-bottom: 0;
        }

        .table-bordered {
            border: 1px solid #f4f4f4;
        }
    </style>


    <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Element", "Density", { role: "style" } ],
        <?php
            if (isset($RecreturnArr) and isset($years)) {
                foreach ($years as $kKey => $rEcd) {
            ?>
                    <?php
                    $sno = 0;
                    $value_loa = 0;
                    $total_share = 0;

                    foreach ($RecFinancArr[$rEcd] as $rrow) {
                        $sno++;

                        $value_loa += $rrow->value_as_per_loa;
                    ?>

                        <?php $share = get_LeadjvAssocById($rrow->bd_projid); ?>

                        <?php
                        $else_value_loa = 0;
                        $lead1 = 0;
                        $jv1 = 0;
                        $assoc1 = 0;

                        if ($share) {

                            foreach ($share as $shrrow) {
                                $leadArr = explode(',', $shrrow->lead_comp_id);
                                $jvArr = explode(',', $shrrow->joint_venture);
                                $assocArr = explode(',', $shrrow->asso_comp);
                                $json_decode = json_decode($shrrow->financial_detail);
                                if (in_array(74, $leadArr)) {
                                    $lead1 = $json_decode->lead_amount74;
                                }

                                if (in_array(74, $jvArr)) {
                                    $jv1 = $json_decode->jv_amount74;
                                }

                                if (in_array(74, $assocArr)) {
                                    $assoc1 = $json_decode->assoc_amount74;
                                }
                            }
                        } else {
                            $else_value_loa = $rrow->value_as_per_loa;
                        }

                        $total_share += (($else_value_loa) + ($lead1) + ($jv1) + ($assoc1));
                        ?>

                    <?php } ?>
                    ["<?= $rEcd; ?>", <?= str_replace(",", "", $total_share); ?>, "#b87333"],
                    
            <?php
                }
            }
            ?>

        
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Density of Precious Metals, in g/cm^3",
        width: 600,
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>


    <script type="text/javascript">
        // Load Charts and the corechart and barchart packages.
        google.charts.load('current', {
            'packages': ['corechart']
        });

        // Draw the pie chart and bar chart when Charts is loaded.
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Year');
            data.addColumn('number', 'Amount');
            <?php
            if (isset($RecreturnArr) and isset($years)) {
                foreach ($years as $kKey => $rEcd) {
            ?>
                    <?php
                    $sno = 0;
                    $value_loa = 0;
                    $total_share = 0;

                    foreach ($RecFinancArr[$rEcd] as $rrow) {
                        $sno++;

                        $value_loa += $rrow->value_as_per_loa;
                    ?>

                        <?php $share = get_LeadjvAssocById($rrow->bd_projid); ?>

                        <?php
                        $else_value_loa = 0;
                        $lead1 = 0;
                        $jv1 = 0;
                        $assoc1 = 0;

                        if ($share) {

                            foreach ($share as $shrrow) {
                                $leadArr = explode(',', $shrrow->lead_comp_id);
                                $jvArr = explode(',', $shrrow->joint_venture);
                                $assocArr = explode(',', $shrrow->asso_comp);
                                $json_decode = json_decode($shrrow->financial_detail);
                                if (in_array(74, $leadArr)) {
                                    $lead1 = $json_decode->lead_amount74;
                                }

                                if (in_array(74, $jvArr)) {
                                    $jv1 = $json_decode->jv_amount74;
                                }

                                if (in_array(74, $assocArr)) {
                                    $assoc1 = $json_decode->assoc_amount74;
                                }
                            }
                        } else {
                            $else_value_loa = $rrow->value_as_per_loa;
                        }

                        $total_share += (($else_value_loa) + ($lead1) + ($jv1) + ($assoc1));
                        ?>

                    <?php } ?>
                    data.addRows([
                        ['<?= $rEcd; ?>', <?= str_replace(",", "", $total_share); ?>],
                    ]);
            <?php
                }
            }
            ?>
            var piechart_options = {
                title: 'Financial Year Wise Order Booking :',
                width: 800,
                height: 800,
                is3D: true

            };

            var piechart = new google.visualization.PieChart(document.getElementById('piechart_div'));
            piechart.draw(data, piechart_options);

        }
    </script>
    <?php

    function stringShort($string2)
    {
        $string = str_replace("_x000D_", "<br>", strip_tags($string2));
        if (strlen($string) > 75) {
            $stringCut = substr($string, 0, 75);
            $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '... <a href="#"> View More..</a>';
        }
        echo $string;
    }
    ?>
</body>