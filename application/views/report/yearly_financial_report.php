<?php
error_reporting(0);
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
.powered-by {
    display: none;
}
</style>        
<!-- Page Content Holder -->
   <link rel="stylesheet" href="<?= FRONTASSETS; ?>css/jquery.multiselect.css" />
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">Home</li>
                            <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                    
                    <div class="body">
                    <h4 class="box-title">Annual Financial Report :</h4> 
                    <div class="table-responsive mt-2">
                        <table class="table  table-striped display">
                        <thead>
                        <tr>
                        <th>Sr.no.</th>
                        <th>Project Id</th>
                        <th>Project Name</th>
                        <th>Value As Per LOA</th>
                        <th>Share</th>
                        <th>Company</th>
                        <th>Share Amount of Company</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $sno = 0;
                        $value_loa = 0; 
                        $total_share = 0;

                        foreach($resultArr as $rrow){
                        $sno++;

                        $value_loa+= $rrow->value_as_per_loa;                                           
                        ?>
                        <tr>
                            
                        <td><?= $sno;?></td>

                        <td><?= $rrow->bd_projid;?></td>

                        <td><?= $rrow->project_name;?></td>

                        <td><?= number_format($rrow->value_as_per_loa,2);?></td>

                        <?php $share = get_LeadjvAssocById($rrow->bd_projid); ?>

                        <td>
                            
                        <?php
                        $else_value_loa = 0;
                        $lead1 = 0;
                        $jv1 = 0;
                        $assoc1 = 0;

                        if($share){  

                        foreach($share as $shrrow){ 
                        $leadArr = explode(',', $shrrow->lead_comp_id);
                        $jvArr = explode(',', $shrrow->joint_venture);
                        $assocArr = explode(',', $shrrow->asso_comp);   
                        $json_decode = json_decode($shrrow->financial_detail);
                        if(in_array(74, $leadArr)){
                        $lead1= $json_decode->lead_amount74;
                        echo number_format($json_decode->lead_amount74,2);
                        }

                        if(in_array(74, $jvArr)){
                        $jv1= $json_decode->jv_amount74;
                        echo number_format($json_decode->jv_amount74,2);
                        }

                        if(in_array(74, $assocArr)){
                        $assoc1= $json_decode->assoc_amount74;
                        echo number_format($json_decode->assoc_amount74,2); 
                        }

                        } 

                        }

                        else {
                        $else_value_loa = $rrow->value_as_per_loa;
                        echo number_format($rrow->value_as_per_loa, 2);
                        }

                        $total_share+= (($else_value_loa)+($lead1)+($jv1)+($assoc1));
                        ?>
                            
                        </td>

                        <td>
                            
                        <?php 
                        if($share){                                                  
                        foreach($share as $shrrow){
                            
                        $leadArr = explode(',', $shrrow->lead_comp_id);

                        $jvArr = explode(',', $shrrow->joint_venture);

                        $assocArr = explode(',', $shrrow->asso_comp);

                        $lead_company = get_companyname($leadArr);

                        $jv_company = get_companyname($jvArr);

                        $assoc_company = get_companyname($assocArr);

                        if($lead_company){
                        foreach($lead_company as $leadrow){
                        echo '<b>(Lead):</b> '.($leadrow->company_name).'<br/><br/>';
                        }
                        }

                        if($jv_company){
                         foreach($jv_company as $jvrow){

                          echo '<b>(JV):</b> '.($jvrow->company_name).'<br/><br/>';
                         }
                        }

                        if($assoc_company){
                        foreach($assoc_company as $assocrow){
                        echo '<b>(Assoc):</b> '.($assocrow->company_name).'<br/><br/>';
                        }
                        }

                        } 

                        }
                        ?>
                        </td>

                        <td>
                            
                        <?php
                        if($share){                                                  
                        foreach($share as $shrrow){ 
                        $leadArr = explode(',', $shrrow->lead_comp_id);
                        $jvArr = explode(',', $shrrow->joint_venture);
                        $assocArr = explode(',', $shrrow->asso_comp);
                        $json_decode = json_decode($shrrow->financial_detail);
                        $lead_company = get_companyname($leadArr);
                        $jv_company = get_companyname($jvArr);
                        $assoc_company = get_companyname($assocArr);

                        if($lead_company){
                        foreach($lead_company as $leadrow){
                        $lead_id= 'lead_amount'.$leadrow->fld_id;
                        if($json_decode->$lead_id){
                        echo number_format($json_decode->$lead_id, 2).'<br/><br/>';
                        }
                        }
                        }

                        if($jv_company){
                        foreach($jv_company as $jvrow){
                        $jv_id= 'jv_amount'.$jvrow->fld_id;
                        if($json_decode->$jv_id){
                        echo number_format($json_decode->$jv_id,2).'<br/><br/>';
                        }
                        }
                        }

                        if($assoc_company){
                        foreach($assoc_company as $assocrow){
                        $assoc_id= 'assoc_amount'.$assocrow->fld_id;    
                        if($json_decode->$assoc_id){
                        echo number_format($json_decode->$assoc_id,2).'<br/><br/>'; 
                        }
                        }
                        }
                        }

                        } 
                        ?>
                        </td>

                        </tr>
                        <?php } ?>

                        <tr rowspan="3"><td colspan="3" style="text-align:center;"><b>TOTAL:</b></td><td><b><?= number_format($value_loa,2);?></b></td><td><b><?= number_format(($total_share),2)?></b></td><td></td><td></td></tr>

                        </tbody>
                        </table>

                        <div style="margin: 0 auto;">
                            <div id="piechart_div" style="width: 900px; margin: 0 auto;"></div>
                        </div>




                         </div>
                </div>
            
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
</body>
<?php $this->load->view('admin/includes/footer'); ?>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" rel="stylesheet"></link>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>

<style>
    .ms-options >ul >li{list-style-type:none;}


    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #ffffff;
        margin-bottom: 20px;
        width: 100%;
        box-shadow: 0 2px 2px rgba(0,0,0,1.0);
    }
    .box-header.with-border {
        border-bottom: 1px solid #f4f4f4;
    }
    .box-header.with-border {
        border-bottom: 1px solid #d2d6de;
    }
    .box-header {
        color: #444;
        display: block;
        padding: 10px;
        position: relative;
    }
    .box-header {
        color: #444;
        display: block;
        padding: 10px;
        position: relative;
    }
    .box-header .box-title {
        display: inline-block;
        font-size: 18px;
        margin: 0;
        line-height: 1;
    }
    .box-header>.box-tools {
        position: absolute;
        right: 10px;
        top: 5px;
    }
    .box-body {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        padding: 10px;
    }
    .box-header:before, .box-body:before, .box-footer:before, .box-header:after, .box-body:after, .box-footer:after {
        content: " ";
        display: table;
    }
    .box-body>.table {
        margin-bottom: 0;
    }
    .table-bordered {
        border: 1px solid #f4f4f4;
    }
</style>

