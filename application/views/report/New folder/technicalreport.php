<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
		<div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li>
                                <a href="#">Technical Report </a>
                            </li>
                        </ul>
                    </div>

                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>




                        <!-- open Phase Section-->
                        <div class="box-inner">
                            <div class="box-content">
                                <div class="tab-content">

                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="colvis"></div>
                                            </div>
                                        </div>
                                        <form action="<?= base_url('report/technicalchart'); ?>" method="post">
                                            <table id="table" class="display" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sr. No</th>
                                                        <th>Tender Details</th>
														<th>Service</th>
                                                        <th>client</th>
                                                        <th>Start Year</th>
                                                        <th>Sector</th>
                                                        <th>State</th>
                                                        <th>Project Code</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>

                                                <tfoot>
                                                    <tr>
                                                        <th>Sr. No</th>
                                                        
                                                        <th>Tender Details</th>
														<th>Service</th>
                                                        <th>client</th>
                                                        <th>Start Year</th>
                                                        <th>Sector</th>
                                                        <th>State</th>
                                                        <th>Project Code</th>
                                                        <th>Action</th>
                                                        
                                                    </tr>
                                                </tfoot>

                                                <tfoot>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                     
                                                        <td>
                                                            <button type="submit" class="btn btn-success">Submit</button>
                                                        </td>
                                                        <td>All <input type="checkbox" id="checkAll"> </td>
                                                    </tr>
                                                </tfoot>


                                                <hr>

                                            </table>
                                        </form>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Close phase Section-->

                </div>

            
        </div>	

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>
        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->

        <?php $this->load->view('include/footer'); ?>
		<?php $this->load->view('include/datatablejs'); ?>
		<style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
        </style>
        <script type="text/javascript">
           
            $(document).ready(function () {
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            });
            
            var table;

            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.

                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo site_url('report/financialreportAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                          

                            // data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                        },
                        //"data": {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>','employeeId' : $('#employeeId').val()},
                    },
                    "dom": 'lBfrtip',
                    "buttons": [
                        {
                            extend: 'collection',
                            text: 'Export',
                            buttons: [
                                'copy',
                                'excel',
                                'csv',
                                'pdf',
                                'print'
                            ]

                        }
                    ],

                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

                });

                var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"

                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });

                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });

            });

        </script>

    </div>
</body>
</html>
