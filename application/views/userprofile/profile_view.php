<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$disAbl = 'readonly="readonly"';
$skillArrlv = array("3" => "Medium", "2" => "Expert", "1" => "BASIC");
$genderidArr = array("1" => "Male", "2" => "Female", "3" => "Other");
?>

<body class="theme-cyan">
    <div id="wrapper">
      <?php
        $this->load->view('admin/includes/sidebar');
      ?>

        <div id="main-content" class="profilepage_1">
            <div class="container-fluid">
                <div class="block-header">

                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>            
                        
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <ul class="nav nav-tabs">                                
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#official">Official</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#personaldetailsview">Personal</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#contactview">Contact</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#experience">Experience</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#skills">Skills</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#educationdetails">Education</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#familydetails">Family</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#documentsdetails">Documents</a></li>
                                </ul>
                            </div>
                            <!-- Data -->
                            <div class="tab-content">

                                <div class="body"> 
                                    <div class="media">
                                        <div class="media-left m-r-15">
                                            <?php if ($loginUserRecd->profileimg) { ?>
                                                <img id="blah" height="105" width="100" src="<?= EMPLPROFILE . $loginUserRecd->profileimg; ?>" class="rounded-circle" alt="">
                                            <?php } ?>
                                        </div> 
                                        <div class="media-body">
                                            <span style="color:red" id="uploaderror2"> File Upload Error </span>
                                            <p>
                                                <br><em id=""> </em>
                                            </p>
                                            <button type="button" class="btn btn-default-dark" id="btn-upload-photo">Upload Photo</button>
                                            <input type="file" id="filePhoto" class="sr-only">
                                        </div>
                                    </div>
                                </div>

                                <!-- Start Professional -->
                                <div class="tab-pane active" id="official">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-official", compact("ofcRecArr", "disAbl")); ?>
                                    </div>
                                </div>
                                <!-- Close Profile Official -->
                                <!-- Start Personal -->
                                <div class="tab-pane" id="personaldetailsview">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-personal", compact("PersonalDetailRecArr", "disAbl", "genderidArr")); ?>
                                    </div>
                                </div>
                                <!-- Close Personal -->
                                <!-- Start Professional -->
                                <div class="tab-pane" id="experience">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-experience", compact("EmplExprRecArr", "disAbl")); ?>
                                    </div>
                                </div>
                                <!-- Close Profile Official -->
                                <!-- Start Contact Details -->
                                <div class="tab-pane" id="contactview">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-contact", compact("ContactDetailsRecArr", "disAbl")); ?>
                                    </div>
                                </div>
                                <!-- Close Contact Details -->
                                <!-- Start Skills -->
                                <div class="tab-pane" id="skills">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-skill", compact("EmplSkillRecArr", "disAbl", "skillArrlv")); ?>
                                    </div>
                                </div>
                                <!-- Close Skills -->
                                <!-- Start Educational -->
                                <div class="tab-pane" id="educationdetails">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-education", compact("EduDetailsRecArr", "disAbl")); ?> 
                                    </div>
                                </div>
                                <!-- Close Educational -->
                                <!-- Start Family Details -->
                                <div class="tab-pane" id="familydetails">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-family", compact("FamilyDetailsRecArr", "disAbl")); ?> 
                                    </div>
                                </div>
                                <!-- Close Family Details -->
                                <!-- Start Documents Details Section-->
                                <div class="tab-pane" id="documentsdetails">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.No.</th>
                                                        <th>Doc Name</th>
                                                        <th>Doc Download</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($EmployeeDocsDetailRec) {
                                                        foreach ($EmployeeDocsDetailRec as $kEy => $recD) {
                                                            if ($recD->attachments) {
                                                                $docsArray = json_decode($recD->attachments);
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td><?= $kEy + 1; ?></td>
                                                                <td><?= (@$recD->name) ? @$recD->name : ""; ?></td>
                                                                <td>
                                                                    <?php
                                                                    echo (@$docsArray[0]->new_name) ? "<a target='_blank' href='" . EMPLOYEEDOCS . $docsArray[0]->new_name . "'> View / Download </a>" : "";
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <tr>
                                                            <td style="color:red" colspan="3"> Record Not Found. </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- Close Documents Details Section-->


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').removeAttr('src');
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function () {
        $("#uploaderror2").hide();
        $("#uploaderror3").hide();

        $('INPUT[type="file"]').change(function () {
            var ext = this.value.match(/\.(.+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'JPG':
                case 'jpeg':
                case 'png':
                case 'PNG':
                    $('#uploadButton').attr('disabled', false);
                    $("#uploaderror2").hide();
                    break;
                default:
                    $("#uploaderror2").show();
                    $("#uploaderror2").html('This is not an allowed file type.');
                    this.value = '';
            }
        });
        $('INPUT[type="file"]').bind('change', function () {
            var imgfilesize = (this.files[0].size / 1020);
            if (imgfilesize > 577) {
                $("#uploaderror2").show();
                $("#uploaderror2").html('Reducing File Size Max Uploads (500 KB). ');
                this.value = '';
            } else {
                $("#uploaderror2").hide();
                readURL(this);
            }
        });
    });

    $(function () {
        // photo upload
        $('#btn-upload-photo').on('click', function () {
            $(this).siblings('#filePhoto').trigger('click');
        });
        // plans
        $('.btn-choose-plan').on('click', function () {
            $('.plan').removeClass('selected-plan');
            $('.plan-title span').find('i').remove();
            $(this).parent().addClass('selected-plan');
            $(this).parent().find('.plan-title').append('<span><i class="fa fa-check-circle"></i></span>');
        });
    });

</script>

<link rel="stylesheet" href="<?= FRONTASSETS; ?>jquery-ui.css">
<script src="<?= FRONTASSETS; ?>jquery-ui.js"></script>
<script src='<?= FRONTASSETS; ?>select2/dist/js/select2.min.js' type='text/javascript'></script>
<link href='<?= FRONTASSETS; ?>select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>