<div class="row clearfix">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No.</th>
                <th>Skill Name</th>
                <th>Years Experience</th>
                <th>Competency Level</th>
                <th>Last Use</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($EmplSkillRecArr) {
                foreach ($EmplSkillRecArr as $kEy => $recD) {
                    ?>
                    <tr>
                        <td><?= $kEy + 1; ?></td>
                        <td><?= ($recD->skillname) ? $recD->skillname : ""; ?></td>
                        <td><?= ($recD->yearsofexp) ? $recD->yearsofexp : ""; ?></td>
                        <td><?= ($recD->competencylevelid) ? $skillArrlv[$recD->competencylevelid] : ""; ?></td>
                        <td><?= ($recD->year_skill_last_used) ? date("d-m-Y", strtotime($recD->year_skill_last_used)) : ""; ?></td>
                    </tr>
                    <?php
                }
            } else { ?>
                <tr>
                    <td style="color:red" colspan="5"> Record Not Found. </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>