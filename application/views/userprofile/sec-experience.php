<div class="row clearfix">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No.</th>
                <th>Company Name</th>
                <th>Company Website</th>
                <th>Designation</th>
                <th>From</th>
                <th>To</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($EmplExprRecArr) {
                foreach ($EmplExprRecArr as $kEy => $recD) {
                    ?>
                    <tr>
                        <td><?= $kEy + 1; ?></td>
                        <td><?= ($recD->comp_name) ? $recD->comp_name : ""; ?></td>
                        <td><?= ($recD->comp_website) ? $recD->comp_website : ""; ?></td>
                        <td><?= ($recD->designation) ? $recD->designation : ""; ?></td>
                        <td><?= ($recD->from_date) ? date("d-m-Y", strtotime($recD->from_date)) : ""; ?></td>
                        <td><?= ($recD->to_date) ? date("d-m-Y", strtotime($recD->to_date)) : ""; ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td style="color:red" colspan="6"> Record Not Found. </td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>