<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */


//################# HRMS Route ##########################
//******************** 07-01-2020 Ash *******************
//Basic Controller
$route['default_controller'] = 'admin';
$route['404_override'] = '';
$route['userdashboard'] = 'userdashboard/home';
$route['logout'] = 'userdashboard/logout';
//User Profile
$route['myprofile'] = 'userProfile_control/myprofile';
$route['mypayslip'] = 'Account_controller/mypayslip';
$route['mysalarydetails'] = 'Account_controller/mysalarydetails';
$route['payslips_data_ajax'] = 'Account_controller/payslips_data_ajax';
$route['myformitr'] = 'Account_controller/myformitr';
$route['myteam'] = 'userProfile_control/myteam';
$route['myleave'] = 'userProfile_control/myleave';
// $route['myholidays'] = 'userProfile_control/myholidays';
$route['myholidays'] = 'userProfile_control/myholidays';
//Visiting Card
$route['applyvisitingcard'] = 'Visiting_card_Controller/applyvisitingcard';
$route['update_visiting_card_rqst/(:num)'] = 'Visiting_card_Controller/update_visiting_card_rqst/$1';
$route['update_visiting_card_rqst_reject/(:num)'] = 'Visiting_card_Controller/update_visiting_card_rqst_reject/$1';
$route['update_visiting_card_rqst_hr/(:num)'] = 'Visiting_card_Controller/update_visiting_card_rqst_hr/$1';
//Reissue ID card
$route['reissue_id_card'] = 'Reissue_id_card_Controller/reissue_id_card';
//Joining report
$route['joining_report_data'] = 'Joining_report_Controller/joining_report_data';
//HR Reports

$route['index123'] = 'Dashboard/index';
$route['add_tender'] = 'Tender/add_tender_new';
$route['tndrinfocsv'] = 'Tender/tndrinfocsv';
$route['tndrtwentyfourcsv'] = 'Tender/tndrtwentyfourcsv';
$route['newProject'] = 'Newproject/index';
$route['activeProject'] = 'Activeproject/active_project';
$route['imp_Project'] = 'Importantproject/index';
$route['review_Project'] = 'Reviewproject/index';

// TO Go Project
$route['onGoingEoiProject'] = 'Togoproject/ongoingeoiproject';
$route['ongoingrfpproject'] = 'Togoproject/ongoingrfpproject';
$route['ongoingfqproject'] = 'Togoproject/ongoingfqproject';
$route['indexAll'] = 'Togoproject/index';
// Submitted Project
$route['submittedeoiproject'] = 'Bidproject/submittedeoiproject';
$route['submittedrfpproject'] = 'Bidproject/submittedrfpproject';
$route['teamRequisition'] = 'Teamrequisition/index';
$route['teamCru'] = 'Cru/index';
$route['orderBookingReports'] = 'report/financial_yearwisereport';
$route['employeeList'] = 'mastercontrol/employeeList';



//Routes created by Gaurav 31032020
$route['financial_yearwisereport'] = 'report/financial_yearwisereport';
$route['crureport'] = 'report/crureport';
$route['educationalreport'] = 'Bidproject/educationalreport';
$route['projectreport'] = 'Bidproject/projectreport';

$route['bidnewreport'] = 'Bidproject/bidnewreport';
$route['financialreport'] = 'report/financialreport';
$route['technicalreport'] = 'report/technicalreport';
$route['marksreport'] = 'report/marksreport';

$route['shortListedEoi'] = 'report/shortListedEoi';




