<?php

/* Database connection start */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Teamrequisition extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->model('Teamrequisition_model', 'teamrequisition');
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
        date_default_timezone_set('Asia/Kolkata');

      
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('loginid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";

        if ($actual_link == "http://localhost/myhrms_bd/") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }

     

    }

    //Dashbord After Login..

    


    public function index() {
        $title = 'Teamrequisition Details';
        $reccc = $this->teamrequisition->get_datatables_new();
        foreach ($reccc as $itemk):
            $this->mastermodel->UpdateRecords('assign_project_manager', array('project_id' => $itemk->project_id), array('proj_status' => '3'));
        endforeach;
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $proposalManager = $this->Front_model->allprojectProposalManager();
        //$DesignationArr = $this->Front_model->allActiveDesignation();
        $SectorDesigArr = $this->Front_model->allActiveDesignationSector();
        $this->load->view('teamrequisition/tender_view', compact('title', 'secId', 'sectorArr', 'proposalManager', 'DesignationArr', 'SectorDesigArr'));
    }





    public function newProjectAll() {
    

        $list = $this->teamrequisition->get_datatables();
        //print_r($this->db->last_query()); die;
        

        $data = array();
        $no = $_POST['start'];
        $link = '';

        
         
        foreach ($list as $togoproject) {
            //if ($togoproject->proj_status != 3) {
               if ($togoproject->proj_status == 3) {
 


                    $satus = '<button  style="cursor:pointer" onclick="setnotrequired(' . "'" . $togoproject->fld_id . "'" . ')" title="Not Required" class="btn btn-danger mt-3 js-sweetalert" ><i class="fa fa-hard-of-hearing"></i> Not Required </button>';
                    $link = '';

					$tenderIdDetail = '<span class="label-success label label-default">' . $togoproject->generated_tenderid . '</span>';
					$teamClone = '';
                } else if ($togoproject->proj_status == 0) {
                    $satus = '<span class="alert alert-warning"><i class="fa fa-ban "></i> Pending </span><br/>  ';
                    
                    $link = '';
                }  else if ($togoproject->proj_status == 1) {
                    $name = $this->SecondDB_model->getUserByID($togoproject->filled_by);
                    $satus = '<div class="alert alert-success"><i class="fa fa-info"></i> Filled By ' . $name->userfullname . '  </div>';
                    $link = '<a href="' . base_url('teamrequisition/teamrequisition_view?pn=' . $togoproject->fld_id) . '" class="btn btn-info btn-sm" target="_blank"> <i class="fa fa-info-circle"></i> View Team</a>';
                } else {
                    $satus = '<span class="label-success label label-default"> Submitted </span>';
                    $link = '';

                   
                
                }


                if($togoproject->proj_status !== 3){
                     $tenderIdDetail = '<div type="button" class="btn btn-info btn-sm mt-2" onclick="assignteam(' . "'" . $togoproject->fld_id . "','" . $togoproject->assign_to . "','" . $togoproject->generated_tenderid . "'" . ')" data-toggle="modal"   data-target="#assignteam"> <i class="fa fa-globe"></i> ' . $togoproject->generated_tenderid . '</div>';
                    $teamClone = '<div type="button" class="btn btn-info btn-sm mt-2" onclick="teamclone(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#teamclone"> <i class="fa fa-copy"></i> Team Clone </div>';


                }
                
                // if ($togoproject->proj_status == 3):
                //     $row[] = '<span class="label-success label label-default">' . $togoproject->generated_tenderid . '</span>';
                //     $row[] = '';
                // else:
                //     $row[] = '<button type="button" class="btn btn-info" onclick="assignteam(' . "'" . $togoproject->fld_id . "','" . $togoproject->assign_to . "','" . $togoproject->generated_tenderid . "'" . ')" data-toggle="collapse" data-target="#assignteam">' . $togoproject->generated_tenderid . '</button>';
                //     $row[] = '<button type="button" class="btn btn-info" onclick="teamclone(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="collapse" data-target="#teamclone"> Team Clone </button>';
                // endif;






                $no++;
                $row = array();
                $row[] = $no;
                
               
                  


 				//$names = $this->SecondDB_model->getUserByID($togoproject->assign_to);

                $row[] = ucFirst($togoproject->TenderDetails) ."<br/>". $tenderIdDetail ." ". $teamClone;
                $row[] = $togoproject->Location;
                $row[] = $togoproject->Organization;
               
                $row[] = $names->userfullname;
                $row[] = $satus;
                $row[] = $link;
               	$row[] = 12; //$view1 . $view2 . $view3;
                $data[] = $row;
               
            //}
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->teamrequisition->count_filtered(),
            "recordsFiltered" => $this->teamrequisition->count_filtered(),
            "data" => $data,
        );
        //output to json format

        echo json_encode($output);
    }

    //Function Copy for test..
    public function newProjectAll_test() {
        $list = $this->teamrequisition->get_datatables();
        
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $link = '';
        foreach ($list as $togoproject) {
            if ($togoproject->proj_status == 0) {
                $satus = '<span class="label-warning label label-default"> Pending </span>';
                $link = '';
            } elseif ($togoproject->proj_status == 1) {
                $name = $this->Front_model->UserNameById($togoproject->filled_by);
                $satus = '<span class="label-warning label label-default"> Filled By ' . $name . ' </span>';
                $link = '<a href="' . base_url('teamrequisition/teamrequisition_view?pn=' . $togoproject->project_id) . '" class="btn btn-info" target="_blank">View Team</a>';
            } else {
                $satus = '<span class="label-success label label-default"> Submitted </span>';
                $link = '';
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $togoproject->TenderDetails;
            $row[] = '<button type="button" class="btn btn-info" onclick="assignteam(' . "'" . $togoproject->project_id . "'" . ')" data-toggle="collapse" data-target="#assignteam">' . $togoproject->generated_tenderid . '</button>';
            $row[] = $togoproject->Location;
            $row[] = $togoproject->Organization;
            if ($bdRole != 1) {
                $row[] = $togoproject->userfullname;
            }
            $row[] = $satus;
            $row[] = $link;
            $row[] = $view1 . $view2 . $view3;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->teamrequisition->count_all(),
            "recordsFiltered" => $this->teamrequisition->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('loginid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('loginid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

    //Update or Insert Req Data...
    public function savereqprojectdata() {
        $data = $_REQUEST;
        for ($i = 0; $count = count($data['team_id']), $i < $count; $i++) {
            $this->db->select('id,lockdata');
            $this->db->from('team_assign');
            $this->db->where('id', $data['team_id'][$i]);
            $res = $this->db->get()->result();
            if ($res[0]->lockdata != '1') {
                $updaterecords = array(
                    'age_limit' => $data['age_limit'][$i],
                    'man_months' => $data['man_months'][$i],
                    'single_comment' => $data['single_comment'][$i],
                    'weightage_marks_rfp' => $data['weightage_marks_rfp'][$i],
                    // 'om_months' => $data['om_months'][$i],
                    'comment' => $data['comment']
                );
                $wheres = array('id' => $data['team_id'][$i]);
                $updateID = $this->mastermodel->UpdateRecords('team_assign', $wheres, $updaterecords);
            }
        }
        if ($data['url_chk'] == '1') {
            redirect(base_url('/teamrequisition/teamrequisition_view?pn=' . $data['project_id']));
        }
    }

    public function deleteassignteam() {
        $id = $this->uri->segment(3);
        $projid = $this->uri->segment(4);
        if ($id) {
            $updaterecords = array(
                'status' => '0',
                'deletedby' => $this->session->userdata('loginid')
            );
            $wheres = array('id' => $id);
            $updateID = $this->mastermodel->UpdateRecords('team_assign', $wheres, $updaterecords);
        }

        // echo $projid; die;

        redirect(base_url('/teamrequisition/teamrequisition_view?pn=' . $projid));
    }

    //export Complete Project Code Edited By Asheesh
    public function exportTeam() {
        if (!empty($_POST['project_id'])) {
            $this->db->select('a.*,b.designation_name,c.TenderDetails,d.generated_tenderid');
            $this->db->from('team_assign as a');
            $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'left');
            $this->db->join('bd_tenderdetail as c', 'a.project_id = c.fld_id', 'left');
            $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
            $this->db->where('a.project_id', $_POST['project_id']);
            $query = $this->db->get();
            if ($query->num_rows() != 0) {
                $resulstArr = $query->result_array();
                $filename = 'team' . $_POST['project_id'] . '_' . date('Y-m-d') . ".csv";
                $fp = fopen('php://output', 'w');
                $fields = array('Sr.No.', 'Designation', 'Age Limit', 'Total Man Months', 'Weightage Marks (RFP)', 'Comments');
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $filename . '";');
                fputcsv($fp, $fields, ',');
                $i = 1;
                foreach ($resulstArr as $row) {
                    $lineData = array($i, $row['designation_name'], $row['age_limit'], $row['man_months'], $row['weightage_marks_rfp'], $row['single_comment']);
                    $i++;
                    fputcsv($fp, $lineData, ',');
                }
            }
        }
    }

    public function getProject() {
        if ($_REQUEST['year']) {
            $financial_year = $_REQUEST['year'];
            $start_date = $financial_year . '-04-01';
            $end_date = ($financial_year + 1) . '-03-31';
            $where_date = "(a.entrydate>='$start_date' AND a.entrydate <= '$end_date')";
            $this->db->select('a.generated_tenderid,b.lockdata,b.project_id');
            $this->db->from('tender_generated_id as a');
            $this->db->join('team_assign as b', 'a.project_id = b.project_id', 'left');
            $this->db->where('b.lockdata', '1');
            $this->db->where($where_date);
            $this->db->group_by('b.project_id');

            $Rec = $this->db->get()->result_array();
            if ($Rec) {
                //$ress .= "<option value='" . $res['project_id'] . "'>" . $res['designation_name'] . "</option>";
                foreach ($Rec as $res) {
                    $ress .= "<option value='" . $res['project_id'] . "'>" . $res['generated_tenderid'] . "</option>";
                }
                echo $ress;
            }
        }
        return false;
    }

    public function cloneteamreq() {
        $copyfrom = $_REQUEST['project_copy'];
        $copyto = $_REQUEST['teamprojid'];
        if (!empty($copyfrom) && !empty($copyto)) {
            $this->db->select('*');
            $this->db->from('team_assign');
            $this->db->where('project_id', $copyfrom);
            $this->db->where('status', 1);
            $res = $this->db->get()->result();
            if (!empty($res)) {
                foreach ($res as $val) {
                    $inserArr = array(
                        'project_id' => $copyto,
                        'proposal_mngr_id' => $val->proposal_mngr_id,
                        'designation_id' => $val->designation_id,
                        'age_limit' => $val->age_limit,
                        'man_months' => $val->man_months,
                        'filled_by_id' => $val->filled_by_id,
                        'single_comment' => $val->single_comment,
                        'weightage_marks_rfp' => $val->weightage_marks_rfp,
                        'comment' => $val->comment,
                        'deletedby' => $val->deletedby,
                        'status' => 1
                    );
                    $Respon = $this->Front_model->insertRecord('team_assign', $inserArr);
                }
                $this->session->set_flashdata('msg', "Data Insert Success.");
            } else {
                $this->session->set_flashdata('msg', "Something Went Wrong !!!.");
            }
        } else {
            $this->session->set_flashdata('msg', "Something Went Wrong !!!.");
        }
        redirect(base_url('/teamrequisition'));
    }

    //Set Not Required.. Code By Asheesh
    public function setnotreq($recid) {
        $updateID = $this->Front_model->updateRecord('assign_project_manager', array('proj_status' => '3'), array('fld_id' => $recid));
        $this->session->set_flashdata('msg', 'Team Not Required Set successfully');
        redirect(base_url('teamrequisition'));
    }

    //Function Edited By Asheesh 12 Jan
    public function teamrequisition_view() {
        $title = 'Teamrequisition Team Details';
        $LastUpdSendMail = '';
        $this->db->SELECT('a.*,b.designation_name,c.TenderDetails,c.Expiry_Date,d.generated_tenderid');
        $this->db->from('team_assign as a');
        $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'left');
        $this->db->join('bd_tenderdetail as c', 'a.project_id = c.fld_id', 'left');
        $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
        $this->db->where('a.status', '1');
        $this->db->where('a.project_id', $_REQUEST['pn']);
        $query = $this->db->get();
        $bdRole = $this->Front_model->bd_rolecheck();

        //Start Last Mail/Lock Updated Date Get By Asheesh... 27-06-2019.
        $this->db->SELECT('at.modified');
        $this->db->from('team_assign as at');
        $this->db->where(array('at.status' => '1', 'at.lockdata' => '1', 'at.project_id' => $_REQUEST['pn']));
        $UpdLockDateRec = $this->db->get()->row();
        if ($UpdLockDateRec) {
            $LastUpdSendMail = date("d-m-Y H:i:s ", strtotime($UpdLockDateRec->modified));
        }
        //Close Last Mail/Lock Updated Date Get By Asheesh... 27-06-2019.
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            $resulstArr[0]['project_id'] = $_REQUEST['pn'];
        }
        //$DesignationArr = $this->Front_model->allActiveDesignation();
        $SectorDesigArr = $this->Front_model->allActiveDesignationSector();

        if ($_REQUEST['sendmail'] and ( $_REQUEST['sendsubmit_empbd'])):
            $IDrecArr = $this->mastermodel->prop_manager_AllIDs();
            $msg = $this->load->view("email_template/teamreq_mail_for_propManager", compact('LastUpdSendMail', 'resulstArr', 'DesignationArr', 'SectorDesigArr'), true);
            $mailList1 = array();
            foreach ($IDrecArr as $mailerow) {
                if ($mailerow['useremail']):
                    array_push($mailList1, $mailerow['useremail']);
                endif;
            }
            $emailArrStr = 'bdcru@cegindia.com';
             // $emailArrStr = 'ts.admin@cegindia.com';
            sendMail("$emailArrStr", 'New Team Requisition', $msg);
            foreach ($resulstArr as $vals) {
                $updateID = $this->mastermodel->UpdateRecords('team_assign', array('id' => $vals['id']), array('modified' => date("Y-m-d H:i:s", time()), 'lockdata' => 1));
            }
            $this->session->set_flashdata('msg', 'Team Requisition Mail Sent successfully');
            redirect(thisurl());
        endif;
        $BdProjectID = $_REQUEST['pn'];
        $this->load->view('/teamrequisition/teamrequisition_view', compact('BdProjectID', 'LastUpdSendMail', 'title', 'resulstArr', 'DesignationArr', 'SectorDesigArr'));
    }

    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //################ 08-08-2019 Code Modified For Metro Modules Code By Asheesh.. ##########
    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


    public function setdesignationcateg_ajax($sectID) {
        if ($sectID == "3") {
            $conWhere = array("status" => "1", "parent_secid" => "2");
        } else {
            $conWhere = array("status" => "1", "parent_secid" => "1");
        }
        $this->db->select('k_id,ktype_name');
        $this->db->from('designationcat_master');
        $this->db->where($conWhere);
        $this->db->order_by("k_id", "ASC");
        $result = $this->db->get()->result();
        echo json_encode($result);
        exit();
    }

    //Add New Designation
    public function addnewdesignation() {
        $designation = $_REQUEST['designation'];
        $this->db->select('fld_id');
        $this->db->from('designation_master_requisition');
        $this->db->where(array("is_active" => "1", "designation_name" => $designation, 'sector_id' => $_REQUEST['sector_id'], 'cat_id' => $_REQUEST['addcat_ids']));
        $NumRow = $this->db->get()->num_rows();
        if ($NumRow > 0) {
            $this->session->set_flashdata('error_msg', 'This Designation name already exists ');
        }
        if ($designation and ( $NumRow < 1)):
            $insertArr = array('designation_name' => $designation, 'sector_id' => $_REQUEST['sector_id'], 'cat_id' => $_REQUEST['addcat_ids']);
            $response = $this->Front_model->insertRecord('designation_master_requisition', $insertArr);
            $this->session->set_flashdata('msg', 'New Designation Added Successfully');
        endif;
        redirect(base_url("teamrequisition"));
    }

    //Get Designation Sector and Cate/Key wise...
    public function setdesignationcateg_teamaddajax() {
        $sectrID_id = $_REQUEST['sectrID_id'];
        $categrID_id = $_REQUEST['categrID_id'];
        if ($sectrID_id and $categrID_id) {
            $this->db->select('fld_id,designation_name');
            $this->db->from('designation_master_requisition');
            $this->db->where(array("is_active" => "1", "sector_id" => $sectrID_id, 'cat_id' => $categrID_id));
            $this->db->order_by("designation_name", "ASC");
            $this->db->group_by("designation_name");
            $resultRec = $this->db->get()->result();
            echo json_encode($resultRec);
            exit();
        } else {
            return null;
        }
    }

    //Add Team Req
    public function assignteamreq() {
        $employeeID = $this->session->userdata('loginid');
        if (!empty($_POST)) {
            $inserArr = array('project_id' => $_POST['project_id'],
                'proposal_mngr_id' => $_REQUEST['assignpropmangrid'],
                'designation_id' => $_POST['designation_ofsec'],
                'cat_id' => $_POST['cat_ids'],
                'age_limit' => $_POST['age_limit'],
                'man_months' => $_POST['man_months'],
                'weightage_marks_rfp' => $_POST['weightage_marks_rfp'],
                'single_comment' => $_POST['single_comment'],
                'filled_by_id' => $employeeID,
                'proj_positioid_metro' => ($_POST['proj_position_idmetro']) ? $_POST['proj_position_idmetro'] : '',
                'status' => '1');

                

            $Respon = $this->Front_model->insertRecord('team_assign', $inserArr);

            

            
            $memberArr = array(
                'proj_status' => '1',
                'filled_by' => $employeeID,
            );
            $where = array('project_id' => $_POST['project_id']);
            $updateID = $this->Front_model->updateRecord('assign_project_manager', $memberArr, $where);
        }

       $message = "Some issues occured ! Please try later";


        if ($_POST['url_chk'] == '1') {
            //$this->session->set_flashdata('msg', "Team Requisition Added Success.");
           // redirect(base_url('/teamrequisition/teamrequisition_view?pn=' . $_POST['project_id']));
           $message = "Team Requisition Added Success.";
        } else {
           // $this->session->set_flashdata('msg', "Team Requisition Added Success.");
            //redirect(base_url('/teamrequisition'));
            $message = "Team Requisition Added Success.";
        }


        $output = array(
             
            "msg" => $message,
        );
        //output to json format
        echo json_encode($output);


    }

}
