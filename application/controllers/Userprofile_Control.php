<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userprofile_Control extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->library('form_validation');
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }

    public function myprofile() {
        $id = $this->session->userdata('loginid');
        $data['loginUserRecd'] = $this->mastermodel->GetBasicRecLoginUser();
        $data['ofcRecArr'] = $this->mastermodel->GetOfficialDataByUserId($id);
        $data['EmplExprRecArr'] = $this->mastermodel->GetExperianceDetailByID($id);
        $data['EmplSkillRecArr'] = $this->mastermodel->GetSkillRecordDetailByID($id);
        $data['PersonalDetailRecArr'] = $this->mastermodel->GetPersonalDetailsRecByID($id);
        $data['ContactDetailsRecArr'] = $this->mastermodel->GetContactDetailRecByID($id);
        $data['EduDetailsRecArr'] = $this->mastermodel->GetEducationDetailRecByID($id);
        $data['FamilyDetailsRecArr'] = $this->mastermodel->GetFamilyDetailRecByID($id);
        $data['EmployeeDocsDetailRec'] = $this->mastermodel->GetEmployeeDocsDetailRecByID($id);
        $data['title'] = "My Profile";
        $this->load->view("userprofile/profile_view", $data);
    }

    //My Team Details..
    public function myteam() {
        $data['error'] = '';
        $id = $this->session->userdata('loginid');
        $data['myTeamDetailsRecArr'] = $this->mastermodel->GetMyTeamDetailsRecByID($id);
        $data['title'] = "My Team Details";
        $this->load->view("self-services/myteam_list_view", $data);
    }

    //User Profile My Leave..
    public function myleave() {
        $data['error'] = '';
        $data['title'] = "My Leave Details";
        $id = $this->session->userdata('loginid');
        $data['myallAppliedLeaveRecArr'] = $this->mastermodel->GetMyAllAppliedLeaves($id);
        $this->load->view("self-services/myleave_list_view", $data);
    }

    //My Holidays Section...
    public function myholidays() {
        $data['error'] = '';
        $data['title'] = "My Holidays Details";
        $hgroupID = '';
        $hyear = date("Y");
        if (@$_REQUEST['filter']) {
            $hgroupID = $_REQUEST['holidaystype'];
            $hyear = $_REQUEST['holidaysyear'];
            $data['holidaystype'] = $hgroupID;
            $data['holidaysyear'] = $hyear;
            $data['HolidaysRecArr'] = $this->mastermodel->GetAllHolidaysListRec($hgroupID, $hyear);
        } else {
            $data['HolidaysRecArr'] = $this->mastermodel->GetAllHolidaysListRec($hgroupID, $hyear);
        }
        $data['holidaysyear'] = $hyear;
        $this->load->view("self-services/myholidays_list_view", $data);
    }

    //Apply Tour Details..
    public function applytour() {
        $data['error'] = '';
        $data['title'] = "My Tour Details";
        $id = $this->session->userdata('loginid');
        //$id = '195';
        $data['AppliedTourRecArr'] = $this->mastermodel->GetAllAppliedTourListRec($id);

        $this->load->view("self-services/applytour_list_view", $data);
    }

}
