<?php

/* Database connection start This Controller Create By Asheesh */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
//error_reporting(E_ALL);
class Report extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Front_model');
        $this->load->model('Mastermodel');
        $this->load->model('Report_model', 'activeproject');
        $this->load->model('shortListedEoi_model', 'shortListedEoi');
      
        $this->load->model('Crureport_model', 'crureport');
        $this->load->helper(array('form', 'url'));
        $this->load->library('session', 'form_validation');
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
        // $this->db1 = $this->load->database('online', TRUE);
        // $this->db2 = $this->load->database('another_db', TRUE);
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $db1 = $this->db1->database;

        $this->db2 = $this->load->database('another_db', TRUE);
        $db2 = $this->db2->database;

        $this->db = $this->load->database('another_db', TRUE);
        $this->db = $this->db2->database;
        
    }

    // public function financialreportAll() {
    //     $list = $this->activeproject->get_datatables();
    //     //echo '<pre>'; print_r($list); die;
    //     $data = array();
    //     $no = $_POST['start'];
    //     foreach ($list as $activeproject) {
    //         if ($activeproject->generate_type == 'P') {
    //             $satus = 'RFP';
    //         } else {
    //             $satus = 'FQ';
    //         }
    //         $no++;
    //         $row = array();
    //         $row[] = $no;
    //         $row[] = $activeproject->project_name;
    //         $row[] = $activeproject->service;
    //         $row[] = $activeproject->client;
    //         $row[] = $activeproject->proj_status;
    //         $row[] = $satus;
    //         /* $row[] = $activeproject->start_year; */
    //         $row[] = $activeproject->sector;
    //         $row[] = $activeproject->state;
    //         $row[] = $activeproject->project_code;
    //         $row[] = '<input name="actchk[]" value="' . $activeproject->fld_id . '" type="checkbox">';
    //         $data[] = $row;
    //     }
    //     $output = array(
    //         "draw" => $_POST['draw'],
    //         "recordsTotal" => '0',
    //         "recordsFiltered" => $this->activeproject->count_filtered(),
    //         "data" => $data,
    //     );
    //     //output to json format
    //     echo json_encode($output);
    // }



    

     public function shortListedEoiReportAll() {
        $list = $this->shortListedEoi->get_datatables();
        //echo '<pre>'; print_r($list); die;
        



        $data = array();
        $no = $_POST['start'];
        foreach ($list as $activeproject) {
             

 

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $activeproject->TenderDetails;

            // print_r("expression"); die;
            $eoiTenderCode = $this->Mastermodel->SelectRecordFldNew('tender_generated_id',  array('project_id' => $activeproject->old_project_id, 'generate_type' => 'E'));


             

             //print_r($eoiTenderCode); die;
            

            // $row[] = $activeproject->old_project_id; //$eoiTenderCode->generated_tenderid;
            // $row[] = $activeproject->new_project_id;

            $row[] = '<div class="highlight" style="height: 40px">'.$eoiTenderCode[0]->generated_tenderid.'</div>';

            $rfpTenderCode = $this->Mastermodel->SelectRecordFldNew('tender_generated_id',  array('project_id' => $activeproject->new_project_id, 'generate_type' => 'P'));
             
            $row[] = '<div class="highlight" style="height: 40px">'.$rfpTenderCode[0]->generated_tenderid.'</div>';
           
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->shortListedEoi->count_filtered(),
            "recordsFiltered" => $this->shortListedEoi->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    public function financialreportAll() {
        $list = $this->activeproject->get_datatables();
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $activeproject) {
            if ($activeproject->generate_type == 'P') {
                $satus = 'RFP';
            } else {
                $satus = 'FQ';
            }


            //  if ($togoproject->project_status == 0) {
            //     $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Awaiting</div>';
  
            // } else if ($togoproject->project_status == 1) {
            //     $statusAlert = '<div class="alert alert-success"><i class="fa fa-info"></i> Won</div>';
                
            // } else if ($togoproject->project_status == 2) {
            //     $statusAlert = '<div class="alert alert-danger"><i class="fa fa-info"></i> Loose</div>';
                 
            // } else if ($togoproject->project_status == 3) {
            //     $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Cancel</div>';
                
            // }



            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $activeproject->project_name;
            $row[] = $activeproject->service;
            $row[] = $activeproject->client;
            if($activeproject->proj_status == "won"){
                $row[] = '<div class="alert alert-success"><i class="fa fa-info"></i> '.ucfirst($activeproject->proj_status).'</div>';
            }else{
                $row[] = '<div class="alert alert-warning"><i class="fa fa-info"></i> '.ucfirst($activeproject->proj_status).'</div>';
            }
            
            $row[] = $satus;
            /* $row[] = $activeproject->start_year; */
            $row[] = $activeproject->sector;
            $row[] = $activeproject->state;
            $row[] = '<div class="highlight">'.$activeproject->project_code.'</div>';
            $row[] = '<label class="fancy-checkbox mt-2"><input name="actchk[]" value="' . $activeproject->fld_id . '" type="checkbox"><span></span>
                                                                </label>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->activeproject->count_filtered(),
            "recordsFiltered" => $this->activeproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function financialreport() {
        $data['error'] = '';
        $data['title'] = 'Financial Report';
        $this->load->view('report/financialreport', $data);
    }

    public function financialchart() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
       

        $chkBoxArr = $this->input->post('actchk');
        //echo "gtest";
        //print_r($chkBoxArr); die;
        //print_r($this->db->last_query()); die;
        if (!empty($chkBoxArr)) {

            $this->db2->select('*');
            $this->db2->from('cegexp_competitor_comp');
            $this->db2->where_in('cegexp_id', $chkBoxArr);
            $res = $this->db2->get->result_array();

            //print_r($res->result_array()); die;



           
        }else{
            $res = array();
        }

        /* $abc = [
          ['Year', 'Sales', 'Expenses', 'Test'],
          ['2004',  1000,      400,     300],
          ['2005',  1170,      460, 260],
          ['2006',  660,       1120,112],
          ['2007',  1030,      540,520]
          ]; */
        //echo '<pre>';
        //$res = $query->result_array();
        $datas1 = array('company');
        foreach ($res as $result) {
            $datas[] = $result['cegexp_id'];
            $comID[] = $result['compt_comp_id'];
        }
        $proID = array_unique($datas);
        foreach ($proID as $val) {
            $projname = $this->activeproject->GetprojectNameById($val);
            $projDetail = $projname[0]->project_name;
            $datas1[] = $projDetail;
        }
        $com = array_unique($comID);
        $comcount = count($com);
        //print_r($com); die;
        foreach ($com as $com1) {
            //echo $com1; die;
            $compcompanyname = $this->mastermodel->GetCompNameById('74');
            print_r($compcompanyname); die;
            $competorcompany[] = array("label" => $compcompanyname);
        }
        $chkData = array();
        foreach ($proID as $val) {
            $projname = $this->activeproject->GetprojectNameById($val);
            $projDetail = $projname[0]->project_name;

            $score = array();
            foreach ($com as $val1) {
                $competior = $this->db2->query('select * from cegexp_competitor_comp where project_id=' . $val . ' and compt_comp_id=' . $val1);
                if ($competior->result_id->num_rows > 0) {
                    $res = $competior->result_object();
                    $financial_score = $res[0]->financial_score;
                    $score[] = array("value" => $financial_score);
                } else {
                    $score[] = array("value" => 0);
                }
            }
            $chkData[] = array("seriesname" => $projDetail, "data" => $score);
            // print_r($score); 
        }
        //echo '<pre>';
        if (!empty($chkBoxArr)) {
            $arr = array();
            $arrDatas = array();
            foreach ($chkBoxArr as $projval) {
                $Res = $this->db2->query('select * from cegexp_competitor_comp where project_id=' . $projval);
                if ($Res->result_id->num_rows > 0) {
                    $result = $Res->result_object();
                    $ids = $result[0]->project_id;
                    $projname = $this->activeproject->GetprojectNameById($ids);
                    $projDetails = $projname[0]->project_name;
                    $arr[] = array('projcode' => $result[0]->project_id, 'projname' => $projDetails);
                    $arrData = array();
                    foreach ($result as $value) {
                        $compcompanyname = $this->mastermodel->GetCompNameById($value->compt_comp_id);
                        $arrData[] = array('compcompanyname' => $compcompanyname, 'compt_comp_id' => $value->compt_comp_id, 'financial_score' => $value->financial_score);
                    }
                    $arrDatas[] = $arrData;
                }
            }
        }
        $this->load->view('report/financialreportchart', compact('arr', 'arrDatas', 'competorcompany', 'chkData', 'comcount', 'reportData'));
    }


    //technicalreport
    public function technicalreport() {
        $data['error'] = '';
        $data['title'] = 'Technical Report';
        $this->load->view('report/technicalreport', $data);
    }

    public function technicalchart() {
        $chkBoxArr = $this->input->post('actchk');
        if (!empty($chkBoxArr)) {
            $chkBoxArr1 = array("1", "9");
            $this->db->select('*');
            $this->db->from('cegexp_competitor_comp');
            $this->db->where_in('cegexp_id', $chkBoxArr);
            $query = $this->db->get();
        }
        ;
        $res = $query->result_array();
        $datas1 = array('company');
        foreach ($res as $result) {
            $datas[] = $result['cegexp_id'];
            $comID[] = $result['compt_comp_id'];
        }
        $proID = array_unique($datas);
        foreach ($proID as $val) {
            $projname = $this->activeproject->GetprojectNameById($val);
            $projDetail = $projname[0]->project_name;
            $datas1[] = $projDetail;
        }
        $com = array_unique($comID);
        $comcount = count($com);
        foreach ($com as $com1) {
            $compcompanyname = $this->mastermodel->GetCompNameById($com1);
            $competorcompany[] = array("label" => $compcompanyname);
        }
        $chkData = array();
        foreach ($proID as $val) {
            $projname = $this->activeproject->GetprojectNameById($val);
            $projDetail = $projname[0]->project_name;

            $score = array();
            foreach ($com as $val1) {
                $competior = $this->db->query('select * from cegexp_competitor_comp where project_id=' . $val . ' and compt_comp_id=' . $val1);
                if ($competior->result_id->num_rows > 0) {
                    $res = $competior->result_object();
                    $technical_score = $res[0]->technical_score;
                    $score[] = array("value" => $technical_score);
                } else {
                    $score[] = array("value" => 0);
                }
            }
            $chkData[] = array("seriesname" => $projDetail, "data" => $score);
        }


        if (!empty($chkBoxArr)) {
            $arr = array();
            $arrDatas = array();
            foreach ($chkBoxArr as $projval) {
                $Res = $this->db->query('select * from cegexp_competitor_comp where project_id=' . $projval);
                if ($Res->result_id->num_rows > 0) {
                    $result = $Res->result_object();
                    $ids = $result[0]->project_id;
                    $projname = $this->activeproject->GetprojectNameById($ids);
                    $projDetails = $projname[0]->project_name;
                    $arr[] = array('projcode' => $result[0]->project_id, 'projname' => $projDetails);
                    $arrData = array();
                    foreach ($result as $value) {
                        $compcompanyname = $this->mastermodel->GetCompNameById($value->compt_comp_id);
                        $arrData[] = array('compcompanyname' => $compcompanyname, 'compt_comp_id' => $value->compt_comp_id, 'technical_score' => $value->technical_score);
                    }
                    $arrDatas[] = $arrData;
                }
            }
        }


        $this->load->view('report/technicalreportchart', compact('comcount', 'arr', 'arrDatas', 'competorcompany', 'chkData'));
    }

    //marksreport
    public function marksreport() {
        $data['error'] = '';
        $data['title'] = 'Total Marks Compression Report';
        $this->load->view('report/marksreport', $data);
    }

     public function shortListedEoi() {
        $data['error'] = '';
        $data['title'] = 'Shortlisted EOI Report';
        $this->load->view('moved-eoi/shortListedEoi', $data);
    }

    public function markschart() {
        $chkBoxArr = $this->input->post('actchk');
        if (!empty($chkBoxArr)) {
            $this->db->select('*');
            $this->db->from('cegexp_competitor_comp');
            $this->db->where_in('cegexp_id', $chkBoxArr);
            $query = $this->db->get();
        }
        ;
        $res = $query->result_array();
        $datas1 = array('company');
        foreach ($res as $result) {
            $datas[] = $result['cegexp_id'];
            $comID[] = $result['compt_comp_id'];
        }
        $proID = array_unique($datas);
        foreach ($proID as $val) {
            $projname = $this->activeproject->GetprojectNameById($val);
            $projDetail = $projname[0]->project_name;
            $datas1[] = $projDetail;
        }
        $com = array_unique($comID);
        $comcount = count($com);
        foreach ($com as $com1) {
            $compcompanyname = $this->mastermodel->GetCompNameById($com1);
            $competorcompany[] = array("label" => $compcompanyname);
        }
        $chkData = array();
        foreach ($proID as $val) {
            $projname = $this->activeproject->GetprojectNameById($val);
            $projDetail = $projname[0]->project_name;

            $score = array();
            foreach ($com as $val1) {
                $competior = $this->db->query('select * from cegexp_competitor_comp where project_id=' . $val . ' and compt_comp_id=' . $val1);
                if ($competior->result_id->num_rows > 0) {
                    $res = $competior->result_object();
                    $total = $res[0]->total;
                    $score[] = array("value" => $total);
                } else {
                    $score[] = array("value" => 0);
                }
            }
            $chkData[] = array("seriesname" => $projDetail, "data" => $score);
        }


        if (!empty($chkBoxArr)) {
            $arr = array();
            $arrDatas = array();
            foreach ($chkBoxArr as $projval) {
                $Res = $this->db->query('select * from cegexp_competitor_comp where project_id=' . $projval);
                if ($Res->result_id->num_rows > 0) {
                    $result = $Res->result_object();
                    $ids = $result[0]->project_id;
                    $projname = $this->activeproject->GetprojectNameById($ids);
                    $projDetails = $projname[0]->project_name;
                    $arr[] = array('projcode' => $result[0]->project_id, 'projname' => $projDetails);
                    $arrData = array();
                    foreach ($result as $value) {
                        $compcompanyname = $this->mastermodel->GetCompNameById($value->compt_comp_id);
                        $arrData[] = array('compcompanyname' => $compcompanyname, 'compt_comp_id' => $value->compt_comp_id, 'total' => $value->total);
                    }
                    $arrDatas[] = $arrData;
                }
            }
        }
        $this->load->view('report/marksreportchart', compact('comcount', 'arr', 'arrDatas', 'competorcompany', 'chkData'));
    }

    public function crureport() {

        $list = $this->crureport->get_datatables();
        //print_r("expression"); die;
        $data['error'] = '';
        $title = 'Project Report';
        $this->load->view('report/crureport', compact('data', 'list', 'title'));
    }

    public function crureportAll() {
        $list = $this->crureport->get_datatables();
        /* echo '<pre>';
          print_r($list); die; */
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $crureport) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $crureport->emp_name;
            $row[] = $crureport->empname;
            $row[] = $crureport->age_limit;
            $row[] = $crureport->empnameother;
            $row[] = $crureport->userfullname;
            $row[] = $crureport->total_project;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->crureport->count_filtered(),
            "recordsFiltered" => $this->crureport->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

   public function getProjectDetailByUserID() {
        //echo $_REQUEST['id']; die;
        $res = $this->activeproject->getProjectDetailByUserID($_REQUEST['id']);
        echo json_encode($res);
    }

    public function getProjectDetailByUserIDother() {
        //echo $_REQUEST['id']; die;
        $res = $this->activeproject->getProjectDetailByUserIDother($_REQUEST['id']);

        echo json_encode($res);
    }

    public function updateProjID() {
        $query = $this->db->query('select * from tender_generated_id');
        $res = $query->result_object();
        foreach ($res as $val) {
            $updArr = array('project_id' => $val->project_id);
            $respupd = $this->mastermodel->UpdateRecords('ceg_exp', array('project_code' => $val->generated_tenderid), $updArr);
        }
    }

    public function insertProjDetail() {
        $query = $this->db->query('select * from ceg_exp WHERE  project_code IS NULL');
        $res = $query->result_object();
        $query1 = $this->db->query('select fld_id from bd_tenderdetail ORDER BY fld_id DESC LIMIT 1');
        $Lastid = $query1->result_object()[0]->fld_id + 1;
        foreach ($res as $val) {
            $updArr = array('project_id' => $Lastid);
            $respupd = $this->mastermodel->UpdateRecords('ceg_exp', array('fld_id' => $val->fld_id), $updArr);
            $insertArr = array('fld_id' => $Lastid, 'created_By' => '260', 'TenderDetails' => $val->project_name, 'Location' => $val->state, 'visible_scope' => 'Bid_project');
            $Rec = $this->Front_model->insertRecord('bd_tenderdetail', $insertArr);
            $insertArr1 = array('user_id' => '216', 'project_id' => $Lastid, 'project_status' => '1');
            $Rec1 = $this->Front_model->insertRecord('bid_project', $insertArr1);
            $Lastid ++;
        }
    }

    //#####################################################################
    //############################# DASHBORD ##############################
    //#####################################################################

     public function financial_yearwisereport() {



        $title = 'Financial Year Wise Report';
        $years = $_REQUEST['financial_year'];


        if (@($_REQUEST['submit']) and @ ($years)) {


            foreach ($years as $YerVal) {
                $returnArr = array();
                $Nextyear = $YerVal + 1;
                $fromDate = $YerVal . "-04-01";
                $ToDate = $Nextyear . "-03-31";
                $this->db2->select('a.bd_projid');
                $this->db2->from('proj_keydate_details as a');
                $where_date = "(a.date_latter_award>='$fromDate' AND a.date_latter_award <= '$ToDate')";
                $this->db2->where($where_date);
                $resultArr = $this->db2->get()->result();
                if ($resultArr) {
                    foreach ($resultArr as $reCd) {
                        $returnArr[] = $reCd->bd_projid;
                    }
                }
                $RecreturnArr[$YerVal] = $returnArr;
                // $RecFinancArr[$YerVal] = $this->GetFianlcialAmountTotal($RecreturnArr[$YerVal]);
                //$RecFinancArr[$YerVal] = $this->GetLeadJvAssoc($RecreturnArr[$YerVal]);
                $RecFinancArr[$YerVal] = $this->yearly_financial_report_new($YerVal);
            }
        }
        $this->load->view('report/project_financial_report_view', compact('title', 'RecreturnArr', 'years', 'RecFinancArr'));
    }


    public function GetFianlcialAmountTotal($Arr) {
        // $Arr = array('47105', '81819');
        $returnSumVal = 0;
        if (count($Arr) > 0) {
            $this->db->select("sum(value_as_per_loa) as value_loa");
            $this->db->from('proj_financial_details');
            $this->db->where_in('bd_projid', $Arr);
            $this->db->where(array("status" => "1"));
            $resultArr = $this->db->get()->row();
            return ($resultArr) ? number_format($resultArr->value_loa, 2) : "0";
        } else {
            return "";
        }
    }

//Get Jv Lead or Associative list...
    public function GetLeadJvAssoc($Arr) {
        if (count($Arr) > 0) {
            $this->db->select('sum(c.lead_amount) as lead, sum(c.jv_amount) as jv, sum(c.assoc_amount) as assoc');
            $this->db->from('jv_cegexp as b');
            $this->db->join('proj_financial_details as c', 'c.bd_projid=b.project_id', 'inner');
            $this->db->where_in('b.project_id', $Arr);
            $this->db->where(array("b.status" => "1"));
            $this->db->where("(b.lead_comp_id='74' OR b.joint_venture LIKE '%74%' OR b.asso_comp LIKE '%74%')", NULL, FALSE);
            $resultArr = $this->db->get()->result();
            foreach ($resultArr as $leadjvassoc) {
                return ($leadjvassoc) ? number_format((($leadjvassoc->lead) + ($leadjvassoc->jv) + ($leadjvassoc->assoc)), 2) : "0";
            }
        } else {
            return "";
        }
    }

//code by durgesh(12-09-2019)
    public function yearly_financial_report() {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $year = $this->uri->segment(3);
        $title = $year . ' Year Financial Report';
        $Nextyear = $year + 1;
        $fromDate = $year . "-04-01";
        $ToDate = $Nextyear . "-03-31";
        $this->db2->select("$db1.tm_projects.project_name,$db1.proj_keydate_details.bd_projid,$db2.proj_financial_details.value_as_per_loa");
        $this->db2->from("$db1.proj_keydate_details");
        $this->db2->join("$db2.proj_financial_details", "$db2.proj_financial_details.bd_projid=$db1.proj_keydate_details.bd_projid", "left");
        $this->db2->join("$db2.bdcegexp_proj_summery", "$db2.bdcegexp_proj_summery.project_id=$db1.proj_keydate_details.bd_projid", "left");
        $this->db2->join("$db1.tm_projects", "$db1.tm_projects.id=$db2.bdcegexp_proj_summery.project_numberid", "left");
        //$this->db->join("$db1.jv_cegexp","$db1.jv_cegexp.project_id=$db1.proj_keydate_details.bd_projid","left");
        $where_date = "($db1.proj_keydate_details.date_latter_award>='$fromDate' AND $db1.proj_keydate_details.date_latter_award <= '$ToDate')";
        $this->db2->where($where_date);
        //$this->db->where("($db1.jv_cegexp.lead_comp_id='74' OR $db1.jv_cegexp.joint_venture LIKE '%74%' OR $db1.jv_cegexp.asso_comp LIKE '%74%')", NULL, FALSE);
        $resultArr = $this->db2->get()->result();
        $this->load->view('report/yearly_financial_report', compact('title', 'year', 'resultArr'));
    }

//code by durgesh (01-10-2019)
    public function yearly_financial_report_new($year_new) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $year = $year_new;
        $Nextyear = $year + 1;
        $fromDate = $year . "-04-01";
        $ToDate = $Nextyear . "-03-31";
        $this->db2->select("$db1.tm_projects.project_name,$db1.proj_keydate_details.bd_projid,$db2.proj_financial_details.value_as_per_loa");
        $this->db2->from("$db1.proj_keydate_details");
        $this->db2->join("$db2.proj_financial_details", "$db2.proj_financial_details.bd_projid=$db1.proj_keydate_details.bd_projid", "left");
        $this->db2->join("$db2.bdcegexp_proj_summery", "$db2.bdcegexp_proj_summery.project_id=$db1.proj_keydate_details.bd_projid", "left");
        $this->db2->join("$db1.tm_projects", "$db1.tm_projects.id=$db2.bdcegexp_proj_summery.project_numberid", "left");
        $where_date = "($db1.proj_keydate_details.date_latter_award>='$fromDate' AND $db1.proj_keydate_details.date_latter_award <= '$ToDate')";
        $this->db2->where($where_date);
        $resultArr = $this->db2->get()->result();
        //echo '<pre>';
        //print_r($resultArr);
        return ($resultArr) ? $resultArr : '';
    }

}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */    