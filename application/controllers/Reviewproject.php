<?php

/* Database connection start */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Reviewproject extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Reviewproject_model', 'reviewproject');
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
		// $this->load->helper(array('form', 'url','user_helper'));
		$this->load->model('Mastermodel');
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('loginid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    //Dashbord After Login..
    public function index() {
		$title = 'Review Project';
        $sectorArr = $this->Front_model->GetActiveSector();
		
        $proposalManager = $this->Front_model->allprojectProposalManager();
		 // print_r($proposalManager); die;
		$secId = '0';
        $this->load->view('reviewproject/tender_view', compact('title','secId', 'sectorArr', 'proposalManager'));
    }

    // Project Display
    public function newProjectAll() {

		


        $list = $this->reviewproject->get_datatables();
        //echo '<pre>'; print_r($this->db->last_query()); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
		// echo $bdRole; die;
        $view = '';
        $action = '';
        $assignTo = '';
        $submitted = '';
		$genertedID = '';



		// echo '<pre>'; print_r($list); die;
        foreach ($list as $reviewproject) {

			
			
			$count = $this->Mastermodel->count_rows('comment_on_project', array('project_id' => $reviewproject->fld_id,'is_active'=>'1'));
			if ($count < 1):
				//$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $reviewproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp' ;
				$comment = '';
			else:
				//$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $reviewproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp&nbsp';
				$comment = '';
			endif;
			if (empty($reviewproject->generated_tenderid)) {

				// <a href="#"><i id="faction" title="Set Proposal Manager" 
				// onclick="assignpm(' . "'" . $reviewproject->fld_id . "'" . ')" 
				// data-toggle="modal" data-target="#assignmanager" 
				// class="glyphicon glyphicon-user icon-white"></i></a>&nbsp&nbsp

				// $assignTo = '<button   class="btn btn-success btn-sm" title="Set Proposal Manager" 
				// onclick="assignpm(' . "'" . $reviewproject->fld_id . "'" . ')" 
				// data-toggle="modal" data-target="#assignmanager"><i class="fa fa-user-o" aria-hidden="true"></i></button>&nbsp&nbsp';


				// $submitted = '<button   class="btn btn-info btn-sm" title="First Assign Proposal Manager"  
				//  ><i class="fa fa-get-pocket" aria-hidden="true"></i></button>&nbsp&nbsp';
				$submitted = '';
				$assignTo = '';
				 $genertedID = '<button class="btn btn-success btn-sm"  title="Project No. Generator" 
				 onclick="assignpm(' . "'" . $reviewproject->fld_id . "'" . ')" 
				 data-toggle="modal" data-target="#generateprojid"  
				 ><i class="fa fa-file-o" aria-hidden="true"></i></button>&nbsp&nbsp';
				


				// $submitted = '<i id="faction" title="First Assign Proposal Manager"  
				// class="glyphicon glyphicon-share-alt"></i>&nbsp&nbsp';
				// $genertedID = '<i id="faction" title="Project No. Generator" 
				// onclick="assignpm(' . "'" . $reviewproject->fld_id . "'" . ')" 
				// data-toggle="modal" data-target="#generateprojid" class="glyphicon glyphicon-grain"></i>&nbsp&nbsp';
			}
                else if (empty($reviewproject->assign_to)) {

					// <a href="#"><i id="faction" title="Set Proposal Manager" 
					// onclick="assignpm(' . "'" . $reviewproject->fld_id . "'" . ')" 
					// data-toggle="modal" data-target="#assignmanager" 
					// class="glyphicon glyphicon-user icon-white"></i></a>&nbsp&nbsp

					$assignTo = '<button   class="btn btn-success btn-sm" title="Set Proposal Manager" 
					onclick="assignpm(' . "'" . $reviewproject->fld_id . "'" . ')" 
					data-toggle="modal" data-target="#assignmanager"><i class="fa fa-user-o" aria-hidden="true"></i></button>&nbsp&nbsp';
					$genertedID = '';
					$submitted = '';

					// $submitted = '<button   class="btn btn-info btn-sm" title="First Assign Proposal Manager"  
					//  ><i class="fa fa-get-pocket" aria-hidden="true"></i></button>&nbsp&nbsp';

					//  $genertedID = '<button class="btn btn-warning btn-sm"  title="Project No. Generator" 
					//  onclick="assignpm(' . "'" . $reviewproject->fld_id . "'" . ')" 
					//  data-toggle="modal" data-target="#generateprojid"  
					//  ><i class="fa fa-file-o" aria-hidden="true"></i></button>&nbsp&nbsp';
					


					// $submitted = '<i id="faction" title="First Assign Proposal Manager"  
					// class="glyphicon glyphicon-share-alt"></i>&nbsp&nbsp';
					// $genertedID = '<i id="faction" title="Project No. Generator" 
					// onclick="assignpm(' . "'" . $reviewproject->fld_id . "'" . ')" 
					// data-toggle="modal" data-target="#generateprojid" class="glyphicon glyphicon-grain"></i>&nbsp&nbsp';
				}  else {
                    $genertedID = '';
					$assignTo = '';
					// $submitted = '<i id="faction" title="To Be Submitted" 
					// onclick="togoproj(' . "'" . $reviewproject->fld_id . "'" . ')" class="glyphicon glyphicon-share-alt"></i>&nbsp&nbsp';
					$submitted = '<button   class="btn btn-info btn-sm" id="faction" title="To Be Submitted" 
					onclick="togoproj(' . "'" . $reviewproject->fld_id . "'" . ')"  
					 ><i class="fa fa-get-pocket" aria-hidden="true"></i></button>&nbsp&nbsp';

				
				
				}
				
				// <i title="View link" style="cursor:pointer" 
				// 		onclick="window.open(' . "'" . base_url('reviewproject/projectconcerndetails?projID=' . $reviewproject->fld_id) . "'
				// 		, '', 'width=600 height=400 left=250 top=150'" . ')" 
						// class="glyphicon glyphicon glyphicon-list-alt"></i>

						// .
						// '<i title="View link" style="cursor:pointer" 
						// onclick="window.open(' . "'" . base_url('reviewproject/projurlview?projID=' . $reviewproject->fld_id) 
						// . "', '', 'width=600 height=400 left=250 top=150'" . ')" 
						// class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'<a href="#"><i style="cursor:pointer" title="No Go" onclick="
				// nogoproj(' . "'" . $reviewproject->fld_id . "','" . 0 . "'" . ')" 
				// class="glyphicon glyphicon-ban-circle"></i></a>


				$action = '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$reviewproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
				</button>&nbsp&nbsp<button   class="btn btn-danger btn-sm" title="No Go" onclick="
				nogoproj(' . "'" . $reviewproject->fld_id . "','" . 0 . "'" . ')"   
				 ><i class="fa fa-bookmark" aria-hidden="true"></i></button> &nbsp&nbsp' .
					
                        $assignTo . $genertedID . $submitted 
						. '&nbsp;&nbsp;&nbsp;&nbsp;<label class="fancy-checkbox"><input name="reviewchk[]" value="' . $reviewproject->fld_id . '" type="checkbox"><span></span></label>
						';

						// <a class="btn btn-warning btn-sm" href="'. base_url('importantproject/projectdetail_view/'.$reviewproject->fld_id).'" target="_blank" title=" View Project Detail" alt=" View Project Detail"> <i class="fa fa-list" aria-hidden="true"></i></a>
			
			
			$user = $this->SecondDB_model->getUserByID($reviewproject->assign_to);
			// echo "tststst"; die;
			// echo "<pre>"; print_r($user); die;
            $detail = $reviewproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
			$row[] = ($reviewproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($reviewproject->Expiry_Date));
			
			 
			
			if($reviewproject->generated_tenderid){
				$hr = '<hr/><p><span class="highlight" style="float:left;">' . $reviewproject->generated_tenderid .' </span><span class="highlight" style="float:right; color:green !important;">' . $user->userfullname  . '</span></p>';
			}else{
				$hr = "";
			}

            $row[] = ucFirst($reviewproject->TenderDetails) .$hr;
            $row[] = $reviewproject->Location;
            $row[] = $reviewproject->Organization;
            // $row[] = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $activeproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp'
            // . '<i style="cursor:pointer" title="Important" onclick="setimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-star icon-white"></i>&nbsp&nbsp' .
            // '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('activeproject/projurlview?projID=' . $activeproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'
            // . '<a  href="javascript:void(0)" title="Not Important" onclick="setnotimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>' . '&nbsp;&nbsp;<input name="actchk[]" value="' . $activeproject->fld_id . '" type="checkbox">';

            $row[] = $comment . $view . $action;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => $this->reviewproject->count_filtered(),
            "recordsFiltered" => $this->reviewproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	//Assign Proposal manager To Bid Project.
    public function assign_proposal_manager() {
        $formArrData = $_REQUEST;
		
        //$respon = $this->Front_model->updateRecord('bid_project', array('proposal_manager' => $formArrData['proposal_manager']), array('project_id' => $formArrData['project_id']));
        $inserArr = array('project_id' => $formArrData['project_id'], 'assign_to' => $formArrData['proposal_manager'], 'assign_by' => $this->session->userdata('loginid'));
        $Respon2 = $this->Front_model->insertRecord('assign_project_manager', $inserArr);

        if (!empty($formArrData['proposal_manager'])) {
            $prjID = $formArrData['project_id'];
            $selectp = $this->db->query("select * from bd_tenderdetail WHERE fld_id=$prjID");
            $QueryRec = $selectp->first_row();
            $pmid = $this->session->userdata('loginid');
            /* $userTbl = $this->Front_model->selectRecord('main_users', array('emailaddress'), array('fld_id' => $formArrData['proposal_manager']));
            $userData = $userTbl->first_row(); */
			$userData = $this->SecondDB_model->getUserByID($formArrData['proposal_manager']);
			
            $emailAddress = array($userData->emailaddress, 'ts.admin@cegindia.com', 'harshita@cegindia.com', 'alok.gandhi@cegindia.com');
            //$emailAddress = array('jitendra00752@gmail.com');
            //$emailAddress = array('ts.admin@cegindia.com');
            /* $name = $this->Front_model->UserNameById($formArrData['proposal_manager']);
            $assignBy = $this->Front_model->UserNameById($pmid);*/
			
			$name = $userData->userfullname;
            $assignBy = $this->SecondDB_model->getUserByID($pmid); 
			$message = '<html>
<head></head>
<body>
<table bgcolor="#ffffff" width="500px;" cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; max-width: 600px;"  >
	<tbody>
		<tr>
			<td valign="top" style="font-size: 14px; padding-top:2.429em;padding-bottom:0.929em;">
				<p style="text-align:center;margin:0;padding:0;">
					<img src="http://www.cegindia.com/assets/images/logo.png" style="display:inline-block;">
				</p>
			</td>
		</tr>
		<tr><td align="center" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top:1px solid #e2e2e2;
    border-left:1px solid #e2e2e2;border-right:1px solid #e2e2e2;border-radius: 4px 4px 0 0 ;background-clip: padding-box;
    border-spacing: 0;"><tbody><tr><td valign="top" style=" color:#505050;font-family:Helvetica;font-size:14px;line-height:20%;  padding-top:3.143em;
    padding-right:3.5em;padding-left:3.5em; padding-bottom:0.714em;text-align:left;" mc:edit="body_content_01">
							<p style="color:#545454;display:block;font-family:Helvetica;font-size:16px;line-height:1.500em;
     font-style:normal;font-weight:normal;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:15px;margin-left:0;
    text-align:left;">Dear <strong>' . $name . ',</strong><br/>New Project/Tender has been assigned to you.</p></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style=" border-left:1px solid #e2e2e2;
    border-right:1px solid #e2e2e2; border-bottom: 1px solid #f0f0f0; padding-bottom: 2.286em;">
					<tbody><tr>
						<td valign="top" style=" color:#505050;font-family:Helvetica;font-size:14px;line-height:20%;padding-top:3.143em;padding-right:3.5em;padding-left:3.5em; padding-bottom:0.714em;
     text-align:left;" mc:edit="body_content">
						<p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 <strong>TenderDetails: </strong>' . $QueryRec->TenderDetails . '<br/><strong><br>Organization: </strong>' . $QueryRec->Organization . '<br/><strong>Location: </strong>' . $QueryRec->Location . '</p>
	<p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 Assignd By <br/> ' . $assignBy->userfullname . '
	 </p>
	 <p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 Regards <br/> TS Team
	 </p>
						</td>
						
						</tr>
					</tbody>
				</table>
						<!-- // END BODY -->
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>';
            $subject = 'New Project Assign ';
            $emailArrS7 = implode(",", $emailAddress); 
            sendMail("$emailArrS7", $subject, $message);
            //sendMail("jitendra00752@gmail", $subject, $message);
        }

		$message = "Some issues occurred !";
        if ($Respon2):
            //$this->session->set_flashdata('msg', "Proposal Manager Assign on Tender Success.");
			//redirect(base_url('/reviewproject'));
			$message = "Proposal Manager Assign on Tender Success.";
        else:
            //$this->session->set_flashdata('msg', "Something Went Wrong.");
			//redirect(base_url('/reviewproject'));
			$message = "Something Went Wrong.";
        endif;
        // redirect(base_url('/reviewproject'));
		// die;

		$output = array(
        
			"msg" => $message,
		);
		echo json_encode($output);
		



    }

	public function projnogoset() {
        if ($_REQUEST['actid']) {
            $businessID = $this->session->userdata('businessunit_id');
			$id = $_REQUEST['actid'];
			$data = $this->Front_model->getProjectDetail($id);
			//echo '<pre>'; print_r($data); 
			$message = "Some issues occured !";
			if($data){
				$inserArr = array(
					'created_By' => $data->created_By,
					'tender24_ID' => $data->tender24_ID,
					'TenderDetails' => $data->TenderDetails,
					'Location' => $data->Location,
					'Organization' => $data->Organization,
					'Tender_url' => $data->Tender_url,
					'Sector_IDs' => $data->Sector_IDs,
					'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
					'Keyword_IDs' => $data->Keyword_IDs,
					'keyword_phase' => $data->keyword_phase,
					'Created_Date' => $data->Created_Date,
					'Expiry_Date' => $data->Expiry_Date,
					'source_file' => $data->source_file,
					'project_type' => $data->project_type,
					'country' => $data->country,
					'remarks' => $data->remarks,
					'national_intern' => $data->national_intern,
					'national_status' => $data->national_status,
					'parent_tbl_id' => $data->project_id,
				);
				
				$insertnogoArr = array(
					'project_id' => $data->project_id,
					'user_id' => $this->session->userdata('loginid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'message'=>'No Go Project'
				);
				
				$inserscopedumpArr = array(
					'project_id' => $data->project_id,
					'visible_scope' => 'No_Go_project',
					'businessunit_id' => $data->businessunit_id
				);
			}
			$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id,'businessunit_id'=>$businessID));
			
			if ($count <= 5 AND $count > 1):
				$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
				$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
				$Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
				$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID));
			else: 	
				$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
				$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
				$Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
				$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID)); 
				$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id)); 
			endif;
			
			if ($Respon > 0):
				//$this->session->set_flashdata('msg', "Project Status  Changed No Go / Not Important. ");
				$message = "Project Status  Changed No Go / Not Important. ";
			endif;



			$output = array(
	
				"msg" => $message,
			);
			echo json_encode($output);
		}
    }

    //Link View Details Popup Open Ash..
    public function projurlview() {
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        if ($Rec) {
            $RowDataArr = $Rec->row();
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        $this->load->view('welcome_link', compact('projId', 'hyperlink'));
    }

    //For set To Go Project..
    public function projtogoset() {
        if ($_REQUEST['actid']) {
            $businessID = $this->session->userdata('businessunit_id');
			$inserArr = array(
                'project_id' => $_REQUEST['actid'],
				'user_id' => $this->session->userdata('loginid'),
                'action_date' => date('Y-m-d H:i:s'),
				'actionIP' => get_client_ip(),
				'message'=>'ToGo Project'
			);
			
            $Respon = $this->Front_model->insertRecord('bdtogo_project_byuser', $inserArr);
			$respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $_REQUEST['actid'],'businessunit_id'=>$businessID), array('visible_scope' => 'To_Go_project'));
        
			$inserArr1 = array(
                'project_id' => $_REQUEST['actid'],
				'user_id' => $this->session->userdata('loginid'),
                'action_date' => date('Y-m-d H:i:s'),
				'actionIP' => get_client_ip(),
				'businessunit_id'=> $businessID
			);
			
            $Respon = $this->Front_model->insertRecord('teamreq_togo_project', $inserArr1);
			
		
		
		}
        if ($Respon > 0):
		   // $this->session->set_flashdata('msg', "Project To Be Submitted successfully . ");
		   $message = "Project To Be Submitted successfully.";
		endif;
		


		$output = array(
	
			"msg" => $message,
		);
		echo json_encode($output);
    }

    //For No Go Project set..
    public function projnogoset_old() {
        if ($_REQUEST['actid']) {
            $businessID = $this->session->userdata('businessunit_id');
			$id = $_REQUEST['actid'];
			$data = $this->Front_model->getProjectDetail($id);
			//echo '<pre>'; print_r($data); 
			if($data){
				$inserArr = array(
					'created_By' => $data->created_By,
					'tender24_ID' => $data->tender24_ID,
					'TenderDetails' => $data->TenderDetails,
					'Location' => $data->Location,
					'Organization' => $data->Organization,
					'Tender_url' => $data->Tender_url,
					'Sector_IDs' => $data->Sector_IDs,
					'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
					'Keyword_IDs' => $data->Keyword_IDs,
					'keyword_phase' => $data->keyword_phase,
					'Created_Date' => $data->Created_Date,
					'Expiry_Date' => $data->Expiry_Date,
					'source_file' => $data->source_file,
					'project_type' => $data->project_type,
					'country' => $data->country,
					'remarks' => $data->remarks,
					'national_intern' => $data->national_intern,
					'national_status' => $data->national_status,
					'parent_tbl_id' => $data->project_id,
				);
				
				$insertnogoArr = array(
					'project_id' => $data->project_id,
					'user_id' => $this->session->userdata('loginid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'message'=>'No Go Project'
				);
				
				$inserscopedumpArr = array(
					'project_id' => $data->project_id,
					'visible_scope' => 'No_Go_project',
					'businessunit_id' => $data->businessunit_id
				);
			}
			$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id,'businessunit_id'=>$businessID));
			
			if ($count <= 5 AND $count > 1):
				$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
				$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
				$Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
				$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID));
			else: 	
				$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
				$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
				$Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
				$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID)); 
				$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id)); 
			endif;
			
			if ($Respon > 0):
				$this->session->set_flashdata('msg', "Project Status  Changed No Go / Not Important. ");
			endif;
		}
    }

    //Submit Project Concern Details..
    public function projectconcerndetails() {
        //Get Project Id..
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('project_concern_details', array('concern_details'), array('project_id' => $projId));
        if ($Rec != '') {
            $data = $Rec->result();
        } else {
            $data[0] = '';
        }
        $this->load->view('welcome_message', compact('projId', 'data'));
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

    //Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            $this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

	//Project Active By Check Box Multi Action..
	public function activeprojectbycheckbox_projectreview() {
        $chkBoxArr = $this->input->post('reviewchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];
		$businessID = $this->session->userdata('businessunit_id');
		$message =  "Please Checked Tender Before Submit";
		 
			
        //Bulk Delete Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "delete")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $data = $this->Front_model->getProjectDetail($tndrID);
				if($data){
					$inserArr = array(
						'created_By' => $data->created_By,
						'tender24_ID' => $data->tender24_ID,
						'TenderDetails' => $data->TenderDetails,
						'Location' => $data->Location,
						'Organization' => $data->Organization,
						'Tender_url' => $data->Tender_url,
						'Sector_IDs' => $data->Sector_IDs,
						'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
						'Keyword_IDs' => $data->Keyword_IDs,
						'keyword_phase' => $data->keyword_phase,
						'Created_Date' => $data->Created_Date,
						'Expiry_Date' => $data->Expiry_Date,
						'source_file' => $data->source_file,
						'project_type' => $data->project_type,
						'country' => $data->country,
						'remarks' => $data->remarks,
						'national_intern' => $data->national_intern,
						'national_status' => $data->national_status,
						'parent_tbl_id' => $data->project_id,
					);
					
					$inserinactiveArr = array(
						'project_id' => $data->project_id,
						'user_id' => $this->session->userdata('loginid'),
						'action_date' => date('Y-m-d H:i:s'),
						'actionIP' => get_client_ip(),
						'message'=>'InActive Project'
					);
						
					$inserscopedumpArr = array(
						'project_id' => $data->project_id,
						'visible_scope' => 'InActive_project',
						'businessunit_id' => $data->businessunit_id
					);
				
					$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $tndrID,'businessunit_id'=>$businessID));
					if ($count <= 5 AND $count > 1):
						$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
						$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
						$Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
						$res = $this->db->delete('bdtender_scope', array('project_id' => $tndrID ,'businessunit_id' => $businessID));
					else: 	
						$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
						$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
						$Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
						$res = $this->db->delete('bdtender_scope', array('project_id' => $tndrID ,'businessunit_id' => $businessID)); 
						$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $tndrID)); 
					endif;
					$countvar ++;	
				}
            }
            //$this->session->set_flashdata('msg', "Total $countvar Tender In Activated successfully .");
			//redirect(base_url('/reviewproject'));
			
			$message = "Total " .$countvar." Tender In Activated successfully .";

        endif;


        //Bulk To Go Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "togo")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
				$inserArr = array(
					'project_id' => $tndrID,
					'user_id' => $this->session->userdata('loginid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'message'=>'ToGo Project'
				);
				
				$Respon = $this->Front_model->insertRecord('bdtogo_project_byuser', $inserArr);
				$respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $_REQUEST['actid'],'businessunit_id'=>$businessID), array('visible_scope' => 'To_Go_project'));
                
				$inserArr1 = array(
					'project_id' => $_REQUEST['actid'],
					'user_id' => $this->session->userdata('loginid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'businessunit_id'=> $businessID
				);
				
				$Respon = $this->Front_model->insertRecord('teamreq_togo_project', $inserArr1);
				
				$countvar ++;
            }
            // $this->session->set_flashdata('msg', "Total $countvar Tender status Has Been Change as To Go.");
			// redirect(base_url('/reviewproject'));
			$message = "Total " .$countvar." Tender status Has Been Change as To Go.";

        endif;

        //Bulk No Go Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "nogo")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $id = $tndrID;
				$data = $this->Front_model->getProjectDetail($id);
				if($data){
					$inserArr = array(
						'created_By' => $data->created_By,
						'tender24_ID' => $data->tender24_ID,
						'TenderDetails' => $data->TenderDetails,
						'Location' => $data->Location,
						'Organization' => $data->Organization,
						'Tender_url' => $data->Tender_url,
						'Sector_IDs' => $data->Sector_IDs,
						'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
						'Keyword_IDs' => $data->Keyword_IDs,
						'keyword_phase' => $data->keyword_phase,
						'Created_Date' => $data->Created_Date,
						'Expiry_Date' => $data->Expiry_Date,
						'source_file' => $data->source_file,
						'project_type' => $data->project_type,
						'country' => $data->country,
						'remarks' => $data->remarks,
						'national_intern' => $data->national_intern,
						'national_status' => $data->national_status,
						'parent_tbl_id' => $data->project_id,
					);
					
					$insertnogoArr = array(
						'project_id' => $data->project_id,
						'user_id' => $this->session->userdata('loginid'),
						'action_date' => date('Y-m-d H:i:s'),
						'actionIP' => get_client_ip(),
						'message'=>'No Go Project'
					);
					
					$inserscopedumpArr = array(
						'project_id' => $data->project_id,
						'visible_scope' => 'No_Go_project',
						'businessunit_id' => $data->businessunit_id
					);
				}
				$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id,'businessunit_id'=>$businessID));
				
				if ($count <= 5 AND $count > 1):
					$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
					$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
					$Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
					$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID));
				else: 	
					$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
					$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
					$Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
					$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID)); 
					$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id)); 
				endif;
				$countvar ++;
            }
            // $this->session->set_flashdata('msg', "Total $countvar Tender status Has Been Change as No Go.");
			// redirect(base_url('/reviewproject'));
			
			$message = "Total " .$countvar." Tender status Has Been Change as No Go.";

        endif;

        // $this->session->set_flashdata('msg', "Please Checked Tender Before Submit");
		// redirect(base_url('/reviewproject'));
		



		$output = array(
	
			"msg" => $message,
		);
		echo json_encode($output);
    }
    public function activeprojectbycheckbox_projectreview_old() {
        $chkBoxArr = $this->input->post('reviewchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];
		$businessID = $this->session->userdata('businessunit_id');
        //Bulk Delete Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "delete")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $data = $this->Front_model->getProjectDetail($tndrID);
				if($data){
					$inserArr = array(
						'created_By' => $data->created_By,
						'tender24_ID' => $data->tender24_ID,
						'TenderDetails' => $data->TenderDetails,
						'Location' => $data->Location,
						'Organization' => $data->Organization,
						'Tender_url' => $data->Tender_url,
						'Sector_IDs' => $data->Sector_IDs,
						'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
						'Keyword_IDs' => $data->Keyword_IDs,
						'keyword_phase' => $data->keyword_phase,
						'Created_Date' => $data->Created_Date,
						'Expiry_Date' => $data->Expiry_Date,
						'source_file' => $data->source_file,
						'project_type' => $data->project_type,
						'country' => $data->country,
						'remarks' => $data->remarks,
						'national_intern' => $data->national_intern,
						'national_status' => $data->national_status,
						'parent_tbl_id' => $data->project_id,
					);
					
					$inserinactiveArr = array(
						'project_id' => $data->project_id,
						'user_id' => $this->session->userdata('loginid'),
						'action_date' => date('Y-m-d H:i:s'),
						'actionIP' => get_client_ip(),
						'message'=>'InActive Project'
					);
						
					$inserscopedumpArr = array(
						'project_id' => $data->project_id,
						'visible_scope' => 'InActive_project',
						'businessunit_id' => $data->businessunit_id
					);
				
					$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $tndrID,'businessunit_id'=>$businessID));
					if ($count <= 5 AND $count > 1):
						$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
						$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
						$Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
						$res = $this->db->delete('bdtender_scope', array('project_id' => $tndrID ,'businessunit_id' => $businessID));
					else: 	
						$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
						$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
						$Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
						$res = $this->db->delete('bdtender_scope', array('project_id' => $tndrID ,'businessunit_id' => $businessID)); 
						$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $tndrID)); 
					endif;
					$countvar ++;	
				}
            }
            $this->session->set_flashdata('msg', "Total $countvar Tender In Activated successfully .");
            redirect(base_url('/reviewproject'));
        endif;


        //Bulk To Go Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "togo")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
				$inserArr = array(
					'project_id' => $tndrID,
					'user_id' => $this->session->userdata('loginid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'message'=>'ToGo Project'
				);
				
				$Respon = $this->Front_model->insertRecord('bdtogo_project_byuser', $inserArr);
				$respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $_REQUEST['actid'],'businessunit_id'=>$businessID), array('visible_scope' => 'To_Go_project'));
                
				$inserArr1 = array(
					'project_id' => $_REQUEST['actid'],
					'user_id' => $this->session->userdata('loginid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'businessunit_id'=> $businessID
				);
				
				$Respon = $this->Front_model->insertRecord('teamreq_togo_project', $inserArr1);
				
				$countvar ++;
            }
            $this->session->set_flashdata('msg', "Total $countvar Tender status Has Been Change as To Go.");
            redirect(base_url('/reviewproject'));
        endif;

        //Bulk No Go Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "nogo")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $id = $tndrID;
				$data = $this->Front_model->getProjectDetail($id);
				if($data){
					$inserArr = array(
						'created_By' => $data->created_By,
						'tender24_ID' => $data->tender24_ID,
						'TenderDetails' => $data->TenderDetails,
						'Location' => $data->Location,
						'Organization' => $data->Organization,
						'Tender_url' => $data->Tender_url,
						'Sector_IDs' => $data->Sector_IDs,
						'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
						'Keyword_IDs' => $data->Keyword_IDs,
						'keyword_phase' => $data->keyword_phase,
						'Created_Date' => $data->Created_Date,
						'Expiry_Date' => $data->Expiry_Date,
						'source_file' => $data->source_file,
						'project_type' => $data->project_type,
						'country' => $data->country,
						'remarks' => $data->remarks,
						'national_intern' => $data->national_intern,
						'national_status' => $data->national_status,
						'parent_tbl_id' => $data->project_id,
					);
					
					$insertnogoArr = array(
						'project_id' => $data->project_id,
						'user_id' => $this->session->userdata('loginid'),
						'action_date' => date('Y-m-d H:i:s'),
						'actionIP' => get_client_ip(),
						'message'=>'No Go Project'
					);
					
					$inserscopedumpArr = array(
						'project_id' => $data->project_id,
						'visible_scope' => 'No_Go_project',
						'businessunit_id' => $data->businessunit_id
					);
				}
				$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id,'businessunit_id'=>$businessID));
				
				if ($count <= 5 AND $count > 1):
					$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
					$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
					$Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
					$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID));
				else: 	
					$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
					$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
					$Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
					$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID)); 
					$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id)); 
				endif;
				$countvar ++;
            }
            $this->session->set_flashdata('msg', "Total $countvar Tender status Has Been Change as No Go.");
            redirect(base_url('/reviewproject'));
        endif;

        $this->session->set_flashdata('msg', "Please Checked Tender Before Submit");
        redirect(base_url('/reviewproject'));
    }

    //Assign Proposal manager To Bid Project.
    public function assign_proposal_manager_old() {
        $formArrData = $_REQUEST;
		
        //$respon = $this->Front_model->updateRecord('bid_project', array('proposal_manager' => $formArrData['proposal_manager']), array('project_id' => $formArrData['project_id']));
        $inserArr = array('project_id' => $formArrData['project_id'], 'assign_to' => $formArrData['proposal_manager'], 'assign_by' => $this->session->userdata('loginid'));
        $Respon2 = $this->Front_model->insertRecord('assign_project_manager', $inserArr);

        if (!empty($formArrData['proposal_manager'])) {
            $prjID = $formArrData['project_id'];
            $selectp = $this->db->query("select * from bd_tenderdetail WHERE fld_id=$prjID");
            $QueryRec = $selectp->first_row();
            $pmid = $this->session->userdata('loginid');
            /* $userTbl = $this->Front_model->selectRecord('main_users', array('emailaddress'), array('fld_id' => $formArrData['proposal_manager']));
            $userData = $userTbl->first_row(); */
			$userData = $this->SecondDB_model->getUserByID($formArrData['proposal_manager']);
			
            $emailAddress = array($userData->emailaddress, 'ts.admin@cegindia.com', 'harshita@cegindia.com', 'v.sagar@cegindia.com');
            //$emailAddress = array('jitendra00752@gmail.com');
            //$emailAddress = array('ts.admin@cegindia.com');
            /* $name = $this->Front_model->UserNameById($formArrData['proposal_manager']);
            $assignBy = $this->Front_model->UserNameById($pmid);*/
			
			$name = $userData->userfullname;
            $assignBy = $this->SecondDB_model->getUserByID($pmid); 
			$message = '<html>
<head></head>
<body>
<table bgcolor="#ffffff" width="500px;" cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; max-width: 600px;"  >
	<tbody>
		<tr>
			<td valign="top" style="font-size: 14px; padding-top:2.429em;padding-bottom:0.929em;">
				<p style="text-align:center;margin:0;padding:0;">
					<img src="http://www.cegindia.com/assets/images/logo.png" style="display:inline-block;">
				</p>
			</td>
		</tr>
		<tr><td align="center" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top:1px solid #e2e2e2;
    border-left:1px solid #e2e2e2;border-right:1px solid #e2e2e2;border-radius: 4px 4px 0 0 ;background-clip: padding-box;
    border-spacing: 0;"><tbody><tr><td valign="top" style=" color:#505050;font-family:Helvetica;font-size:14px;line-height:20%;  padding-top:3.143em;
    padding-right:3.5em;padding-left:3.5em; padding-bottom:0.714em;text-align:left;" mc:edit="body_content_01">
							<p style="color:#545454;display:block;font-family:Helvetica;font-size:16px;line-height:1.500em;
     font-style:normal;font-weight:normal;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:15px;margin-left:0;
    text-align:left;">Dear <strong>' . $name . ',</strong><br/>New Project/Tender has been assigned to you.</p></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style=" border-left:1px solid #e2e2e2;
    border-right:1px solid #e2e2e2; border-bottom: 1px solid #f0f0f0; padding-bottom: 2.286em;">
					<tbody><tr>
						<td valign="top" style=" color:#505050;font-family:Helvetica;font-size:14px;line-height:20%;padding-top:3.143em;padding-right:3.5em;padding-left:3.5em; padding-bottom:0.714em;
     text-align:left;" mc:edit="body_content">
						<p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 <strong>TenderDetails: </strong>' . $QueryRec->TenderDetails . '<br/><strong><br>Organization: </strong>' . $QueryRec->Organization . '<br/><strong>Location: </strong>' . $QueryRec->Location . '</p>
	<p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 Assignd By <br/> ' . $assignBy->userfullname . '
	 </p>
	 <p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 Regards <br/> TS Team
	 </p>
						</td>
						
						</tr>
					</tbody>
				</table>
						<!-- // END BODY -->
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>';
            $subject = 'New Project Assign ';
            $emailArrS7 = implode(",", $emailAddress); 
            sendMail("$emailArrS7", $subject, $message);
            //sendMail("jitendra00752@gmail", $subject, $message);
        }


        if ($Respon2):
            $this->session->set_flashdata('msg', "Proposal Manager Assign on Tender Success.");
            redirect(base_url('/reviewproject'));
        else:
            $this->session->set_flashdata('msg', "Something Went Wrong.");
            redirect(base_url('/reviewproject'));
        endif;
        redirect(base_url('/reviewproject'));
        die;
    }

    
    
//    function test() {
//        $to = 'ts.admin@cegindia.com';
//        $subject = 'the subject';
//        $message = "Hello When I use target(to) email is gmail such as target@gmail.com, every thing ok, I can receive email from CI. But when I use yahoo mail such as target@yahoo.com, target email can  is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
//        //echo mail($to, $subject, $message, $headers);
//        echo sendMail($to, $subject, $message);
//    }

    
    
    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('loginid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('loginid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

}
