<?php

/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Activeproject extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Activeproject_model', 'activeproject');
        $this->load->model('Front_model');
		$this->load->model('SecondDB_model');
		// $this->load->helper(array('form', 'url','user_helper'));
        $this->load->model('Mastermodel');
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('loginid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }
	
 
	
    //Dashbord After Login..
    public function active_project() {
		$title = 'Active Project';
		// echo "test"; die;
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';

        // print_r($this->db->last_query()); die;
        $this->load->view('activeproject/tender_view', compact('title','secId', 'sectorArr'));
    }

    // Project Display
    public function newProjectAll() {
        $list = $this->activeproject->get_datatables();
		// echo "<pre>"; print_r($this->db->last_query()); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
		// echo $bdRole; die;
        $view = '';
        $action = '';
        foreach ($list as $activeproject) {
			$count = $this->Mastermodel->count_rows('comment_on_project', array('project_id' => $activeproject->fld_id,'is_active'=>'1'));
			if ($count < 1):
                //$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $activeproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp';
                $comment = '';
			else:
                //$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $activeproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp&nbsp';
                $comment = '';
			endif;
                /* <a  href="javascript:void(0)" title="Not Important" onclick="setnotimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a><i style="cursor:pointer" title="Important" onclick="setimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-star icon-white"></i>&nbsp&nbsp' .
                        '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('activeproject/projurlview?projID=' . $activeproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i> */
                $action = '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$activeproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
                </button>&nbsp&nbsp<button   class="btn btn-success btn-sm" title="Important" onclick="setimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-bookmark" aria-hidden="true"></i></button>&nbsp&nbsp'
                        . '<button title="Not Important" class="btn btn-danger btn-sm" onclick="setnotimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="icon-trash"></i></button>' . '&nbsp;&nbsp; <label class="fancy-checkbox"><input type="checkbox" name="actchk[]" value="' . $activeproject->fld_id . '"><span></span></label>';

            $detail = $activeproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($activeproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($activeproject->Expiry_Date));
            // $row[] = $no;
            $row[] = ucFirst($activeproject->TenderDetails);
            $row[] = $activeproject->Location;
            $row[] = $activeproject->Organization;
            $row[] = $comment. $view . $action;
			
			$data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => $this->activeproject->count_filtered(),
            "recordsFiltered" => $this->activeproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function newProjectnational() {
        $list = $this->activeproject->get_datatables();
		// echo "<pre>"; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
		// echo $bdRole; die;
        $view = '';
        $action = '';
        foreach ($list as $activeproject) {
			$count = $this->Mastermodel->count_rows('comment_on_project', array('project_id' => $activeproject->fld_id,'is_active'=>'1'));
			if ($count < 1):
                //$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $activeproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp';
                $comment = '';
			else:
                //$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $activeproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp&nbsp';
                $comment = '';
			endif;
                /* <a  href="javascript:void(0)" title="Not Important" onclick="setnotimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a><i style="cursor:pointer" title="Important" onclick="setimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-star icon-white"></i>&nbsp&nbsp' .
                        '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('activeproject/projurlview?projID=' . $activeproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i> */
                $action = '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$activeproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
                </button>&nbsp&nbsp<button   class="btn btn-success btn-sm" title="Important" onclick="setimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-bookmark" aria-hidden="true"></i></button>&nbsp&nbsp'
                        . '<button title="Not Important" class="btn btn-danger btn-sm" onclick="setnotimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="icon-trash"></i></button>' . '&nbsp;&nbsp; <label class="fancy-checkbox"><input type="checkbox" name="actchk[]" value="' . $activeproject->fld_id . '"><span></span></label>';

            $detail = ucFirst($activeproject->TenderDetails);
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($activeproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($activeproject->Expiry_Date));
            // $row[] = $no;
            $row[] = $activeproject->TenderDetails;
            $row[] = $activeproject->Location;
            $row[] = $activeproject->Organization;
            $row[] = $comment. $view . $action;
			
			$data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => $this->activeproject->count_filtered(),
            "recordsFiltered" => $this->activeproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    
    
    public function newProjectInterNational() {
        $list = $this->activeproject->get_datatables();
		// echo "<pre>"; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
		// echo $bdRole; die;
        $view = '';
        $action = '';
        foreach ($list as $activeproject) {
			$count = $this->Mastermodel->count_rows('comment_on_project', array('project_id' => $activeproject->fld_id,'is_active'=>'1'));
			if ($count < 1):
                //$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $activeproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp';
                $comment = '';
			else:
                //$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $activeproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp&nbsp';
                $comment = '';
			endif;
                /* <a  href="javascript:void(0)" title="Not Important" onclick="setnotimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a><i style="cursor:pointer" title="Important" onclick="setimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-star icon-white"></i>&nbsp&nbsp' .
                        '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('activeproject/projurlview?projID=' . $activeproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i> */
                $action = '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$activeproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
                </button>&nbsp&nbsp<button   class="btn btn-success btn-sm" title="Important" onclick="setimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-bookmark" aria-hidden="true"></i></button>&nbsp&nbsp'
                        . '<button title="Not Important" class="btn btn-danger btn-sm" onclick="setnotimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="icon-trash"></i></button>' . '&nbsp;&nbsp; <label class="fancy-checkbox"><input type="checkbox" name="actchk[]" value="' . $activeproject->fld_id . '"><span></span></label>';

            $detail = $activeproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($activeproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($activeproject->Expiry_Date));
            // $row[] = $no;
            $row[] = ucFirst($activeproject->TenderDetails);
            $row[] = $activeproject->Location;
            $row[] = $activeproject->Organization;
            $row[] = $comment. $view . $action;
			
			$data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => $this->activeproject->count_filtered(),
            "recordsFiltered" => $this->activeproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    
	

    //Link View Details Popup Open Ash..
    public function projurlview() {
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        if ($Rec) {
            $RowDataArr = $Rec->row();
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        $this->load->view('welcome_link', compact('projId', 'hyperlink'));
    }

    //For Important Mark from user lavel 2...
    public function projImportantMark() {
        if ($_REQUEST['actid']) {
			$businessID = $this->session->userdata('businessunit_id');
			$inserArr = array(
                'project_id' => $_REQUEST['actid'],
				'user_id' => $this->session->userdata('loginid'),
                'action_date' => date('Y-m-d H:i:s'),
				'actionIP' => get_client_ip(),
				'message'=>'Important Project'
			);
			
            $Respon = $this->Front_model->insertRecord('bdimportant_project_byuser', $inserArr);
			$respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $_REQUEST['actid'],'businessunit_id'=>$businessID), array('visible_scope' => 'Important_project'));
        }
		// if ($Respon > 0):
        //     $this->session->set_flashdata('msg', "Tender Marked as Important.");
        // endif;
		/* if ($_REQUEST['actid']) {
            $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('import_for_user', $inserArr);
        }
        if ($Respon):
            $this->visibilityStatus('Important_project', $_REQUEST['actid']);
            //Notification Status..
            $this->notified($_REQUEST['actid'], 'Tender Marked as Important.');
            $this->session->set_flashdata('msg', "Tender Marked as Important. ");
        endif; */

        if ($Respon > 0) {
            $message = "Tender Marked as Important.";

        }else{
            $message =  "Some issues occured !";
        }
            
        $output = array(
    
            "msg" => $message,
        );
        echo json_encode($output);
		
    }

    //For Not Important Mark Not .....
    public function projNotImportantMark() {
        if ($_REQUEST['actid']) {
            $businessID = $this->session->userdata('businessunit_id');
			$id = $_REQUEST['actid'];
			$data = $this->Front_model->getProjectDetail($id);
			//echo '<pre>'; print_r($data); 
			if($data){
				$inserArr = array(
					'created_By' => $data->created_By,
					'tender24_ID' => $data->tender24_ID,
					'TenderDetails' => $data->TenderDetails,
					'Location' => $data->Location,
					'Organization' => $data->Organization,
					'Tender_url' => $data->Tender_url,
					'Sector_IDs' => $data->Sector_IDs,
					'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
					'Keyword_IDs' => $data->Keyword_IDs,
					'keyword_phase' => $data->keyword_phase,
					'Created_Date' => $data->Created_Date,
					'Expiry_Date' => $data->Expiry_Date,
					'source_file' => $data->source_file,
					'project_type' => $data->project_type,
					'country' => $data->country,
					'remarks' => $data->remarks,
					'national_intern' => $data->national_intern,
					'national_status' => $data->national_status,
					'parent_tbl_id' => $data->project_id,
				);
				
				$insertnotimpArr = array(
					'project_id' => $data->project_id,
					'user_id' => $this->session->userdata('loginid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'message'=>'Not Important Project'
				);
				
				$inserscopedumpArr = array(
					'project_id' => $data->project_id,
					'visible_scope' => 'Not_Important_project',
					'businessunit_id' => $data->businessunit_id
				);
			}
			$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id,'businessunit_id'=>$businessID));
			
			if ($count <= 5 AND $count > 1):
				$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
				$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
				$Respon2 = $this->Front_model->insertRecord('bdnotimp_project_byuser', $insertnotimpArr);
				$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID));
			else: 	
				$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
				$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
				$Respon2 = $this->Front_model->insertRecord('bdnotimp_project_byuser', $insertnotimpArr);
				$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID)); 
				$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id)); 
			endif;
			
			// if ($Respon > 0):
			// 	$this->session->set_flashdata('msg', "Project Status Not Important Changed.");
            // endif;
            
            
        }
       
        if ($Respon > 0) {
            $message = "Project Status set to Not Important.";

        }else{
            $message =  "Some issues occured !";
        }
            
        $output = array(
    
            "msg" => $message,
        );
        echo json_encode($output);
		
		/* if ($_REQUEST['actid']) {
            $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('notimport_for_user', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('Not_Important_project', $_REQUEST['actid']);
            //Notification Status..
            $this->notified($_REQUEST['actid'], 'Tender Marked as Not Important.');
            $this->session->set_flashdata('msg', "Project Status Not Important Changed. ");
        endif; */
    }


    


    public function impProject() {


        // print_r("testSubmit");


        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];
        $businessID = $this->session->userdata('businessunit_id');
        
         
        $message =  "Some issues occured !";
       
       // print_r($_REQUEST); die;
        //Set As Important Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "Important")){
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
				$inserArr = array(
					'project_id' => $tndrID,
					'user_id' => $this->session->userdata('loginid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'message'=>'Important Project'
				);
			
				$Respon = $this->Front_model->insertRecord('bdimportant_project_byuser', $inserArr);
				$respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $tndrID,'businessunit_id'=>$businessID), array('visible_scope' => 'Important_project'));
				$countvar ++;
            }
            // $this->session->set_flashdata('msg', "$countvar Tender Mark as a Important Success.");
            // redirect(base_url('/activeProject'));
            if ($Respon > 0) {
                $message = $countvar." Project Status set to Important.";
    
            }
                
            
        }
        //Set As Not Important Process.. 
        if (count($chkBoxArr) > 0 && ($actTyle == "Notimportant")){
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                /* $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('notimport_for_user', $inserArr);
                $this->visibilityStatus('Not_Important_project', $tndrID); */
				
				$id = $tndrID;
				$data = $this->Front_model->getProjectDetail($id);
				if($data){
					$inserArr = array(
						'created_By' => $data->created_By,
						'tender24_ID' => $data->tender24_ID,
						'TenderDetails' => $data->TenderDetails,
						'Location' => $data->Location,
						'Organization' => $data->Organization,
						'Tender_url' => $data->Tender_url,
						'Sector_IDs' => $data->Sector_IDs,
						'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
						'Keyword_IDs' => $data->Keyword_IDs,
						'keyword_phase' => $data->keyword_phase,
						'Created_Date' => $data->Created_Date,
						'Expiry_Date' => $data->Expiry_Date,
						'source_file' => $data->source_file,
						'project_type' => $data->project_type,
						'country' => $data->country,
						'remarks' => $data->remarks,
						'national_intern' => $data->national_intern,
						'national_status' => $data->national_status,
						'parent_tbl_id' => $data->project_id,
					);
					
					$insertnotimpArr = array(
						'project_id' => $data->project_id,
						'user_id' => $this->session->userdata('loginid'),
						'action_date' => date('Y-m-d H:i:s'),
						'actionIP' => get_client_ip(),
						'message'=>'Not Important Project'
					);
					
					$inserscopedumpArr = array(
						'project_id' => $data->project_id,
						'visible_scope' => 'Not_Important_project',
						'businessunit_id' => $data->businessunit_id
					);
				}
				$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id,'businessunit_id'=>$businessID));
				
				if ($count <= 5 AND $count > 1){
					$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
					$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
					$Respon2 = $this->Front_model->insertRecord('bdnotimp_project_byuser', $insertnotimpArr);
					$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID));
                }else{ 	
					$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
					$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
					$Respon2 = $this->Front_model->insertRecord('bdnotimp_project_byuser', $insertnotimpArr);
					$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID)); 
					$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id)); 
                }
				
				
			
                $countvar ++;
            }
			// if ($Respon > 0){
			// 	$this->session->set_flashdata('msg', "$countvar Tender Marked as a Not Important.");
            // }
            // redirect(base_url('/activeProject'));

            if ($Respon > 0) {
                $message = $countvar ." Project Status set to Not Important.";
    
            }
            
            
           
             
        }




        $output = array(
        
            "msg" => $message,
        );


        echo json_encode($output);
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
		if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

    //Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            $this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

    //Set As Important and Not Important With check box...
   /*  public function importantprojectbycheckbox() {
        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];

        //Set As Important Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "Important")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('import_for_user', $inserArr);
                $this->visibilityStatus('Important_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, "$countvar Tender Marked as Important.");
            $this->session->set_flashdata('msg', "$countvar Tender Mark as a Important Success.");
            redirect(base_url('/activeproject'));
        endif;
        //Set As Not Important Process.. 
        if (count($chkBoxArr) > 0 && ($actTyle == "Notimportant")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('notimport_for_user', $inserArr);
                $this->visibilityStatus('Not_Important_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, " $countvar Tender Marked as Not Important.");
            $this->session->set_flashdata('msg', "$countvar Tender Marked as a Not Important.");
            redirect(base_url('/activeproject'));
        endif;
    } */ 
	
	
	public function importantprojectbycheckbox() {


        // print_r("testSubmit");


        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];
        $businessID = $this->session->userdata('businessunit_id');
        

       // print_r($_REQUEST); die;
        //Set As Important Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "Important")){
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
				$inserArr = array(
					'project_id' => $tndrID,
					'user_id' => $this->session->userdata('loginid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'message'=>'Important Project'
				);
			
				$Respon = $this->Front_model->insertRecord('bdimportant_project_byuser', $inserArr);
				$respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $tndrID,'businessunit_id'=>$businessID), array('visible_scope' => 'Important_project'));
				$countvar ++;
            }
            $this->session->set_flashdata('msg', "$countvar Tender Mark as a Important Success.");
            redirect(base_url('/activeProject'));
        }
        //Set As Not Important Process.. 
        if (count($chkBoxArr) > 0 && ($actTyle == "Notimportant")){
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                /* $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('notimport_for_user', $inserArr);
                $this->visibilityStatus('Not_Important_project', $tndrID); */
				
				$id = $tndrID;
				$data = $this->Front_model->getProjectDetail($id);
				if($data){
					$inserArr = array(
						'created_By' => $data->created_By,
						'tender24_ID' => $data->tender24_ID,
						'TenderDetails' => $data->TenderDetails,
						'Location' => $data->Location,
						'Organization' => $data->Organization,
						'Tender_url' => $data->Tender_url,
						'Sector_IDs' => $data->Sector_IDs,
						'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
						'Keyword_IDs' => $data->Keyword_IDs,
						'keyword_phase' => $data->keyword_phase,
						'Created_Date' => $data->Created_Date,
						'Expiry_Date' => $data->Expiry_Date,
						'source_file' => $data->source_file,
						'project_type' => $data->project_type,
						'country' => $data->country,
						'remarks' => $data->remarks,
						'national_intern' => $data->national_intern,
						'national_status' => $data->national_status,
						'parent_tbl_id' => $data->project_id,
					);
					
					$insertnotimpArr = array(
						'project_id' => $data->project_id,
						'user_id' => $this->session->userdata('loginid'),
						'action_date' => date('Y-m-d H:i:s'),
						'actionIP' => get_client_ip(),
						'message'=>'Not Important Project'
					);
					
					$inserscopedumpArr = array(
						'project_id' => $data->project_id,
						'visible_scope' => 'Not_Important_project',
						'businessunit_id' => $data->businessunit_id
					);
				}
				$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id,'businessunit_id'=>$businessID));
				
				if ($count <= 5 AND $count > 1){
					$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
					$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
					$Respon2 = $this->Front_model->insertRecord('bdnotimp_project_byuser', $insertnotimpArr);
					$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID));
                }else{ 	
					$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
					$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
					$Respon2 = $this->Front_model->insertRecord('bdnotimp_project_byuser', $insertnotimpArr);
					$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID)); 
					$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id)); 
                }
				
				
			
                $countvar ++;
            }
			if ($Respon > 0){
				$this->session->set_flashdata('msg', "$countvar Tender Marked as a Not Important.");
            }
            redirect(base_url('/activeProject'));
        }
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('loginid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('loginid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

}
