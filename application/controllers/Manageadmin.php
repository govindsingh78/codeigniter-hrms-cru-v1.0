<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Manageadmin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Sector_model', 'sector');
        $this->load->model('SecondDB_model');
        $this->load->model('Keyword_model');
        $this->load->model('Userrole_model');
        $this->load->model('Mailrole_model');
        $this->load->model('Role_model');
        $this->load->model('Front_model');
        $this->load->model('mastermodel');
		$this->load->helper(array('form', 'url','user_helper'));
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('uid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }
	
	public function sector() {
		$sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $this->load->view('admin/sector_view', compact('sectorArr'));
    }

    // Project Display
    public function newProjectAll() {
		$list = $this->sector->get_datatables();
		$data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
			$no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->sectName;
            $row[] = $val->keyName;
            
            $data[] = $row;
        }
		$output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->sector->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	
	public function keyword() {
		$keywordArr = $this->Front_model->GetActiveKeyword();
        $keyId = '0';
        $this->load->view('admin/keyword_view', compact('keywordArr'));
    }

    // Project Display
    public function keywordProjectAll() {
		$list = $this->Keyword_model->get_datatables();
		$data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
			$no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->keywordname;
            $row[] = $val->name;
            
            $data[] = $row;
        }
		$output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Keyword_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	public function add_sector(){
		$this->form_validation->set_rules('sector_name', 'Sector Name', 'required|min_length[3]|max_length[100]|is_unique[sector.sectName]');
        if ($this->form_validation->run() == FALSE) {
            redirect(base_url('Manageadmin/sector'));
        } else {
			$arr = array('sectName'=>$_REQUEST['sector_name'],'is_active'=>'1');
            $respon = $this->mastermodel->InsertMasterData($arr,'sector');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Sector Added successfully');
                redirect(base_url('Manageadmin/sector'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('Manageadmin/sector'));
            endif;
        }
	}
	
	public function add_sectorkey(){
		if(!empty($_REQUEST)){
			$count = $this->mastermodel->count_rows('sectorkey', array('sectID' => $_REQUEST['sector_id'], 'keyName' => $_REQUEST['service1_name'], 'is_active' => 1));
			if($count<1):
				$arr = array('keyName'=>$_REQUEST['service1_name'],'sectID'=>$_REQUEST['sector_id'],'is_active'=>'1');
				$respon = $this->mastermodel->InsertMasterData($arr,'sectorkey');
				if ($respon):
					$this->session->set_flashdata('success_msg', 'Sector Key Added successfully');
					redirect(base_url('Manageadmin/sector'));
				else:
					$this->session->set_flashdata('error_msg', 'Something went gone wrong');
					redirect(base_url('Manageadmin/sector'));
				endif;
			else:
				$this->session->set_flashdata('error_msg', 'Something went gone wrong');
				redirect(base_url('Manageadmin/sector'));
			endif;
		}
    }
	
	
	public function add_keyword(){
		$this->form_validation->set_rules('keyword_name', 'Keyword Name', 'required|min_length[3]|max_length[100]|is_unique[bd_keyword.keywordname]');
        if ($this->form_validation->run() == FALSE) {
            redirect(base_url('Manageadmin/keyword'));
        } else {
			$arr = array('keywordname'=>$_REQUEST['keyword_name'],'status'=>'1');
            $respon = $this->mastermodel->InsertMasterData($arr,'bd_keyword');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Keyword Added successfully');
                redirect(base_url('Manageadmin/keyword'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('Manageadmin/keyword'));
            endif;
        }
	}
	
	public function add_keywordphase(){
		if(!empty($_REQUEST)){
			$count = $this->mastermodel->count_rows('bd_keyword_master', array('phase_id' => $_REQUEST['keyword_id'], 'name' => $_REQUEST['keyword_name'], 'is_active' => 1));
			if($count<1):
				$res = $this->mastermodel->GetTableData('bd_keyword', array('id'=>$_REQUEST['keyword_id'],'status'=>1));
				$phase = $res[0]->keywordname; 
				$arr = array('name'=>$_REQUEST['keyword_name'],'phase_id'=>$_REQUEST['keyword_id'],'phase'=>$phase,'is_active'=>'1');
				$respon = $this->mastermodel->InsertMasterData($arr,'bd_keyword_master');
				if ($respon):
					$this->session->set_flashdata('success_msg', 'Keyword Added successfully');
					redirect(base_url('Manageadmin/keyword'));
				else:
					$this->session->set_flashdata('error_msg', 'Something went gone wrong');
					redirect(base_url('Manageadmin/keyword'));
				endif;
			else:
				$this->session->set_flashdata('error_msg', 'Something went gone wrong');
				redirect(base_url('Manageadmin/keyword'));
			endif;
		}
    }
	
	public function role(){
		$this->load->view('admin/role_view', compact('keywordArr'));
	}
	
	public function add_role(){
		$this->form_validation->set_rules('role_name', 'Role Name', 'required|min_length[3]|max_length[100]|is_unique[role.role_name]');
        if ($this->form_validation->run() == FALSE) {
            redirect(base_url('Manageadmin/role'));
        } else {
			$arr = array('role_name'=>$_REQUEST['role_name'],'description'=>$_REQUEST['description'],'is_active'=>'1');
            $respon = $this->mastermodel->InsertMasterData($arr,'role');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Role Added successfully');
                redirect(base_url('Manageadmin/role'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('Manageadmin/role'));
            endif;
        }
	}
	
	public function roleList() {
		$list = $this->Role_model->get_datatables();
		$data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
			$no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->role_name;
            $row[] = $val->description;
            //$row[] = '<a href="'.base_url('/Manageadmin/deleterole/'.$val->id).'">Delete</a>';
            
            $data[] = $row;
        }
		$output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Role_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	public function deleterole(){
		$id = $this->uri->segment(3); 
		if($id){
			$this->db->delete('role', array('id' => $id)); 
		}
		redirect(base_url('Manageadmin/role'));
	}
	
	public function userRole(){
		$employee = $this->Front_model->getAllemployees();
		$businessunit = $this->SecondDB_model->getBusinessunit();
		$query = $this->db->query('select * from role where is_active = 1');
		$role = $query->result();
		// echo '<pre>';
		// print_r($businessunit); die; 
		
		$this->load->view('admin/userrole_view', compact('employee','role','businessunit'));
	}
	
	public function getDept(){
		$id = $_REQUEST['id'];
		$dept = $this->SecondDB_model->getDept($id);
		foreach($dept as $val){
			$deptArr[] = array('id'=>$val->id,'deptname'=>$val->deptname);
		}
		echo json_encode($deptArr);
	}
	
	public function getUser(){
		$id = $_REQUEST['id'];
		$user = $this->SecondDB_model->getUser($id);
		foreach($user as $val){
			$userfullname = $val->userfullname . '('. $val->employeeId . ')';
			$userArr[] = array('id'=>$val->user_id,'userfullname'=>$userfullname);
		}
		echo json_encode($userArr); 
	}
	
	
	
	public function mailrole(){
		$employee = $this->Front_model->getAllemployees();
		$businessunit = $this->SecondDB_model->getBusinessunit();
		$query = $this->db->query('select * from mail_type where is_active = 1');
		$mail_type = $query->result();
		$this->load->view('admin/mailrole_view', compact('employee','mail_type','businessunit'));
	}
	
	
	public function add_mailrole(){
		//echo '<pre>'; print_r($_REQUEST); die;
		$this->form_validation->set_rules('business_id', 'Business Unit', 'required');
		$this->form_validation->set_rules('dept_id', 'Department Name ', 'required');
		$this->form_validation->set_rules('user_id', 'User Name', 'required|is_unique[mailtype_role.user_id]');
        if ($this->form_validation->run() == FALSE) {
            redirect(base_url('Manageadmin/mailrole'));
        } else {
			$mailid = implode(',',$_REQUEST['mail_id']);
			$arr = array('user_id'=>$_REQUEST['user_id'],'mail_id'=>$mailid,'business_id'=>$_REQUEST['business_id'],'dept_id'=>$_REQUEST['dept_id']);
            $respon = $this->mastermodel->InsertMasterData($arr,'mailtype_role');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'User Role Added successfully');
                redirect(base_url('Manageadmin/mailrole'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('Manageadmin/mailrole'));
            endif;
        }
	}
	
	public function mailroleList() {
		$list = $this->Mailrole_model->get_datatables();
		$data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
			$mailid = $val->mail_id;
			$query = $this->db->query('select * from mail_type where is_active = 1 and id IN ('. $mailid .')');
			$mail_type = $query->result();
			$type = '';
			foreach($mail_type as $val1){
				$type .=  $val1->mailtype . ' <br/> ';
			}
			$no++;
            $row = array();
            $row[] = $no;
            $row[] = $type;
            $row[] = $val->userfullname;
            $row[] = '<a href="'.base_url('/Manageadmin/deletemailrole/'.$val->id).'">Delete</a>';
            
            $data[] = $row;
        }
		$output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Mailrole_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	public function deletemailrole(){
		$id = $this->uri->segment(3); 
		if($id){
			$this->db->delete('mailtype_role', array('id' => $id)); 
		}
		redirect(base_url('Manageadmin/mailrole'));
	}
	
	
	public function add_userrole(){
		$this->form_validation->set_rules('role_id', 'Role Name', 'required');
		$this->form_validation->set_rules('business_id', 'Business Unit', 'required');
		$this->form_validation->set_rules('dept_id', 'Department Name ', 'required');
		$this->form_validation->set_rules('user_id', 'User Name', 'required|is_unique[bd_role.user_id]');
        if ($this->form_validation->run() == FALSE) {
            redirect(base_url('Manageadmin/userRole'));
        } else {
			//echo '<pre>'; print_r($_REQUEST); die;
			$res = $this->mastermodel->GetTableData('role', array('id'=>$_REQUEST['role_id']));
			$rolename = $res[0]->role_name; 
			$arr = array('user_id'=>$_REQUEST['user_id'],'role_id'=>$_REQUEST['role_id'],'role_name'=>$rolename,'business_id'=>$_REQUEST['business_id'],'dept_id'=>$_REQUEST['dept_id']);
            $respon = $this->mastermodel->InsertMasterData($arr,'bd_role');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'User Role Added successfully');
                redirect(base_url('Manageadmin/userRole'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('Manageadmin/userRole'));
            endif; 
        }
	}
	
	public function userroleList() {
		$list = $this->Userrole_model->get_datatables();
		//echo '<pre>'; print_r($list); die;
		$data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
			$no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->role_name;
            $row[] = $val->userfullname;
            $row[] = '<a href="'.base_url('/Manageadmin/deleteuserrole/'.$val->id).'">Delete</a>';
            
            $data[] = $row;
        }
		$output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Userrole_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	public function deleteuserrole(){
		$id = $this->uri->segment(3); 
		if($id){
			$this->db->delete('bd_role', array('id' => $id)); 
		}
		redirect(base_url('Manageadmin/userRole'));
	}
	
	public function userRolepermission(){
		$rolepermission = array();
		$roleid = '';
		$query = $this->db->query('select * from role where is_active = 1');
		$role = $query->result();
		
		/* $query1 = $this->db->query('select * from menu_table where is_active = 1');
		$menu = $query1->result(); */
		$sidemenu = $this->Front_model->getsideMenu();
		$topmenu = $this->Front_model->gettopMenu();
		
		$this->load->view('admin/permission/rolepermission_view', compact('topmenu','roleid','rolepermission','sidemenu','role','businessunit'));
	}
	
	public function savepermission(){
		//echo '<pre>'; print_r($_REQUEST); die;
		$this->form_validation->set_rules('role_id', 'Role Name', 'required');
		if ($this->form_validation->run() == FALSE) {
            redirect(base_url('Manageadmin/userRolepermission'));
        } else {
			if($_REQUEST){
				$roleID = $_REQUEST['role_id'];
				$menuID = $_REQUEST['menu_id'];
				$submenuID = $_REQUEST['submenu_id'];
				for($i = 0 ; $i < count($menuID); $i++){
					$arr = array(
							'role_id'=>$roleID,
							'menu_id'=>$menuID[$i]
						);
					$respon = $this->mastermodel->InsertMasterData($arr,'role_permission');
				}
				
				for($i = 0 ; $i < count($submenuID); $i++){
					$arr = array(
							'role_id'=>$roleID,
							'submenu_id'=>$submenuID[$i]
						);
					$respon = $this->mastermodel->InsertMasterData($arr,'role_permission');
				}
				if ($respon):
					$this->session->set_flashdata('success_msg', 'User Role Permission Added successfully');
					redirect(base_url('Manageadmin/userRolepermission'));
				else:
					$this->session->set_flashdata('error_msg', 'Something went gone wrong');
					redirect(base_url('Manageadmin/userRolepermission'));
				endif; 
			}else{
				$this->session->set_flashdata('error_msg', 'Something went gone wrong');
				redirect(base_url('Manageadmin/userRolepermission'));
			}
        }
	}
	
	public function getsubMenu(){
		/* if($_REQUEST['roleid']){
			//print_r($_REQUEST); die;
			$roleid =  $_REQUEST['roleid'];
			$query1 = $this->db->query('select * from role_permission where active = 1 and role_id = '.$roleid);
			$rolepermission = $query1->result();
			
			foreach($rolepermission as $per){
				$submenuID[] =  $per->submenu_id;
			}
			$submenu = $this->Front_model->getsubMenu($_REQUEST['menuid']);
			foreach($submenu as $val){
				$checked = "";
				if(in_array($val->id,$submenuID)){
					$checked = "checked";
				}
				//$submenuArr[] = array('id'=>$val->id,'submenu'=>$val->menuname);
				$submenuArr[] =  '<label class="container1">'. $val->menuname . '<input '. $checked .' type="checkbox" id="submenu_id" name="submenu_id[]" value="'. $val->id .'"></input><span class="checkmark1"></span></label>';
			}
			echo json_encode($submenuArr);
			//$('.submenu_'+menuid).append('<label class="container1">'+ value.submenu +'<input type="checkbox" id="submenu_id" name="submenu_id[]" value="'+ value.id +'"></input><span class="checkmark1"></span></label>');
			
		}else{ */
			$submenu = $this->Front_model->getsubMenu($_REQUEST['menuid']);
			//echo '<pre>'; print_r($submenu); die;
			foreach($submenu as $val){
				$submenuArr[] = array('id'=>$val->id,'submenu'=>$val->menuname);
			}
			echo json_encode($submenuArr);
		//}
	}
	
	
	public function edituserRolepermission(){
		$rolepermission = array();
		$roleid = '';
		$query = $this->db->query('select * from role where is_active = 1');
		$role = $query->result();
		
		if($_REQUEST){
			$roleid =  $_REQUEST['role_id'];
			$query1 = $this->db->query('select * from role_permission where active = 1 and role_id = '.$roleid);
			$rolepermission = $query1->result();
			//echo '<pre>'; print_r($rolepermission);
		}
		/* $query1 = $this->db->query('select * from menu_table where is_active = 1');
		$menu = $query1->result(); */
		$sidemenu = $this->Front_model->getsideMenu();
		$topmenu = $this->Front_model->gettopMenu();
		
		$this->load->view('admin/permission/editrolepermission_view', compact('topmenu','roleid','rolepermission','sidemenu','role','businessunit'));
	}
	
	
	public function saveeditpermission(){
		//echo '<pre>'; print_r($_REQUEST); die;
		$this->form_validation->set_rules('roleid', 'Role Name', 'required');
		if ($this->form_validation->run() == FALSE) {
            redirect(base_url('Manageadmin/edituserRolepermission'));
        } else {
			if($_REQUEST){
				$roleID = $_REQUEST['roleid'];
				$menuID = $_REQUEST['menu_id'];
				$submenuID = $_REQUEST['submenu_id'];
				
				$this->db->where('role_id', $roleID);
				$this->db->delete('role_permission');
				
				for($i = 0 ; $i < count($menuID); $i++){
					$arr = array('role_id'=>$roleID,'menu_id'=>$menuID[$i]);
					$respon = $this->mastermodel->InsertMasterData($arr,'role_permission');
				}
				
				for($i = 0 ; $i < count($submenuID); $i++){
					$arr = array(
							'role_id'=>$roleID,
							'submenu_id'=>$submenuID[$i]
						);
					$respon = $this->mastermodel->InsertMasterData($arr,'role_permission');
				}
				if ($respon):
					$this->session->set_flashdata('success_msg', 'User Role Permission Added successfully');
					redirect(base_url('Manageadmin/edituserRolepermission'));
				else:
					$this->session->set_flashdata('error_msg', 'Something went gone wrong');
					redirect(base_url('Manageadmin/edituserRolepermission'));
				endif;
			}else{
				$this->session->set_flashdata('error_msg', 'Something went gone wrong');
				redirect(base_url('Manageadmin/edituserRolepermission'));
			}
        }
	}
}
