<?php
/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Front_model');
        $this->load->model('mastermodel');
        $this->load->model('Accountdashboard_model');
        $this->load->model('Dashboardac_model', 'dashboardacmodel');
        $this->load->model('SecondDB_model');
        $this->load->model('Activeproject_model', 'activeproject');
        $this->load->model('Importantproject_model', 'importantproject');
        $this->load->model('Bidproject_model', 'bidproject');
        $this->load->model('Newreportsummary_model');
        $this->load->model('Accountsummaryreport_model', 'accountsummaryreport');
        $this->load->model('Projectassigntoemp_model', 'projectassigntoemp');
        $this->load->model('vacant_position__model');

        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }

        $bduserid = $this->session->userdata('loginid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        
        if ($actual_link == "http://bd.cegtechno.com") {
            if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
                
            else: 
                redirect(base_url('welcome/logout'));
            endif;
        }

        // $this->db2 = $this->load->database('another_db', TRUE);
        // $db2 = $this->db2->database;
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

	 /*
            // echo $title; die;
            // CODE BY ASheesh Financial Year... 10-05-2019..
            $checkdate = date("d-m-Y");
            $current_years = date("Y",strtotime("-1 year"));
            // echo $current_years; die;
            $current_years_chk = '01-04-' . $current_years;
            // echo $checkdate; die;
            if ($current_years_chk <= $checkdate) {
            $start_date = $current_years . '-04-01';
            $end_date = ($current_years + 1) . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
            } else {
            $start_date = ($current_years - 1) . '-04-01';
            $end_date = $current_years . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
            }

            //Count Rec For Ongoing Biding Code Asheesh..
            $query1 = $this->db->query('select count(b.fld_id) as tobesubmitted from bd_tenderdetail as b left join bdtender_scope as s on b.fld_id  = s.project_id left join tender_generated_id as t on b.fld_id = t.project_id where t.generate_type != "E" AND s.visible_scope="To_Go_project"');
            $res1 = $query1->result();
            $tobesubmittedcount = isset($res1[0]->tobesubmitted) ? $res1[0]->tobesubmitted : '0';
            $this->db->select('a.fld_id,c.generate_type');
            $this->db->from('bd_tenderdetail as a');
            $this->db->join('bdtender_scope as b', 'a.fld_id  = b.project_id', 'left');
            $this->db->join('tender_generated_id as c', 'a.fld_id  = c.project_id', 'left');
            $this->db->where('b.visible_scope', 'To_Go_project');
            // $this->db->where('a.created_By !=', '414');
            $this->db->where($where_date);
            $submittedres = $this->db->get()->result();

            //Code Rec For Won On Dashboard..
            $this->db->select('aa.id');
            $this->db->from('bdproject_status as aa');
            $this->db->where(array('aa.is_active' => '1', 'aa.project_status' => '1'));
            $this->db->where($where_date2);
            $woncount = $this->db->get()->num_rows();
            // Code Rec of won:- FQ and RFP.. Asheesh 10-05-2019..
            $this->db->select('aaa.project_id,c.generate_type');
            $this->db->from('bdproject_status as aaa');
            $this->db->join('tender_generated_id as c', 'aaa.project_id  = c.project_id', 'left');
            $this->db->where('aaa.project_status', '1');
            $this->db->where('aaa.is_active', '1');
            $this->db->where('aaa.user_id !=', '414');
            $this->db->where($where_date3);
            $wonresdata = $this->db->get()->result();
            //Code Rec of Result Awaiting..
            $this->db->select('aaaa.id,t.generate_type');
            $this->db->from('bdproject_status as aaaa');
            $this->db->join('tender_generated_id as t', 'aaaa.project_id  = t.project_id', 'INNER');
            $this->db->where(array('aaaa.is_active' => '1', 'aaaa.project_status' => '0'));
            $this->db->where('aaaa.user_id !=', '414');
            $this->db->where($where_date4);
            $awaitingres = $this->db->get()->result();

            $businessID = $this->session->userdata('businessunit_id');
            $this->db->select('a.*,b.generated_tenderid,b.generate_type,c.assign_to,e.*');
            $this->db->from('bd_tenderdetail as a');
            $this->db->join('bdproject_status as e', 'a.fld_id = e.project_id', 'left');
            $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
            $this->db->join('assign_project_manager as c', 'a.fld_id = c.project_id', 'left');
            $this->db->join('bdtender_scope as f', "a.fld_id = f.project_id", "left");
            //$this->db->where('f.visible_scope', 'Bid_project');
            // $this->db->where('f.businessunit_id', $businessID);
            $this->db->where(array('a.is_active' => "1", 'f.businessunit_id' => $businessID, 'f.visible_scope' => 'Bid_project'));
            $this->db->where($where_date5);
            $queryres = $this->db->get()->result_object();
            $submitcount = isset($queryres) ? count($queryres) : '0';


            //EOI And RFP Project Count
            $this->db->select('SUM(IF(`b`.`generate_type` = "P", 1,0)) AS `rfp`,SUM(IF(`b`.`generate_type` = "E", 1,0)) AS `eoi`');
            $this->db->from('bd_tenderdetail as a');
            $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
            $this->db->join('bdtender_scope as c', 'a.fld_id = c.project_id', 'left');
            $this->db->where('c.visible_scope', 'To_Go_project');
            //$this->db->where($where_date);
            $queryreseoicount = $this->db->get()->result_object()[0];

            // Eoi Project
            $this->db->select('a.TenderDetails,a.Created_Date');
            $this->db->from('bd_tenderdetail as a');
            $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
            $this->db->where('b.generate_type', 'e');
            $this->db->order_by('a.fld_id', 'desc');
            $this->db->limit('5');
            $eoi = $this->db->get()->result_array();

            // RFP Project
            $this->db->select('a.TenderDetails,a.Created_Date,b.project_id');
            $this->db->from('bd_tenderdetail as a');
            $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
            $this->db->where('b.generate_type', 'P');
            $this->db->order_by('a.fld_id', 'desc');
            $this->db->limit('5');
            $rfp = $this->db->get()->result_array();

            //Result Awaiting RFP
            $this->db->select('a.TenderDetails,a.Created_Date,b.project_id');
            $this->db->from('bd_tenderdetail as a');
            $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
            $this->db->join('bdtender_scope as f', "a.fld_id = f.project_id", "left");
            $this->db->join('bdproject_status as e', 'a.fld_id = e.project_id', 'left');
            $this->db->where('f.visible_scope', 'Bid_project');
            $this->db->where('f.businessunit_id', $businessID);
            $this->db->where('e.project_status', '0');
            $this->db->where('b.generate_type', 'P');
            $this->db->order_by('a.fld_id', 'desc');
            $this->db->limit('5');
            $resultawaitrfp = $this->db->get()->result_array();

            //Result Won
            $this->db->select('a.TenderDetails,a.Created_Date,b.project_id');
            $this->db->from('bd_tenderdetail as a');
            $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
            $this->db->join('bdtender_scope as f', "a.fld_id = f.project_id", "left");
            $this->db->join('bdproject_status as e', 'a.fld_id = e.project_id', 'left');
            $this->db->where('f.visible_scope', 'Bid_project');
            $this->db->where('f.businessunit_id', $businessID);
            $this->db->where('e.project_status', '1');
            $this->db->order_by('a.fld_id', 'desc');
            $this->db->limit('5');
            $resultwonrfp = $this->db->get()->result_array();
            // echo '<pre>'; print_r($resultwonrfp);
            // Important Project
            $query2 = $this->db->query('select fld_id,TenderDetails,Created_Date from bd_tenderdetail left join bdtender_scope on bd_tenderdetail.fld_id  = bdtender_scope.project_id where bdtender_scope.visible_scope="Important_project" order by fld_id desc limit 5');
            $important = $query2->result_array();

            // Submitted Project
            $query3 = $this->db->query('select fld_id,TenderDetails,Created_Date from bd_tenderdetail left join bdtender_scope on bd_tenderdetail.fld_id  = bdtender_scope.project_id where bdtender_scope.visible_scope ="Bid_project" order by fld_id desc limit 5');
            $submitted = $query3->result_array();
            
            $this->load->view('dashboard_view', compact('title', 'resultawaitrfp', 'resultwonrfp', 'queryres', 'awaitingres', 'wonresdata', 'submittedres', 'queryreseoicount', 'tobesubmittedcount', 'eoi', 'rfp', 'important', 'submitted', 'woncount', 'awatingcount', 'submitcount'));
            
            */
    public function index() {
	    $title = 'Dashboard ';
        $this->load->view('dashboard_view', compact('title'));
    }
 

    public function dashboardAnalytics() {
	 
        $checkdate = date("d-m-Y");
        $current_years = date("Y",strtotime("-1 year"));
		 
        $current_years_chk = '01-04-' . $current_years;
 
        if ($current_years_chk <= $checkdate) {
            $start_date = $current_years . '-04-01';
            $end_date = ($current_years + 1) . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
        } else {
            $start_date = ($current_years - 1) . '-04-01';
            $end_date = $current_years . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
        }

        //Count Rec For Ongoing Biding Code Asheesh..
        $query1 = $this->db->query('select count(b.fld_id) as tobesubmitted from bd_tenderdetail as b left join bdtender_scope as s on b.fld_id  = s.project_id left join tender_generated_id as t on b.fld_id = t.project_id where t.generate_type != "E" AND s.visible_scope="To_Go_project"');
        $res1 = $query1->result();
        $tobesubmittedcount = isset($res1[0]->tobesubmitted) ? $res1[0]->tobesubmitted : '0';
        $this->db->select('a.fld_id,c.generate_type');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('bdtender_scope as b', 'a.fld_id  = b.project_id', 'left');
        $this->db->join('tender_generated_id as c', 'a.fld_id  = c.project_id', 'left');
        $this->db->where('b.visible_scope', 'To_Go_project');
		// $this->db->where('a.created_By !=', '414');
        $this->db->where($where_date);
        $submittedres = $this->db->get()->result();

        //Code Rec For Won On Dashboard..
        $this->db->select('aa.id');
        $this->db->from('bdproject_status as aa');
        $this->db->where(array('aa.is_active' => '1', 'aa.project_status' => '1'));
        $this->db->where($where_date2);
        $woncount = $this->db->get()->num_rows();
        // Code Rec of won:- FQ and RFP.. Asheesh 10-05-2019..
        $this->db->select('aaa.project_id,c.generate_type');
        $this->db->from('bdproject_status as aaa');
        $this->db->join('tender_generated_id as c', 'aaa.project_id  = c.project_id', 'left');
        $this->db->where('aaa.project_status', '1');
        $this->db->where('aaa.is_active', '1');
        $this->db->where('aaa.user_id !=', '414');
        $this->db->where($where_date3);
        $wonresdata = $this->db->get()->result();
        //Code Rec of Result Awaiting..
        $this->db->select('aaaa.id,t.generate_type');
        $this->db->from('bdproject_status as aaaa');
        $this->db->join('tender_generated_id as t', 'aaaa.project_id  = t.project_id', 'INNER');
        $this->db->where(array('aaaa.is_active' => '1', 'aaaa.project_status' => '0'));
        $this->db->where('aaaa.user_id !=', '414');
        $this->db->where($where_date4);
        $awaitingres = $this->db->get()->result();

        $businessID = $this->session->userdata('businessunit_id');
        $this->db->select('a.*,b.generated_tenderid,b.generate_type,c.assign_to,e.*');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('bdproject_status as e', 'a.fld_id = e.project_id', 'left');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->join('assign_project_manager as c', 'a.fld_id = c.project_id', 'left');
        $this->db->join('bdtender_scope as f', "a.fld_id = f.project_id", "left");
        //$this->db->where('f.visible_scope', 'Bid_project');
        // $this->db->where('f.businessunit_id', $businessID);
        $this->db->where(array('a.is_active' => "1", 'f.businessunit_id' => $businessID, 'f.visible_scope' => 'Bid_project'));
        $this->db->where($where_date5);
        $queryres = $this->db->get()->result_object();
        $submitcount = isset($queryres) ? count($queryres) : '0';


        //EOI And RFP Project Count
        $this->db->select('SUM(IF(`b`.`generate_type` = "P", 1,0)) AS `rfp`,SUM(IF(`b`.`generate_type` = "E", 1,0)) AS `eoi`');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->join('bdtender_scope as c', 'a.fld_id = c.project_id', 'left');
        $this->db->where('c.visible_scope', 'To_Go_project');
        //$this->db->where($where_date);
        $queryreseoicount = $this->db->get()->result_object()[0];
  
        $query2 = $this->db->query('select fld_id,TenderDetails,Created_Date from bd_tenderdetail left join bdtender_scope on bd_tenderdetail.fld_id  = bdtender_scope.project_id where bdtender_scope.visible_scope="Important_project" order by fld_id desc limit 5');
        $important = $query2->result_array();

        // Submitted Project
        $query3 = $this->db->query('select fld_id,TenderDetails,Created_Date from bd_tenderdetail left join bdtender_scope on bd_tenderdetail.fld_id  = bdtender_scope.project_id where bdtender_scope.visible_scope ="Bid_project" order by fld_id desc limit 5');
        $submitted = $query3->result_array();


        // $this->load->view('dashboard_view', compact('title', 'resultawaitrfp', 'resultwonrfp', 'queryres', 'awaitingres', 'wonresdata', 'submittedres', 'queryreseoicount', 'tobesubmittedcount', 'eoi', 'rfp', 'important', 'submitted', 'woncount', 'awatingcount', 'submitcount'));
        
        $arrMessage = array(
          
            // 'resultawaitrfp' => $resultawaitrfp, 
            // 'resultwonrfp' => $resultwonrfp, 
            'queryres' => $queryres, 
            'awaitingres' => $awaitingres, 
            'wonresdata' => $wonresdata, 
            'submittedres' => $submittedres, 
            'queryreseoicount' => $queryreseoicount, 
            'tobesubmittedcount' => $tobesubmittedcount, 
            // 'eoi' => $eoi, 
            // 'rfp' => $rfp, 
            'important' => $important, 
            'submitted' => $submitted, 
            'woncount' => $woncount, 
            'awatingcount' => $awatingcount, 
            'submitcount' => $submitcount,
           
        );

        echo json_encode($arrMessage);
   
    }

    public function dashboardAnalyticsOn() {
	 
        $checkdate = date("d-m-Y");
        $current_years = date("Y",strtotime("-1 year"));
		 
        $current_years_chk = '01-04-' . $current_years;
 
        if ($current_years_chk <= $checkdate) {
            $start_date = $current_years . '-04-01';
            $end_date = ($current_years + 1) . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
        } else {
            $start_date = ($current_years - 1) . '-04-01';
            $end_date = $current_years . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
        }

        //Count Rec For Ongoing Biding Code Asheesh..
        $query1 = $this->db->query('select count(b.fld_id) as tobesubmitted from bd_tenderdetail as b left join bdtender_scope as s on b.fld_id  = s.project_id left join tender_generated_id as t on b.fld_id = t.project_id where t.generate_type != "E" AND s.visible_scope="To_Go_project"');
        $res1 = $query1->result();
        $tobesubmittedcount = isset($res1[0]->tobesubmitted) ? $res1[0]->tobesubmitted : '0';
        $this->db->select('a.fld_id,c.generate_type');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('bdtender_scope as b', 'a.fld_id  = b.project_id', 'left');
        $this->db->join('tender_generated_id as c', 'a.fld_id  = c.project_id', 'left');
        $this->db->where('b.visible_scope', 'To_Go_project');
		// $this->db->where('a.created_By !=', '414');
        $this->db->where($where_date);
        $submittedres = $this->db->get()->result();



         //EOI And RFP Project Count
         $this->db->select('SUM(IF(`b`.`generate_type` = "P", 1,0)) AS `rfp`,SUM(IF(`b`.`generate_type` = "E", 1,0)) AS `eoi`');
         $this->db->from('bd_tenderdetail as a');
         $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
         $this->db->join('bdtender_scope as c', 'a.fld_id = c.project_id', 'left');
         $this->db->where('c.visible_scope', 'To_Go_project');
         //$this->db->where($where_date);
         $queryreseoicount = $this->db->get()->result_object()[0];
  


        $arrMessage = array(
          
           
            'submittedres' => $submittedres,
            'queryreseoicount' => $queryreseoicount 
            
           
        );

        echo json_encode($arrMessage);
   
    }

    
    
    public function dashboardAnalyticsWon() {
	 
        $checkdate = date("d-m-Y");
        $current_years = date("Y",strtotime("-1 year"));
		 
        $current_years_chk = '01-04-' . $current_years;
 
        if ($current_years_chk <= $checkdate) {
            $start_date = $current_years . '-04-01';
            $end_date = ($current_years + 1) . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
        } else {
            $start_date = ($current_years - 1) . '-04-01';
            $end_date = $current_years . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
        }

        
        //Code Rec For Won On Dashboard..
        $this->db->select('aa.id');
        $this->db->from('bdproject_status as aa');
        $this->db->where(array('aa.is_active' => '1', 'aa.project_status' => '1'));
        $this->db->where($where_date2);
        $woncount = $this->db->get()->num_rows();
        // Code Rec of won:- FQ and RFP.. Asheesh 10-05-2019..




        $this->db->select('aaa.project_id,c.generate_type');
        $this->db->from('bdproject_status as aaa');
        $this->db->join('tender_generated_id as c', 'aaa.project_id  = c.project_id', 'left');
        $this->db->where('aaa.project_status', '1');
        $this->db->where('aaa.is_active', '1');
        $this->db->where('aaa.user_id !=', '414');
        $this->db->where($where_date3);
        $wonresdata = $this->db->get()->result();


        $businessID = $this->session->userdata('businessunit_id');
         

        // $this->load->view('dashboard_view', compact('title', 'resultawaitrfp', 'resultwonrfp', 'queryres', 'awaitingres', 'wonresdata', 'submittedres', 'queryreseoicount', 'tobesubmittedcount', 'eoi', 'rfp', 'important', 'submitted', 'woncount', 'awatingcount', 'submitcount'));
        
        $arrMessage = array(
          
             
            'wonresdata' => $wonresdata, 
             
           
        );

        echo json_encode($arrMessage);
   
    }


    
    public function dashboardAnalyticsSub() {
	 
        $checkdate = date("d-m-Y");
        $current_years = date("Y",strtotime("-1 year"));
		 
        $current_years_chk = '01-04-' . $current_years;
 
        if ($current_years_chk <= $checkdate) {
            $start_date = $current_years . '-04-01';
            $end_date = ($current_years + 1) . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
        } else {
            $start_date = ($current_years - 1) . '-04-01';
            $end_date = $current_years . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
        }

         
        $businessID = $this->session->userdata('businessunit_id');
        $this->db->select('a.*,b.generated_tenderid,b.generate_type,c.assign_to,e.*');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('bdproject_status as e', 'a.fld_id = e.project_id', 'left');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->join('assign_project_manager as c', 'a.fld_id = c.project_id', 'left');
        $this->db->join('bdtender_scope as f', "a.fld_id = f.project_id", "left");
        //$this->db->where('f.visible_scope', 'Bid_project');
        // $this->db->where('f.businessunit_id', $businessID);
        $this->db->where(array('a.is_active' => "1", 'f.businessunit_id' => $businessID, 'f.visible_scope' => 'Bid_project'));
        $this->db->where($where_date5);
        $queryres = $this->db->get()->result_object();
         

        
       
 
        $arrMessage = array(
          
       
            'queryres' => $queryres, 
             
           
        );

        echo json_encode($arrMessage);
   
    }


    
    public function dashboardAnalyticsAwait() {
	 
        $checkdate = date("d-m-Y");
        $current_years = date("Y",strtotime("-1 year"));
		 
        $current_years_chk = '01-04-' . $current_years;
 
        if ($current_years_chk <= $checkdate) {
            $start_date = $current_years . '-04-01';
            $end_date = ($current_years + 1) . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
        } else {
            $start_date = ($current_years - 1) . '-04-01';
            $end_date = $current_years . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $where_date2 = "(aa.status_date>='$start_date' AND aa.status_date <= '$end_date')";
            $where_date3 = "(aaa.status_date>='$start_date' AND aaa.status_date <= '$end_date')";
            $where_date4 = "(aaaa.status_date>='$start_date' AND aaaa.status_date <= '$end_date')";
            $where_date5 = "(e.status_date>='$start_date' AND e.status_date <= '$end_date')";
        }

        
        //Code Rec of Result Awaiting..
        $this->db->select('aaaa.id,t.generate_type');
        $this->db->from('bdproject_status as aaaa');
        $this->db->join('tender_generated_id as t', 'aaaa.project_id  = t.project_id', 'INNER');
        $this->db->where(array('aaaa.is_active' => '1', 'aaaa.project_status' => '0'));
        $this->db->where('aaaa.user_id !=', '414');
        $this->db->where($where_date4);
        $awaitingres = $this->db->get()->result();

        


        // $this->load->view('dashboard_view', compact('title', 'resultawaitrfp', 'resultwonrfp', 'queryres', 'awaitingres', 'wonresdata', 'submittedres', 'queryreseoicount', 'tobesubmittedcount', 'eoi', 'rfp', 'important', 'submitted', 'woncount', 'awatingcount', 'submitcount'));


        // print_r($this->db->last_query()); die;
        
        $arrMessage = array(
          
            'awaitingres' => $awaitingres, 
             
           
        );

        echo json_encode($arrMessage);
   
    }


    


    public function tableProp() {
		 
        $this->db->select('a.TenderDetails,a.Created_Date,b.project_id');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->where('b.generate_type', 'P');
        $this->db->order_by('a.fld_id', 'desc');
        $this->db->limit('5');
        $rfp = $this->db->get()->result();

      
	 // print_r($this->db->last_query());

	 
        $data = array();
        $no = $_POST['start'];
       
        //echo '<pre>'; print_r($list);
        foreach ($rfp as $rfpdata) {
            $row = array();
            $row[] = $rfpdata->TenderDetails;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($rfp),
            "recordsFiltered" => count($rfp),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }



    public function tableEoi() {
		 
        $this->db->select('a.TenderDetails,a.Created_Date');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->where('b.generate_type', 'e');
        $this->db->order_by('a.fld_id', 'desc');
        $this->db->limit('5');
        $eoi = $this->db->get()->result();


        $data = array();
        $no = $_POST['start'];
       
        //echo '<pre>'; print_r($list);
        foreach ($eoi as $eoidata) {
            $row = array();
            $row[] = $eoidata->TenderDetails;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($eoi),
            "recordsFiltered" => count($eoi),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function tableAwait() {
        // resultawaitrfp
        $businessID = $this->session->userdata('businessunit_id');
        $this->db->select('a.TenderDetails,a.Created_Date,b.project_id');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->join('bdtender_scope as f', "a.fld_id = f.project_id", "left");
        $this->db->join('bdproject_status as e', 'a.fld_id = e.project_id', 'left');
        $this->db->where('f.visible_scope', 'Bid_project');
        $this->db->where('f.businessunit_id', $businessID);
        $this->db->where('e.project_status', '0');
        $this->db->where('b.generate_type', 'P');
        $this->db->order_by('a.fld_id', 'desc');
        $this->db->limit('5');
        $resultawaitrfp = $this->db->get()->result();


        


        // echo '<pre>'; print_r($this->db->last_query()); die;
	 
        $data = array();
        $no = $_POST['start'];
       
        //echo '<pre>'; print_r($list);
        foreach ($resultawaitrfp as $eoidata) {
            $row = array();
            $row[] = $eoidata->TenderDetails;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($resultawaitrfp),
            "recordsFiltered" => count($resultawaitrfp),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function tableWon() {
        // resultwonrfp
        $businessID = $this->session->userdata('businessunit_id'); 
        $this->db->select('a.TenderDetails,a.Created_Date,b.project_id');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->join('bdtender_scope as f', "a.fld_id = f.project_id", "left");
        $this->db->join('bdproject_status as e', 'a.fld_id = e.project_id', 'left');
        $this->db->where('f.visible_scope', 'Bid_project');
        $this->db->where('f.businessunit_id', $businessID);
        $this->db->where('e.project_status', '1');
        $this->db->order_by('a.fld_id', 'desc');
        $this->db->limit('5');
        $resultwonrfp = $this->db->get()->result();


	 
        $data = array();
        $no = $_POST['start'];
       
        //echo '<pre>'; print_r($list);
        foreach ($resultwonrfp as $eoidata) {
            $row = array();
            $row[] = $eoidata->TenderDetails;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($resultwonrfp),
            "recordsFiltered" => count($resultwonrfp),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }


//Dashbord After Login..
    public function indexasd() {
        $title = 'Dashboard';
		
        $query1 = $this->db->query('select count(b.fld_id) as tobesubmitted from bd_tenderdetail as b left join bdtender_scope as s on b.fld_id  = s.project_id left join tender_generated_id as t on b.fld_id = t.project_id where t.generate_type != "E" AND s.visible_scope="To_Go_project"');
        $res1 = $query1->result();
        $tobesubmittedcount = isset($res1[0]->tobesubmitted) ? $res1[0]->tobesubmitted : '0';

        $this->db->select('a.fld_id,c.generate_type');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('bdtender_scope as b', 'a.fld_id  = b.project_id', 'left');
        $this->db->join('tender_generated_id as c', 'a.fld_id  = c.project_id', 'left');
        $this->db->where('b.visible_scope', 'To_Go_project');
        $submittedres = $this->db->get()->result();

//count Won
        /* $querywon = $this->db->query('select count(fld_id) as won from bid_project where project_status="1" and is_active="1"');
          $res2 = $querywon->result();
          $woncount = isset($res2[0]->won)? $res2[0]->won : '0';

          //count Awating
          $queryawating = $this->db->query('select count(fld_id) as awating from bid_project where project_status="0" and is_active="1"');
          $res3 = $queryawating->result();
          $awatingcount = isset($res3[0]->awating)? $res3[0]->awating : '0';

          //count Submitted
          $queryasub = $this->db->query('select count(fld_id) as submitted from bd_tenderdetails where visible_scope="Bid_project" and is_active="1"');
          $res4 = $queryasub->result();
          $submitcount = isset($res4[0]->submitted)? $res4[0]->submitted : '0'; */

        /* $checkdate =  date("d-m-Y");
          $current_years = date("Y");
          $current_years_chk = '01-04-'.$current_years;
          if($current_years_chk <= $checkdate){
          $start_date = $current_years.'-04-01';
          $end_date = ($current_years + 1).'-03-31';
          $where_date = "(b.entrydate>='$start_date' AND b.entrydate <= '$end_date')";
          $this->db->where($where_date);
          }else{
          $start_date = ($current_years - 1).'-04-01';
          $end_date = $current_years.'-03-31';
          $where_date = "(b.entrydate>='$start_date' AND b.entrydate <= '$end_date')";
          $this->db->where($where_date);
          }
          $this->db->select('e.project_status');
          $this->db->from('bd_tenderdetails as a');
          $this->db->join('bid_project as e', 'a.fld_id = e.project_id', 'left');
          $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
          $this->db->join('assign_project_manager as c', 'a.fld_id = c.project_id', 'left');
          $this->db->join('main_users as d', 'd.fld_id = c.assign_to', 'left');

          $this->db->where('a.is_active', '1');
          $this->db->where('a.visible_scope', 'Bid_project');

          $queryres = $this->db->get()->result_object();
          $submitcount =  isset($queryres) ? count($queryres) : '0';
          $awatingcount = $woncount  = 0;
          $i = $j =  0;
          if(!empty($queryres)){
          foreach ($queryres as $res) {
          if ($res->project_status == 0) {
          $awatingcount = $i;
          $i++;
          }else if ($res->project_status == 1) {
          $woncount = $j;
          $j++;
          }
          }
          } */
        $curryear = date('Y');
        $query12 = $this->db->query('select count(id) as wonres from bdproject_status where YEAR(status_date) = "' . $curryear . '" AND project_status="1" and is_active ="1"');
        $res12 = $query12->result();
        $woncount = isset($res12[0]->wonres) ? $res12[0]->wonres : '0';

        $this->db->select('a.project_id,c.generate_type');
        $this->db->from('bdproject_status as a');
        $this->db->join('tender_generated_id as c', 'a.project_id  = c.project_id', 'left');
        $this->db->where('a.project_status', '1');
        $this->db->where('a.is_active', '1');
        $where = "(YEAR(a.status_date) = '$curryear')";
        $this->db->where($where);
        $wonresdata = $this->db->get()->result();


        $query13 = $this->db->query('select count(b.id) as awaitres from bdproject_status as b  left join tender_generated_id as t on b.project_id = t.project_id where  b.project_status="0"  and b.is_active ="1"');
        $res13 = $query13->result();
        $awatingcount = isset($res13[0]->awaitres) ? $res13[0]->awaitres : '0';

        $this->db->select('a.project_id,c.generate_type');
        $this->db->from('bdproject_status as a');
        $this->db->join('tender_generated_id as c', 'a.project_id  = c.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('a.project_status', '0');
        $awaitingres = $this->db->get()->result();


        $checkdate = date("d-m-Y");
        $current_years = date("Y");
        $current_years_chk = '01-04-' . $current_years;
        if ($current_years_chk <= $checkdate) {
            $start_date = $current_years . '-04-01';
            $end_date = ($current_years + 1) . '-03-31';
            $where_date = "(b.entrydate>='$start_date' AND b.entrydate <= '$end_date')";
            $this->db->where($where_date);
        } else {
            $start_date = ($current_years - 1) . '-04-01';
            $end_date = $current_years . '-03-31';
            $where_date = "(b.entrydate>='$start_date' AND b.entrydate <= '$end_date')";
            $this->db->where($where_date);
        }

        $businessID = $this->session->userdata('businessunit_id');
        $this->db->select('a.*,b.generated_tenderid,b.generate_type,c.assign_to,e.*');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('bdproject_status as e', 'a.fld_id = e.project_id', 'left');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->join('assign_project_manager as c', 'a.fld_id = c.project_id', 'left');
        $this->db->join('bdtender_scope as f', "a.fld_id = f.project_id", "left");
        $this->db->where('f.visible_scope', 'Bid_project');
        $this->db->where('f.businessunit_id', $businessID);
        $this->db->where('e.user_id != ', '414');
		
        $queryres = $this->db->get()->result_object();
        $submitcount = isset($queryres) ? count($queryres) : '0';

//EOI And RFP Project Count
        $this->db->select('SUM(IF(`b`.`generate_type` = "P", 1,0)) AS `rfp`,SUM(IF(`b`.`generate_type` = "E", 1,0)) AS `eoi`');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->join('bdtender_scope as c', 'a.fld_id = c.project_id', 'left');
        $this->db->where('c.visible_scope', 'To_Go_project');
        $queryreseoicount = $this->db->get()->result_object()[0];


// Eoi Project
        $this->db->select('a.TenderDetails,a.Created_Date');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->where('b.generate_type', 'e');
        $this->db->order_by('a.fld_id', 'desc');
        $this->db->limit('5');
        $eoi = $this->db->get()->result_array();


// RFP Project
        $this->db->select('a.TenderDetails,a.Created_Date,b.project_id');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->where('b.generate_type', 'P');
        $this->db->order_by('a.fld_id', 'desc');
        $this->db->limit('5');
        $rfp = $this->db->get()->result_array();


// Important Project
        $query2 = $this->db->query('select fld_id,TenderDetails,Created_Date from bd_tenderdetail left join bdtender_scope on bd_tenderdetail.fld_id  = bdtender_scope.project_id where bdtender_scope.visible_scope="Important_project" order by fld_id desc limit 5');
        $important = $query2->result_array();

// Submitted Project
        $query3 = $this->db->query('select fld_id,TenderDetails,Created_Date from bd_tenderdetail left join bdtender_scope on bd_tenderdetail.fld_id  = bdtender_scope.project_id where bdtender_scope.visible_scope ="Bid_project" order by fld_id desc limit 5');
        $submitted = $query3->result_array();

        $this->load->view('dashboard_view', compact('title', 'queryres', 'awaitingres', 'wonresdata', 'submittedres', 'queryreseoicount', 'tobesubmittedcount', 'eoi', 'rfp', 'important', 'submitted', 'woncount', 'awatingcount', 'submitcount'));
    }

    public function dashboard() {
        $title = 'Dashboard';
        $years = $_REQUEST['financial_year'];
        $data = array();
        foreach ($years as $value) {
            $start_date = $value . '-04-01';
            $end_date = ($value + 1) . '-03-31';
            $where_date = "(d.date_latter_award>='$start_date' AND d.date_latter_award <= '$end_date')";
            //$where_date1 = "(c.entry_date>='$start_date' AND c.entry_date <= '$end_date')";
            $this->db->where($where_date);
            //$this->db->where($where_date1);
            $this->db->select('e.project_status,c.value_as_per_loa');
            $this->db->from('bdtender_scope as a');
            $this->db->join('bdproject_status as e', 'a.project_id = e.project_id', 'left');
            $this->db->join('tender_generated_id as b', 'a.project_id = b.project_id', 'left');
            $this->db->join('proj_financial_details as c', 'a.project_id = c.bd_projid', 'left');
            $this->db->join('proj_keydate_details as d', 'a.project_id = d.bd_projid', 'left');
            /* $this->db->join('assign_project_manager as c', 'a.fld_id = c.project_id', 'left');
              $this->db->join('main_users as d', 'd.fld_id = c.assign_to', 'left');
             */
//$this->db->where('a.is_active', '1');
            $this->db->where('a.visible_scope', 'Bid_project');

            $queryres = $this->db->get()->result_object();
            $submitcount = isset($queryres) ? count($queryres) : '0';
            $awatingcount = $woncount = $loosecount = $cancelcount = $billed_amount = 0;
            $i = $j = $k = $l = 0;
            if (!empty($queryres)) {
                foreach ($queryres as $res) {
                    if ($res->project_status == 0) {
                        $awatingcount = $i;
                        $i++;
                    } else if ($res->project_status == 1) {
                        $woncount = $j;
                        $j++;
                    } else if ($res->project_status == 2) {
                        $loosecount = $k;

                        $k++;
                    } else if ($res->project_status == 3) {
                        $cancelcount = $l;
                        $l++;
                    }
                    $billed_amount = $res->value_as_per_loa;
                }
            }
            $data[] = array('year' => $value, 'submitted' => $submitcount, 'awaiting' => $awatingcount, 'won' => $woncount, 'loose' => $loosecount, 'cancel' => $cancelcount, 'billed_amount' => $billed_amount);
        }
//print_r($data);
        $this->load->view('dashboard2_view', compact('title', 'data', 'years'));
    }

//Cru Dashboard
    public function cruDashboard() {
        $this->load->view('dashboard_cruview');
    }

//Latest To G List For Dashboard...
    public function latestToGoListDashboard() {
        return $this->Front_model->GetlatestToGoListDashboard();
    }

//Latest Important List For Dashboard...
    public function latestImportListDashboard() {
        return $this->Front_model->GetlatestImportantListDashboard();
    }

//Active Project List..
    public function latestActiveProjListDashboard() {
        return $this->Front_model->GetlatestActiveListDashboard();
    }

//Latest Bid Project..
    public function latestBidProjListDashboard() {
        return $this->Front_model->GetlatestBidProjectDashboard();
    }

//No Of Active Project,,,,,
    public function noOfActiveProject() {
        $Rec = $this->Front_model->selectRecord('active_project_byuser', array('fld_id'), array('is_active' => '1'));
        if ($Rec) {
            return $result = $Rec->num_rows();
        }
        return false;
    }

//No Of Active Project,,,,,
    public function noOfImportantProject() {
        $Rec = $this->Front_model->selectRecord('import_for_user', array('fld_id'), array('is_active' => '1'));
        if ($Rec) {
            return $result = $Rec->num_rows();
        }
        return false;
    }

//No Of InActive Proj..'
    public function noOfInActiveTrashProject() {
        $Rec = $this->Front_model->selectRecord('trash_for_user', array('fld_id'), array('is_active' => '1'));
        if ($Rec) {
            return $result = $Rec->num_rows();
        }
        return false;
    }

//No of Bid Project
    public function noOfBidProject() {
        $Rec = $this->Front_model->selectRecord('bid_project', array('fld_id'), array('is_active' => '1'));
        if ($Rec) {
            return $result = $Rec->num_rows();
        }
        return false;
    }

//No of To Do...
    public function noOfToGoProject() {
        $Rec = $this->Front_model->selectRecord('proj_togo_for_user', array('fld_id'), array('is_active' => '1'));
        if ($Rec) {
            return $result = $Rec->num_rows();
        }
        return '0';
    }

    public function newproject() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->GetAllTenderDetails();
        $secId = '0';
        $this->load->view('tender_view', compact('sectorArr', 'secId'));
    }

    public function newprojectdesign() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->GetAllTenderDetails();
        $secId = '0';
        $this->load->view('tender_newtender_design_view', compact('sectorArr', 'secId'));
    }

    public function newprojectconstruction() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->GetAllTenderDetails();
        $secId = '0';
        $this->load->view('tender_newtender_construction_view', compact('sectorArr', 'secId'));
    }

    public function newprojectelectrical() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->GetAllTenderDetails();
        $secId = '0';
        $this->load->view('tender_newtender_electrical_view', compact('sectorArr', 'secId'));
    }

    public function newprojectmaintenance() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->GetAllTenderDetails();
        $secId = '0';
        $this->load->view('tender_newtender_maintenance_view', compact('sectorArr', 'secId'));
    }

    public function newprojectother() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->GetAllTenderDetails();
        $secId = '0';
        $this->load->view('tender_newtender_other_view', compact('sectorArr', 'secId'));
    }

//All Active Project
    public function activeprojectbyuser() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->Front_model->GetActiveProjByUser();
//For Design..
        $forDesignprojIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $projdetailrow['sectorNaame'] = $this->GetAllSectorByID($projdetailrow['Sector_IDs']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Design", $PhaseIDsArr))) {
                    array_push($forDesignprojIDArr, $projdetailrow);
                }
            }
        }
//Construction...
        $forConstructionIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $projdetailrow['sectorNaame'] = $this->GetAllSectorByID($projdetailrow['Sector_IDs']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Construction", $PhaseIDsArr))) {
                    array_push($forConstructionIDArr, $projdetailrow);
                }
            }
        }
//Maintenance Phase..
        $forMaintenanceIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $projdetailrow['sectorNaame'] = $this->GetAllSectorByID($projdetailrow['Sector_IDs']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Maintenance", $PhaseIDsArr))) {
                    array_push($forMaintenanceIDArr, $projdetailrow);
                }
            }
        }
        $forOtherIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $projdetailrow['sectorNaame'] = $this->GetAllSectorByID($projdetailrow['Sector_IDs']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Other", $PhaseIDsArr))) {
                    array_push($forOtherIDArr, $projdetailrow);
                }
            }
        }
//For Other....
        $this->load->view('activetender_view', compact('TenderDetailArr', 'sectorArr', 'forDesignprojIDArr', 'forConstructionIDArr', 'forMaintenanceIDArr', 'forOtherIDArr'));
    }

//View table Inactive Project By User..
    public function inactiveprojectbyuser() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->Front_model->GetinActiveProjByUser();
        $secId = '0';

//Start Design Phse.
        $forDesignprojIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Design", $PhaseIDsArr))) {
                    array_push($forDesignprojIDArr, $projdetailrow);
                }
            }
        }

//Start Maintenance Phse.
        $forMaintenanceIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Maintenance", $PhaseIDsArr))) {
                    array_push($forMaintenanceIDArr, $projdetailrow);
                }
            }
        }
//Close Maintenance Phse.
//Construction Phase..
        $forConstructionIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Construction", $PhaseIDsArr))) {
                    array_push($forConstructionIDArr, $projdetailrow);
                }
            }
        }
//Close Construction close..
//Start Other Phase..
        $forOtherIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Other", $PhaseIDsArr))) {
                    array_push($forOtherIDArr, $projdetailrow);
                }
            }
        }
//Close Other Phase..
//Start Electrical Phase..
        $forElectricalIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Electrical", $PhaseIDsArr))) {
                    array_push($forElectricalIDArr, $projdetailrow);
                }
            }
        }
        $this->load->view('inactivetender_view', compact('sectorArr', 'TenderDetailArr', 'forDesignprojIDArr', 'forConstructionIDArr', 'forMaintenanceIDArr', 'forOtherIDArr'));
    }

//View table Get All Important Project By User...
    public function importantmarked() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->Front_model->GetAllImportantProjByID();
        $secId = '0';
//Start Design Phse.
        $forDesignprojIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Design", $PhaseIDsArr))) {
                    array_push($forDesignprojIDArr, $projdetailrow);
                }
            }
        }
//Start Maintenance Phse.
        $forMaintenanceIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Maintenance", $PhaseIDsArr))) {
                    array_push($forMaintenanceIDArr, $projdetailrow);
                }
            }
        }
//Close Maintenance Phse.
//Construction Phase..
        $forConstructionIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Construction", $PhaseIDsArr))) {
                    array_push($forConstructionIDArr, $projdetailrow);
                }
            }
        }
//Close Construction close..
//Start Other Phase..
        $forOtherIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Other", $PhaseIDsArr))) {
                    array_push($forOtherIDArr, $projdetailrow);
                }
            }
        }
//Close Other Phase..
//Start Electrical Phase..
        $forElectricalIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Electrical", $PhaseIDsArr))) {
                    array_push($forElectricalIDArr, $projdetailrow);
                }
            }
        }
        $this->load->view('importantmarked_view', compact('sectorArr', 'TenderDetailArr', 'forDesignprojIDArr', 'forConstructionIDArr', 'forMaintenanceIDArr', 'forOtherIDArr'));
    }

//View table Not Important Tabs...
    public function notimportantmarked() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->Front_model->GetAllNotImportantProjByID();
        $secId = '0';

//Start Design Phse.
        $forDesignprojIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Design", $PhaseIDsArr))) {
                    array_push($forDesignprojIDArr, $projdetailrow);
                }
            }
        }
//Start Maintenance Phse.
        $forMaintenanceIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Maintenance", $PhaseIDsArr))) {
                    array_push($forMaintenanceIDArr, $projdetailrow);
                }
            }
        }
//Close Maintenance Phse.
//Construction Phase..
        $forConstructionIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Construction", $PhaseIDsArr))) {
                    array_push($forConstructionIDArr, $projdetailrow);
                }
            }
        }
//Close Construction close..
//Start Other Phase..
        $forOtherIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Other", $PhaseIDsArr))) {
                    array_push($forOtherIDArr, $projdetailrow);
                }
            }
        }
//Close Other Phase..
//Start Electrical Phase..
        $forElectricalIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Electrical", $PhaseIDsArr))) {
                    array_push($forElectricalIDArr, $projdetailrow);
                }
            }
        }
        $this->load->view('notimportantmarked_view', compact('sectorArr', 'TenderDetailArr', 'forDesignprojIDArr', 'forConstructionIDArr', 'forMaintenanceIDArr', 'forOtherIDArr'));
    }

//View table To Go Project View 
    public function togoproject() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->Front_model->GetToGoProjByUser();
        $secId = '0';

//For EOI
        $forEOIIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['projectgenid']) {
                $IDsArr = explode("/", $projdetailrow['projectgenid']);
                $typerKey = $IDsArr[3];
                $prfix = substr($typerKey, 0, 1);
                if ($prfix == 'E') {
                    array_push($forEOIIDArr, $projdetailrow);
                }
            }
        }
//Close EOI..
//Start RFP
        $forRFPIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['projectgenid']) {
                $IDsArr = explode("/", $projdetailrow['projectgenid']);
                $typerKey = $IDsArr[3];
                $prfix = substr($typerKey, 0, 1);
                if ($prfix == 'P') {
                    array_push($forRFPIDArr, $projdetailrow);
                }
            }
        }



//Close RFP..
//Start FQ..
        $forFQIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['projectgenid']) {
                $IDsArr = explode("/", $projdetailrow['projectgenid']);
                $typerKey = $IDsArr[3];
                $prfix = substr($typerKey, 0, 2);
                if ($prfix == 'FQ') {
                    array_push($forFQIDArr, $projdetailrow);
                }
            }
        }
//Close FQ..
        $this->load->view('togotender_view', compact('sectorArr', 'TenderDetailArr', 'forFQIDArr', 'forRFPIDArr', 'forEOIIDArr'));
    }

//change to go project status by punit...
    public function deletetogo() {
        $projid = $_REQUEST['tndrid'];
        if (isset($projid)) {
            $res = $this->Front_model->updateRecord('proj_togo_for_user', array('is_active' => '0'), array('project_id' => $projid));
            if ($res) {
                echo 'success';
                die;
            } else {
                echo 'fail';
                die;
            }
        }
    }

//update expiry date by punit...
    public function saveeditdate() {
        $proj_id = $_REQUEST['tndrid'];
        $data['expdate'] = date('d M y', strtotime($_REQUEST['expdate']));
        $data['newdate'] = $_REQUEST['expdate'];
        if (isset($proj_id) && isset($data['expdate'])) {
            $res = $this->Front_model->updateRecord('bd_tenderdetail', array('Expiry_Date' => $data['expdate']), array('fld_id' => $proj_id));
//$Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($res) {
                echo $data['expdate'] . ',';
                echo $data['newdate'];
                die;
            }
        } else {
            echo "false";
            die;
        }
    }

//View table Get All Review Project By User..
    public function projectreview() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->Front_model->GetAllReviewProjByID();
        $secId = '0';

//Start Design Phse.
        $forDesignprojIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Design", $PhaseIDsArr))) {
                    array_push($forDesignprojIDArr, $projdetailrow);
                }
            }
        }
//Start Maintenance Phse.
        $forMaintenanceIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Maintenance", $PhaseIDsArr))) {
                    array_push($forMaintenanceIDArr, $projdetailrow);
                }
            }
        }
//Close Maintenance Phse.
//Construction Phase..
        $forConstructionIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Construction", $PhaseIDsArr))) {
                    array_push($forConstructionIDArr, $projdetailrow);
                }
            }
        }
//Close Construction close..
//Start Other Phase..
        $forOtherIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Other", $PhaseIDsArr))) {
                    array_push($forOtherIDArr, $projdetailrow);
                }
            }
        }
//Close Other Phase..
//Start Electrical Phase..
        $forElectricalIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Electrical", $PhaseIDsArr))) {
                    array_push($forElectricalIDArr, $projdetailrow);
                }
            }
        }
//print_r($forConstructionIDArr); die;
        $this->load->view('projectreview_view', compact('sectorArr', 'TenderDetailArr', 'forDesignprojIDArr', 'forConstructionIDArr', 'forMaintenanceIDArr', 'forOtherIDArr'));
    }

//View table No Go Section View .. 
    public function nogoproject() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->Front_model->GetNoGoProjByUser();
        $secId = '0';

//Start Design Phse.
        $forDesignprojIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Design", $PhaseIDsArr))) {
                    array_push($forDesignprojIDArr, $projdetailrow);
                }
            }
        }
//Start Maintenance Phse.
        $forMaintenanceIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Maintenance", $PhaseIDsArr))) {
                    array_push($forMaintenanceIDArr, $projdetailrow);
                }
            }
        }
//Close Maintenance Phse.
//Construction Phase..
        $forConstructionIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Construction", $PhaseIDsArr))) {
                    array_push($forConstructionIDArr, $projdetailrow);
                }
            }
        }
//Close Construction close..
//Start Other Phase..
        $forOtherIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Other", $PhaseIDsArr))) {
                    array_push($forOtherIDArr, $projdetailrow);
                }
            }
        }
//Close Other Phase..
//Start Electrical Phase..
        $forElectricalIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['keyword_phase']) {
                $IDsArr = explode(",", $projdetailrow['keyword_phase']);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Electrical", $PhaseIDsArr))) {
                    array_push($forElectricalIDArr, $projdetailrow);
                }
            }
        }

        $this->load->view('nogotender_view', compact('sectorArr', 'TenderDetailArr', 'forDesignprojIDArr', 'forConstructionIDArr', 'forMaintenanceIDArr', 'forOtherIDArr'));
    }

//View table 
    public function bidprojects() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->Front_model->GetAllbidprojects();
        /* echo '<pre>';
          print_r($TenderDetailArr); */
        $secId = '0';

//For EOI
        $forEOIIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['generated_tenderid']) {
                $IDsArr = explode("/", $projdetailrow['generated_tenderid']);
                $typerKey = $IDsArr[3];
                $prfix = substr($typerKey, 0, 1);
                if ($prfix == 'E') {
                    array_push($forEOIIDArr, $projdetailrow);
                }
            }
        }
//Close EOI..
//Start RFP
        $forRFPIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['generated_tenderid']) {
                $IDsArr = explode("/", $projdetailrow['generated_tenderid']);
                $typerKey = $IDsArr[3];
                $prfix = substr($typerKey, 0, 1);
                if ($prfix == 'P') {
                    array_push($forRFPIDArr, $projdetailrow);
                }
            }
        }



//Close RFP..
//Start FQ..
        $forFQIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['generated_tenderid']) {
                $IDsArr = explode("/", $projdetailrow['generated_tenderid']);
                $typerKey = $IDsArr[3];
                $prfix = substr($typerKey, 0, 2);
                if ($prfix == 'FQ') {
                    array_push($forFQIDArr, $projdetailrow);
                }
            }
        }
//Close FQ..

        $userID = $this->session->userdata('loginid');
        $userData = $this->db->query('select * from main_users where fld_id=' . $userID);
        $user = $userData->result();

        $this->load->view('bidprojects_view', compact('user', 'sectorArr', 'TenderDetailArr', 'forFQIDArr', 'forRFPIDArr', 'forEOIIDArr'));


//$this->load->view('bidprojects_view', compact('sectorArr', 'TenderDetailArr', 'forDesignprojIDArr', 'forConstructionIDArr', 'forMaintenanceIDArr', 'forOtherIDArr'));
    }

//All Record no Any Filter..
    public function resultsector() {
        $keyinsectorArr = array();
        $secId = '';

//Filter...
        $sectorArr = $this->GetActiveSector();
        $keyinsectorArr = $this->GetActiveSectorinKeyword($sectID);
//For All..
        $TenderDetailArr = $this->GetTenderDetails($sectID);

//Start Design Phse.
        $forDesignprojIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow->keyword_phase) {
                $IDsArr = explode(",", $projdetailrow->keyword_phase);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Design", $PhaseIDsArr))) {
                    array_push($forDesignprojIDArr, $projdetailrow);
                }
            }
        }
//Start Design Phse.
        $forMaintenanceIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow->keyword_phase) {
                $IDsArr = explode(",", $projdetailrow->keyword_phase);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Maintenance", $PhaseIDsArr))) {
                    array_push($forMaintenanceIDArr, $projdetailrow);
                }
            }
        }
//Close Design Phse.
//Construction Phase..
        $forConstructionIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow->keyword_phase) {
                $IDsArr = explode(",", $projdetailrow->keyword_phase);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Construction", $PhaseIDsArr))) {
                    array_push($forConstructionIDArr, $projdetailrow);
                }
            }
        }
//Close Construction close..
//Start Other Phase..
        $forOtherIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow->keyword_phase) {
                $IDsArr = explode(",", $projdetailrow->keyword_phase);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Other", $PhaseIDsArr))) {
                    array_push($forOtherIDArr, $projdetailrow);
                }
            }
        }
//Close Other Phase..
//Start Electrical Phase..
        $forElectricalIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow->keyword_phase) {
                $IDsArr = explode(",", $projdetailrow->keyword_phase);
                $PhaseIDsArr = array_filter($IDsArr);
                if ((in_array("Electrical", $PhaseIDsArr))) {
                    array_push($forElectricalIDArr, $projdetailrow);
                }
            }
        }
//Close Electrical Phase..   
        $secId = $sectID;
        $this->load->view('tender_view', compact('secId', 'sectorArr', 'keyinsectorArr', 'TenderDetailArr', 'forDesignprojIDArr', 'forMaintenanceIDArr', 'forConstructionIDArr', 'forOtherIDArr', 'forElectricalIDArr'));
    }

//After Sector Wise Filter..
    public function resultsectorfilter() {
        $keyinsectorArr = array();
        $secId = '';
        $sectID = $_REQUEST['sectorinput'];
//Filter...
        $Data['sectorArr'] = $this->GetActiveSector();
        if ($sectID) {
            $Data['keyinsectorArr'] = $this->GetActiveSectorinKeyword($sectID);
            $Data['TenderDetailArr'] = $this->GetTenderDetails($sectID);
            $Data['secId'] = $sectID;
        }
        $this->load->view('tender_view', $Data);
    }

//Get Active Sector..
    public function GetActiveSector() {
//$this->load->model('Front_model');
        $Rec = $this->Front_model->selectRecord('sector', array('*'), array('is_active' => '1'));
        if ($Rec) {
            return $Rec->result();
        } else {
            return false;
        }
    }

//Submit Project Concern Details..
    public function projectconcerndetails() {
//Get Project Id..
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('project_concern_details', array('concern_details'), array('project_id' => $projId));
        if ($Rec != '') {
            $data = $Rec->result();
        } else {
            $data[0] = '';
        }
        $this->load->view('welcome_message', compact('projId', 'data'));
    }

//Link View Details Popup Open Ash..
    public function projurlview() {
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        if ($Rec) {
            $RowDataArr = $Rec->row();
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        $this->load->view('welcome_link', compact('projId', 'hyperlink'));
    }

//Submit Project Concern...
    public function projconcernsubmit() {
        $projId = $_REQUEST['projId'];
        $projDetails = $_REQUEST['editor1'];
        $inserArr = array('user_id' => $this->session->userdata('loginid'),
            'project_id' => $projId,
            'concern_details' => $projDetails,
            'link' => '');
        $Respon = $this->Front_model->insertRecord('Project_concern_details', $inserArr);

        if ($Respon > 0):
            echo "<h1 align='center'>Project Details Submitted Successful .</h1>";
            ?>
            &nbsp;&nbsp;&nbsp; <button class="btn btn-info btn-sm" onclick="window.close()"> Close </button>
            <?php
        endif;
    }

//Get Sector By Id..
    public function GetActiveSectorById($secId) {
//$this->load->model('Front_model');
        $Rec = $this->Front_model->selectRecord('sector', array('sectName'), array('is_active' => '1', 'fld_id' => $secId));
        if ($Rec) {
            $recvalArr = $Rec->row();
            return $recvalArr->sectName;
        } else {
            return false;
        }
    }

//Get Active Keyword in Sector..
    public function GetActiveSectorinKeyword($secId) {
//$this->load->model('Front_model');
        $Rec = $this->Front_model->selectRecord('sectorkey', array('*'), array('is_active' => '1', 'sectID' => $secId));
        if ($Rec) {
            return $Rec->result();
        } else {
            return false;
        }
    }

//Get Tender Details By Sector Id..
    public function GetTenderDetails($sectorinput) {
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('*'), array('is_active' => '1', 'visible_scope' => 'New_project'));
        if ($Rec) {
            $resulatArr = $Rec->result();
            $viewDataArr = array();

            foreach ($resulatArr as $valRow) {
                if ($valRow->Sector_IDs) {
                    $sectIdArr = array_filter((explode(",", $valRow->Sector_IDs)));
                    if ((in_array($sectorinput, $sectIdArr)) and ( count($sectIdArr) > 0)) {
//Sector Name...
                        $valRow->sectorNaame = $this->GetAllSectorByID($valRow->Sector_IDs);
                        $valRow->keyword_phase = (isset($valRow->keyword_phase)) ? $this->Getkeyword_phaseReal($valRow->keyword_phase) : '';
                        array_push($viewDataArr, $valRow);
                    }
                }
            }

            return $viewDataArr;
        } else {
            return false;
        }
    }

//Get tender Detailks No Filter...
    public function GetAllTenderDetails() {
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('*'), array('is_active' => '1', 'visible_scope' => 'New_project'));
        if ($Rec) {
            $resulatArr = $Rec->result();
            $viewDataArr = array();
            foreach ($resulatArr as $valRow) {
//Sector Name...
                $valRow->sectorNaame = $this->GetAllSectorByID($valRow->Sector_IDs);
//Keyword Name..
                $valRow->keyword_phase = (isset($valRow->keyword_phase)) ? $this->Getkeyword_phaseReal($valRow->keyword_phase) : '';
                array_push($viewDataArr, $valRow);
            }
            return $viewDataArr;
        } else {
            return false;
        }
    }

//Get All Sectors By ID..
    public function GetAllSectorByID($Sector_IDs) {
//$this->load->model('Front_model');
        $strSecName = '';
        if ($Sector_IDs) {
            $IDsArr = explode(",", $Sector_IDs);
            $KeyIDSArr = array_unique(array_filter($IDsArr));
            foreach ($KeyIDSArr as $recRw) {
                $strSecName .= ", " . $this->GetActiveSectorById($recRw);
            }
            return ltrim($strSecName, ', ');
        } else {
            return '';
        }
    }

//Get Sector Name By Id..
    public function Getkeyword_phaseReal($keyword_phase) {
//$this->load->model('Front_model');
        $strSecName = '';
        if ($keyword_phase) {
            $IDsArr = explode(",", $keyword_phase);
            $SectorIDSArr = array_unique(array_filter($IDsArr));
            foreach ($SectorIDSArr as $recRw) {
                $strSecName .= ", " . $recRw;
            }
            return ltrim($strSecName, ', ');
        } else {
            return '';
        }
    }

//Active Project By User...
    public function activeproject() {
        if (($_REQUEST['actid']) and ( isset($_REQUEST['sectid']))) {
            $inserArr = array(
                'user_id' => $this->session->userdata('loginid'),
                'project_id' => $_REQUEST['actid'],
                'Sector_ID' => $_REQUEST['sectid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('active_project_byuser', $inserArr);
        }
        if ($Respon > 0):
//Update Project Status...
            $this->visibilityStatus('Active_project', $_REQUEST['actid']);
//sending mail... by punit...
//            $this->activemail();
//Notification Set..
//Notification Status..
            $this->notified($_REQUEST['actid'], 'Tender Activated.');
            if ($_REQUEST['sectid'] > 0) {
                $this->session->set_flashdata('msg', "Tender Activated.");
                redirect(base_url('/dashboard/resultsector?sectorinput=' . $_REQUEST['sectid']));
            } else {
                $this->session->set_flashdata('msg', "Tender Activated.");
                redirect(base_url('/dashboard/newproject'));
            }
        endif;
    }

//send mail for activate project by punit...
    function activemail() {
//        $query = $this->Front_model->selectRecordOrderByASC('active_project_byuser', array('*'), "is_active = 1 And action_date >= date ('".date('Y-m-d'.$time1)."') And action_date <= date ('".date('Y-m-d'.$time2)."')");
        $res = $this->Front_model->selectActiveProjects();
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "mail.cegindia.com";
        $config['smtp_port'] = "25";
        $config['smtp_user'] = "marketing@cegindia.com";
        $config['smtp_pass'] = "MARK-2015ceg";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $ci->email->initialize($config);
        $ci->email->from('marketing@cegindia.com');
//        $ci->email->from('pharma@cegtesthouse.com');
//        $list = array('rahulsaxenajpr@gmail.com');
//        $list = array('CSukhlecha@cegtesthouse.com');
        $list = array('marketing@cegindia.com');
//        $list = array('pntsrm26@gmail.com');
        $ci->email->to($list);
//        $ci->email->cc('Marketing@cegindia.com');
        $ci->email->subject('Today activate projects.');
        $ci->email->message('<p style="font-family:Constantia;color:#222;">Hello Sir/madem,     Today ' . $res . ' Projects are activated. <br/> <br/>'
                . '<a href=' . base_url() . '"/dashboard/activeprojectbyuser" title="Active Projects">Click Here</a> to see Today Active projects. <br/> <br/> Thanks & Regards <br/> <br/>CEG India</p>');
//        $ci->email->attach( 'C:\Users\AshishYadav\Desktop\Dr. Charu\attachment\Form 37- Approval.pdf');
//        $ci->email->attach( 'C:\Users\AshishYadav\Desktop\Dr. Charu\attachment\SCHEDULE LIST FOR TESTING OF DRUGS.docx');
//        $result = $ci->email->send();
        if ($ci->email->send()) {
            return "true";
        } else {
            return "false";
        }
    }

//InActive Project ,,,
    public function gototrashbyuser() {
        if (($_REQUEST['delid']) and ( isset($_REQUEST['sectid']))) {
            $inserArr = array(
                'user_id' => $this->session->userdata('loginid'),
                'project_id' => $_REQUEST['delid'],
                'Sector_ID' => $_REQUEST['sectid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('trash_for_user', $inserArr);
        }
        if ($_REQUEST['sectid'] > 0):
//Visible Activities Change..
            $this->visibilityStatus('InActive_project', $_REQUEST['delid']);
//Notification Status..
            $this->notified($_REQUEST['delid'], 'Tender Deactivated.');
            $this->session->set_flashdata('msg', "Tender Inactivated.");
            redirect(base_url('/dashboard/resultsector?sectorinput=' . $_REQUEST['sectid']));
        else:
            $this->visibilityStatus('InActive_project', $_REQUEST['delid']);
            $this->session->set_flashdata('msg', "Tender Inactivated.");
            redirect(base_url('/dashboard/newproject'));
        endif;
    }

//Project Active By Check Box Multi Action..
    public function activeprojectbycheckbox() {
        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];

//Bulk Active Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "Active")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'Sector_ID' => $secId,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('active_project_byuser', $inserArr);
                $this->visibilityStatus('Active_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, " $countvar Tender Activated.");
            if ($secId > 0) {
                $this->session->set_flashdata('msg', "Total $countvar Tender Activated.");
                redirect(base_url('/dashboard/resultsector?sectorinput=' . $secId));
            } else {
                $this->session->set_flashdata('msg', "Total $countvar Tender Activated.");
                redirect(base_url('/dashboard/newproject'));
            }
            redirect(base_url('/dashboard/newproject'));
        endif;


//Bulk InActive Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "InActive")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'Sector_ID' => $secId,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('trash_for_user', $inserArr);
                $this->visibilityStatus('InActive_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, "Total $countvar Tender Deactivated.");
            if ($secId > 0) {
                $this->session->set_flashdata('msg', "Total $countvar Tender Inactivated.");
                redirect(base_url('/dashboard/resultsector?sectorinput=' . $secId));
            } else {
                $this->session->set_flashdata('msg', " Total $countvar Tender Inactivated.");
                redirect(base_url('/dashboard/newproject'));
            }
            redirect(base_url('/dashboard/newproject'));
        endif;
        redirect(base_url('/dashboard/newproject'));
    }

//For Important Mark from user lavel 2...
    public function projImportantMark() {
        if ($_REQUEST['actid'] && (isset($_REQUEST['sectid']))) {
            $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'],
                'Sector_ID' => $_REQUEST['sectid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('import_for_user', $inserArr);
        }
        if ($Respon):
            $this->visibilityStatus('Important_project', $_REQUEST['actid']);
//Notification Status..
            $this->notified($_REQUEST['actid'], 'Tender Marked as Important.');


            $this->session->set_flashdata('msg', "Project Mark as a Important Success.");
            redirect(base_url('/dashboard/activeprojectbyuser?sectorinput=' . $_REQUEST['sectid']));
        endif;
    }

//For Not Important Mark Not .....
    public function projNotImportantMark() {
        if ($_REQUEST['actid'] && (isset($_REQUEST['sectid']))) {
            $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'],
                'Sector_ID' => $_REQUEST['sectid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('notimport_for_user', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('Not_Important_project', $_REQUEST['actid']);
//Notification Status..
            $this->notified($_REQUEST['actid'], 'Tender Marked as Not Important.');
            $this->session->set_flashdata('msg', "Project Status Not Important Changed. ");
            redirect(base_url('/dashboard/activeprojectbyuser?sectorinput=' . $_REQUEST['sectid']));
        endif;
        redirect(base_url('/dashboard/activeprojectbyuser?sectorinput=' . $_REQUEST['sectid']));
    }

//Set As Important and Not Important With check box...
    public function importantprojectbycheckbox() {
        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];

//Set As Important Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "Important")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'Sector_ID' => $secId,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('import_for_user', $inserArr);
                $this->visibilityStatus('Important_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, "$countvar Tender Marked as Important.");
            $this->session->set_flashdata('msg', "$countvar Tender Mark as a Important Success.");
            redirect(base_url('/dashboard/activeprojectbyuser'));
        endif;
//Set As Not Important Process.. 
//Set As Not Important Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "Notimportant")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'Sector_ID' => $secId,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('notimport_for_user', $inserArr);
                $this->visibilityStatus('Not_Important_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, " $countvar Tender Marked as Not Important.");

            $this->session->set_flashdata('msg', "$countvar Tender Marked as a Not Important.");
            redirect(base_url('/dashboard/activeprojectbyuser'));
        endif;
    }

//For set To Go Project..
    public function projtogoset() {
        if ($_REQUEST['actid'] && (isset($_REQUEST['sectid']))) {
            $contName = $_REQUEST['contName'];
            $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'],
                'Sector_ID' => $_REQUEST['sectid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('proj_togo_for_user', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('To_Go_project', $_REQUEST['actid']);

//Notification Status..
            $this->notified($_REQUEST['actid'], 'Tender Marked as To Go.');

            $this->session->set_flashdata('msg', "Project To Go Success. ");
            redirect(base_url("/dashboard/$contName?sectorinput=" . $_REQUEST['sectid']));
        endif;
    }

//For Project Bid..
    public function bidprojectbyuser() {
        if ($_REQUEST['projid'] && (isset($_REQUEST['sectid']))) {
            $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['projid'],
                'Sector_ID' => $_REQUEST['sectid'], 'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('bid_project', $inserArr);
        }

        if ($Respon > 0):
            $this->visibilityStatus('Bid_project', $_REQUEST['projid']);
//Notification Status..
            $this->notified($_REQUEST['projid'], 'A New Tender Bid Submission Success.');
            $this->session->set_flashdata('msg', "Project Bid Success. ");
            redirect(base_url("/dashboard/togoproject?sectorinput=" . $_REQUEST['sectid']));
        endif;
    }

//For No Go Project set..
    public function projnogoset() {
        if ($_REQUEST['actid'] && (isset($_REQUEST['sectid']))) {
            $contName = $_REQUEST['contName'];
            $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'],
                'Sector_ID' => $_REQUEST['sectid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('proj_nogo_for_user', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('No_Go_project', $_REQUEST['actid']);
            $this->notified($_REQUEST['actid'], 'Tender Status Changed as No Go.');
            $this->session->set_flashdata('msg', "Project Status  Changed No Go . ");
//            redirect(base_url("/dashboard/$contName?sectorinput=" . $_REQUEST['sectid']));
            redirect($_SERVER['HTTP_REFERER']);
        endif;
    }

//Review On Project...
    public function reviewproject() {
        if ($_REQUEST['actid'] && (isset($_REQUEST['sectid']))) {
            $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'],
                'Sector_ID' => $_REQUEST['sectid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('proj_review_for_user', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('In_Review_project', $_REQUEST['actid']);
            $this->notified($_REQUEST['actid'], 'Tender Review Submitted.');
            $this->session->set_flashdata('msg', "Project Review Added.");
            redirect(base_url('/dashboard/importantmarked?sectorinput=' . $_REQUEST['sectid']));
        endif;
    }

//Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

//Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            $this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

//Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

//Notification Visible Hide..
    public function notificationseen() {
        return $this->Front_model->updateRecord('notification', array('seen_by_user' => 'yes'), array('user_to_notify' => $this->session->userdata('loginid')));
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('loginid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('loginid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

//    public function test() {
//        echo $this->session->userdata('loginid');
//    }
//Assign Proposal manager To Bid Project.
    public function assign_proposal_manager() {
        $formArrData = $_REQUEST;
        $respon = $this->Front_model->updateRecord('bid_project', array('proposal_manager' => $formArrData['proposal_manager']), array('project_id' => $formArrData['project_id']));
        $inserArr = array('project_id' => $formArrData['project_id'], 'assign_to' => $formArrData['proposal_manager'], 'assign_by' => $this->session->userdata('loginid'));
        $Respon2 = $this->Front_model->insertRecord('assign_project_manager', $inserArr);

        if (!empty($formArrData['proposal_manager'])) {
            $selectp = $this->db->query('select a.*,b.* from bd_tenderdetail as a left join tender_generated_id b on a.fld_id=b.project_id where b.project_id =' . $formArrData['project_id']);
            $query = $selectp->result();
            $pmid = $this->session->userdata('loginid');
            $userTbl = $this->Front_model->selectRecord('main_users', array('emailaddress'), array('fld_id' => $formArrData['proposal_manager']));
            $userData = $userTbl->first_row();
            $emailAddress = array($userData->emailaddress);
            $name = $this->Front_model->UserNameById($formArrData['proposal_manager']);
            $assignBy = $this->Front_model->UserNameById($pmid);
            $message = '<html>
<head></head>
<body>
<table bgcolor="#ffffff" width="500px;" cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; max-width: 600px;"  >
	<tbody>
		<tr>
			<td valign="top" style="font-size: 14px; padding-top:2.429em;padding-bottom:0.929em;">
				<p style="text-align:center;margin:0;padding:0;">
					<img src="http://www.cegindia.com/assets/images/logo.png" style="display:inline-block;">
				</p>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top:1px solid #e2e2e2;
    border-left:1px solid #e2e2e2;border-right:1px solid #e2e2e2;border-radius: 4px 4px 0 0 ;background-clip: padding-box;
    border-spacing: 0;">
				<tbody>
					<tr>
						<td valign="top" style=" color:#505050;font-family:Helvetica;font-size:14px;line-height:150%;    padding-top:3.143em;
    padding-right:3.5em;padding-left:3.5em; padding-bottom:0.714em;text-align:left;" mc:edit="body_content_01">
							<p style="color:#545454;display:block;font-family:Helvetica;font-size:16px;line-height:1.500em;
     font-style:normal;font-weight:normal;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:15px;margin-left:0;
    text-align:left;">Dear <strong>' . $name . ',</strong><br/>New Project/Tender has been assign to you.</p>
								<h1 style=" color:#2e2e2e;display:block;font-family:Helvetica;font-size:26px;line-height:1.385em;
     font-style:normal;font-weight:normal;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:15px;margin-left:0;
     text-align:left;"><strong>Project ID:' . $query[0]->generated_tenderid . '</strong></h1>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style=" border-left:1px solid #e2e2e2;
    border-right:1px solid #e2e2e2; border-bottom: 1px solid #f0f0f0; padding-bottom: 2.286em;">
					<tbody><tr>
						<td valign="top" style=" color:#505050;font-family:Helvetica;font-size:14px;line-height:150%;padding-top:3.143em;padding-right:3.5em;padding-left:3.5em; padding-bottom:0.714em;
     text-align:left;" mc:edit="body_content">
						<p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 <strong>TenderDetails: </strong>' . $query[0]->TenderDetails . '<br/><strong>Organization: </strong>' . $query[0]->Organization . '<br/><strong>Location: </strong>' . $query[0]->Location . '</p>
	<p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 Assignd By <br/> ' . $assignBy . '
	 </p>
	 <p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 Regards <br/> TS Team
	 </p>
						</td>
						
						</tr>
					</tbody>
				</table>
						<!-- // END BODY -->
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>';
            $subject = 'New Project Assign ';
            array_push($emailAddress, "harshita@cegindia.com");
            array_push($emailAddress, "v.sagar@cegindia.com");
            array_push($emailAddress, "ts.admin@cegindia.com");

            $emailArrStr7 = implode(",", $emailAddress);

            sendMail("$emailArrStr7", $subject, $message);
//die;
        }
        if ($respon):
            $this->session->set_flashdata('msg', "Proposal Manager Assign on Tender Success.");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            $this->session->set_flashdata('msg', "Something Went Wrong.");
            redirect($_SERVER['HTTP_REFERER']);
        endif;
    }

//Assign Proposal manager To Bid Project.
    /* public function changeprojstatus() {
      $formArrData = $_REQUEST;
      // get tender Generated Id By ProjId..
      $genratedId = $this->mastermodel->SelectRecordSingle('tender_generated_id', array('project_id' => $formArrData['project_id']));
      @$TendrgenrId = $genratedId->generated_tenderid;
      $prjstatus = array('0' => 'awaiting', '1' => 'won', '2' => 'loose', '3' => 'cancel');

      $respon = $this->Front_model->updateRecord('bid_project', array('project_status' => $formArrData['proposal_manager']), array('project_id' => $formArrData['project_id']));
      if ($respon):
      $rec_cegCex = $this->mastermodel->SelectRecordSingle('ceg_exp', array('project_code' => $TendrgenrId));
      if ($rec_cegCex) {
      //Update if record Existance..
      $updRec = array('proj_status' => $prjstatus[$formArrData['proposal_manager']],'project_id' => $formArrData['project_id']);
      $this->mastermodel->UpdateRecords('ceg_exp', array('project_code' => $TendrgenrId), $updRec);
      } else {
      //Insert if record New
      $insertedArr = array('project_name' => ProjNameById($formArrData['project_id']), 'proj_status' => $prjstatus[$formArrData['proposal_manager']], 'project_code' => $TendrgenrId,'project_id' => $formArrData['project_id']);
      $this->mastermodel->InsertMasterData($insertedArr, 'ceg_exp');
      }

      $this->session->set_flashdata('msg', "Project Status Changed Successfully.");
      redirect($_SERVER['HTTP_REFERER']);
      else:
      $this->session->set_flashdata('msg', "Something Went Wrong.");
      redirect($_SERVER['HTTP_REFERER']);
      endif;
      } */


    public function changeprojstatus() {
        $formArrData = $_REQUEST;

       

        if ($formArrData) {
            $projstatus = $this->mastermodel->SelectRecordSingle('bdproject_status', array('project_id' => $formArrData['project_id']));
            if ($projstatus) {
//Update if record Existance..
                $updRec = array('project_status' => $formArrData['project_status']);
                $this->mastermodel->UpdateRecords('bdproject_status', array('project_id' => $formArrData['project_id']), $updRec);
            } else {
//Insert if record New
                $insertedArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $formArrData['project_id'],
                    'project_status' => $formArrData['project_status'],
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip()
                );
                $this->mastermodel->InsertMasterData($insertedArr, 'bdproject_status');
            }
            //$this->session->set_flashdata('msg', "Project Status Changed Successfully.");
            //redirect($_SERVER['HTTP_REFERER']);

            $message = "Project Status Changed Successfully.";
        } else {
            //$this->session->set_flashdata('msg', "Something Went Wrong.");
            //redirect($_SERVER['HTTP_REFERER']);
             $message = "Something Went Wrong.";

        }


        $output = array(
        
            "msg" => $message,
        );
        echo json_encode($output);




    }

//Generate Project No..
    public function generate_project_no() {
        $formArrData = $_REQUEST;
//        echo '<pre>'; 
//        print_r($formArrData); die;
//Before Update Insert New ID..
        $insertedArr = array('project_id' => $formArrData['project_id'], 'generated_tenderid' => '', 'generate_by' => $this->session->userdata('loginid'));
        $insertedID = $this->Front_model->insertRecord('tender_generated_id', $insertedArr);
//After Insert Update ID In Same Table..
        if ($formArrData['prefx_name'] == 'E'):
            $last = $formArrData['E'];
        elseif ($formArrData['prefx_name'] == 'P'):
            $last = $formArrData['P'];
        elseif ($formArrData['prefx_name'] == 'FQ'):
            $last = $formArrData['FQ'];
        endif;
//        echo $last;
//         die;
        $generatedId = 'CEGHO/BD/' . date('Y') . "-" . (date('Y') + 1) . "/" . $formArrData['prefx_name'] . '0' . ($last + 1);
//        echo $generatedId; die;
        $respon = $this->Front_model->updateRecord('tender_generated_id', array('generated_tenderid' => $generatedId), array('fld_id' => $insertedID));
// $generatedId = $formArrData['prefx_name'] . uniqid();
        $respon = $this->Front_model->updateRecord('proj_togo_for_user', array('projectgenid' => $generatedId), array('project_id' => $formArrData['project_id']));
        if ($respon):
            $res = $this->Front_model->updateRecord('projectno_prefx', array('last_generate_id' => ($last + 1)), array('prefix_id' => $formArrData['prefx_name']));
            $this->session->set_flashdata('msg', "TenderId Generated Success.");
            redirect(base_url('/dashboard/togoproject'));
        else:
            redirect(base_url('/dashboard/togoproject'));
        endif;
    }

//Save Team Req.. Mail Code By Asheesh
    public function savereqprojectdata() {
        $formArrData = $_REQUEST;
//Insert New ID..
        $insertedidArr = [];
        if ($formArrData['update'] == 'Update') {
            /* echo '<pre>';
              print_r($formArrData);
              die; */
            if ($formArrData["des_id" . $j] == '') {
                $formArrData["des_id" . $j] = '0';
            }
            if ($formArrData["noofpositions" . $j] == '') {
                $formArrData["noofpositions" . $j] = '0';
            }
            if ($formArrData["age_limit" . $j] == '') {
                $formArrData["age_limit" . $j] = '0';
            }
            if ($formArrData["man_months" . $j] == '') {
                $formArrData["man_months" . $j] = '0';
            }
            $projid = $formArrData['project_id'];
            $this->db->query("DELETE team_projectwise,team_members FROM team_projectwise,team_members 
				WHERE team_projectwise.tmid=team_members.fld_id AND team_projectwise.project_id= " . $projid . "");
            for ($j = 0; $j <= $formArrData['totpos'] - 1; $j++) {
                $pmid = $this->session->userdata('loginid');
                $insertedArr = array(
                    'proposal_mngr_id' => $this->session->userdata('loginid'),
                    'noofpositions' => $formArrData["noofpositions" . $j],
                    'age_limit' => $formArrData["age_limit" . $j],
                    'man_months' => $formArrData["man_months" . $j],
                    'req_status' => '1',
                    'comment' => $formArrData["comment"]
                );
                $where = array('id' => $formArrData["ids" . $j]);
                $updateID = $this->Front_model->updateRecord('team_reqbypm', $insertedArr, $where);
                array_push($insertedidArr, $updateID);
                $noofposition = $formArrData["noofpositions" . $j];
                for ($p = 1; $p <= $noofposition; $p++) {
                    $this->savecrureq($formArrData['project_id'], $formArrData["des_id" . $j]);
                }
            }

            if (!empty($formArrData['project_id'])) {
                $selectp = $this->db->query('select a.*,b.* from bd_tenderdetail as a left join tender_generated_id b on a.fld_id=b.project_id where b.project_id =' . $formArrData['project_id']);
                $query = $selectp->result();
                $pmid = $this->session->userdata('loginid');
                $userTbl = $this->Front_model->selectRecord('main_users', array('emailaddress'), array('dept_id' => 14));
                $userData = $userTbl->result();
                $emailAddress = [];
                foreach ($userData as $emails) {
                    array_push($emailAddress, $emails->emailaddress);
                }
            }
        } else {
            for ($j = 0; $j <= $formArrData['totpos'] - 1; $j++) {
                if ($formArrData["des_id" . $j] == '') {
                    $formArrData["des_id" . $j] = '0';
                }
                if ($formArrData["noofpositions" . $j] == '') {
                    $formArrData["noofpositions" . $j] = '0';
                }
                if ($formArrData["age_limit" . $j] == '') {
                    $formArrData["age_limit" . $j] = '0';
                }
                if ($formArrData["man_months" . $j] == '') {
                    $formArrData["man_months" . $j] = '0';
                }
                $pmid = $this->session->userdata('loginid');
                $employeeID = array("260");
                if (in_array($pmid, $employeeID)) {
                    $insertedArr = array(
                        'project_id' => $formArrData['project_id'],
                        'designation_id' => $formArrData["des_id" . $j],
                        'proposal_mngr_id' => $formArrData["proposal_mngr_id"],
                        'noofpositions' => $formArrData["noofpositions" . $j],
                        'age_limit' => $formArrData["age_limit" . $j],
                        'man_months' => $formArrData["man_months" . $j],
                        'project_no' => $formArrData["project_no"],
                        'req_status' => '3',
                        'filled_by_id' => $this->session->userdata('loginid'),
                        'comment' => $formArrData["comment"]
                    );
                } else {
                    $insertedArr = array(
                        'project_id' => $formArrData['project_id'],
                        'designation_id' => $formArrData["des_id" . $j],
                        'proposal_mngr_id' => $this->session->userdata('loginid'),
                        'noofpositions' => $formArrData["noofpositions" . $j],
                        'age_limit' => $formArrData["age_limit" . $j],
                        'man_months' => $formArrData["man_months" . $j],
                        'project_no' => $formArrData["project_no"],
                        'req_status' => '1',
                        'filled_by_id' => $this->session->userdata('loginid'),
                        'comment' => $formArrData["comment"]
                    );
                }

                $insertedID = $this->Front_model->insertRecord('team_reqbypm', $insertedArr);
                array_push($insertedidArr, $insertedID);
                $noofposition = $formArrData["noofpositions" . $j];
                for ($p = 1; $p <= $noofposition; $p++) {
                    $this->savecrureq($formArrData['project_id'], $formArrData["des_id" . $j]);
                }
            }
        }

//Custom Code For Mail By Asheesh..
        if (!empty($insertedidArr)):
            $emailsAddress = array();
            $emailsAddress = $this->Front_model->GetCruEmailsList();
            array_push($emailsAddress, 'harshita@cegindia.com');
            array_push($emailsAddress, 'v.sagar@cegindia.com');
            array_push($emailsAddress, 'ts.admin@cegindia.com');
            $subject = 'New Team Requisition Submitted';

            $data['teamreqidsArr'] = $insertedidArr;
            $msg = $this->load->view('email_template/teamrequisition_email', $data, TRUE);

            $emailArrStr77 = implode(",", $emailsAddress);
            sendMail("$emailArrStr77", $subject, $msg);
            $this->session->set_flashdata('msg', "Team Requisition Saved Successfully.");
            redirect(base_url('/dashboard/teamrequisition'));
        else:
            $this->session->set_flashdata('msg', "Something went wrong.");
            redirect(base_url('/dashboard/teamrequisition'));
        endif;
    }

//Set Team / Project / Requisition For Usert.. 
    public function teamrequisition() {
        $data = [];
        $DesignationArr = $this->allActiveDesignation();
        $assigntomeArr = $this->Front_model->getReqRecordbyUser();
        foreach ($assigntomeArr as $reqData) {
            $teamreqArr = $this->Front_model->getReqDataByPid($reqData->project_id, $reqData->assign_to);
            array_push($data, $teamreqArr);
        }
        $this->load->view('teamrequisition_view', compact('DesignationArr', 'assigntomeArr', 'data'));
    }

//cru project function by Punit...
    public function cruproject() {
        $assigntoArr = $this->Front_model->getAllReqRecord();
        $this->load->view('cruprojects_view', compact('assigntoArr'));
    }

//cru project function by Punit...
    public function cruform() {
        $pid = $_REQUEST['pn'];
        $assigntoArr = $this->Front_model->getReqRecordbyProj($pid);
        /* echo '<pre>';
          print_r($assigntoArr); die; */
        $this->load->view('cruform_view', compact('assigntoArr'));
    }

//Generate Project No..
    public function savecrureq($projid, $desid) {
        /* $formArrData = $data;
          //Insert New ID..
          for ($j=0; $j<=$formArrData['totpos']; $j++){
          if($formArrData["des_id".$j] == ''){
          echo $formArrData["des_id".$j] = '0';
          }
          if($formArrData["userfullname".$j] == ''){
          echo $formArrData["userfullname".$j] = '0';
          }
          if($formArrData["marks".$j] == ''){
          echo $formArrData["marks".$j] = '0';
          }
          if($formArrData["firm_name".$j] == ''){
          echo $formArrData["firm_name".$j] = '0';
          }
          if($formArrData["man_months".$j] == ''){
          echo $formArrData["man_months".$j] = '0';
          }
          if($formArrData["age".$j] == ''){
          echo $formArrData["age".$j] = '0';
          }
          if($formArrData["certificate".$j] == ''){
          echo $formArrData["certificate".$j] = '0';
          }
          if($formArrData["undertaking".$j] == ''){
          echo $formArrData["undertaking".$j] = '0';
          }
          if($formArrData["salary".$j] == ''){
          echo $formArrData["salary".$j] = '0';
          }
          if($formArrData["emailaddress".$j] == ''){
          echo $formArrData["emailaddress".$j] = '0';
          }
          if($formArrData["contactnumber".$j] == ''){
          echo $formArrData["contactnumber".$j] = '0';
          }
          if($formArrData["remarks".$j] == ''){
          echo $formArrData["remarks".$j] = '0';
          } */
        $memberArr = array(
            'userfullname' => '',
            'emailaddress' => '',
            'contactnumber' => '',
            'empipaddress' => '',
            'createdby' => ''
        );

        $insertedID = $this->Front_model->insertRecord('team_members', $memberArr);
        if ($insertedID != '') {
            $teamArr = array(
                'tmid' => $insertedID,
                'project_id' => $projid,
                'designation_id' => $desid,
                'marks' => '',
                'firm_name' => '',
                'man_months' => '',
                'age' => '',
                'certificate' => '',
                'undertaking' => '',
                'salary' => '',
                'remarks' => ''
            );
            $insertID = $this->Front_model->insertRecord('team_projectwise', $teamArr);
//                array_push($insertedidArr, $insertID);
        }
//        }
    }

//Generate Project No..
    public function update() {
        $formArrData = $_REQUEST;
        /* echo '<pre>';
          print_r($formArrData); die; */
//Update New ID..
        /* if ($formArrData["userfullname"] == '') {
          echo $formArrData["userfullname"] = '0';
          }
          if ($formArrData["marks"] == '') {
          echo $formArrData["marks"] = '0';
          }
          if ($formArrData["firm_name"] == '') {
          echo $formArrData["firm_name"] = '0';
          }
          if ($formArrData["man_months"] == '') {
          echo $formArrData["man_months"] = '0';
          }
          if ($formArrData["age"] == '') {
          echo $formArrData["age"] = '0';
          }
          if ($formArrData["certificate"] == '') {
          echo $formArrData["certificate"] = '0';
          }
          if ($formArrData["undertaking"] == '') {
          echo $formArrData["undertaking"] = '0';
          }
          if ($formArrData["salary"] == '') {
          echo $formArrData["salary"] = '0';
          }
          if ($formArrData["emailaddress"] == '') {
          echo $formArrData["emailaddress"] = '0';
          }
          if ($formArrData["contactnumber"] == '') {
          echo $formArrData["contactnumber"] = '0';
          }
          if ($formArrData["remarks"] == '') {
          echo $formArrData["remarks"] = '0';
          } */
        /* if($formArrData['types'] == 'negotiation_team'){
          $negotiation_status = 1;
          }else{
          $negotiation_status = '';
          }

          if($formArrData['types'] == 'contract_team'){
          $contract_status = 1;
          }else{
          $contract_status = '';
          } */
        $memberArr = array(
            'userfullname' => $formArrData["userfullname"],
            'emailaddress' => $formArrData["emailaddress"],
            'contactnumber' => $formArrData["contactnumber"],
            'empipaddress' => $_SERVER['REMOTE_ADDR'],
            'modifiedby' => $this->session->userdata('loginid')
        );
        $where = array('fld_id' => $formArrData["tmid"]);
        $updateID = $this->Front_model->updateRecord('team_members', $memberArr, $where);
        $teamArr = array(
            'tmid' => $formArrData["tmid"],
            'project_id' => $formArrData["project_id"],
            'marks' => $formArrData["marks"],
            'firm_name' => $formArrData["firm_name"],
            'man_months' => $formArrData["man_months"],
            'age' => $formArrData["age"],
            'certificate' => $formArrData["certificate"],
            'undertaking' => $formArrData["undertaking"],
            'salary' => $formArrData["salary"],
            'remarks' => $formArrData["remarks"],
            'negotiation_status' => $negotiation_status,
            'contract_status' => $contract_status
        );
        $wheretp = array('tmid' => $formArrData["tmid"], 'designation_id' => $formArrData["des_id"]);
        $updateDes = $this->Front_model->updateRecord('team_projectwise', $teamArr, $wheretp);
        if ($updateDes):
            $this->session->set_flashdata('msg', "Team Requisition from CRU Updated Successfully.");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            $this->session->set_flashdata('msg', "Something went wrong.");
            redirect(base_url('/dashboard/cruproject'));
        endif;
    }

//Team Row Locking...
    public function lockteamrow() {
        $data = $_REQUEST;
        $teamArr = array("is_locked" => '1', "modifiedby" => $_SERVER['REMOTE_ADDR']);
        $wheretp = array("fld_id" => $data['tmid']);
        $updateDes = $this->Front_model->updateRecord('team_members', $teamArr, $wheretp);
        if ($updateDes) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

// All active Designation Master...
    public function allActiveDesignation() {
        $Rec = $this->Front_model->selectRecordOrderByASC('designation_master_requisition', array('designation_name', 'fld_id'), array('is_active' => '1'));
        if ($Rec) {
            return $result = $Rec->result();
        }
        return false;
    }

//Assign To All Requisition By Login ID..
    public function assignedprojecttome() {
        $Rec = $this->Front_model->selectRecordOrderByASC('assign_project_manager', array('*'), array('is_active' => '1', 'assign_to' => $this->session->userdata('loginid')));
        if ($Rec) {
            return $result = $Rec->result();
        }
        return false;
    }

//Check Box Activities From Project Review..
//Project Active By Check Box Multi Action..
    public function activeprojectbycheckbox_projectreview() {
        $chkBoxArr = $this->input->post('reviewchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];

//Bulk Delete Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "delete")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'Sector_ID' => $secId,
                    'actionIP' => get_client_ip());

                $Respon = $this->Front_model->insertRecord('trash_for_user', $inserArr);
                $this->visibilityStatus('InActive_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, " $countvar Tender In Active .");
            if ($secId > 0) {
                $this->session->set_flashdata('msg', "Total $countvar Tender In Activated.");
                redirect(base_url('/dashboard/projectreview?sectorinput=' . $secId));
            } else {
                $this->session->set_flashdata('msg', "Total $countvar Tender In Activated.");
                redirect(base_url('/dashboard/projectreview'));
            }
            redirect(base_url('/dashboard/projectreview'));
        endif;


//Bulk To Go Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "togo")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'Sector_ID' => $secId,
                    'actionIP' => get_client_ip());

                $Respon = $this->Front_model->insertRecord('proj_togo_for_user', $inserArr);
                $this->visibilityStatus('To_Go_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, " $countvar Tender status Has Been Change as To Go.");

            if ($secId > 0) {
                $this->session->set_flashdata('msg', "Total $countvar Tender status Has Been Change as To Go.");
                redirect(base_url('/dashboard/projectreview?sectorinput=' . $secId));
            } else {
                $this->session->set_flashdata('msg', "Total $countvar Tender status Has Been Change as To Go.");
                redirect(base_url('/dashboard/projectreview'));
            }
            redirect(base_url('/dashboard/projectreview'));
        endif;

//Bulk No Go Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "nogo")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'Sector_ID' => $secId,
                    'actionIP' => get_client_ip());

                $Respon = $this->Front_model->insertRecord('proj_nogo_for_user', $inserArr);
                $this->visibilityStatus('No_Go_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, " $countvar Tender status Has Been Change as No Go.");

            if ($secId > 0) {
                $this->session->set_flashdata('msg', "Total $countvar Tender status Has Been Change as No Go.");
                redirect(base_url('/dashboard/projectreview?sectorinput=' . $secId));
            } else {
                $this->session->set_flashdata('msg', "Total $countvar Tender status Has Been Change as No Go.");
                redirect(base_url('/dashboard/projectreview'));
            }
            redirect(base_url('/dashboard/projectreview'));
        endif;

        $this->session->set_flashdata('msg', "Please Checked Tender Before Submit");
        redirect(base_url('/dashboard/projectreview'));
    }

//Coded by jitendra
// Designation add on master table
    public function addDesignation() {
        $designation = $_REQUEST['designation'];
        if ($designation):
            $insertArr = array('designation_name' => $designation);
            $response = $this->Front_model->insertRecord('designation_master_requisition', $insertArr);
        endif;
        return $response;
    }

//For No Submitted Project set..
    public function projnosubmit() {
        if ($_REQUEST['actid'] && (isset($_REQUEST['sectid']))) {
            /* $contName = $_REQUEST['contName'];
              $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'],
              'Sector_ID' => $_REQUEST['sectid'],
              'actionIP' => get_client_ip());
              $Respon = $this->Front_model->insertRecord('proj_nogo_for_user', $inserArr); */

            $this->visibilityStatus('No_Submit_project', $_REQUEST['actid']);
            $this->notified($_REQUEST['actid'], 'Tender Status Changed as No Submit.');
            $this->session->set_flashdata('msg', "Project Status  Changed No Submit . ");
//            redirect(base_url("/dashboard/$contName?sectorinput=" . $_REQUEST['sectid']));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

//For No Go Project set..
    public function nogosubmit() {
        if ($_REQUEST['actid'] && (isset($_REQUEST['sectid']))) {
// $contName = $_REQUEST['contName'];
            $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'],
                'Sector_ID' => $_REQUEST['sectid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('proj_nogo_for_user', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('No_Go_project', $_REQUEST['actid']);
            $this->notified($_REQUEST['actid'], 'Tender Status Changed as No Go.');
            $this->session->set_flashdata('msg', "Project Status  Changed No Go . ");
//            redirect(base_url("/dashboard/$contName?sectorinput=" . $_REQUEST['sectid']));
            redirect($_SERVER['HTTP_REFERER']);
        endif;
    }

// Show No Submitted Project 
    public function nosubmitproject() {
        $title = 'No Submit Project';
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->Front_model->GetNoSubmitProjByUser();
        $this->load->view('nosubmittender_view', compact('TenderDetailArr', 'title'));
    }

// Show Complete Project List
    public function completeListProject() {
        $sectorArr = $this->GetActiveSector();
        $TenderDetailArr = $this->Front_model->GetcompleteProjByUser();

        $secId = '0';

//For EOI
        $forEOIIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['generated_tenderid']) {
                $IDsArr = explode("/", $projdetailrow['generated_tenderid']);
                $typerKey = $IDsArr[3];
                $prfix = substr($typerKey, 0, 1);
                if ($prfix == 'E') {
                    array_push($forEOIIDArr, $projdetailrow);
                }
            }
        }
//Close EOI..
//Start RFP
        $forRFPIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['generated_tenderid']) {
                $IDsArr = explode("/", $projdetailrow['generated_tenderid']);
                $typerKey = $IDsArr[3];
                $prfix = substr($typerKey, 0, 1);
                if ($prfix == 'P') {
                    array_push($forRFPIDArr, $projdetailrow);
                }
            }
        }



//Close RFP..
//Start FQ..
        $forFQIDArr = array();
        foreach ($TenderDetailArr as $projdetailrow) {
            if ($projdetailrow['generated_tenderid']) {
                $IDsArr = explode("/", $projdetailrow['generated_tenderid']);
                $typerKey = $IDsArr[3];
                $prfix = substr($typerKey, 0, 2);
                if ($prfix == 'FQ') {
                    array_push($forFQIDArr, $projdetailrow);
                }
            }
        }

//Close FQ..
        $this->load->view('completetender_view', compact('sectorArr', 'TenderDetailArr', 'forFQIDArr', 'forRFPIDArr', 'forEOIIDArr'));




//       $this->load->view('completetender_view', compact('TenderDetailArr'));
    }

// Designation add on master table
    public function addBidUrl() {
        $projectids = $_REQUEST['projectids'];
        $url = $_REQUEST['url'];
        if ($url):
            $response = $this->Front_model->updateRecord('bid_project', array('bid_url' => $url), array('project_id' => $projectids));
        endif;
        return $response;
    }

    function positionMailCron() {
//$this->load->view('team_position_cron_mail', compact('TenderDetailArr'));
        $selectp = $this->db->query("SELECT * from team_projectwise where is_active='1'");
        $query = $selectp->result();
        $arr = [];
        foreach ($query as $rows) {
            if ($rows->firm_name == '' || $rows->certificate == 0 || $rows->certificate == '') {
//print_r($rows);
                $arr[] = $rows->project_id;
//$projectID = array_unique($arr);
            }
        }
        $projectID = array_unique($arr);


        $vals = array();
        $i = 0;
        foreach ($projectID as $userId) {
            $this->db->select('tm.project_id , dm.designation_name,bd.TenderDetails,tg.generated_tenderid');
            $this->db->from('team_projectwise tm');
            $this->db->join('bd_tenderdetail bd', 'bd.fld_id=tm.project_id', 'left');
            $this->db->join('tender_generated_id tg', 'tg.project_id=tm.project_id', 'left');
            $this->db->join('designation_master_requisition dm', 'tm.designation_id=dm.fld_id', 'left');
            $this->db->where('tm.project_id', $userId);
            $query = $this->db->get();
            $results = $query->result_array();
            $vals[$i] = [];
            foreach ($results as $rowdata) {
                if (empty($vals[$i])) {
                    $val = array('project_id' => $rowdata['project_id'], 'generated_tenderid' => $rowdata['generated_tenderid'], 'TenderDetails' => $rowdata['TenderDetails'], 'data' => array(array('designation_name' => $rowdata['designation_name'])));
                    $vals[$i] = $val;
                } else {
                    $val = array('designation_name' => $rowdata['designation_name']);
                    array_push($vals[$i]['data'], $val);
                }
            }
            $i++;
        }
        $i = 1;
        foreach ($vals as $valsrow) {
            $degname = array();
            foreach ($valsrow['data'] as $rows1) {
                array_push($degname, $rows1['designation_name']);
            }
            if ($i % 2 == 0) {
                $class = "#e9f6fc";
            } else {
                $class = "#ffffff";
            }
            $detail[] = '<tr bgcolor=' . $class . '>
					<td style="border-right:2px solid #BBBBBB;border-bottom: 2px solid #BBBBBB;">' . $i . '</td>
					<td style="border-right:2px solid #BBBBBB;border-bottom: 2px solid #BBBBBB;">' . $valsrow['generated_tenderid'] . '</td>
					<td style="border-right:2px solid #BBBBBB;border-bottom: 2px solid #BBBBBB;">' . implode(' ,<br/> ', $degname) . '</td>
				</tr>';
            $i++;
        }
//print_r($detail); die;
        $message = '<html>
<head></head>
<body>
		<div style="width:100%;">
            <div style="background-color:#eeeeee; width:800px; margin:0 auto; position:relative;">
            <div style="float:right;"><img src="http://www.cegindia.com/assets/images/logo.png" onError="this.src=http://hrms.cegindia.com/public/media/images/mail_pngs/hrms_logo.png" height="62" width="319" /></div>
            <div style="padding:20px 20px 50px 20px;">
                    <div>
                        <h1 style="font-family:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; border-bottom:1px dashed #999; padding-bottom:15px;">Team Requisition Detail</h1>
                    </div>
                    
                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:normal; line-height:30px; margin:0 0 20px 0;">
                        <div>
												<div>Dear Team,</div>
												
<div>
                <table width="100%" cellspacing="0" cellpadding="15" border="0" style="border:3px solid #BBBBBB; font-size:16px; font-family:Arial, Helvetica, sans-serif; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#e9f6fc">
                        <th width="2%" style="border-right:2px solid #BBBBBB; border-bottom: 2px solid #BBBBBB;">S.N.</td>
                        <th width="20%" style="border-right:2px solid #BBBBBB; border-bottom: 2px solid #BBBBBB;">Project ID</td>
                        <th width="78%" style="border-right:2px solid #BBBBBB; border-bottom: 2px solid #BBBBBB;">Position</td>
                      </tr>' . implode(' , ', $detail) . '      
                </tbody></table>

            </div>
            </div>
                    </div>
                    
                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:normal; line-height:30px;">
                        Regards,<br />
                        <b>Consulting Engineers Group Ltd.</b>
                    </div>
            </div>
            </div>
    	</div>
</body>
</html>';

        $userTbl = $this->Front_model->selectRecord('main_users', array('emailaddress'), array('dept_id' => 14));
        $userData = $userTbl->result();
        $emailAddress = [];
        foreach ($userData as $emails) {
            array_push($emailAddress, $emails->emailaddress);
        }
        $subject = 'vacant Position Detail';
        $emailArrSt17 = implode(",", $emailAddress);
        echo sendMail("$emailArrSt17", $subject, $message);
    }

//export Complete Project
    public function exportCompleteProject() {
//print_r($_POST);
//$date = date('d_m_Y');
        if ($_POST['sectorinput'] == 'All') {
            $search = array('No_Submit_project', 'Bid_project', 'To_Go_project');
        } elseif ($_POST['sectorinput'] == 'Bid_project') {
            $search = array('Bid_project');
        } elseif ($_POST['sectorinput'] == 'To_Go_project') {
            $search = array('To_Go_project');
        } elseif ($_POST['sectorinput'] == 'No_Submit_project') {
            $search = array('No_Submit_project');
        }
        if (!empty($search)) {
            $this->db->select('bd.*, d.generated_tenderid');
            $this->db->from('bd_tenderdetail bd');
            $this->db->join('tender_generated_id d', 'bd.fld_id=d.project_id', 'left');
            $this->db->where('bd.is_active', '1');
            $this->db->where_in('bd.visible_scope', $search);
            $query = $this->db->get();
            if ($query->num_rows() != 0) {
                $resulstArr = $query->result_array();
                $filename = $_POST['sectorinput'] . '_' . date('Y-m-d') . ".csv";
                $fp = fopen('php://output', 'w');

                $fields = array('Sr.No.', 'Exp-Date', 'Project', 'Location', 'Organization', 'Status');
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $filename . '";');
                fputcsv($fp, $fields, ',');
                $i = 1;
                foreach ($resulstArr as $row) {
                    $lineData = array($i, $row['Expiry_Date'], $row['TenderDetails'], $row['Location'], $row['Organization'], $row['visible_scope']);
                    $i++;
                    fputcsv($fp, $lineData, ',');
                }
            }
            exit;
        }
    }

    public function awardproject() {
        $assigntoArr = $this->db->query('select bd.fld_id,bd.TenderDetails,gt.generated_tenderid from bd_tenderdetail as bd left join tender_generated_id as gt on bd.fld_id=gt.project_id where bd.fld_id=' . $_REQUEST['pn']);
        $dataArr = $assigntoArr->result();
//print_r($dataArr);
        $this->load->view('award_view', compact('dataArr'));
    }

    /* public function awardteamview(){
      $pid = $_REQUEST['pn'];
      $type = $_REQUEST['type'];
      if($_REQUEST['type'] == 'tender_team'){
      $assigntoArr = $this->Front_model->getReqRecordbyProj($pid);
      }elseif($_REQUEST['type'] == 'negotiation_team'){
      $assigntoArr = $this->Front_model->getReqRecordbyProj($pid);
      }elseif($_REQUEST['type'] == 'contract_team'){
      $assigntoArr = $this->Front_model->getReqRecordbyProj($pid);
      }
      $secId = '0';
      $userData = $this->Front_model->GetAllUserRec();
      $this->load->view('award_team_view',compact('userData','assigntoArr','type'));
      } */

    public function tenderteam() {
        $pid = $_REQUEST['pn'];
        $secId = '0';
        $assigntoArr = $this->Front_model->getReqRecordbyProj($pid);
        $userData = $this->Front_model->GetAllUserRec();
        $this->load->view('tender_team_view', compact('userData', 'assigntoArr', 'type'));
    }

    public function negotiationteam() {
        $pid = $_REQUEST['pn'];
        $secId = '0';
        $assigntoArr = $this->Front_model->getReqRecordbyProj($pid);
        $selectp = $this->db->query('select a.* , b.designation_name , c.userfullname from negotiation_team as a left join designation_master_requisition b on a.designation_id=b.fld_id left join main_users as c on a.user_id = c.fld_id where a.project_id =' . $pid . ' order by a.designation_id');
        $querys = $selectp->result();
        /* echo '<pre>';
          print_r($querys);
          die; */
        $userData = $this->Front_model->GetAllUserRec();
        $this->load->view('negotiation_team_view', compact('querys', 'userData', 'assigntoArr', 'type'));
    }

    public function contractteam() {
        $pid = $_REQUEST['pn'];
        $secId = '0';
        $selectp = $this->db->query('select a.* , b.designation_name , c.userfullname from contractteam as a left join designation_master_requisition b on a.designation_id=b.fld_id left join main_users as c on a.user_id = c.fld_id where a.project_id =' . $pid . ' order by a.designation_id');
        $querys = $selectp->result();
        $assigntoArr = $this->Front_model->getReqRecordbyProj($pid);
        $userData = $this->Front_model->GetAllUserRec();
        $userID = $this->session->userdata('loginid');
        $userData = $this->db->query('select * from main_users where fld_id=' . $userID);
        $user = $userData->result();
//print_r($user);
        $this->load->view('contract_team_view', compact('user', 'querys', 'userData', 'assigntoArr', 'type'));
    }

    public function negotiationupdate() {
        $formArrData = $_REQUEST;
//echo '<pre>';
//print_r($formArrData); die;
        if ($formArrData['userID'] == '0') {
            $userid = 0;
        }
        if ($formArrData['updatetype'] == 'updateData') {
            $memberArr = array(
                'username' => $formArrData["other"],
                'marks' => $formArrData["marks"],
                'firm_name' => $formArrData["firm_name"],
                'man_months' => $formArrData["man_months"],
                'age' => $formArrData["age"],
                'certificate' => $formArrData["certificate"],
                'undertaking' => $formArrData["undertaking"],
                'salary' => $formArrData["salary"],
                'contactnumber' => $formArrData["contactnumber"],
                'emailaddress' => $formArrData["emailaddress"],
                'remarks' => $formArrData["remarks"],
                'user_id' => isset($formArrData['userID']) ? $formArrData['userID'] : $userid,
            );
            $where = array('id' => $formArrData["id"]);
            $updateID = $this->Front_model->updateRecord('negotiation_team', $memberArr, $where);


            $memberArr1 = array(
                'username' => $formArrData["other"],
                'marks' => $formArrData["marks"],
                'firm_name' => $formArrData["firm_name"],
                'man_months' => $formArrData["man_months"],
                'age' => $formArrData["age"],
                'certificate' => $formArrData["certificate"],
                'undertaking' => $formArrData["undertaking"],
                'salary' => $formArrData["salary"],
                'contactnumber' => $formArrData["contactnumber"],
                'emailaddress' => $formArrData["emailaddress"],
                'remarks' => $formArrData["remarks"],
                'user_id' => isset($formArrData['userID']) ? $formArrData['userID'] : $userid,
            );
            $where1 = array('id' => $formArrData["id"]);
            $updateID1 = $this->Front_model->updateRecord('contractteam', $memberArr1, $where11);
        } else {
            $inserArr = array('user_id' => $formArrData['userID'],
                'tmid' => $formArrData["tmid"],
                'designation_id' => $formArrData["des_id"],
                'username' => $formArrData["other"],
                'project_id' => $formArrData["project_id"],
                'marks' => $formArrData["marks"],
                'firm_name' => $formArrData["firm_name"],
                'man_months' => $formArrData["man_months"],
                'age' => $formArrData["age"],
                'certificate' => $formArrData["certificate"],
                'undertaking' => $formArrData["undertaking"],
                'salary' => $formArrData["salary"],
                'contactnumber' => $formArrData["contactnumber"],
                'emailaddress' => $formArrData["emailaddress"],
                'contract_id' => 1,
                'remarks' => $formArrData["remarks"]
            );
            $Respon = $this->Front_model->insertRecord('negotiation_team', $inserArr);

            $inserArr1 = array('user_id' => $formArrData['userID'],
                'tmid' => $formArrData["tmid"],
                'designation_id' => $formArrData["des_id"],
                'username' => $formArrData["other"],
                'project_id' => $formArrData["project_id"],
                'marks' => $formArrData["marks"],
                'firm_name' => $formArrData["firm_name"],
                'man_months' => $formArrData["man_months"],
                'age' => $formArrData["age"],
                'certificate' => $formArrData["certificate"],
                'undertaking' => $formArrData["undertaking"],
                'salary' => $formArrData["salary"],
                'contactnumber' => $formArrData["contactnumber"],
                'emailaddress' => $formArrData["emailaddress"],
                'contract_id' => 1,
                'remarks' => $formArrData["remarks"]
            );
            $Respon1 = $this->Front_model->insertRecord('contractteam', $inserArr1);

            $teamArr = array(
                'negotiation_status' => 1,
            );
            $wheretp = array('tmid' => $formArrData["tmid"], 'designation_id' => $formArrData["des_id"]);
            $updateDes = $this->Front_model->updateRecord('team_projectwise', $teamArr, $wheretp);
        }
    }

    public function contractupdate() {
        $formArrData = $_REQUEST;
        if ($formArrData['userID'] == '0') {
            $userid = 0;
        }
        $memberArr = array(
            'username' => $formArrData["other"],
            'marks' => $formArrData["marks"],
            'firm_name' => $formArrData["firm_name"],
            'man_months' => $formArrData["man_months"],
            'age' => $formArrData["age"],
            'certificate' => $formArrData["certificate"],
            'undertaking' => $formArrData["undertaking"],
            'salary' => $formArrData["salary"],
            'contactnumber' => $formArrData["contactnumber"],
            'emailaddress' => $formArrData["emailaddress"],
            'remarks' => $formArrData["remarks"],
            'user_id' => isset($formArrData['userID']) ? $formArrData['userID'] : $userid,
        );
        $where = array('id' => $formArrData["id"]);
        $updateID = $this->Front_model->updateRecord('contractteam', $memberArr, $where);
        $teamArr = array('contract_status' => 1);
        $wheretp = array('tmid' => $formArrData["tmid"], 'designation_id' => $formArrData["des_id"]);
        $updateDes = $this->Front_model->updateRecord('team_projectwise', $teamArr, $wheretp);
    }

    public function lockrowdataupdate() {
        $data = $_REQUEST;
        $teamArr = array("is_locked" => '1', "modifiedby" => $_SERVER['REMOTE_ADDR']);
        $wheretp = array("id" => $data['tmid']);
        $updateDes = $this->Front_model->updateRecord('negotiation_team', $teamArr, $wheretp);
        if ($updateDes) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function finallock() {

        $data = $_REQUEST;
        $userID = $this->session->userdata('loginid');
//die;
        $userData = $this->db->query('select * from main_users where fld_id=' . $userID);
        $user = $userData->result();
        $arr = array('254', '253', '346');
        if ($user[0]->emprole == 2) {
            $teamArr = array("is_locked_mgmt" => '1', "is_locked_mgmtname" => $userID, "is_locked_mgmtrole" => 2);
        } elseif ($user[0]->dept_id == 14) {
            $teamArr = array("is_locked_cru" => '1', "is_locked_cruname" => $userID, "is_locked_cru_dept" => 14);
        } elseif (in_array($userID, $arr)) {
            $teamArr = array("is_locked_manager" => '1', "is_locked_managername" => $userID);
        }
        $wheretp = array("id" => $data['tmid']);
        $updateDes = $this->Front_model->updateRecord('contractteam', $teamArr, $wheretp);
        if ($updateDes) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function userDatas() {
        $userData = $this->db->query('select * from main_users where isactive="1" and fld_id=' . $_POST['userID']);
        $user = $userData->result();
        $response = array($user[0]->contactnumber, $user[0]->emailaddress);
        if ($response) {
            echo json_encode($response);
        }
    }

    public function userDataview() {
        $data = $_REQUEST['user_id'];
        $username = $_REQUEST['username'];
        $userData = $this->Front_model->GetAllUserRec();
        $this->load->view('ajax_data', compact('data', 'userData', 'username'));
    }

//New Code By Asheesh 25/08/2018
//Code By Asheesh Atten New ...
    public function attendance_new() {
        $this->db->select('a.fld_id,a.designation_name');
        $this->db->from('designation_master_requisition as a');
        $this->db->where('a.is_active', '1');
        $this->db->where('a.sector_id', '1');
        $this->db->order_by('a.designation_name', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resDesignArr = $query->result();
        }
        $this->db2->select('a.id,a.project_name');
        $this->db2->from('tm_projects as a');
        $this->db2->where('a.is_active', '1');
        $this->db2->order_by('a.project_name', 'ASC');
        $query = $this->db2->get();
        if ($query->num_rows() != 0) {
            $resProjArr = $query->result();
        }
        $this->load->view('attendance_new_view_asheesh', compact('resProjArr', 'resDesignArr'));
    }

    public function save_attendance_new() {
        $project_id = $_REQUEST['project_id'];
        $designation_id = $_REQUEST['designation_id'];
        $this->db->select('a.id');
        $this->db->from('assign_finalteam as a');
        $this->db->where('a.project_id', $project_id);
        $this->db->where('a.designation_id', $designation_id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
//insert...
            $dataQarr = array('project_id' => $project_id, 'designation_id' => $designation_id);
            $this->db->insert('assign_finalteam', $dataQarr);
            $this->session->set_flashdata('msg', "Record Inserted");
        }
        redirect(base_url('dashboard/attendance_new'));
    }

    public function ajax_list_homepage() {
        $list = $this->dashboardacmodel->get_datatables();
//echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';

        foreach ($list as $comprow) {
            $projectname = $this->SecondDB_model->getProjectBYId($comprow->project_numberid);

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->TenderDetails;
            $row[] = $projectname->project_name;
            $row[] = $this->getempnamebyid($comprow->emp_idcoodg);
            $row[] = $this->getempnamebyid($comprow->engempid);
            $row[] = $comprow->client;
//  $row[] = $comprow->sectName;
//            $row[] = $comprow->state_name;
//            $row[] = $comprow->funding;
            $row[] = '<a href="viewedit/' . $comprow->project_id . '"><i title="View" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp;' .
                    '<i style="cursor:pointer; color:#d34836;" title="Cordinator" data-toggle="modal" onclick="setprojforcodei(' . "'" . $comprow->project_id . "'" . ')" data-target="#myModalcod" class="glyphicon glyphicon-user icon-green"></i></a>&nbsp&nbsp;' .
                    '<i style="cursor:pointer; color:#00aced;" title="Engineer" data-toggle="modal" onclick="setprojforengr(' . "'" . $comprow->project_id . "'" . ')" data-target="#myModalengnr" class="glyphicon glyphicon-user icon-white"></i></a>';

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->dashboardacmodel->count_filtered(),
            "data" => $data,
        );
//output to json format
        echo json_encode($output);
    }

    public function submitcordinator() {
        $userID = $this->session->userdata('loginid');
        if ($_REQUEST) {
            $bdprojId = $_REQUEST['codeig_projectid'];
            $emplId = $_REQUEST['empascodeigname'];
            $HrmsIdId = $this->getprojidbdtohrms($bdprojId);
            $numExt = $this->db->where(['bd_project_id' => $bdprojId])->from("project_coordinator")->count_all_results();
            if ($numExt > 0) {
                $updtArr = array('emp_id' => $emplId, 'enytyupd_by' => $userID);
                $this->db->set($updtArr);
                $resp = $this->db->update("project_coordinator", array('bd_project_id' => $bdprojId));
                $this->session->set_flashdata('msg', "Coordinator Updated Success..");
            } else {
                $insertArr = array('bd_project_id' => $bdprojId,
                    'hrms_projid' => $HrmsIdId,
                    'emp_id' => $emplId,
                    'enytyupd_by' => $userID);
                $resp = $this->db->insert("project_coordinator", $insertArr);
                $this->session->set_flashdata('msg', "Coordinator Added Success..");
            }
        }
        redirect(base_url('dashboard/acdashboard'));
    }

    public function submitengr() {
        $userID = $this->session->userdata('loginid');
        if ($_REQUEST) {
            $bdprojId = $_REQUEST['codeig_projectidengr'];
            $emplId = $_REQUEST['empasengrname'];
            $HrmsIdId = $this->getprojidbdtohrms($bdprojId);

            $numExt = $this->db->where(['bd_project_id' => $bdprojId])->from("project_assign_engnr")->count_all_results();
            if ($numExt > 0) {
                $updtArr = array('emp_id' => $emplId, 'enytyupd_by' => $userID);
                $this->db->set($updtArr);
                $resp = $this->db->update("project_assign_engnr", array('bd_project_id' => $bdprojId));
                $this->session->set_flashdata('msg', "Project Engineer Updated Success..");
            } else {
                $insertArr = array('bd_project_id' => $bdprojId,
                    'hrms_projid' => $HrmsIdId,
                    'emp_id' => $emplId,
                    'enytyupd_by' => $userID);
                $resp = $this->db->insert("project_assign_engnr", $insertArr);
                $this->session->set_flashdata('msg', "Project Engineer Added Success..");
            }
        }
        redirect(base_url('dashboard/acdashboard'));
    }

    public function getprojidbdtohrms($projID) {
        $this->db->select("project_numberid");
        $this->db->from("bdcegexp_proj_summery");
        $this->db->where("project_id", $projID);
        $RecArr = $this->db->get()->row();
        


        return ($RecArr) ? $RecArr->project_numberid : null;
    }

//Get Emp Name By Id...
    public function getempnamebyid($empID = '') {
        $this->db2 = $this->load->database('another_db', TRUE);
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_employees_summary.user_id,$db2.main_employees_summary.userfullname");
        $this->db->from("$db2.main_employees_summary");
        $this->db->where("$db2.main_employees_summary.user_id", $empID);
        $empArr = $this->db->get()->row();
        return ($empArr) ? $empArr->userfullname : '';
    }

//client Address Edit Update...
    public function clientaddressupdinsert() {
        $BDprojId = $this->input->post("project_id");
        $insertArr = array('bdproj_id' => $BDprojId, 'full_address' => $this->input->post("clientfull_address"), 'client_city' => $this->input->post("clientcityname"),
            'client_postalpin' => $this->input->post("clientpostalcodepin"), 'client_contactno' => $this->input->post("client_contno"), 'client_cont_pers_name' => $this->input->post("client_contpersname"),
            'client_cont_pers_contact' => $this->input->post("client_contpers_num"),
            'client_email_pers' => $this->input->post("client_email_pers"),
            'contact_person_pos' => $this->input->post("cont_per_pos"),
            'contact_person_address' => $this->input->post("contperfull_address"),
            'entry_by' => $this->session->userdata('loginid'),
            'hrmsproj_id' => $this->getprojidbdtohrms($BDprojId));
//Chk If Exists...
        $this->db->select('bdproj_id')->from('bdceg_exp_clientmore_details')->where(array('bdproj_id' => $BDprojId));
        $q = $this->db->get();
        if ($q->num_rows() < 1) {
            $resp = $this->db->insert("bdceg_exp_clientmore_details", $insertArr);
        } else {
            $UpDdata = array('full_address' => $this->input->post("clientfull_address"),
                'client_city' => $this->input->post("clientcityname"),
                'client_email_pers' => $this->input->post("client_email_pers"),
                'client_postalpin' => $this->input->post("clientpostalcodepin"), 'client_contactno' => $this->input->post("client_contno"), 'client_cont_pers_name' => $this->input->post("client_contpersname"), 'client_cont_pers_contact' => $this->input->post("client_contpers_num"), 'contact_person_pos' => $this->input->post("cont_per_pos"), 'contact_person_address' => $this->input->post("contperfull_address"), 'entry_by' => $this->session->userdata('loginid'));
            $this->db->where(array('bdproj_id' => $BDprojId));
            $resp = $this->db->update('bdceg_exp_clientmore_details', $UpDdata);
        }
        if ($resp) {
            $this->session->set_flashdata('msg', "Client Address Details Added Successfully.");
        } else {
            $this->session->set_flashdata('msg', "Something Went Wrong.");
        }
        // redirect(base_url('dashboard/viewedit/' . $BDprojId));
        redirect($_SERVER['HTTP_REFERER']);
    }

//Address Project Offc Address Insert or Update...
    public function offcaddressupdinsert() {
        $BDprojId = $this->input->post("project_id");
        $insertArr = array('bdproj_id' => $BDprojId, 'full_address' => $this->input->post("clientfull_address"), 'projoffc_city' => $this->input->post("clientcityname"),
            'projoffc_postalpin' => $this->input->post("clientpostalcodepin"), 'projoffc_contactno' => $this->input->post("client_contno"), 'projoffc_cont_pers_name' => $this->input->post("client_contpersname"),
            'projoffc_cont_pers_contact' => $this->input->post("client_contpers_num"),
            'entry_by' => $this->session->userdata('loginid'),
            'hrmsproj_id' => $this->getprojidbdtohrms($BDprojId));
//Chk If Exists...
        $this->db->select('bdproj_id')->from('bdceg_exp_offcaddress')->where(array('bdproj_id' => $BDprojId));
        $q = $this->db->get();
        if ($q->num_rows() < 1) {
            $resp = $this->db->insert("bdceg_exp_offcaddress", $insertArr);
        } else {
            $UpDdata = array('full_address' => $this->input->post("clientfull_address"), 'projoffc_city' => $this->input->post("clientcityname"), 'projoffc_postalpin' => $this->input->post("clientpostalcodepin"), 'projoffc_contactno' => $this->input->post("client_contno"), 'projoffc_cont_pers_name' => $this->input->post("client_contpersname"), 'projoffc_cont_pers_contact' => $this->input->post("client_contpers_num"), 'entry_by' => $this->session->userdata('loginid'));
            $this->db->where(array('bdproj_id' => $BDprojId));
            $resp = $this->db->update('bdceg_exp_offcaddress', $UpDdata);
        }
        if ($resp) {
            $this->session->set_flashdata('msg', "Project Office Address Details Added Successfully.");
        } else {
            $this->session->set_flashdata('msg', "Something Went Wrong.");
        }
        //redirect(base_url('dashboard/viewedit/' . $BDprojId));
        redirect($_SERVER['HTTP_REFERER']);
    }

//Ajax For Popup Client address Details..
    public function ajax_client_addressbyid() {
        $bdprojId = $_REQUEST['projid'];
        if ($bdprojId) {
            $this->db->SELECT('a.*');
            $this->db->FROM('bdceg_exp_clientmore_details as a');
            $this->db->WHERE(array('a.bdproj_id' => $bdprojId, 'a.status' => '1'));
            $resultdata = $this->db->get()->row();
            echo ($resultdata) ? json_encode($resultdata) : '';
        }
    }

    public function Getclient_addbyid($bdprojId) {
        if ($bdprojId) {
            $this->db->SELECT('a.*');
            $this->db->FROM('bdceg_exp_clientmore_details as a');
            $this->db->WHERE(array('a.bdproj_id' => $bdprojId, 'a.status' => '1'));
            $resultdata = $this->db->get()->result();
            return ($resultdata) ? $resultdata : '';
        } else {
            return false;
        }
    }

//Project Ofc Address Details...
    public function Getprojofcaddress_addbyid($bdprojId) {
        if ($bdprojId) {
            $this->db->SELECT('a.*');
            $this->db->FROM('bdceg_exp_offcaddress as a');
            $this->db->WHERE(array('a.bdproj_id' => $bdprojId, 'a.status' => '1'));
            $resultdata = $this->db->get()->row();
            return ($resultdata) ? $resultdata : '';
        } else {
            return false;
        }
    }

    public function editorupdatetlofcmngr() {

        if ($this->input->post()) {
            $projId = $this->input->post('project_id');

            $insertArr1 = array(
                'bd_project_id' => $projId,
                'hrms_projid' => $this->getprojidbdtohrms($projId),
                'emp_id' => $this->input->post('project_engineer'),
                'enytyupd_by' => $this->session->userdata('loginid')
            );

            $insertArr2 = array(
                'bd_project_id' => $projId,
                'hrms_projid' => $this->getprojidbdtohrms($projId),
                'emp_id' => $this->input->post('project_coordinator'),
                'enytyupd_by' => $this->session->userdata('loginid')
            );

            //Chk Edit exists For Edit Update..
            $this->db->select('bd_project_id')->from('project_assign_engnr')->where(array('bd_project_id' => $projId));
            $q1 = $this->db->get();
            if ($q1->num_rows() < 1) {
                $Respon1 = $this->Front_model->insertRecord('project_assign_engnr', $insertArr1);
            } else {
                $this->db->where(array('bd_project_id' => $projId));
                $resp1 = $this->db->update('project_assign_engnr', array('emp_id' => $this->input->post('project_engineer')));
            }
            $insertArr = array(
                'bd_projid' => $projId,
                'hrms_projid' => $this->getprojidbdtohrms($projId),
                'emp_id' => $this->input->post('emp_id'),
                'cont_pers_typer' => $this->input->post('cont_pers_typer'),
                'entry_by' => $this->session->userdata('loginid')
            );
            $typer = $this->input->post('cont_pers_typer');


           


            //Chk Edit exists For Edit Update..
            $this->db->select('bd_projid')->from('cegexp_proj_cont_persdetails')->where(array('bd_projid' => $projId, 'cont_pers_typer' => 
            $typer));
            $q = $this->db->get();
            if ($q->num_rows() < 1) {
                $Respon = $this->Front_model->insertRecord('cegexp_proj_cont_persdetails', $insertArr);
            } else {
                $this->db->where(array('bd_projid' => $projId, 'cont_pers_typer' => $typer));
                $resp = $this->db->update('cegexp_proj_cont_persdetails', array('emp_id' => $this->input->post('emp_id')));
            }

            //Chk Edit exists For Edit Update..
            $this->db->select('bd_project_id')->from('project_coordinator')->where(array('bd_project_id' => $projId));
            $q2 = $this->db->get();
            if ($q2->num_rows() < 1) {
                $Respon2 = $this->Front_model->insertRecord('project_coordinator', $insertArr2);
            } else {
                $this->db->where(array('bd_project_id' => $projId));
                $resp2 = $this->db->update('project_coordinator', array('emp_id' => $this->input->post('project_coordinator')));
            }

            // $this->session->set_flashdata('msg', "Contact Person Details Added Successfully.");
            //redirect(base_url('dashboard/viewedit/' . $projId));
            // redirect($_SERVER['HTTP_REFERER']);


            $message = "Contact Person Details Added Successfully.";

        }

        $arrMessage = array(
            'msg' => $message,
        );

        echo json_encode($arrMessage);


    }

    public function replacement_data_ajax() {
        $replc_mentID = $_REQUEST['Ereplacement_id'];
        $rplcDataArr = getreplacemendata($replc_mentID);
        if ($rplcDataArr) {
            foreach ($rplcDataArr['username'] as $rowR) {
                ?>
                <tr>
                    <td><?php echo DesignationNameById($rowR->designation_id); ?></td>
                    <td><?php echo $rowR->userfullname; ?></td>
                                        <!--<td><?php //echo $rowR->account_date;  ?></td>-->
                    <td><?php echo $rowR->date_of_joining; ?></td>
                    <?php $resign_date = $this->getoldreplacemnetresigndate($rowR->empname);
                    ?>
                    <!--<td><?php //echo $rowR->man_months;  ?></td>-->
                    <td><?php echo $resign_date['resign_date']; ?></td>
                </tr>
                <?php
            }
        }
    }

    //code by durgesh for get resign date of old replacement
    public function getoldreplacemnetresigndate($emp_id) {
        $this->db->select('resign_date');
        $this->db->from('old_replacement_emp_data');
        $this->db->where('empl_id', $emp_id);
        $resultrow = $this->db->get()->row_array();
        if ($resultrow) {
            return ($resultrow) ? $resultrow : '';
        }
    }

//Account Summery Report..
    public function accountsummaryreport_ajax() {
        $list = $this->accountsummaryreport->get_datatables();
// echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = $no;
            // $row[] = "<a target='_blank' href='" . base_url('projectplanning/userdetail/' . $val->project_id) . "'>" . $val->project_name . "</a>";
            $row[] = date("F Y", strtotime($val->invoice_date));
            // $row[] = number_format($val->contract_value, 2);
            $row[] = number_format((($val->current_month_bill) + ($val->escalaration)), 2);
            //$row[] = number_format($val->total_cumulative, 2);
            $row[] = number_format($val->remuneration, 2);
            $row[] = number_format($val->reimbursable, 2);
            $row[] = number_format($val->taxtotalinvcwise, 2);
            // $row[] = number_format($val->balance, 2);
            $data[] = $row;
        }
        $output = array("draw" => $_POST['draw'],
            "recordsTotal" => $this->accountsummaryreport->count_all(),
            //"recordsTotal" => '0',
            "recordsFiltered" => $this->accountsummaryreport->count_filtered(),
            "data" => $data,
        );
//output to json format
        echo json_encode($output);
    }

    public function accountsummaryreport() {
        $data['error'] = '';
        $data['title'] = "Account Summary Report";
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
        $this->db->from("$db2.tm_projects");
        $this->db->where(array("$db2.tm_projects.is_active" => '1'));
        $this->db->where("($db2.tm_projects.project_category='ie' OR $db2.tm_projects.project_category='ae')", NULL, FALSE);
        $this->db->order_by("$db2.tm_projects.project_name", 'ASC');
        $data['ProjRec'] = $this->db->get()->result();

######## Layer 2 ###########
        $this->db->select("$db2.main_employees_summary.user_id,$db2.main_employees_summary.userfullname,$db2.main_employees_summary.employeeId");
        $this->db->from("$db2.main_employees_summary");
        $this->db->where("($db2.main_employees_summary.businessunit_id='1' OR $db2.main_employees_summary.businessunit_id='3')", NULL, FALSE);
        $this->db->where("$db2.main_employees_summary.isactive", '1');
        $this->db->order_by("$db2.main_employees_summary.userfullname", "ASC");
        $data['empArr'] = $this->db->get()->result();
//For Topper Div Val..
        $data['getCv'] = $this->Newreportsummary_model->dashboard_totalcv();
        $data['getTotalBilled'] = $this->Newreportsummary_model->dashboard_totalcumulative();


        $this->load->view('dashboardaccdept/accountsummaryreport_view', $data);
    }

//Another DashBoard.. Code By Asheesh 2-01-2018...
    public function acdashboard() {
        $title = 'Dashboard';
        $this->db2 = $this->load->database('another_db', TRUE);
        $db2 = $this->db2->database;

        $this->db->select("$db2.main_employees_summary.user_id,$db2.main_employees_summary.userfullname,$db2.main_employees_summary.employeeId");
        $this->db->from("$db2.main_employees_summary");
        $this->db->where("($db2.main_employees_summary.businessunit_id='1' OR $db2.main_employees_summary.businessunit_id='3')", NULL, FALSE);
        $this->db->where("$db2.main_employees_summary.isactive", '1');
        $this->db->order_by("$db2.main_employees_summary.userfullname", "ASC");
        $empArr = $this->db->get()->result();

//For Topper Div Val edit by durgesh..
        //$getCv = $this->Newreportsummary_model->dashboard_totalcv();
        // $getTotalBilled = $this->Newreportsummary_model->dashboard_totalcumulative();
        $ResultRecord = $this->Newreportsummary_model->get_topper_div();
        $totCv = 0;
        $totComulative = 0;
        $currentbill = 0;
        if ($ResultRecord):
            foreach ($ResultRecord as $RowRec):
                $CvValue = $RowRec->contract_value;
                $totcumulative = $RowRec->total_cumulative;
                $currentbill = $RowRec->current_month_bill;
                $totCv += $CvValue;
                $totComulative += $totcumulative;
                $currentbill += $currentbill;
            endforeach;
        endif;
        $getCv = $totCv;
        $getTotalBilled = $totComulative;
        $getbill = $currentbill;

        $this->load->view('dashboardnew_view', compact("title", "empArr", "getCv", "getTotalBilled", "getbill"));
    }

//New Function For Employee Project Assign in Other oficial Data Project.
    public function assignproject() {
        $data['error'] = '';
        $title = 'Assign on Project';
        $this->db2 = $this->load->database('another_db', TRUE);
        $db2 = $this->db2->database;
        $emplListArr = $this->Front_model->GetAllEmplRecList();
        $projectListArr = $this->Front_model->getAllProject();
        if (@$_REQUEST['emplid']) {
            @$empLiD = $_REQUEST['emplid'];
            $empOnProj = $this->ProjDetailsByEmpId($empLiD);
            $empOnProjArr = explode(",", $empOnProj);
            $selectedEmpId = $_REQUEST['emplid'];
        }
        if ((@$_REQUEST['staticsubmit']) and ( @$_REQUEST['emplid'])) {
            $projectides = $_REQUEST['projectides'];
            $twoArr = array_merge($projectides, $empOnProjArr);
            $twoArr2 = array_unique($twoArr);
            $projUpd = implode(",", $twoArr2);
            $this->db->set("$db2.emp_otherofficial_data.on_project", "$projUpd");
            $this->db->where("$db2.emp_otherofficial_data.user_id", "$empLiD");
            $returnU = $this->db->update("$db2.emp_otherofficial_data");
            redirect(base_url("dashboard/assignproject"));
        }
        $this->load->view('dashboardaccdept/assignproject_toemp_view', compact("emplListArr", "projectListArr", "title", "empOnProjArr", "selectedEmpId"));
    }

    public function ProjDetailsByEmpId($empLiD) {
        $this->db2 = $this->load->database('another_db', TRUE);
        $db2 = $this->db2->database;
        $this->db->select("$db2.emp_otherofficial_data.on_project");
        $this->db->from("$db2.emp_otherofficial_data");
        $this->db->where("$db2.emp_otherofficial_data.user_id", $empLiD);
        $this->db->where("$db2.emp_otherofficial_data.status", "1");
        $empProjArr = $this->db->get()->row();
        return ($empProjArr) ? $empProjArr->on_project : '';
    }

    public function assign_projemp_ajax() {
        $list = $this->projectassigntoemp->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->userfullname;
            $row[] = $val->businessunit_name;
            $row[] = $val->department_name;
            $row[] = $val->projlist;
            $data[] = $row;
        }
        $output = array("draw" => $_POST['draw'],
            "recordsTotal" => $this->projectassigntoemp->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->projectassigntoemp->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    // Key Date Add Edit Update ...
    public function keydatesaddupdate() {
        $bdProjId = $this->input->post('projectid');
        $this->db->select("bd_projid");
        $this->db->from("proj_keydate_details");
        $this->db->where(array("bd_projid" => $bdProjId, "status" => "1"));
        $singleRow = $this->db->get();
        if ($singleRow->num_rows() < 1) {
            //inserted..
            $insertEdArr = array("bd_projid" => $bdProjId, "date_bid_submission" => date("Y-m-d", strtotime($this->input->post('date_bid_submission'))),
                "date_financialopening" => date("Y-m-d", strtotime($this->input->post('date_financialopening'))),
                "date_latter_award" => date("Y-m-d", strtotime($this->input->post('date_latter_award'))),
                "date_contract_agrement" => date("Y-m-d", strtotime($this->input->post('date_contract_agrement'))),
                "date_of_mov" => date("Y-m-d", strtotime($this->input->post('internal_mou'))),
                "client_mou" => date("Y-m-d", strtotime($this->input->post('client_mou'))),
                "date_of_commencement" => date("Y-m-d", strtotime($this->input->post('date_of_commencement'))),
                "date_stipulatedcompletion" => date("Y-m-d", strtotime($this->input->post('date_stipulatedcompletion'))),
                "date_of_assignment" => date("Y-m-d", strtotime($this->input->post('date_of_assignment'))),
                "entry_by" => $this->session->userdata('loginid')
            );
            $resp = $this->mastermodel->InsertMasterData($insertEdArr, 'proj_keydate_details');
        } else {
            $UpdEdArr = array("date_bid_submission" => date("Y-m-d", strtotime($this->input->post('date_bid_submission'))),
                "date_financialopening" => date("Y-m-d", strtotime($this->input->post('date_financialopening'))),
                "date_latter_award" => date("Y-m-d", strtotime($this->input->post('date_latter_award'))),
                "date_contract_agrement" => date("Y-m-d", strtotime($this->input->post('date_contract_agrement'))),
                "date_of_mov" => date("Y-m-d", strtotime($this->input->post('internal_mou'))),
                "client_mou" => date("Y-m-d", strtotime($this->input->post('client_mou'))),
                "date_of_commencement" => date("Y-m-d", strtotime($this->input->post('date_of_commencement'))),
                "date_stipulatedcompletion" => date("Y-m-d", strtotime($this->input->post('date_stipulatedcompletion'))),
                "date_of_assignment" => date("Y-m-d", strtotime($this->input->post('date_of_assignment'))),
                "entry_by" => $this->session->userdata('loginid')
            );
            $this->db->where(array('bd_projid' => $bdProjId));
            $resp = $this->db->update('proj_keydate_details', $UpdEdArr);
        }
        $this->session->set_flashdata('msg', "Record Updated Successfully.");
        //redirect(base_url("dashboard/viewedit/" . $bdProjId));
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function siteofcdetailsave() {
        if ($_REQUEST) {
            $bdProjId = $this->input->post('projectid');
            $insertArr = array("project_id" => $this->input->post('projectid'), "street_address" => $this->input->post('street_address'), "countries_idnew" => $this->input->post('countries_idnew'), "state_idnew" => $this->input->post('state_idnew'), "city_idnew" => $this->input->post('city_idnew'), "siteofc_pincode" => $this->input->post('siteofc_pincode'), "cont_persname" => $this->input->post('cont_persname'), "entry_by" => $this->session->userdata('loginid'));
            $insertResP = $this->mastermodel->InsertMasterData($insertArr, 'proj_siteofc_detail');
            $this->session->set_flashdata('msg', "Site office Address Details Inserted Successfully.");
        }
        //redirect(base_url("dashboard/viewedit/" . $bdProjId));
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function viewedit($bdproj_id) {
        $data['title'] = "Project Edit";
        $data['error'] = '';
        $projstatus = '';
        $compnameArr = array();
        $cegexpID = $this->uri->segment(3);
        if ($cegexpID) {
// Find Data
            $data['bddetail'] = $this->mastermodel->SelectRecordSingle('bd_tenderdetail', array('fld_id' => $cegexpID));
            $data['project_status'] = $this->mastermodel->SelectRecordSingle('bdproject_status', array('project_id' => $cegexpID));
            $data['invoice_cv'] = $this->db->get_where('invoice_after_xlsdownload', array('project_id' => $cegexpID, 'srpos_no' => 'sub_total'))->row();

            //code by durgesh for get latest invoice month balance(27-08-2019)
            $newdate = date("Y-m", strtotime("-1 months"));
            $this->db->select('a.balance,a.total_cumulative');
            $this->db->from('invoice_after_xlsdownload as a');
            $this->db->join('invoicedetail as b', 'a.invoice_id=b.id', 'inner');
            $this->db->where(array('a.project_id' => $cegexpID, 'a.srpos_no' => 'sub_total'));
            $this->db->order_by('b.invoice_date', 'desc');
            //$this->db->where('b.invoice_date', $newdate);
            $result = $this->db->get()->row();
            $data['latest_invoice_balance'] = $result;


            //bdcegexp data
            $this->db->select('a.*,c.service_name,d.generated_tenderid');
            $this->db->from('bdceg_exp as a');
            $this->db->join('main_services as c', 'a.service_id = c.id', 'left');
            $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
            $this->db->where('a.project_id', $cegexpID);

            $resultdata = $this->db->get()->result();
            $data['expres'] = $resultdata[0];
            $data['bid_security_type'] = $this->db->get_where('bidsecurity_type', array('is_active' => '1'))->result();
            $data['projdetail'] = $this->mastermodel->SelectRecordSingle('project_description', array('project_id' => $cegexpID));
//$country_id = $data['expres']->countries_id;
            $country_id = $data['projdetail']->country_id;
            $bid_securitytype = $data['projdetail']->bid_securitytype;
            $sectors = $data['projdetail']->sector_id;
            $service_id = $data['projdetail']->service_id;
            $data['stateArr'] = $this->mastermodel->SelectRecordS('states', array('country_id' => $country_id, 'status' => '1'));
            $data['securitytype'] = $this->mastermodel->SelectRecordSingle('bidsecurity_type', array('id' => $bid_securitytype, 'is_active' => '1'));
            $data['project'] = $this->SecondDB_model->getProject();
            $data['cegpds'] = $this->mastermodel->SelectRecordSingle('ceg_pds_detail', array('project_id' => $cegexpID));
            $data['expsummery'] = $this->mastermodel->SelectRecordSingle('bdcegexp_proj_summery', array('project_id' => $cegexpID, 'status' => '1'));
            $data['sectorname'] = $this->mastermodel->SelectRecordSingle('sector', array('fld_id' => $sectors));
            $data['service'] = $this->mastermodel->SelectRecordSingle('main_services', array('id' => $service_id));
            $data['projectname'] = $this->SecondDB_model->getProjectBYId($data['expsummery']->project_numberid);
            $data['cegcompcomp'] = $this->mastermodel->SelectRecordFld('cegexp_competitor_comp', array('project_id' => $cegexpID, 'status' => '1'));
            $data['tqRecArr'] = $this->mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $cegexpID));
            $data['mailby'] = $this->SecondDB_model->getUserByID($this->session->userdata('loginid'));
//echo '<pre>'; print_r($data['securitytype']); die;
//Delete Rec...
            if (isset($_REQUEST['delc'])):
                $respp = $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $_REQUEST['delc']), array('status' => '0'));
                $this->session->set_flashdata('msg', 'Record Deleted Successfully');
                redirect(base_url('company/cegexpedit/' . $cegexpID));
            endif;
//Summery...
            $respCount = $this->mastermodel->count_allByCond('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
            if ($respCount < 1):
                $this->mastermodel->InsertMasterData(array('cegexp_id' => $cegexpID), 'cegexp_proj_summery');
            endif;
//Comptitors Company List
            $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
            if ($Rec != '') {
                $data['compnameArr'] = $Rec->result();
            }

//Edit Proc..
            $data['cegexpSummery'] = $this->mastermodel->SelectRecordSingle('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
            $data['cegexpRec'] = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $cegexpID));
            $data['country_Arr'] = $this->mastermodel->GetAllRecCountArr();

            $data['security_Type'] = $this->mastermodel->SelectRecord('bidsecurity_type', array('is_active' => '1'));
            $countID = $data['cegexpRec']->countries_id;
// $data['stateArr'] = $this->mastermodel->SelectRecordS('states', array('country_id' => $countID, 'status' => '1'));
            $ProjId = $this->mastermodel->ProjIdByGenId($data['cegexpRec']->project_code);
            @$consortiumRecArr = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('project_id' => $ProjId, 'status' => '1'));
            $projectID = $data['cegexpRec']->project_id;
            $data['ceglane'] = $this->mastermodel->SelectRecord('ceg_lane', array('project_id' => $cegexpID));
            $data['sector'] = $this->Front_model->GetActiveSector();
            $data['serviceArr'] = $this->Front_model->mainServices();
            $data['servicename'] = $this->mastermodel->SelectRecordSingle('main_services', array('id' => $data['cegexpRec']->service_id));
            if ($consortiumRecArr):
                $data['consortiumArr'] = $consortiumRecArr;
            else:
                $data['consortiumArr'] = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('cegexp_id' => $cegexpID, 'status' => '1'));
            endif;
//echo '<pre>'; print_r($data); die;
//Send Mail Code By Asheesh..
            if ($_REQUEST['mailsender']):
                $msgDetails = $this->load->view('email_template/comptitor_list_email', $data, true);
                $emailsAddress = $this->SecondDB_model->GetBdEmailsList();
//echo '<pre>'; print_r($emailsAddress); die;
//$emailArrStr77 = implode(",", $emailsAddress);
                $email = 'bdcru@cegindia.com';
                sendcomptitorMail("$email", 'Comptitor List of Project', $msgDetails);
//sendcomptitorMail("jitendra00752@gmail.com", 'Comptitor List of Project', $msgDetails);
                $this->session->set_flashdata('msg', "Comptitor List Mail Successfully.");
                redirect(base_url('dashboard/viewedit/' . $cegexpID));
            endif;

            if ($_REQUEST['mailsendercom']):
//echo '<pre>'; print_r($data); die;
                $msgDetails = $this->load->view('email_template/comptitor_list_email8020', $data, true);
                $email = 'bdcru@cegindia.com';
// $email =  'jitendra00752@gmail';
                sendcomptitorProjectMail("$email", 'Project Result', $msgDetails);

                //sendcomptitorProjectMail("$cruemailArrStr77", 'Project Result', $msgDetails);
                $this->session->set_flashdata('msg', "Project Result Mail Successfully.");
                redirect(base_url('dashboard/viewedit/' . $cegexpID));
            endif;

//Code By Asheesh... Coordinator , Proj Name of Eng..
            $db1 = $this->db1->database;
            $db2 = $this->db2->database;
            $this->db->SELECT("$db2.main_users.userfullname,$db2.main_users.contactnumber");
            $this->db->FROM("$db1.project_assign_engnr");
            $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.project_assign_engnr.emp_id", "LEFT");
            $this->db->WHERE(array("$db1.project_assign_engnr.bd_project_id" => $cegexpID, "$db1.project_assign_engnr.status" => '1'));
            $engnrArr = $this->db->get()->row();
            $data['recname_eng'] = ($engnrArr) ? $engnrArr : '';

//Project Coodinator Get...
            $this->db->SELECT("$db2.main_users.userfullname,$db2.main_users.contactnumber");
            $this->db->FROM("$db1.project_coordinator");
            $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.project_coordinator.emp_id", "LEFT");
            $this->db->WHERE(array("$db1.project_coordinator.bd_project_id" => $cegexpID, "$db1.project_coordinator.status" => '1'));
            $coordArr = $this->db->get()->row();
            $data['recname_coordinator'] = ($coordArr) ? $coordArr : '';

//Get By Project Coordinator Durgesh...   
            $this->db->select('emp_id');
            $this->db->from('project_coordinator');
            $this->db->where(array('bd_project_id' => $cegexpID, 'status' => 1));
            $coordinator_result = $this->db->get()->row_array();
            $data['coordinator_row'] = ($coordinator_result) ? $coordinator_result : '';


//Get By Project engineer Durgesh...   
            $this->db->select('emp_id');
            $this->db->from('project_assign_engnr');
            $this->db->where(array('bd_project_id' => $cegexpID, 'status' => 1));
            $engineer_result = $this->db->get()->row_array();
            $data['engineer_row'] = ($engineer_result) ? $engineer_result : '';


//Financial details Get By Durgesh...   
            $this->db->select('*');
            $this->db->from('proj_financial_details');
            $this->db->where(array('bd_projid' => $cegexpID, 'status' => 1));
            $financial_result = $this->db->get()->row();
            $data['financial_record'] = ($financial_result) ? $financial_result : '';

//Performance BD Details Get By Durgesh...   
            $this->db->select('*');
            $this->db->from('proj_performance_Bd_details');
            $this->db->where(array('bd_projid' => $cegexpID, 'status' => 1));
            $performance_bd_result = $this->db->get()->row();
            $data['perf_bd_record'] = ($performance_bd_result) ? $performance_bd_result : '';

//important condition Get By Durgesh...   
            $this->db->select('*');
            $this->db->from('bdproject_important_condition');
            $this->db->where(array('bdproj_id' => $cegexpID, 'status' => 1));
            $impcond_bd_results = $this->db->get()->result();
            $data['imp_condition'] = ($impcond_bd_results) ? $impcond_bd_results : '';

//Performance BD Details Get By Durgesh...   
            $this->db->select('*');
            $this->db->from('proj_performance_Bd_details');
            $this->db->where(array('bd_projid' => $cegexpID, 'status' => 1));
            $performance_bd_results = $this->db->get()->result();
            $data['perf_bd_records'] = ($performance_bd_results) ? $performance_bd_results : '';

//Edit/Update TL And Ofc Manager Section..
            $this->db->select("$db2.main_employees_summary.user_id,$db2.main_employees_summary.userfullname,$db2.main_employees_summary.employeeId");
            $this->db->from("$db2.main_employees_summary");
            $this->db->where("($db2.main_employees_summary.businessunit_id='1' OR $db2.main_employees_summary.businessunit_id='2' OR $db2.main_employees_summary.businessunit_id='3')", NULL, FALSE);
            $this->db->where("$db2.main_employees_summary.isactive", '1');
            $this->db->order_by("$db2.main_employees_summary.userfullname", "ASC");
            $empArr = $this->db->get()->result();
            $data['empListArr'] = ($empArr) ? $empArr : '';

//Fetch Details of TL ..
            $this->db->SELECT("$db2.main_employees_summary.position_name,$db2.main_users.userfullname,$db2.main_users.contactnumber");
            $this->db->FROM("$db1.cegexp_proj_cont_persdetails");
            $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->WHERE(array("$db1.cegexp_proj_cont_persdetails.bd_projid" => $cegexpID, "$db1.cegexp_proj_cont_persdetails.status" => '1', "$db1.cegexp_proj_cont_persdetails.cont_pers_typer" => 'tl'));
            $projTLEmp = $this->db->get()->row();
            $data['projtlemp'] = ($projTLEmp) ? $projTLEmp : '';
            //$data['projtlemp'] = ($projTLEmp) ? $projTLEmp->userfullname . " ( <small style='color:green'>" . $projTLEmp->contactnumber . " )</small>" : '';
//Fetch Details of Project Ofc Manager ..
            $this->db->SELECT("$db2.main_employees_summary.position_name,$db2.main_users.id,$db2.main_users.userfullname,$db2.main_users.contactnumber");
            $this->db->FROM("$db1.cegexp_proj_cont_persdetails");
            $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->WHERE(array("$db1.cegexp_proj_cont_persdetails.bd_projid" => $cegexpID, "$db1.cegexp_proj_cont_persdetails.status" => '1', "$db1.cegexp_proj_cont_persdetails.cont_pers_typer" => 'ofc_mngr'));
            $projTLEmp = $this->db->get()->row();
            $data['projofc_mngremp'] = ($projTLEmp) ? $projTLEmp : '';
            //$data['projofc_mngremp'] = ($projTLEmp) ? $projTLEmp->userfullname . " ( <small style='color:green'>" . $projTLEmp->contactnumber . " )</small>" : '';
//Fetch Details..
            $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
            $this->db->from("$db2.tm_projects");
            $this->db->where(array("$db2.tm_projects.is_active" => '1'));
            // $this->db->where("($db2.tm_projects.project_category='ie' OR $db2.tm_projects.project_category='ae')", NULL, FALSE);
            $this->db->order_by("$db2.tm_projects.project_name", 'ASC');
            $data['ProjRec'] = $this->db->get()->result();
//Client Address Details...
            $data['clientaddrsdetails'] = $this->Getclient_addbyid($cegexpID);
//Get Client Address Details...
            $data['getclientaddrsdetails'] = $this->db->get_where(' bdceg_exp_clientmore_details', array('bdproj_id' => $cegexpID))->row_array();
//Get project Contract personal Details...	
            $this->db->select("$db2.main_employees_summary.position_name,$db1.cegexp_proj_cont_persdetails.emp_id as con_emp_id,$db1.cegexp_proj_cont_persdetails.cont_pers_typer as con_per_type,$db1.project_coordinator.emp_id as coor_emp_id,$db1.project_assign_engnr.emp_id as eng_emp_id");
            $this->db->from("$db1.cegexp_proj_cont_persdetails");
            $this->db->where("$db1.cegexp_proj_cont_persdetails.bd_projid", $cegexpID);
            $this->db->join("$db1.project_coordinator", "$db1.project_coordinator.bd_project_id=$db1.cegexp_proj_cont_persdetails.bd_projid", "inner");
            $this->db->join("$db1.project_assign_engnr", "$db1.project_assign_engnr.bd_project_id=$db1.cegexp_proj_cont_persdetails.bd_projid", "inner");
            $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id=$db1.cegexp_proj_cont_persdetails.emp_id", 'inner');
            $data['proj_con_per_detail'] = $this->db->get()->row_array();
//Project Ofc Address..
            $data['projoffcAddress'] = $this->Getprojofcaddress_addbyid($cegexpID);
            $data['hrmsprojid'] = $this->getprojidbdtohrms($cegexpID);
            $data['invcAmntDateWise'] = $this->Newreportsummary_model->get_datatables();
// code by durgesh for Get Detail About the lead, Jv, Associate Company.....            
            $this->db->select('b.company_name,a.project_id,a.compt_comp_id');
            $this->db->from('cegexp_competitor_comp as a');
            $this->db->where(array('a.project_id' => $cegexpID, 'a.status' => 1));
            $this->db->join('main_company as b', 'a.compt_comp_id=b.fld_id', 'inner');
            $comp_record = $this->db->get()->result();
            $data['competitor_jv_record'] = ($comp_record) ? $comp_record : '';
            $data['Jv_Lead_Associate_Comp'] = $this->mastermodel->GetCompNameDetailById($competitor_jv_record[0]->compt_comp_id, $cegexpID);
//Get Staffs Arr Code By Asheesh...
            $data['keyProfStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "1");
            $data['subProfessionalStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "2");
            $data['adminStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "3");
//March 2019..
            $this->db->select("$db1.proj_keydate_details.*");
            $this->db->from("$db1.proj_keydate_details");
            $this->db->where(array("$db1.proj_keydate_details.bd_projid" => $cegexpID, "$db1.proj_keydate_details.status" => "1"));
            $data['ReckeydateSingle'] = $this->db->get()->row();
            //Get All  Ofc Details....
            $this->db->SELECT("$db1.proj_siteofc_detail.*,$db2.countries.country_name,$db2.states.state_name,$db2.cities.city_name,$db2.main_employees_summary.userfullname as ofc_manager_userfullname,$db2.main_employees_summary.contactnumber as ofc_manager_contactnumber,$db2.main_employees_summary.emailaddress as ofc_manager_emailaddress,$db2.main_employees_summary.department_name as ofc_manager_department_name,$db2.d.userfullname as contper_userfullname,$db2.d.contactnumber as contper_contactnumber,$db2.d.emailaddress as contper_emailaddress,$db2.d.department_name as contper_department_name");
            $this->db->FROM("$db1.proj_siteofc_detail");
            $this->db->Join("$db2.countries", "$db1.proj_siteofc_detail.countries_idnew = $db2.countries.country_id", "LEFT");
            $this->db->Join("$db2.states", "$db1.proj_siteofc_detail.state_idnew = $db2.states.state_id", "LEFT");
            $this->db->Join("$db2.cities", "$db1.proj_siteofc_detail.city_idnew = $db2.cities.city_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary", "$db1.proj_siteofc_detail.ofc_mnger_empid = $db2.main_employees_summary.user_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary as d", "$db1.proj_siteofc_detail.cont_persname = $db2.d.user_id", "LEFT");
            $this->db->WHERE(array("$db1.proj_siteofc_detail.project_id" => $cegexpID, "$db1.proj_siteofc_detail.status" => "1"));
            $this->db->order_by("$db1.proj_siteofc_detail.fld_id", "DESC");
            $data['RecSiteOfcDetails'] = $this->db->get()->result();

            //Get single  Ofc Details....
            $this->db->SELECT("$db2.main_employees_summary.user_id,$db1.proj_siteofc_detail.*,$db2.countries.country_id,$db2.countries.country_name,$db2.states.state_name,$db2.cities.city_name,$db2.main_employees_summary.userfullname as ofc_manager_userfullname,$db2.main_employees_summary.contactnumber as ofc_manager_contactnumber,$db2.main_employees_summary.emailaddress as ofc_manager_emailaddress,$db2.main_employees_summary.department_name as ofc_manager_department_name,$db2.d.userfullname as contper_userfullname,$db2.d.contactnumber as contper_contactnumber,$db2.d.emailaddress as contper_emailaddress,$db2.d.department_name as contper_department_name");
            $this->db->FROM("$db1.proj_siteofc_detail");
            $this->db->Join("$db2.countries", "$db1.proj_siteofc_detail.countries_idnew = $db2.countries.country_id", "LEFT");
            $this->db->Join("$db2.states", "$db1.proj_siteofc_detail.state_idnew = $db2.states.state_id", "LEFT");
            $this->db->Join("$db2.cities", "$db1.proj_siteofc_detail.city_idnew = $db2.cities.city_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary", "$db1.proj_siteofc_detail.ofc_mnger_empid = $db2.main_employees_summary.user_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary as d", "$db1.proj_siteofc_detail.cont_persname = $db2.d.user_id", "LEFT");
            $this->db->WHERE(array("$db1.proj_siteofc_detail.project_id" => $cegexpID, "$db1.proj_siteofc_detail.status" => "1"));
            $this->db->order_by("$db1.proj_siteofc_detail.fld_id", "DESC");
            $data['RecSingleSiteOfcDetails'] = $this->db->get()->row_array();

//Replacement Count Order Code By Asheesh..
            //$data['replcRecArray'] = $this->Front_model->ReplacementCountOrder($cegexpID);

            $data['replcRec_key'] = $this->Front_model->replacementdataGetNew($cegexpID, 1);
            $data['replcRec_sub'] = $this->Front_model->replacementdataGetNew($cegexpID, 2);
            $data['replcRec_admin'] = $this->Front_model->replacementdataGetNew($cegexpID, 3);

            $data['keyemployeename'] = $this->Front_model->getemployee_withreplcond($cegexpID, 1);
            $data['subemployeename'] = $this->Front_model->getemployee_withreplcond($cegexpID, 2);
            $data['staffemployeename'] = $this->Front_model->getemployee_withreplcond($cegexpID, 3);
            $data['EOTrecData'] = $this->eotdetailsByProjid($cegexpID);
            // $data['vacantRecd'] = $this->getvacantprojwise($cegexpID);
            $data['vacantRecd'] = '';
            //Vacant Report Data...
//            echo "<pre>";
//            print_r($data['vacantRecd']);
//            die;                    

            $this->load->view('dashboardaccdept/viewedit_view', $data);
        } else {
            redirect(base_url('dashboard/acdashboard'));
        }
    }
	
	
	
    public function getvacantprojwise($bdprojID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.invc_vacant_resign_date.fld_id as resignid,$db1.invc_vacant_resign_date.emp_id,$db1.invc_vacant_typemaster.type_name,$db1.invc_vacant_resign_date.project_id,$db1.invc_vacant_resign_date.designation_id,$db1.invc_vacant_resign_date.last_date_resign,$db2.tm_projects.project_name,$db1.designation_master_requisition.designation_name");
        $this->db->from("$db1.invc_vacant_resign_date");
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.invc_vacant_resign_date.project_id", 'INNER');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.invc_vacant_resign_date.designation_id", 'LEFT');
        $this->db->join("$db1.invc_vacant_typemaster", "$db1.invc_vacant_typemaster.fld_id=$db1.invc_vacant_resign_date.vacant_typeid", 'LEFT');
        $this->db->where(array("$db1.invc_vacant_resign_date.status" => '1', "$db1.invc_vacant_resign_date.project_id" => $bdprojID));

        $recReturn = $this->db->get()->result();
        $retArr = array();

        if ($recReturn):
            foreach ($recReturn as $recD) {
                $val = $this->GetTimeLineProgress($recD->project_id, $recD->designation_id);
                if ($val == "") {
                    $StsTss = "<span style='color:red'> Insearch </span>";
                } else {
                    if (($val->curr_status) and ( $val->curr_status == "Pending")) {
                        $StsTss = "<span style='color:#6f1e1e'> Sent to Coordinator </span>";
                    }
                    if (($val->curr_status) and ( $val->curr_status == "Approved")) {
                        $StsTss = "<span style='color:green'> Approved By Coordinator </span>";
                    }
                    if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "1")) {
                        $StsTss = "<span style='color:green'> Cv Sent To Client </span>";
                    }
                    if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "2")) {
                        $StsTss = "<span style='color:green'> Approved by Client </span>";
                    }
                    if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "3")) {
                        $StsTss = "<span style='color:green'> Progress in Fianl Approval </span>";
                    }
                    if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "3") and ( $val->final_cvappr_yesorno == "yes")) {
                        $StsTss = "<span style='color:green'> Approved & Mobilized </span>";
                    }
                    if (( $val->client_cvappr_yesorno == "no") or ( $val->final_cvappr_yesorno == "no") or ( $val->curr_status == "Rejected")) {
                        $StsTss = "<span style='color:#f300e0'> Rejected (X) </span>";
                        if (in_array($loginId, $editPermitedArr)) {
                            $links3 = "<a href='" . base_url('crutraker/edit_vacantrec/' . $newproject->resignid) . "'><i style='color:#47748b;' title='Status Change' class='glyphicon glyphicon-edit icon-red'></i></a>";
                        }
                    }
                }
                $recD->projvc_status = $StsTss;
                //update by durgesh
                $proj_cood = $this->getprojcoordinatorname($bdprojID);
                $recD->proj_coodn = $proj_cood->userfullname;
                $recD->user_coodn_id = $proj_cood->id;
                //
                $retArr[] = $recD;
            }
        endif;
        return ($retArr) ? $retArr : false;
    }

    public function GetTimeLineProgress($projId, $DesignationId) {
        // $projId = "89246";
        // $DesignationId = "6026";
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcruvacant_crupanel.project_id,$db1.bdcruvacant_crupanel.designation_id,$db1.bdcruvacant_crupanel.id,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.entry_date,$db2.main_employees_summary.userfullname as entryby,$db2.coord.userfullname as coord_userfullname,$db1.bdcruvacant_crupanel.approval_status_chng_by,$db1.bdcruvacant_crupanel.approval_status_chng_date,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.step_by_cru,$db1.bdcruvacant_crupanel.cv_sent_to_client,$db2.cvsentclientby.userfullname as cvsentclientby_userfullname,$db2.clntcvappryesornoentryby.userfullname as clntcvappryesornoentryby_userfullname,$db1.bdcruvacant_crupanel.client_cvappr_yesorno,$db1.bdcruvacant_crupanel.interaction_date,$db1.bdcruvacant_crupanel.final_cvappr_yesorno,$db1.bdcruvacant_crupanel.mobilization_date");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as coord", "$db1.bdcruvacant_crupanel.approval_status_chng_by=$db2.coord.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as cvsentclientby", "$db1.bdcruvacant_crupanel.cv_sent_to_client_by=$db2.cvsentclientby.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as clntcvappryesornoentryby", "$db1.bdcruvacant_crupanel.client_cvappr_entryby=$db2.clntcvappryesornoentryby.user_id", "LEFT");
        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.project_id" => $projId, "$db1.bdcruvacant_crupanel.designation_id" => $DesignationId));
        $this->db->where("$db1.bdcruvacant_crupanel.curr_status!=", "Rejected", NULL, FALSE);
        $this->db->where("$db1.bdcruvacant_crupanel.client_cvappr_yesorno!=", "no", NULL, FALSE);
        $this->db->where("$db1.bdcruvacant_crupanel.final_cvappr_yesorno!=", "no", NULL, FALSE);

        $recDeptArr = $this->db->get()->row();
        return ($recDeptArr) ? $recDeptArr : null;
    }

    //update by durgesh
    //Get Project Coordinator Name By Proj Id..
    public function getprojcoordinatorname($projID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db2.main_users.userfullname,$db2.main_users.id");
        $this->db->FROM("$db1.project_coordinator");
        $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.project_coordinator.emp_id", "LEFT");
        $this->db->WHERE(array("$db1.project_coordinator.bd_project_id" => $projID, "$db1.project_coordinator.status" => '1'));
        $coordArr = $this->db->get()->row();
        return ($coordArr) ? $coordArr : '';
    }

    //update by durgesh
//code by durgesh for edit site office details
    public function edit_site_office_address() {
        $id = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('proj_siteofc_detail');
        $this->db->where('fld_id', $id);
        $result = $this->db->get()->row_array();
        echo json_encode($result);
    }

//code by durgesh for edit client details
    public function edit_client() {
        $id = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('bdceg_exp_clientmore_details');
        $this->db->where('fld_id', $id);
        $result = $this->db->get()->row_array();
        echo json_encode($result);
    }

// code by durgesh for site office details update   
    public function siteofcdetailupdate() {
        //print_r($this->input->post());
        // die;
        if ($this->input->post()) {
            $bdProjId = $this->input->post('projectid');
            $id_site = $this->input->post('id_site');
            $updateArr = array(
                "project_id" => $bdProjId,
                "ofc_mnger_empid" => $this->input->post('ofc_mnger_empid'),
                "street_address" => $this->input->post('street_address'),
                "countries_idnew" => $this->input->post('countries_idnew'),
                "state_idnew" => $this->input->post('state_idnew'),
                "city_idnew" => $this->input->post('city_idnew'),
                "siteofc_pincode" => $this->input->post('siteofc_pincode'),
                "cont_persname" => $this->input->post('cont_persname'),
                "entry_by" => $this->session->userdata('loginid')
            );
            $this->db->where('fld_id', $id_site);
            $updateArr = $this->db->update('proj_siteofc_detail', $updateArr);
            if ($updateArr):
                $this->session->set_flashdata('msg', "Site office Address Details Updated Successfully.");
                //redirect(base_url("dashboard/viewedit/" . $bdProjId));
                redirect($_SERVER['HTTP_REFERER']);
            else:
                // redirect(base_url("dashboard/viewedit/" . $bdProjId));
                redirect($_SERVER['HTTP_REFERER']);
            endif;
        }
    }

//code by durgesh client Address Update..
    public function clientaddressupdate() {
        $BDprojId = $this->input->post("project_id");
        $ids = $this->input->post("ids");
        $updateArr = array(
            'bdproj_id' => $BDprojId,
            'full_address' => $this->input->post("clientfull_address"),
            'client_city' => $this->input->post("clientcityname"),
            'client_postalpin' => $this->input->post("clientpostalcodepin"),
            'client_contactno' => $this->input->post("client_contno"),
            'client_cont_pers_name' => $this->input->post("client_contpersname"),
            'client_cont_pers_contact' => $this->input->post("client_contpers_num"),
            'client_email_pers' => $this->input->post("client_email_pers"),
            'contact_person_pos' => $this->input->post("cont_per_pos1"),
            'contact_person_address' => $this->input->post("contperfull_address1"),
            'entry_by' => $this->session->userdata('loginid'),
            'hrmsproj_id' => $this->getprojidbdtohrms($BDprojId)
        );
        $this->db->where('fld_Id', $ids);
        $resp = $this->db->update("bdceg_exp_clientmore_details", $updateArr);
        if ($resp) {
            $this->session->set_flashdata('msg', "Client Address Details Updated Successfully.");
        } else {
            $this->session->set_flashdata('msg', "Something Went Wrong.");
        }
        //redirect(base_url('dashboard/viewedit/' . $BDprojId));
        redirect($_SERVER['HTTP_REFERER']);
    }

//code by durgesh 
    // public function addimportantcondition() {
    // $BDprojId = $this->input->post("project_id");
    // $insertArr = array(
    // 'bdproj_id' => $BDprojId,
    // 'imp_content' => $this->input->post("imp_content"),
    // 'imp_heading' => $this->input->post("imp_heading"),
    // 'entry_by' => $this->session->userdata('loginid'),
    // );
    // $resp = $this->db->insert("bdproject_important_condition", $insertArr);
    // if ($resp) {
    // $this->session->set_flashdata('msg', "important condition Added Successfully.");
    // } else {
    // $this->session->set_flashdata('msg', "Something Went Wrong.");
    // }
    // redirect(base_url('dashboard/viewedit/' . $BDprojId));
    // }
    //code by durgesh(05-07-2019)
    public function addimportantcondition() {
        $Arr = $this->input->post();
        if ($Arr) {
            $BDprojId = $Arr['project_id'];
            if ($Arr['select_clause'] == 'Replacement') {
                $insertArr = array(
                    'bdproj_id' => $BDprojId,
                    'imp_content' => $Arr['imp_content'],
                    'imp_heading' => 'Replacement',
                    'entry_by' => $this->session->userdata('loginid')
                );
            }
            if ($Arr['select_clause'] == 'Escalation') {
                $insertArr = array(
                    'bdproj_id' => $BDprojId,
                    'imp_content' => $Arr['imp_content'],
                    'imp_heading' => 'Escalation',
                    'entry_by' => $this->session->userdata('loginid')
                );
            }
            if ($Arr['select_clause'] == 'Other') {
                $insertArr = array(
                    'bdproj_id' => $BDprojId,
                    'imp_content' => $Arr['imp_content'],
                    'imp_heading' => $Arr['imp_heading'],
                    'entry_by' => $this->session->userdata('loginid')
                );
            }
            $resp = $this->db->insert("bdproject_important_condition", $insertArr);

            if ($resp) {
                $this->session->set_flashdata('msg', "important condition Added Successfully.");
            } else {
                $this->session->set_flashdata('msg', "Something Went Wrong.");
            }
        }
        redirect(base_url('dashboard/viewedit/' . $BDprojId));
    }

    // code by durgesh 

    public function get_record() {
        $bd_prjid = $this->uri->segment(3);
        $this->load->library('pagination');
        $config = array();
        $config["base_url"] = base_url();
        $config["total_rows"] = $this->db->get_where('timeshet_fill', array('project_id' => $bd_prjid))->num_rows();
        $config["per_page"] = 20;
        $config["uri_segment"] = 4;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = "<ul class='pagination'>";
        $config["full_tag_close"] = "</ul>";
        $config["first_tag_open"] = "<li>";
        $config["first_tag_close"] = "</li>";
        $config["last_tag_open"] = "<li>";
        $config["last_tag_close"] = "</li>";
        $config["next_link"] = "&gt;";
        $config["next_tag_open"] = "<li>";
        $config["next_tag_close"] = "</li>";
        $config["prev_link"] = "&lt;";
        $config["prev_tag_open"] = "<li>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_link"] = 1;
        $this->pagination->initialize($config);
        $page = $this->uri->segment(4);
        $start = ($page - 1) * $config["per_page"];

        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $output = '';
        $this->db->select("a.*,c.designation_name,b.userfullname");
        $this->db->from("$db1.timeshet_fill as a");
        $this->db->where(array("a.project_id" => $bd_prjid));
        $this->db->limit($config["per_page"], $start);
        $this->db->join("$db1.designation_master_requisition as c", "c.fld_id=a.designation_id", "left");
        $this->db->join("$db2.main_employees_summary as b", "b.user_id=a.emp_id", "left");
        $this->db->order_by(array("a.year" => "ASC"));
        $record_result = $this->db->get()->result_array();
        $output .= '<table class="table table-bordered"><thead><tr><th>SNo.</th><th>Name</th><th>position</th><th>Year</th><th>Month</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th><th>17</th><th>18</th><th>19</th><th>20</th><th>21</th><th>22</th><th>23</th><th>24</th><th>25</th><th>26</th><th>27</th><th>28</th><th>29</th><th>30</th><th>31</th></tr></thead>';
        $sno = 0;
        foreach ($record_result as $rrow) {
            $sno++;

            $output .= '<tr><tr><td>' . $sno . '</td><td>' . $rrow["userfullname"] . '</td><td>' . $rrow["designation_name"] . '</td><td>' . $rrow["year"] . '</td><td>' . $rrow["month"] . '</td><td>' . $rrow["1"] . '</td><td>' . $rrow["2"] . '</td><td>' . $rrow["3"] . '</td><td>' . $rrow["4"] . '</td><td>' . $rrow["5"] . '</td><td>' . $rrow["6"] . '</td><td>' . $rrow["7"] . '</td><td>' . $rrow["8"] . '</td><td>' . $rrow["9"] . '</td><td>' . $rrow["10"] . '</td><td>' . $rrow["11"] . '</td><td>' . $rrow["12"] . '</td><td>' . $rrow["13"] . '</td><td>' . $rrow["14"] . '</td><td>' . $rrow["15"] . '</td><td>' . $rrow["16"] . '</td><td>' . $rrow["17"] . '</td><td>' . $rrow["18"] . '</td><td>' . $rrow["19"] . '</td><td>' . $rrow["20"] . '</td><td>' . $rrow["21"] . '</td><td>' . $rrow["22"] . '</td><td>' . $rrow["23"] . '</td><td>' . $rrow["24"] . '</td><td>' . $rrow["25"] . '</td><td>' . $rrow["26"] . '</td><td>' . $rrow["27"] . '</td><td>' . $rrow["28"] . '</td><td>' . $rrow["29"] . '</td><td>' . $rrow["30"] . '</td><td>' . $rrow["31"] . '</td></tr>';
        }
        $output .= '</table>';
        $pagi = $this->pagination->create_links();
        $result = array(
            "pagination_link" => $pagi,
            "monthly_emp_atten" => $output
        );
        echo json_encode($result);
    }

    //Previous Data Ajax..
    public function previous_attendata_ajax() {
        $prevDate = $_REQUEST['prevdateD'];
        $cegexpID = $_REQUEST['bdprojid'];

        $keyemployeename = $this->Front_model->getemployee_withreplcond($cegexpID, 1);
        $subemployeename = $this->Front_model->getemployee_withreplcond($cegexpID, 2);
        $staffemployeename = $this->Front_model->getemployee_withreplcond($cegexpID, 3);

        $ddd = strtotime($prevDate . "-01");
        $priVos = date("Y-m", strtotime("-1 month", $ddd));

        $month = date("m", strtotime($priVos));
        $year = date("Y", strtotime($priVos));

        $SundListrecArr = GetAllSundyDateList($month, $year);
        ?>
        <div class="tab-content">
            <table border="1px" cellpadding="10px" cellspacing="100px">
                <tr>
                    <th style="padding-left:1%;">Sr.No.</th>
                    <th style="padding-left:1%; width:20%">Employee Name</th>
                    <th style="padding-left:1%; width:20%">Position</th>
                    <th style="color:<?= (in_array("01", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">01</th>
                    <th style="color:<?= (in_array("02", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">02</th>
                    <th style="color:<?= (in_array("03", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">03</th>
                    <th style="color:<?= (in_array("04", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">04</th>
                    <th style="color:<?= (in_array("05", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">05</th>
                    <th style="color:<?= (in_array("06", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">06</th>
                    <th style="color:<?= (in_array("07", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">07</th>
                    <th style="color:<?= (in_array("08", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">08</th>
                    <th style="color:<?= (in_array("09", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">09</th>
                    <th style="color:<?= (in_array("10", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">10</th>
                    <th style="color:<?= (in_array("11", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">11</th>
                    <th style="color:<?= (in_array("12", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">12</th>
                    <th style="color:<?= (in_array("13", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">13</th>
                    <th style="color:<?= (in_array("14", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">14</th>
                    <th style="color:<?= (in_array("15", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">15</th>
                    <th style="color:<?= (in_array("16", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">16</th>
                    <th style="color:<?= (in_array("17", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">17</th>
                    <th style="color:<?= (in_array("18", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">18</th>
                    <th style="color:<?= (in_array("19", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">19</th>
                    <th style="color:<?= (in_array("20", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">20</th>
                    <th style="color:<?= (in_array("21", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">21</th>
                    <th style="color:<?= (in_array("22", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">22</th>
                    <th style="color:<?= (in_array("23", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">23</th>
                    <th style="color:<?= (in_array("24", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">24</th>
                    <th style="color:<?= (in_array("25", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">25</th>
                    <th style="color:<?= (in_array("26", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">26</th>
                    <th style="color:<?= (in_array("27", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">27</th>
                    <th style="color:<?= (in_array("28", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">28</th>
                    <th style="color:<?= (in_array("29", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">29</th>
                    <th style="color:<?= (in_array("30", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">30</th>
                    <th style="color:<?= (in_array("31", $SundListrecArr)) ? "red" : ""; ?>" class="tablespace">31</th>
                </tr>

                <tr>
                    <th colspan="34" style="color:green; text-align: center;" align="center"> <h4>Key Professional</h4> </th>
                </tr>

                <?php
                // echo "====>".$bddetail->fld_id;
                if ($keyemployeename) {
                    foreach ($keyemployeename as $key => $val) {
                        $keyarr = getmonthattdence($month, $year, $cegexpID, $val->empname, $val->designation_id);
                        ?>
                        <tr>
                            <td style="padding-left:1%;"> <?= $key + 1; ?></td>
                            <td style="padding-left:1%; width:20%"> <?= isset($val->userfullname) ? ucwords(strtolower($val->userfullname)) : ''; ?> </td>
                            <td style="padding-left:1%; width:20%"><?= @$val->designation_name; ?></td>

                            <td class="tablespace"><?= isset($keyarr['1']) ? strtoupper($keyarr['1']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['2']) ? strtoupper($keyarr['2']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['3']) ? strtoupper($keyarr['3']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['4']) ? strtoupper($keyarr['4']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['5']) ? strtoupper($keyarr['5']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['6']) ? strtoupper($keyarr['6']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['7']) ? strtoupper($keyarr['7']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['8']) ? strtoupper($keyarr['8']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['9']) ? strtoupper($keyarr['9']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['10']) ? strtoupper($keyarr['10']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['11']) ? strtoupper($keyarr['11']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['12']) ? strtoupper($keyarr['12']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['13']) ? strtoupper($keyarr['13']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['14']) ? strtoupper($keyarr['14']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['15']) ? strtoupper($keyarr['15']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['16']) ? strtoupper($keyarr['16']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['17']) ? strtoupper($keyarr['17']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['18']) ? strtoupper($keyarr['18']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['19']) ? strtoupper($keyarr['19']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['20']) ? strtoupper($keyarr['20']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['21']) ? strtoupper($keyarr['21']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['22']) ? strtoupper($keyarr['22']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['23']) ? strtoupper($keyarr['23']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['24']) ? strtoupper($keyarr['24']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['25']) ? strtoupper($keyarr['25']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['26']) ? strtoupper($keyarr['26']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['27']) ? strtoupper($keyarr['27']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['28']) ? strtoupper($keyarr['28']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['29']) ? strtoupper($keyarr['29']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['30']) ? strtoupper($keyarr['30']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['31']) ? strtoupper($keyarr['31']) : '--'; ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                <tr>
                    <th colspan="34" style="color:green; text-align: center;" align="center"> <h4>Sub Professional</h4> </th>
                </tr>

                <?php
                if ($subemployeename) {
                    foreach ($subemployeename as $keys => $vals) {
                        $keyarr = getmonthattdence($month, $year, $cegexpID, $vals->empname, $vals->designation_id);
                        ?>
                        <tr>
                            <td style="padding-left:1%;"> <?= $keys + 1; ?></td>
                            <td style="padding-left:1%; width:20%"> <?= isset($vals->userfullname) ? ucwords(strtolower($vals->userfullname)) : ''; ?> </td>
                            <td style="padding-left:1%; width:20%"><?= @$vals->designation_name; ?></td>

                            <td class="tablespace"><?= isset($keyarr['1']) ? strtoupper($keyarr['1']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['2']) ? strtoupper($keyarr['2']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['3']) ? strtoupper($keyarr['3']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['4']) ? strtoupper($keyarr['4']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['5']) ? strtoupper($keyarr['5']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['6']) ? strtoupper($keyarr['6']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['7']) ? strtoupper($keyarr['7']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['8']) ? strtoupper($keyarr['8']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['9']) ? strtoupper($keyarr['9']) : '--'; ?></td> 
                            <td class="tablespace"><?= isset($keyarr['10']) ? strtoupper($keyarr['10']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['11']) ? strtoupper($keyarr['11']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['12']) ? strtoupper($keyarr['12']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['13']) ? strtoupper($keyarr['13']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['14']) ? strtoupper($keyarr['14']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['15']) ? strtoupper($keyarr['15']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['16']) ? strtoupper($keyarr['16']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['17']) ? strtoupper($keyarr['17']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['18']) ? strtoupper($keyarr['18']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['19']) ? strtoupper($keyarr['19']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['20']) ? strtoupper($keyarr['20']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['21']) ? strtoupper($keyarr['21']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['22']) ? strtoupper($keyarr['22']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['23']) ? strtoupper($keyarr['23']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['24']) ? strtoupper($keyarr['24']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['25']) ? strtoupper($keyarr['25']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['26']) ? strtoupper($keyarr['26']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['27']) ? strtoupper($keyarr['27']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['28']) ? strtoupper($keyarr['28']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['29']) ? strtoupper($keyarr['29']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['30']) ? strtoupper($keyarr['30']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['31']) ? strtoupper($keyarr['31']) : '--'; ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                <tr>
                    <th colspan="34" style="color:green; text-align: center;" align="center"> <h4> Admin Staff </h4> </th>
                </tr>
                <?php
                if ($staffemployeename) {
                    foreach ($staffemployeename as $keyAd => $valAd) {
                        $keyarr = getmonthattdence($month, $year, $cegexpID, $valAd->empname, $valAd->designation_id);
                        ?>
                        <tr>
                            <td style="padding-left:1%;"> <?= $keyAd + 1; ?></td>
                            <td style="padding-left:1%; width:20%"> <?= isset($valAd->userfullname) ? ucwords(strtolower($valAd->userfullname)) : ''; ?> </td>
                            <td style="padding-left:1%; width:20%"><?= @$valAd->designation_name; ?></td>

                            <td class="tablespace"><?= isset($keyarr['1']) ? strtoupper($keyarr['1']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['2']) ? strtoupper($keyarr['2']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['3']) ? strtoupper($keyarr['3']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['4']) ? strtoupper($keyarr['4']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['5']) ? strtoupper($keyarr['5']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['6']) ? strtoupper($keyarr['6']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['7']) ? strtoupper($keyarr['7']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['8']) ? strtoupper($keyarr['8']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['9']) ? strtoupper($keyarr['9']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['10']) ? strtoupper($keyarr['10']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['11']) ? strtoupper($keyarr['11']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['12']) ? strtoupper($keyarr['12']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['13']) ? strtoupper($keyarr['13']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['14']) ? strtoupper($keyarr['14']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['15']) ? strtoupper($keyarr['15']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['16']) ? strtoupper($keyarr['16']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['17']) ? strtoupper($keyarr['17']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['18']) ? strtoupper($keyarr['18']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['19']) ? strtoupper($keyarr['19']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['20']) ? strtoupper($keyarr['20']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['21']) ? strtoupper($keyarr['21']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['22']) ? strtoupper($keyarr['22']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['23']) ? strtoupper($keyarr['23']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['24']) ? strtoupper($keyarr['24']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['25']) ? strtoupper($keyarr['25']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['26']) ? strtoupper($keyarr['26']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['27']) ? strtoupper($keyarr['27']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['28']) ? strtoupper($keyarr['28']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['29']) ? strtoupper($keyarr['29']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['30']) ? strtoupper($keyarr['30']) : '--'; ?></td>
                            <td class="tablespace"><?= isset($keyarr['31']) ? strtoupper($keyarr['31']) : '--'; ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </table>
        </div>
        <?php
    }

    public function previous_hiddendate_ajax() {
        $prevDate = $_REQUEST['prevdateD'];
        $ddd = strtotime($prevDate . "-01");
        $priVos = date("Y-m", strtotime("-0 month", $ddd));
        echo $priVos;
    }

    //Get EOT Details By ID.. Code By Asheesh 30-07-2019.
    public function eotdetailsByProjid($bdprojid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.eot_maineot_record.*");
        $this->db->from("$db1.eot_maineot_record");
        $this->db->where(array("$db1.eot_maineot_record.bd_proj_id" => $bdprojid, "$db1.eot_maineot_record.status" => "1"));
        $eoTrecArr = $this->db->get()->result();
        return ($eoTrecArr) ? $eoTrecArr : null;
    }

    // code by durgesh 31-07-2019.
    public function accountdashboard_top() {
        $title = 'Total CV';
        $ResultRecord = $this->Newreportsummary_model->get_topper_div();
        $totCv = 0;
        $totComulative = 0;
        $currentbill = 0;
        if ($ResultRecord):
            foreach ($ResultRecord as $RowRec):
                $CvValue = $RowRec->contract_value;
                $totcumulative = $RowRec->total_cumulative;
                $currentbill = $RowRec->current_month_bill;
                $totCv += $CvValue;
                $totComulative += $totcumulative;
                $currentbill += $currentbill;
            endforeach;
        endif;
        $getCv = $totCv;
        $getTotalBilled = $totComulative;
        $getbill = $currentbill;

        $this->load->view('accountdashboard/accountdashboard_view', compact("title", "empArr", "getCv", "getTotalBilled", "getbill"));
    }
	
	
	// code by Gaurav 
    public function ajax_list_vacant_position() {
        
		// $proj_id = 157300;
		// $pro_id = $this->uri->total_segments();
		$pro_id = $this->uri->segment(3);
		// echo $proj_id; die;
		$list = $this->vacant_position__model->get_datatables($pro_id);
		$CurrStatus = Array("" => "Insearch", "1" => "CV Sent To Coordinator", "2" => "Approved by Coordinator", "3" => "Rejected by Coordinator", "4" => "CV Formatted Yes", "5" => "CV Sent To Client", "6" => "Interaction Date Added", "7" => "Final Approved & Mobilize", "8" => "Final Rejected");
        $CurrStatus_Color = Array("" => "red", "1" => "green", "2" => "green", "3" => "red", "4" => "#005ab3", "5" => "#00c4ff", "6" => "#6f5499", "7" => "#00aced", "8" => "red");
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
		// echo $proj_id; die;
		// echo "<pre>";
		// print_r($list);die;
        foreach ($list as $vacpos) {
            $no++;
            $row = array();
            $row[] = $no;
            // $tender_name = $this->vacant_position__model->GetprojectTenderName($comprow->project_id);
            // $prjhrms_name = $this->vacant_position__model->GetprojectHrmsName($comprow->project_numberid);
            $row[] = $vacpos->userfullname;
            $row[] = $vacpos->designation_name;
            $row[] = "<span style='color:" . $CurrStatus_Color[$vacpos->curr_status] . "'>" . $CurrStatus[$vacpos->curr_status] . "</span>";
            $row[] = $vacpos->last_date_resign;
            // $row[] = $no;
            // $row[] = number_format($comprow->total_cumulative, 2);
            // $recBlnc = ($comprow->contract_value - $comprow->total_cumulative);
            // $row[] = number_format($recBlnc, 2);
            // $row[] = number_format($comprow->current_month_bill, 2);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->vacant_position__model->count_filtered(),
            "data" => $data,
        );
//output to json format
        echo json_encode($output);
    }
	
    // code by durgesh 31-07-2019.
    public function ajax_list_top() {
        $list = $this->Accountdashboard_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';

        foreach ($list as $comprow) {
            $no++;
            $row = array();
            $row[] = $no;
            $tender_name = $this->Accountdashboard_model->GetprojectTenderName($comprow->project_id);
            $prjhrms_name = $this->Accountdashboard_model->GetprojectHrmsName($comprow->project_numberid);
            $row[] = $tender_name['TenderDetails'];
            $row[] = $prjhrms_name['project_name'];
            $row[] = $comprow->invoice_name;
            $row[] = date('d-m-Y', strtotime($comprow->invoice_date));
            $row[] = number_format($comprow->contract_value, 2);
            $row[] = number_format($comprow->total_cumulative, 2);
            $recBlnc = ($comprow->contract_value - $comprow->total_cumulative);
            $row[] = number_format($recBlnc, 2);
            $row[] = number_format($comprow->current_month_bill, 2);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Accountdashboard_model->count_filtered(),
            "data" => $data,
        );
//output to json format
        echo json_encode($output);
    }

    // code by durgesh
    public function accountinfo_ajax() {
        $list = $this->accountsummaryreport->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date("F Y", strtotime($val->invoice_date));
            $row[] = number_format($val->remuneration, 2);
            $row[] = number_format($val->reimbursable, 2);
            $row[] = number_format($val->taxtotalinvcwise, 2);
            $row[] = '<a href="' . base_url('dashboard/edit_amount_received/' . $val->id) . '"><span  style="cursor:pointer;color:green;"  title="Edit Amount receieved" class="glyphicon glyphicon-edit"></span></a>';
            $data[] = $row;
        }
        $output = array("draw" => $_POST['draw'],
            "recordsTotal" => $this->accountsummaryreport->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->accountsummaryreport->count_filtered(),
            "data" => $data,
        );
//output to json format
        echo json_encode($output);
    }

    // code by durgesh
    public function edit_amount_received() {
        $id = $this->uri->segment(3);
        $data['title'] = "Edit Amount Received";
        $data['invoice_download'] = $this->invoice_xsm_download($id);
        $data['invoice_details'] = $this->invoice_details($id);
        $data['invoice'] = $this->invoice_details2($id);
        $data['esclaration_details'] = $this->invoice_esclaration_details($id);
        $data['proj_name'] = $this->SecondDB_model->getProjectBYId($data['invoice_details']['project_numberid']);
        $this->load->view('payment_amount_received/edit_amount_received', $data);
    }

    // code by durgesh
    public function add_amount_received() {
        $id = $this->uri->segment(3);
        $invoiceid = $this->uri->segment(4);
        $project_tender_id = $this->db->get_where('bdcegexp_proj_summery', array('project_numberid' => $id))->row();
        $data['deci'] = $this->db->get_where('accountinfo', array('project_numberid' => $id))->row();
        $data['invoice'] = $this->invoice_details2($invoiceid);
        $cegexpID = $project_tender_id->project_id;
        $data['title'] = "Add Amount Received";
        $data['proj_name'] = $this->SecondDB_model->getProjectBYId($id);
        $data['keyProfStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "1");
        $data['subProfessionalStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "2");
        $data['adminStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "3");
        $this->load->view('payment_amount_received/add_amount_received', $data);
    }

    // code by durgesh
    public function add_reimbursable_amount_received() {
        $id = $this->uri->segment(3);
        $project_tender_id = $this->db->get_where('bdcegexp_proj_summery', array('project_numberid' => $id))->row();
        $cegexpID = $project_tender_id->project_id;
        $title = "Add Reimbursable Amount Received";
        $proj_name = $this->SecondDB_model->getProjectBYId($id);
        $cur_mm = $this->invoice_details1($proj_name->id);
        $this->load->view('payment_amount_received/add_reimbursable_amount_received', compact('title', 'cegexpID', 'proj_name', 'cur_mm'));
    }

    // code by durgesh
    public function invoice_xsm_download($id) {
        $Arr = array('total_rs', 'grand_total_rs');
        $this->db->select('*');
        $this->db->from('invoice_after_xlsdownload');
        $this->db->where(array('invoice_id' => $id));
        $this->db->where_not_in('srpos_no', $Arr);
        $result = $this->db->get()->result();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    // code by durgesh
    public function invoice_details($id) {
        $this->db->select('*');
        $this->db->from('invoicedetail');
        $this->db->where('id', $id);
        $result = $this->db->get()->row_array();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    // code by durgesh
    public function invoice_details1($id) {
        $this->db->select('*');
        $this->db->from('invoicedetail');
        $this->db->where('project_numberid', $id);
        $result = $this->db->get()->row_array();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    // code by durgesh
    public function invoice_details2($id) {
        $this->db->select('*');
        $this->db->from('invoicedetail');
        $this->db->where('id', $id);
        $result = $this->db->get()->row();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    // code by durgesh
    public function invoice_esclaration_details($id) {
        $this->db->select('*');
        $this->db->from('escalation_tax');
        $this->db->where('invoice_id', $id);
        $result = $this->db->get()->result();
        if ($result) {
            return ($result) ? $result : '';
        }
    }

    //code by durgesh
    public function proj_cat_pos() {
        $title = 'Project Cat Position';
        $bdprojid = '192847';
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.designation_master_requisition.designation_name,$db2.main_employees_summary.userfullname,$db1.designationcat_master.ktype_name");
        $this->db->from("$db1.assign_finalteam");
        $this->db->where(array("$db1.assign_finalteam.project_id" => $bdprojid, "$db1.assign_finalteam.status" => "1"));
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id", "inner");
        $this->db->join("$db2.main_employees_summary", "$db1.assign_finalteam.empname=$db2.main_employees_summary.user_id", "inner");
        $this->db->join("$db1.designationcat_master", "$db1.designationcat_master.k_id=$db1.designation_master_requisition.cat_id", "inner");
        $results = $this->db->get()->result();
        $this->load->view('proj_position/proj_position', compact('title', 'results'));
    }

    // code by durgesh for user dashboard(16-09-2019)
    public function user_dashboard() {
        $title = 'Dashboard - Personal Information';
        $user_id = $this->session->userdata('loginid');
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_employees_summary.profileimg,$db2.main_employees_summary.userfullname,$db2.main_employees_summary.date_of_joining,$db2.main_employees_summary.emailaddress,$db2.main_employees_summary.contactnumber,$db2.main_employees_summary.reporting_manager_name,$db2.main_employees_summary.department_name,$db2.main_employees_summary.position_name,$db2.emp_otherofficial_data.reviewing_officer_ro");
        $this->db->from("$db2.main_employees_summary");
        $this->db->join("$db2.emp_otherofficial_data", "$db2.emp_otherofficial_data.user_id=$db2.main_employees_summary.user_id", "inner");
        $this->db->where(array("$db2.main_employees_summary.user_id" => $user_id, "$db2.main_employees_summary.isactive" => "1"));
        $user_details = $this->db->get()->row();
        $ro = $this->GetROUserDashboard($user_details->reviewing_officer_ro);
        $this->load->view('userdashboard_view', compact('title', 'user_details', 'ro'));
    }

    // code by durgesh GET RO Name For user dashboard(16-09-2019)
    public function GetROUserDashboard($RoID) {

        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_employees_summary.userfullname");
        $this->db->from("$db2.main_employees_summary");
        $this->db->where(array("$db2.main_employees_summary.user_id" => $RoID, "$db2.main_employees_summary.isactive" => "1"));
        $ro_name = $this->db->get()->row();
        if ($ro_name) {
            return ($ro_name) ? $ro_name : '';
        }
    }

}
