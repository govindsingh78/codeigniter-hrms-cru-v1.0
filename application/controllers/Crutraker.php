<?php

/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Crutraker extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('crutraker/Crutrakermodel');
        $this->load->model('crutraker/Projectvacantposition_model');
        $this->load->model('crutraker/Projectlist_model');
        $this->load->helper(array('form', 'url', 'user_helper'));

        date_default_timezone_set("Asia/Kolkata");
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);

        $bduserid = $this->session->userdata('uid');
        if ($bduserid == "") {
            redirect(base_url('welcome/logout'));
        }
    }

    //Ajax For Data Table Durgesh...
//    public function Ajax_GetRecord_vacant_project() {
//        $list = $this->Projectvacantposition_model->get_datatables();
//        $loginId = $this->session->userdata('uid');
//        $links1 = '';
//        $links2 = '';
//        $data = array();
//        $no = $_POST['start'];
//        foreach ($list as $val) {
//            $no++;
//            $StsTss = "<span style='color:red'> Insearch </span>";
//            if (($val->curr_status) and ( $val->curr_status == "Pending")) {
//                $StsTss = "<span style='color:#6f1e1e'> Sent to Coordinator </span>";
//            }
//            if (($val->curr_status) and ( $val->curr_status == "Approved")) {
//                $StsTss = "<span style='color:green'> Approved By Coordinator </span>";
//            }
//            if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "1")) {
//                $StsTss = "<span style='color:green'> Cv Sent To Client </span>";
//            }
//            if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "2")) {
//                $StsTss = "<span style='color:green'> Approved by Client </span>";
//            }
//            if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "3")) {
//                $StsTss = "<span style='color:green'> Progress in Fianl Approval </span>";
//            }
//            if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "3") and ( $val->final_cvappr_yesorno == "yes")) {
//                $StsTss = "<span style='color:green'> Approved & Mobilized </span>";
//            }
//            if (( $val->client_cvappr_yesorno == "no") or ( $val->final_cvappr_yesorno == "no")) {
//                $StsTss = "<span style='color:red'> Rejected </span>";
//            }
//            $row = array();
//            $row[] = $no;
//            $row[] = $val->project_name;
//            $row[] = $val->designation_name;
//            $row[] = '<span style="color:green;">' . date('Y-m-d', strtotime($val->last_date_resign)) . '</span>';
//            $check = $this->db->get_where('bdcruvacant_crupanel', array('project_id' => $val->project_id, 'designation_id' => $val->designation_id))->row_array();
//            $staus = $this->db->get_where('bdcruvacant_status', array('status' => '1'))->result_array();
//            $row[] = $StsTss;
//            $deptArr = $this->GetAllCruDeptUserIdArr();
//            if (in_array($loginId, $deptArr)) {
//                $links1 = "<a href='" . base_url('crutraker/edit_crutraker_board_project/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:red;' title='CRU' class='glyphicon glyphicon-edit icon-red'></i></a>";
//            }
//            if ($loginId == "271") {
//                $links2 = "<a href='" . base_url('crutraker/project_coordinator/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:green;' title='Cordinator' class='glyphicon glyphicon-user icon-green'></i></a>";
//            }
//            if ($loginId == "297" or $loginId == "296" or $loginId == "190") {
//                $links1 = "<a href='" . base_url('crutraker/edit_crutraker_board_project/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:red;' title='CRU' class='glyphicon glyphicon-edit icon-red'></i></a>";
//                $links2 = "<a href='" . base_url('crutraker/project_coordinator/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:green;' title='Cordinator' class='glyphicon glyphicon-user icon-green'></i></a>";
//            }
////            if ($loginId == "190") {
////                $links1 = "<a href='" . base_url('crutraker/edit_crutraker_board_project/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:red;' title='CRU' class='glyphicon glyphicon-edit icon-red'></i></a>";
////                $links2 = "<a href='" . base_url('crutraker/project_coordinator/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:green;' title='Cordinator' class='glyphicon glyphicon-user icon-green'></i></a>";
////            }
//            $row[] = $links1 . "&nbsp;&nbsp" . $links2;
//            $data[] = $row;
//        }
//
//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->Projectvacantposition_model->count_all(),
//            "recordsTotal" => '0',
//            "recordsFiltered" => $this->Projectvacantposition_model->count_filtered(),
//            "data" => $data,
//        );
//        //output to json format
//        echo json_encode($output);
//    }
    //Get Employee Details By ID.
    public function get_inhouse_userdetail_byid() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($_REQUEST['inhouseuserids']) {
            $this->db->select("$db2.main_employees_summary.contactnumber,$db2.main_employees_summary.emailaddress");
            $this->db->from("$db2.main_employees_summary");
            $this->db->where(array("$db2.main_employees_summary.user_id" => $_REQUEST['inhouseuserids']));
            $UserRecResult = $this->db->get()->row();
        }
        echo json_encode($UserRecResult);
    }

    public function get_other_userdetail_byid() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($_REQUEST['otheruserids']) {
            $this->db->select("$db1.team_req_otheremp.*");
            $this->db->from("$db1.team_req_otheremp");
            $this->db->where(array("$db1.team_req_otheremp.fld_id" => $_REQUEST['otheruserids'], "$db1.team_req_otheremp.status" => "1"));
            $OtherUserRecResult = $this->db->get()->row();
        }
        echo json_encode($OtherUserRecResult);
    }

    //Code by durgesh for client
    public function project_client() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        $data['error'] = '';
        $data['title'] = 'Edit Project Client Tracker Board Project';
        $data['client_record'] = $this->db->get_where('bdcruvacant_crupanel', array('project_id' => $projid, 'designation_id' => $desiid))->row_array();
        $data['cv_sent'] = $this->db->get_where('bdcruvacant_crupanel', array('project_id' => $projid, 'designation_id' => $desiid))->row_array();
        $data['desig_detail'] = $this->Crutrakermodel->designation_name($desiid);
        $data['coor_details'] = $this->Crutrakermodel->Get_project_name_or_code_designation_record($projid);
        $data['project_cru_rport'] = $this->db->get_where('bdcruvacant_crupanel', array('project_id' => $projid, 'designation_id' => $desiid, 'status' => 'Approved'))->result();
        $this->load->view('cru_traker/edit_clienttraker_board_project', $data);
    }

    public function project_coordinator_approve_reject() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        $Arr = $_REQUEST;
        if ($Arr) {
            $updateArr = array('status' => $Arr['coord_action'], 'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('project_id' => $projid, 'designation_id' => $desiid, 'name' => $Arr['emp_name']));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
        }
        if ($response):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
        endif;
        redirect(base_url('crutraker/project_coordinator/' . $projid . '/' . $desiid));
    }

    //code by durgesh.....
    public function interaction_date() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        $Arr = $_REQUEST;
        if ($Arr) {
            $updateArr = array('interaction_date' => $Arr['interaction_date'], 'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('project_id' => $projid, 'designation_id' => $desiid));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
        }
        if ($response):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
        endif;
        redirect($_SERVER['HTTP_REFERER']);
    }

    //code by durgesh.....
    public function mobilization_date() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        $Arr = $_REQUEST;
        if ($Arr) {
            $updateArr = array('mobilization_date' => $Arr['mobilization_date'], 'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('project_id' => $projid, 'designation_id' => $desiid));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
        }
        if ($response):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
        endif;
        redirect($_SERVER['HTTP_REFERER']);
    }

    protected function getUploadConfig($folder) {
        $config = array();
        $config['upload_path'] = $folder;
        $config['max_size'] = '79000'; //7MB
        $config['allowed_types'] = 'PDF|pdf|doc|DOC|jpg|jpge|docx|DOCX';
        $config['file_name'] = "resume_" . time();
        return $config;
    }

    //Step Submited Step 1...
    public function rowaction_first() {
        $recFormData = $this->input->post();
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($recFormData) {
            $UpDdata = array("$db1.bdcruvacant_crupanel.step_by_cru" => "1", "$db1.bdcruvacant_crupanel.cv_sent_to_client_by" => $this->session->userdata('uid'), "$db1.bdcruvacant_crupanel.cv_sent_to_client" => date("Y-m-d", strtotime($recFormData['cv_send_toclient_date'])));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $recFormData['rowaction_1']);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $recFormData['rowaction_1'], "$db1.cruvacant_crupanel_step_history.action_name" => "Cv Send To Client", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => date("Y-m-d", strtotime($recFormData['cv_send_toclient_date'])));
            $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
            $this->session->set_flashdata('msg', 'Step 1 Cv Sent To Client successfully');
        }
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $recFormData['bdprojid'] . "/" . $recFormData['designation_id']));
    }

    //Step Submitted Step 2...
    public function rowaction_second() {
        $recFormData = $this->input->post();
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($recFormData) {
            $UpDdata = array("$db1.bdcruvacant_crupanel.interaction_date" => ($recFormData['interaction_date']) ? date("Y-m-d", strtotime($recFormData['interaction_date'])) : NULL, "$db1.bdcruvacant_crupanel.step_by_cru" => "2", "$db1.bdcruvacant_crupanel.client_cvappr_yesorno" => $recFormData['client_cvappr_yesorno'], "$db1.bdcruvacant_crupanel.client_cvappr_entryby" => $this->session->userdata('uid'));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $recFormData['rowaction_2']);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            $YesOrN = $recFormData['client_cvappr_yesorno'];
            $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $recFormData['rowaction_2'], "$db1.cruvacant_crupanel_step_history.action_name" => "Cv Approve By Client $YesOrN And Interaction Date", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => ($recFormData['interaction_date']) ? date("Y-m-d", strtotime($recFormData['interaction_date'])) : NULL);
            $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
            $this->session->set_flashdata('msg', 'Step 2 Cv Approve By Client Status Set Successfully');
        }
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $recFormData['bdprojid'] . "/" . $recFormData['designation_id']));
    }

    //Save Step 3 ... Final Step...
    public function rowaction_thirdfinal() {
        $recFormData = $this->input->post();
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($recFormData) {
            $UpDdata = array("$db1.bdcruvacant_crupanel.final_cvappr_date" => ($recFormData['final_cvappr_date']) ? date("Y-m-d", strtotime($recFormData['final_cvappr_date'])) : NULL, "$db1.bdcruvacant_crupanel.mobilization_date" => ($recFormData['mobilization_date']) ? date("Y-m-d", strtotime($recFormData['mobilization_date'])) : NULL, "$db1.bdcruvacant_crupanel.step_by_cru" => "3", "$db1.bdcruvacant_crupanel.final_cvappr_yesorno" => $recFormData['final_cvappr_yesorno']);
            $this->db->where("$db1.bdcruvacant_crupanel.id", $recFormData['rowaction_3']);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            $YesOrN = $recFormData['final_cvappr_yesorno'];
            $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $recFormData['rowaction_3'], "$db1.cruvacant_crupanel_step_history.action_name" => "Final Approval is : $YesOrN ", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => ($recFormData['final_cvappr_date']) ? date("Y-m-d", strtotime($recFormData['final_cvappr_date'])) : NULL);
            $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
            $this->session->set_flashdata('msg', 'Final Approval Step Set Successfully');
        }
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $recFormData['bdprojid'] . "/" . $recFormData['designation_id']));
    }

    //Get All CRU Dept UserIDs...
    public function GetAllCruDeptUserIdArr() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_employees_summary.user_id");
        $this->db->from("$db2.main_employees_summary");
        $this->db->where(array("$db2.main_employees_summary.isactive" => "1", "$db2.main_employees_summary.department_id" => "14"));
        $recDeptArr = $this->db->get()->result();
        $reTarr = array();
        if ($recDeptArr):
            foreach ($recDeptArr as $rec) {
                $reTarr[] = $rec->user_id;
            }
        endif;
        return $reTarr;
    }

    // Time Line Progress
    public function GetTimeLineProgress($projId, $DesignationId) {
        // $projId = "89246";
        // $DesignationId = "6026";
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.entrybyhr.userfullname as userfullnamehr,$db1.cv_tracker_databyhr.mobilization_date as mobilization_date_hr,$db1.cv_tracker_databyhr.entry_date as entry_datehr,$db1.cv_tracker_databyhr.joining_confirm,$db1.bdcruvacant_crupanel.project_id,$db1.bdcruvacant_crupanel.designation_id,$db1.bdcruvacant_crupanel.id,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.entry_date,$db2.main_employees_summary.userfullname as entryby,$db2.coord.userfullname as coord_userfullname,$db1.bdcruvacant_crupanel.approval_status_chng_by,$db1.bdcruvacant_crupanel.approval_status_chng_date,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.step_by_cru,$db1.bdcruvacant_crupanel.cv_sent_to_client,$db2.cvsentclientby.userfullname as cvsentclientby_userfullname,$db2.clntcvappryesornoentryby.userfullname as clntcvappryesornoentryby_userfullname,$db1.bdcruvacant_crupanel.client_cvappr_yesorno,$db1.bdcruvacant_crupanel.interaction_date,$db1.bdcruvacant_crupanel.final_cvappr_yesorno,$db1.bdcruvacant_crupanel.mobilization_date");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as coord", "$db1.bdcruvacant_crupanel.approval_status_chng_by=$db2.coord.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as cvsentclientby", "$db1.bdcruvacant_crupanel.cv_sent_to_client_by=$db2.cvsentclientby.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as clntcvappryesornoentryby", "$db1.bdcruvacant_crupanel.client_cvappr_entryby=$db2.clntcvappryesornoentryby.user_id", "LEFT");
        //Join For Hr Modules..
        $this->db->join("$db1.cv_tracker_databyhr", "$db1.cv_tracker_databyhr.bdcru_id=$db1.bdcruvacant_crupanel.id AND $db1.cv_tracker_databyhr.status=1", "LEFT");
        $this->db->join("$db2.main_employees_summary as entrybyhr", "$db1.cv_tracker_databyhr.entry_by=$db2.entrybyhr.user_id AND $db1.cv_tracker_databyhr.status=1", "LEFT");
        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.project_id" => $projId, "$db1.bdcruvacant_crupanel.designation_id" => $DesignationId));
        $this->db->where("$db1.bdcruvacant_crupanel.curr_status!=", "Rejected", NULL, FALSE);
        $this->db->where("$db1.bdcruvacant_crupanel.client_cvappr_yesorno!=", "no", NULL, FALSE);
        $this->db->where("$db1.bdcruvacant_crupanel.final_cvappr_yesorno!=", "no", NULL, FALSE);

        $recDeptArr = $this->db->get()->row();
        return ($recDeptArr) ? $recDeptArr : null;
    }

    //Edit Status By Harish..
    public function edit_vacantrec($entryRec) {
        $data['error'] = "";
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db2.main_users.userfullname,$db1.invc_vacant_resign_date.*,$db2.tm_projects.project_name,$db1.designation_master_requisition.designation_name");
        $this->db->FROM("$db1.invc_vacant_resign_date");
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.invc_vacant_resign_date.project_id", 'INNER');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.invc_vacant_resign_date.designation_id", 'LEFT');
        $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.invc_vacant_resign_date.emp_id", "LEFT");
        $this->db->WHERE(array("$db1.invc_vacant_resign_date.fld_id" => $entryRec, "$db1.invc_vacant_resign_date.status" => '1'));
        $data['ResignArr'] = $this->db->get()->row();
        $data['allvacantTypeArr'] = $this->GetAllVacantTypeMasterArr();
        $data['actionid'] = $entryRec;
        $this->load->view("cru_traker/resign_data_statuschange", $data);
    }

    public function all_inactive_list() {
        $data['error'] = '';
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->SELECT("$db2.main_users.userfullname,$db1.invc_vacant_resign_date.*,$db2.tm_projects.project_name,$db1.designation_master_requisition.designation_name");
        $this->db->FROM("$db1.invc_vacant_resign_date");
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.invc_vacant_resign_date.project_id", 'INNER');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.invc_vacant_resign_date.designation_id", 'LEFT');
        $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.invc_vacant_resign_date.emp_id", "LEFT");
        // $this->db->WHERE(array("$db1.invc_vacant_resign_date.fld_id" => $entryRec, "$db1.invc_vacant_resign_date.status" => '1'));
        $data['projListArr'] = $this->db->get()->result();
        $this->load->view("cru_traker/inactive_list_view", $data);
    }

    public function statusupdate_inactive() {
        $ina = $_REQUEST['ina'];
        if ($ina) {
            $updateArr = array("status" => "1");
            $this->db->where(array('fld_id' => $ina));
            $response = $this->db->update('invc_vacant_resign_date', $updateArr);
            $this->session->set_flashdata('msg', 'Record Updated Successfully..');
        }
        redirect(base_url("crutraker/all_inactive_list"));
    }

    //Save Status Chng..
    public function vacant_status_update() {
        if ($_REQUEST) {
            $updateArr = array("vacant_typeid" => $_REQUEST['vacant_typeid'],
                "last_date_resign" => $_REQUEST['last_date_resign'],
                "status" => ($_REQUEST['status']) ? $_REQUEST['status'] : "0");
            $this->db->where(array('fld_id' => $_REQUEST['vact_actid']));
            $response = $this->db->update('invc_vacant_resign_date', $updateArr);
            $this->session->set_flashdata('msg', 'Record Updated Successfully..');
        }
        redirect(base_url("crutraker/project_vacant"));
    }

    //Get Project Coordinator Name By Proj Id..
    public function getprojcoordinatorname($projID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db2.main_users.userfullname");
        $this->db->FROM("$db1.project_coordinator");
        $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.project_coordinator.emp_id", "LEFT");
        $this->db->WHERE(array("$db1.project_coordinator.bd_project_id" => $projID, "$db1.project_coordinator.status" => '1'));
        $coordArr = $this->db->get()->row();
        return ($coordArr) ? $coordArr->userfullname : '';
    }

    //Get All Project Coordinator UserIDs...
    public function GetAllProjectArr() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
        $this->db->from("$db2.tm_projects");
        $this->db->where(array("$db2.tm_projects.is_active" => "1"));
        $projListArr = $this->db->get()->result();
        return ($projListArr) ? $projListArr : '';
    }

    //Get All Vacnt Type Master Data...
    public function GetAllVacantTypeMasterArr() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.invc_vacant_typemaster.fld_id,$db1.invc_vacant_typemaster.type_name");
        $this->db->from("$db1.invc_vacant_typemaster");
        $this->db->where(array("$db1.invc_vacant_typemaster.status" => "1"));
        $VTypeListArr = $this->db->get()->result();
        return ($VTypeListArr) ? $VTypeListArr : '';
    }

    public function GetAllCoordEmailList($bdprojId = '') {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_users.id,$db2.main_users.userfullname,$db2.main_users.emailaddress");
        $this->db->from("$db1.project_coordinator");
        $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.project_coordinator.emp_id", "LEFT");
        $this->db->where(array("$db1.project_coordinator.status" => "1", "$db1.project_coordinator.bd_project_id" => $bdprojId));
        $this->db->group_by(array("$db1.project_coordinator.emp_id"));
        $CoordrecArr = $this->db->get()->result();
        return ($CoordrecArr) ? $CoordrecArr : array("id" => "271", "userfullname" => "Mr. Sanjay Bajpai", "emailaddress" => "bajpaisanjay@cegindia.com");
    }

    public function GetRecDetailByID($bdcruTbl_RowId) {
        //  $bdcruTbl_RowId = 5;
        // $DesignationId = "6026";
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.coord.emailaddress as coord_emailaddress,$db1.bdcruvacant_crupanel.id,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.resourceuser_inhouse.userfullname as resourceuser_inhouse_userfullname,$db2.resourceuser_inhouse.emailaddress as resourceuser_inhouse_emailaddress,$db2.resourceuser_inhouse.contactnumber as resourceuser_inhouse_contactnumber,$db2.tm_projects.project_name,$db1.designation_master_requisition.designation_name,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.entry_date,$db2.main_employees_summary.userfullname as entryby,$db2.coord.userfullname as coord_userfullname,$db1.bdcruvacant_crupanel.approval_status_chng_by,$db1.bdcruvacant_crupanel.approval_status_chng_date,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.step_by_cru,$db1.bdcruvacant_crupanel.cv_sent_to_client,$db2.cvsentclientby.userfullname as cvsentclientby_userfullname,$db2.clntcvappryesornoentryby.userfullname as clntcvappryesornoentryby_userfullname,$db1.bdcruvacant_crupanel.client_cvappr_yesorno,$db1.bdcruvacant_crupanel.interaction_date,$db1.bdcruvacant_crupanel.final_cvappr_yesorno,$db1.bdcruvacant_crupanel.mobilization_date,$db1.bdcruvacant_crupanel.inhouse_or_other,$db2.main_employees_summary.emailaddress as entrybyemail");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as coord", "$db1.bdcruvacant_crupanel.approval_status_chng_by=$db2.coord.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as cvsentclientby", "$db1.bdcruvacant_crupanel.cv_sent_to_client_by=$db2.cvsentclientby.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as clntcvappryesornoentryby", "$db1.bdcruvacant_crupanel.client_cvappr_entryby=$db2.clntcvappryesornoentryby.user_id", "LEFT");
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.bdcruvacant_crupanel.designation_id", 'LEFT');
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.bdcruvacant_crupanel.project_id", 'LEFT');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
        $this->db->join("$db2.main_employees_summary as resourceuser_inhouse", "$db1.bdcruvacant_crupanel.user_id=$db2.resourceuser_inhouse.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.id" => $bdcruTbl_RowId));
        $recDeptArr = $this->db->get()->row();
        return ($recDeptArr) ? $recDeptArr : null;
    }

    //Send Mail To Coordinator .. After Fill Employee Details..
    public function SendMailToCoordinator($vacntID, $BDprojid) {
        $data['data'] = $this->GetRecDetailByID($vacntID);
        $reCa = $this->GetAllCoordEmailList($BDprojid);
        if ($reCa) {
            foreach ($reCa as $cordRow) {
                $data['coordUserID'] = $cordRow->id;
                $data['coordUserName'] = $cordRow->userfullname;
                $toEmail_PC = $cordRow->emailaddress;
                $msgDetails = $this->load->view('email_template/cvtracker_mailtocoordinator', $data, true);
                $reCc = $this->sendcvtrackerMail($toEmail_PC, "Vacant Position Entry By CRU Team", $msgDetails);
            }
        }
        return ($reCc) ? $reCc : false;
    }

    //Reject Code By Ash...
//    public function reject_by_pc() {
//        $actID = $projid = $this->uri->segment(3);
//        $db1 = $this->db1->database;
//        $db2 = $this->db2->database;
//        if ($actID) {
//            $this->db->select("$db2.main_users.userfullname as cruentryby,$db1.bdcruvacant_crupanel.*,$db2.main_employees_summary.userfullname as userfullname_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.main_employees_summary.emailaddress as emailaddress_ihr,$db2.main_employees_summary.contactnumber as contactnumber_ihr");
//            $this->db->from("$db1.bdcruvacant_crupanel");
//            $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.user_id=$db2.main_employees_summary.user_id", "LEFT");
//            $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
//            $this->db->join("$db2.main_users", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_users.id", "LEFT");
//            $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.id" => $actID));
//            $RowRecdata = $this->db->get()->row();
//        }
//        if ($RowRecdata):
//            $UpDdata = array("$db1.bdcruvacant_crupanel.curr_status" => "Rejected", "$db1.bdcruvacant_crupanel.approval_status_chng_by" => $this->session->userdata('uid'), "$db1.bdcruvacant_crupanel.approval_status_chng_date" => date("Y-m-d"));
//            $this->db->where("$db1.bdcruvacant_crupanel.id", $actID);
//            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
//            if ($reps):
//                $CoordnUserID = $this->session->userdata('uid');
//                $this->sendcruteam_after_projectcoordaction($actID, $CoordnUserID);
//            endif;
//            $this->session->set_flashdata('msg', 'Employee CV. Record Rejected successfully');
//        endif;
//        redirect(base_url("crutraker/project_coordinator/" . $RowRecdata->project_id . "/" . $RowRecdata->designation_id));
//    }
    //Send Mail To CruTeam on the Action of Approve or Reject By P-coordinator.
    //Send Mail To CruTeam on the Action of Approve or Reject By P-coordinator.
    public function sendcruteam_after_projectcoordaction($vacntID, $coordUserID) {
        $data['data'] = $this->Crutrakermodel->GetRecDetailsForMail($vacntID);
        $data['coordUserID'] = $coordUserID;
        $msgDetails = $this->load->view('email_template/cvtracker_tocru_actbyprojcoord_email', $data, true);
        $resp = $this->sendcvtrackerMail($data['data']->entryby_emailaddress, "CV Status Changed", $msgDetails);
        return ($resp) ? $resp : false;
    }

    public function cancelvacantrow() {
        $projId = $this->uri->segment(3);
        $designationID = $this->uri->segment(4);
        $actionID = $this->uri->segment(5);
        if ($projId and $designationID and $actionID):
            //, 'entry_by' => $this->session->userdata('uid')
            $updateArr = array('status' => "0");
            $this->db->where(array('project_id' => $projId, 'id' => $actionID, 'designation_id' => $designationID));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
            //History Insert..
            $hInserArr = array("project_id" => $projId, "designation_id" => $designationID, "bdcruvacant_tbid" => $actionID, "action_by" => $this->session->userdata('uid'));
            $reTresp = $this->db->insert("$db1.bdcruvacant_cancel_history", $hInserArr);
            $this->session->set_flashdata('msg', 'Cancel / Delete Status set successfully');
        endif;
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $projId . "/" . $designationID));
    }

    //Cv Sent For Formatting By Bd To Cru...
    public function cvsendforformatting_bdtocru() {
        $emplID = $_REQUEST['notif_mail_emp'];
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        if ($emplID and $_REQUEST['priority'] and $_REQUEST['rowactid'] and $_REQUEST['bdprojid'] and $_REQUEST['designation_id']) {
            $MailToEmpBD = $this->Crutrakermodel->GetEmpDetailsID($emplID);
            $data['data'] = $this->GetRecDetailByID($_REQUEST['rowactid']);
            $data['data']->userfullname = $MailToEmpBD->userfullname;
            $data['data']->priority = $_REQUEST['priority'];
            //$toEmail_BD = "asheesh9308@gmail.com";
            $toEmail_BD = $MailToEmpBD->emailaddress;
            $ccMto = $data['data']->coord_emailaddress;

            $msgDetails = $this->load->view('email_template/cvtracker_cvformatting', $data, true);
            $respMail = $this->sendcvtrackerMail($toEmail_BD, "Candidate Cv For Formating", $msgDetails);
            // echo "<pre>"; print_r($_REQUEST); die;
            //Step Update..
            $updateArr = array('cv_formatting_bdteam' => "1", "cv_formatting_reqbyid" => $this->session->userdata('uid'));
            $this->db->where(array('id' => $_REQUEST['rowactid']));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
            //Insert History..
            $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $_REQUEST['rowactid'], "$db1.cruvacant_crupanel_step_history.action_name" => "Cv Send To BD Team For Formatting", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => date("Y-m-d"));
            $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
            $this->session->set_flashdata('msg', 'Cv Sent To BD Team for formatting.');
        }
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $_REQUEST['bdprojid'] . "/" . $_REQUEST['designation_id']));
    }

    ####################### CV Formatting Section ##################################################
    ######################### Code By Asheesh ######################################################

    public function bdteam_cvformatting() {
        $data["error"] = "";
        $data["title"] = "Cv Formatting By BD Team";
        $data['CvFormattingDataArr'] = $this->Crutrakermodel->GetCvFormattetingData();
        $this->load->view('cru_traker/cv_formatting_bdteam_view', $data);
    }

    //Save/Update After Cv formatting By BD Team..
    public function formattedcv_upd_uploadsave() {
        $updArr['cv_upload'] = '';
        if ($_FILES['aftercv_formatting_bd']['name'] and $_REQUEST['actrowid'] and $_REQUEST['submit']) {
            $folder = 'uploads/formatted_resume/';
            $configThm = $this->getUploadConfig($folder);
            $this->load->library('upload', $configThm);
            $recc = $this->upload->initialize($configThm);
            if ($this->upload->do_upload('aftercv_formatting_bd')) {
                $fileData = $this->upload->data();
                $updArr['cv_upload'] = $fileData['file_name'];
                $this->session->set_flashdata('msg', 'Formatted Cv Update successfully.');
            }
            if ($updArr['cv_upload']) {
                $updateArr = array('aftercv_formatting_bd' => $updArr['cv_upload'],
                    "cv_formatteby" => $this->session->userdata('uid'));
                $updateArr['cv_formating_lock'] = ($_REQUEST['cv_formating_lock'] == 1) ? "1" : null;
                $updateArr['cv_formatting_bdteam'] = ($_REQUEST['cv_formating_lock'] == 1) ? "2" : "1";
                $this->db->where(array('id' => $_REQUEST['actrowid']));
                $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
            }
            //Send Mail BD To Cru Request By & cc project coordinator...
            $singleRecDetails = $this->Crutrakermodel->GetCvFormattetingDataDetailsbyid($_REQUEST['actrowid']);
            $reqtobduser_emailaddress = $singleRecDetails->reqtobduser_emailaddress;
            $reqtobduser_userfullname = $singleRecDetails->reqtobduser_userfullname;

            if ($reqtobduser_emailaddress and $reqtobduser_userfullname) {
                $data['data'] = $singleRecDetails;
                $data['data']->userfullname = $reqtobduser_userfullname;
                $msgDetails = $this->load->view('email_template/cvtracker__after_cvformatting', $data, true);
                $respMail = $this->sendcvtrackerMail($reqtobduser_emailaddress, "Candidate Cv formatted", $msgDetails);
            }
        }
        redirect(base_url("crutraker/bdteam_cvformatting"));
    }

    //Lock Proccess...
    public function bdteam_cvformatted_lock() {
        $lockID = $_REQUEST["lockid"];
        $updateArr = array();
        if ($lockID) {
            $updateArr = array("cv_formatteby" => $this->session->userdata('uid'));
            $updateArr['cv_formating_lock'] = "1";
            $updateArr['cv_formatting_bdteam'] = "2";
            $this->db->where(array('id' => $lockID));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
            $this->session->set_flashdata('msg', 'Formatting Cv Locked.');
        }
        redirect(base_url("crutraker/bdteam_cvformatting"));
    }

    //CRU Cv Tracker CRU...
    public function cvtrackerchat_ajax() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $projid = $_REQUEST['proj_id'];
        $desiid = $_REQUEST['designation_id'];
        $loginEmpID = $this->session->userdata('uid');
        $this->db->select("$db2.main_users.id,$db2.main_users.userfullname,$db2.main_users.profileimg");
        $this->db->from("$db2.main_users");
        $this->db->where(array("$db2.main_users.isactive" => "1", "$db2.main_users.id" => $loginEmpID));
        $loginEmpRec = $this->db->get()->row();
        if ($loginEmpRec->profileimg) {
            $loginEmpRec->loginuserprofileimg_big = "http://hrms.cegindia.com/public/uploads/profile/" . $loginEmpRec->profileimg;
        }
        if ($loginEmpRec->profileimg == "") {
            $loginEmpRec->loginuserprofileimg_big = "http://hrms.cegindia.com/public/media/images/employee-deafult-pic.jpg";
        }
        echo json_encode($loginEmpRec);
    }

    //Chat put In Db ...
    public function dataputchat_ajax() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $chatrecdatatxt = $this->input->post('chatrecdatatxt');
        if ($chatrecdatatxt) {
            $insertArr = array("bd_projid" => $this->input->post('proj_id'), "designation_id" => $this->input->post('designation_id'), "emp_id" => $this->session->userdata('uid'), "msg_chat_data" => $chatrecdatatxt);
            $response = $this->db->insert('chat_cvtracker', $insertArr);
        }

        $this->db->select("$db1.chat_cvtracker.msg_chat_data,$db1.chat_cvtracker.entry_date,$db2.main_users.userfullname,$db2.main_users.profileimg");
        $this->db->from("$db1.chat_cvtracker");
        $this->db->join("$db2.main_users", "$db1.chat_cvtracker.emp_id=$db2.main_users.id", "LEFT");
        $this->db->where(array("$db1.chat_cvtracker.bd_projid" => $this->input->post('proj_id'), "$db1.chat_cvtracker.designation_id" => $this->input->post('designation_id'), "$db1.chat_cvtracker.status" => "1"));
        $recDs = $this->db->get()->result();
        $returnArr = array();
        if ($recDs) {
            foreach ($recDs as $rEcd) {
                if ($rEcd->profileimg) {
                    $rEcd->loginuserprofileimg_big = "http://hrms.cegindia.com/public/uploads/profile/" . $rEcd->profileimg;
                }
                if ($rEcd->profileimg == "") {
                    $rEcd->loginuserprofileimg_big = "http://hrms.cegindia.com/public/media/images/employee-deafult-pic.jpg";
                }
                $rEcd->entry_dateNm = date('d F Y', strtotime($rEcd->entry_date));
                $rEcd->entry_hourminute = date('H:i A', strtotime($rEcd->entry_date));

                $returnArr[] = $rEcd;
            }
        }
        echo json_encode($returnArr);
    }

    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    //[************************************** 17-09-2019 Again New Cv Traker Ash*******************************************]
    //######################################################################################################################
    //Master of Add New Record...
    public function addnew_otherempl() {
        $Formrec = $this->input->post();
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.team_req_otheremp.fld_id");
        $this->db->from("$db1.team_req_otheremp");
        $this->db->where("($db1.team_req_otheremp.emp_email='" . $Formrec['emp_email'] . "' OR $db1.team_req_otheremp.emp_contact='" . $Formrec['emp_contact'] . "')", NULL, FALSE);
        $numRowChk = $this->db->get()->num_rows();
        if ($numRowChk > 0) {
            $this->session->set_flashdata('error_msg', 'Error : This Employee Record already exists.');
        } else {
            $inserTerArr = array("$db1.team_req_otheremp.emp_name" => $Formrec['emp_name'], "$db1.team_req_otheremp.emp_email" => $Formrec['emp_email'], "$db1.team_req_otheremp.emp_contact" => $Formrec['emp_contact'], "$db1.team_req_otheremp.details" => $Formrec['details'], "$db1.team_req_otheremp.entry_by" => $this->session->userdata('uid'));
            $reTresp = $this->db->insert("$db1.team_req_otheremp", $inserTerArr);
            $this->session->set_flashdata('msg', 'Details submited successfully');
        }
        redirect(base_url('crutraker/edit_crutraker_board_project/' . $Formrec['bdprojid'] . '/' . $Formrec['designation_id']));
    }

    public function cru_vacant_home_ajax() {
        $list = $this->Projectlist_model->get_datatables();
        $loginId = $this->session->userdata('uid');
        $BddeptArr = $this->Crutrakermodel->GetEmpIDslistByDeptID("3");
        $CRUdeptArr = $this->Crutrakermodel->GetEmpIDslistByDeptID("14");

        $CurrStatus = Array("" => "Insearch", "1" => "CV Sent To Coordinator", "2" => "Approved by Coordinator", "3" => "Rejected by Coordinator", "4" => "CV Formatted Yes", "5" => "CV Sent To Client", "6" => "Interaction Date Added", "7" => "Final Approved & Mobilize", "8" => "Final Rejected");
        $CurrStatus_Color = Array("" => "red", "1" => "green", "2" => "green", "3" => "red", "4" => "#005ab3", "5" => "#00c4ff", "6" => "#6f5499", "7" => "#00aced", "8" => "red");

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $newproject) {
            $no++;
            $newImg = '';
            $StsTss = '';
            $row = array();
            $editCruLink = '';
            if ($newproject->if_proj_neworold == "1") {
                $newImg = "<img src='" . HOSTNAME . "assets/new.png" . "' />";
            } else {
                $newImg = "";
            }
            $row[] = $no;
            $row[] = $this->getprojcoordinatorname($newproject->project_id);
            $row[] = $newproject->project_name . $newImg;
            $row[] = $newproject->designation_name;
            $row[] = $newproject->type_name;
            $row[] = date("d-m-Y", strtotime($newproject->last_date_resign));
            $row[] = "<span style='color:" . $CurrStatus_Color[$newproject->curr_status] . "'>" . $CurrStatus[$newproject->curr_status] . "</span>";
            $projCoordArr = $this->GetAllCoordinatorUserIdArr();
            //Edit Action...
            if (in_array($loginId, $CRUdeptArr)) {
                $editCruLink = "<a href='" . base_url('crutraker/edit_crutraker_board_project/' . $newproject->project_id . '/' . $newproject->designation_id) . "'><i style='color:red;' title='CRU' class='glyphicon glyphicon-edit icon-red'></i></a>";
            }
            if (in_array($loginId, $projCoordArr)) {
                $ProjCoordinatorlinks2 = "<a href='" . base_url('crutraker/project_coordinator/' . $newproject->project_id . '/' . $newproject->designation_id) . "'><i style='color:green;' title='Cordinator' class='glyphicon glyphicon-user icon-green'></i></a>";
            }
            $row[] = $editCruLink . "&nbsp;" . $ProjCoordinatorlinks2;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Projectlist_model->count_all(),
            "recordsFiltered" => $this->Projectlist_model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    //Edit PermIds..
    public function GetDeptIDsArr($typer) {
        if ($typer == "bd") {
            $TeamIdsArr = $this->Crutrakermodel->GetTeamEmplistByDeptID("3");
        }
        if ($typer == "cru") {
            $TeamIdsArr = $this->Crutrakermodel->GetTeamEmplistByDeptID("14");
        }
        return ($TeamIdsArr) ? $TeamIdsArr : null;
    }

    public function datediffgap($date1) {
        $date2 = date("Y-m-d");
        $diff = abs(strtotime($date2) - strtotime($date1));
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $dumArr = array("0" => $years, "1" => $months, "2" => $days);
        return $dumArr;
    }

    // Home Page of CRU vacant Possition Data table..
    public function project_vacant() {
        $data['error'] = '';
        $data['title'] = 'CRU Vacant';
        $data['hrmsProjListArr'] = $this->GetAllProjectArr();
        $data['vacantTypeListArr'] = $this->GetAllVacantTypeMasterArr();
        $data['BDteamIds'] = $this->Crutrakermodel->GetEmpIDslistByDeptID("3");
        $data['CountRowsForBDteam'] = $this->Crutrakermodel->GetTblNumRowsByCond("bdcruvacant_crupanel", ["curr_status" => "2", "status" => "1"]);
        $this->load->view("cru_traker/cruvacant_homepage_view", $data);
    }

    //Get All Project Coordinator UserIDs...
    public function GetAllCoordinatorUserIdArr() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.project_coordinator.emp_id");
        $this->db->from("$db1.project_coordinator");
        $this->db->where(array("$db1.project_coordinator.status" => "1"));
        $this->db->group_by(array("$db1.project_coordinator.emp_id"));
        $projCoordrecArr = $this->db->get()->result();
        $reTarr = array("297", "296");
        if ($projCoordrecArr):
            foreach ($projCoordrecArr as $rec) {
                $reTarr[] = $rec->emp_id;
            }
        endif;
        return $reTarr;
    }

    //Code by Asheesh project coordinator
    public function project_coordinator() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        if ($projid == "" OR $desiid == "") {
            redirect(base_url('project_vacant'));
        }
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $data['error'] = '';
        $data['title'] = 'Edit Cv Tracker Board Project';

        $data['desig_detail'] = $this->Crutrakermodel->designation_name($desiid);
        $data['tendor_detail'] = $this->Crutrakermodel->Get_project_name_or_code_designation_record($projid);
        $data['user_detail'] = $this->Crutrakermodel->GetUserList();
        $data['otherEmprec'] = $this->Crutrakermodel->GetAllOtherEmployeeList();
        $data['desig_detail']['totvacant_age'] = $this->datediffgap($data['desig_detail']['last_date_resign']);
        $data['EntryByCruVacantEmpRec'] = $this->Crutrakermodel->GetRecCvonProject($projid, $desiid);
        $data['EntryByCruActiveSingleRec'] = $this->Crutrakermodel->ActiveRecSingleCv($projid, $desiid);
        $this->load->view('cru_traker/edit_coordinatortraker_board_project', $data);
    }

    //Cv Approved By PC..
    public function apprv_by_pc() {
        $actID = $this->uri->segment(3);
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $RowRecdata = $this->Crutrakermodel->GetRecDetailsForMail($actID);
        $CoordnUserID = $this->session->userdata('uid');
        if ($RowRecdata and $RowRecdata->curr_status == "1"):
            $UpDdata = array("$db1.bdcruvacant_crupanel.curr_status" => "2", "$db1.bdcruvacant_crupanel.pcid_apprv_reject" => $CoordnUserID, "$db1.bdcruvacant_crupanel.apprv_reject_datebypc" => date("Y-m-d H:i:s"));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $actID);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            if ($reps):
                $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $actID, "$db1.cruvacant_crupanel_step_history.action_name" => "Cv Approved By PC", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => date("Y-m-d"));
                $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
                $this->sendcruteam_after_projectcoordaction($actID, $CoordnUserID);
            endif;
            $this->session->set_flashdata('msg', 'Employee CV. Record Approved successfully');
        endif;
        redirect(base_url("crutraker/project_coordinator/" . $RowRecdata->project_id . "/" . $RowRecdata->designation_id));
    }

    //Cv Rejected By PC..
    public function reject_by_pc() {
        $actID = $this->uri->segment(3);
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $RowRecdata = $this->Crutrakermodel->GetRecDetailsForMail($actID);
        $CoordnUserID = $this->session->userdata('uid');
        if ($RowRecdata and $RowRecdata->curr_status == "1"):
            $UpDdata = array("$db1.bdcruvacant_crupanel.curr_status" => "3", "$db1.bdcruvacant_crupanel.pcid_apprv_reject" => $CoordnUserID, "$db1.bdcruvacant_crupanel.apprv_reject_datebypc" => date("Y-m-d H:i:s"));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $actID);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            if ($reps):
                $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $actID, "$db1.cruvacant_crupanel_step_history.action_name" => "Cv Rejected By PC", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => date("Y-m-d"));
                $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
                $this->sendcruteam_after_projectcoordaction($actID, $CoordnUserID);
            endif;
            $this->session->set_flashdata('msg', 'Employee CV. Record Rejected.');
        endif;
        redirect(base_url("crutraker/project_coordinator/" . $RowRecdata->project_id . "/" . $RowRecdata->designation_id));
    }

    //Formatted Cv Sent To Client ...
    public function formated_cvsento_clientsave() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $updateArr = array();
        if ($_REQUEST['bdcru_actid'] and $_REQUEST['formated_cv_senttoclient'] and $_REQUEST['cvsent_clientdate']) {
            //Resume Uploaded...
            if ($_FILES['aftercv_formatting_bd']['name']) {
                $folder = 'uploads/resume/';
                $configThm = $this->getUploadConfig($folder);
                $this->load->library('upload', $configThm);
                $recc = $this->upload->initialize($configThm);
                if ($this->upload->do_upload('aftercv_formatting_bd')) {
                    $fileData = $this->upload->data();
                }
            }
            $updateArr = ['curr_status' => "5", 'cv_sent_toclient_date' => date("Y-m-d", strtotime($_REQUEST['cvsent_clientdate'])), 'formatted_cv_resume' => ($fileData['file_name']) ? $fileData['file_name'] : '', "cv_sent_toclient_byuid" => $this->session->userdata('uid')];
            $this->db->where(array('id' => $_REQUEST['bdcru_actid']));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
            //Insert History..
            $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $_REQUEST['bdcru_actid'], "$db1.cruvacant_crupanel_step_history.action_name" => "Cv Sent To Client", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => date("Y-m-d"));
            $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
            $this->session->set_flashdata('msg', 'Cv Sent To Client By CRU Team.');
        }
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $_REQUEST['bdprojid'] . "/" . $_REQUEST['designation_id']));
    }

    //save/set Interaction Date .. 
    public function date_interaction_datesave() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database; 
        
        $updateArr = '';
        if ($_REQUEST['bdcru_actid'] and $_REQUEST['interaction_date']) {
            //Resume Uploaded...
            if ($_FILES['aftercv_formatting_bd']['name']) {
                $folder = 'uploads/resume/';
                $configThm = $this->getUploadConfig($folder);
                $this->load->library('upload', $configThm);
                $recc = $this->upload->initialize($configThm);
                $this->upload->do_upload('aftercv_formatting_bd');

                if ($this->upload->do_upload('aftercv_formatting_bd')) {
                    $fileData = $this->upload->data();
                    $updateArr = array('formatted_cv_resume' => $fileData['file_name'], 'curr_status' => "6", 'interaction_date' => date("Y-m-d", strtotime($_REQUEST['interaction_date'])), "mobil_interdate_setby" => $this->session->userdata('uid'));
                }
            }
            if ($updateArr == "") {
                $updateArr = array('curr_status' => "6", 'interaction_date' => date("Y-m-d", strtotime($_REQUEST['interaction_date'])), "mobil_interdate_setby" => $this->session->userdata('uid'));
            }
            $this->db->where("$db1.bdcruvacant_crupanel.id", $_REQUEST['bdcru_actid']);
            $response = $this->db->update("$db1.bdcruvacant_crupanel", $updateArr);
            //Insert History..
            $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $_REQUEST['bdcru_actid'], "$db1.cruvacant_crupanel_step_history.action_name" => "Interaction & Mobilization Date set", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => date("Y-m-d"));
            $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
            $this->session->set_flashdata('msg', 'Interaction Date set successfully');
        }
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $_REQUEST['bdprojid'] . "/" . $_REQUEST['designation_id']));
    }

    //Cancel ...
    public function cancelrowbypc() {
        $actionID = $this->uri->segment(3);
        if ($actionID):
            $updateArr = array('status' => "0");
            $this->db->where(['id' => $actionID]);
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
            //History Insert..
            $hInserArr = array("crupanel_tblid" => $actionID, "action_name" => "Employee / Cv Cancel", "date_on_actv" => date("d-m-Y"), "action_by" => $this->session->userdata('uid'));
            $reTresp = $this->db->insert("$db1.cruvacant_crupanel_step_history", $hInserArr);
            $this->session->set_flashdata('msg', 'Cancel / Delete Status Set successfully');
        endif;
        redirect(base_url("crutraker/project_vacant"));
    }

    public function defaultchatdata_ajax() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.chat_cvtracker.msg_chat_data,$db1.chat_cvtracker.entry_date,$db2.main_users.userfullname,$db2.main_users.profileimg");
        $this->db->from("$db1.chat_cvtracker");
        $this->db->join("$db2.main_users", "$db1.chat_cvtracker.emp_id=$db2.main_users.id", "LEFT");
        $this->db->where(array("$db1.chat_cvtracker.bd_projid" => $this->input->post('proj_id'), "$db1.chat_cvtracker.designation_id" => $this->input->post('designation_id'), "$db1.chat_cvtracker.status" => "1"));
        $recDs = $this->db->get()->result();
        $returnArr = array();
        if ($recDs) {
            foreach ($recDs as $rEcd) {
                if ($rEcd->profileimg) {
                    $rEcd->loginuserprofileimg_big = "http://hrms.cegindia.com/public/uploads/profile/" . $rEcd->profileimg;
                }
                if ($rEcd->profileimg == "") {
                    $rEcd->loginuserprofileimg_big = "http://hrms.cegindia.com/public/media/images/employee-deafult-pic.jpg";
                }
                $rEcd->entry_dateNm = date('d F Y', strtotime($rEcd->entry_date));
                $rEcd->entry_hourminute = date('H:i A', strtotime($rEcd->entry_date));
                $returnArr[] = $rEcd;
            }
        }
        echo json_encode($returnArr);
    }

    //Code by Asheesh edit CRU project
    public function edit_crutraker_board_project() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        if ($projid == "" OR $desiid == "") {
            redirect(base_url('project_vacant'));
        }
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $data['error'] = '';
        $data['title'] = 'Edit CV Tracker Board Project';

        $data['desig_detail'] = $this->Crutrakermodel->designation_name($desiid);
        $data['tendor_detail'] = $this->Crutrakermodel->Get_project_name_or_code_designation_record($projid);
        $data['user_detail'] = $this->Crutrakermodel->GetUserList();
        $data['otherEmprec'] = $this->Crutrakermodel->GetAllOtherEmployeeList();
        $data['desig_detail']['totvacant_age'] = $this->datediffgap($data['desig_detail']['last_date_resign']);
        $data['EntryByCruVacantEmpRec'] = $this->Crutrakermodel->GetRecCvonProject($projid, $desiid);
        $data['EntryByCruActiveSingleRec'] = $this->Crutrakermodel->ActiveRecSingleCv($projid, $desiid);

        $this->load->view('cru_traker/edit_crutraker_board_project', $data);
    }

    //CRU Pannel Save..
    public function savecru_paneldata() {
        $Arr = $_REQUEST;
        if (($Arr['bdprojid'] and $Arr['designation_id'] and $Arr['empltype']) and ( $Arr['emp_userid_hrms'] or $Arr['emp_userid_other'])) {
            if ($Arr['empltype'] == "inhouse"):
                $inserArr = array("project_id" => $Arr['bdprojid'], "designation_id" => $Arr['designation_id'],
                    "user_id" => $Arr['emp_userid_hrms'],
                    "inhouse_or_other" => $Arr['empltype'],
                    'entry_by' => $this->session->userdata('uid'));
            endif;
            if ($Arr['empltype'] == "other"):
                $inserArr = array("project_id" => $Arr['bdprojid'],
                    "designation_id" => $Arr['designation_id'],
                    "user_id" => $Arr['emp_userid_other'],
                    "inhouse_or_other" => $Arr['empltype'],
                    'entry_by' => $this->session->userdata('uid'));
            endif;
            //Resume Uploaded...
            if (($_FILES['emp_cv_file']['name']) and ( $Arr['bdprojid'] and $Arr['designation_id'] and $Arr['empltype'])) {
                $folder = 'uploads/resume/';
                $configThm = $this->getUploadConfig($folder);
                $this->load->library('upload', $configThm);
                $recc = $this->upload->initialize($configThm);
                if ($this->upload->do_upload('emp_cv_file')) {
                    $fileData = $this->upload->data();
                    $inserArr['cv_upload'] = $fileData['file_name'];
                }
            }
            $this->db->insert('bdcruvacant_crupanel', $inserArr);
            $LastinsertID = $this->db->insert_id();
        }

        if ($LastinsertID):
            //Mail Process.. Code By Asheesh..
            $data['RecDataForMail'] = $this->Crutrakermodel->GetRecDetailsForMail($LastinsertID);
            $toEmail_PC = $data['RecDataForMail']->projcoorduser_emailaddress;
            $toEmail_CRU = $data['RecDataForMail']->entryby_emailaddress;
            $msgDetails = $this->load->view('email_template/cvtracker_mailtocoordinator', $data, true);
            $msgDetails_CRU = $this->load->view('email_template/cvtracker_mailtocruteam', $data, true);
            $reCc = $this->sendcvtrackerMail($toEmail_PC, "Vacant Position Entry By CRU Team", $msgDetails);
            $reCc = $this->sendcvtrackerMail($toEmail_CRU, "CV Sent To Project Coordinator", $msgDetails_CRU);
            //Close Mail Process..
            $this->session->set_flashdata('msg', 'Details submited successfully');
        else:
            $this->session->set_flashdata('error_msg', 'Error : Something Went Wrong');
        endif;
        redirect(base_url('crutraker/edit_crutraker_board_project/' . $Arr['bdprojid'] . '/' . $Arr['designation_id']));
    }

    //Cv Formated YHes By Bd Team ... 
    public function cvformatting_yes_bybdteam($actID) {
        $RecRow = $this->Crutrakermodel->GetTblRecByIDSingleRow("bdcruvacant_crupanel", ["status" => "1", "curr_status" => "2", "id" => $actID]);
        if ($RecRow->curr_status == 2) {
            $UpDdata = array("$db1.bdcruvacant_crupanel.curr_status" => "4", "$db1.bdcruvacant_crupanel.bdempid_cvform_yes" => $this->session->userdata('uid'), "$db1.bdcruvacant_crupanel.cvformt_statuschng_date" => date("Y-m-d H:i:s"));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $actID);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            if ($reps):
                $BdTeamUserID = $this->session->userdata('uid');
                $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $actID, "$db1.cruvacant_crupanel_step_history.action_name" => "Cv By BD Team Yes", "$db1.cruvacant_crupanel_step_history.action_by" => $BdTeamUserID, "$db1.cruvacant_crupanel_step_history.date_on_actv" => date("Y-m-d"));
                $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
                //Send Mail/...
                $data['RecDataForMail'] = $this->Crutrakermodel->GetRecDetailsForMail($actID);
                $toEmail_CRUentryBty = $data['RecDataForMail']->entryby_emailaddress;
                $msgDetails_CRU = $this->load->view('email_template/cvtracker__after_cvformatting', $data, true);
                $reCc = $this->sendcvtrackerMail($toEmail_CRUentryBty, "CV Formated By BD Team", $msgDetails_CRU);
            endif;
            $this->session->set_flashdata('msg', 'CV Formated Status Set successfully');
        }
        redirect(base_url("crutraker/project_vacant"));
    }

    //Final Approval & Mobilization..
    public function date_mobilization_datesave() {
        $updateArr = array();
        if ($_REQUEST['bdcru_actid'] and $_REQUEST['final_approval']) {
            //Resume Uploaded...
            if ($_REQUEST['final_approval'] == "1") {
                $updateArr = ['curr_status' => "7", 'mobilization_date' => ($_REQUEST['mobilization_date']) ? date("Y-m-d", strtotime($_REQUEST['mobilization_date'])) : '', "final_apprv_set" => $this->session->userdata('uid')];
                $this->db->where(array('id' => $_REQUEST['bdcru_actid']));
                $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
                //Insert History..
                $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $_REQUEST['bdcru_actid'], "$db1.cruvacant_crupanel_step_history.action_name" => "Interaction & Mobilization Date set", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => date("Y-m-d"));
                $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
                $this->session->set_flashdata('msg', 'Mobilization Date & Final Approval Yes Set Successfully');
                $this->finalapprovalyesmail($_REQUEST['bdcru_actid']);
            }
            if ($_REQUEST['final_approval'] == "2") {
                $updateArr = ['curr_status' => "8", "mobil_interdate_setby" => $this->session->userdata('uid'), "final_apprv_set" => $this->session->userdata('uid')];
                $this->db->where(array('id' => $_REQUEST['bdcru_actid']));
                $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
                //Insert History..
                $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $_REQUEST['bdcru_actid'], "$db1.cruvacant_crupanel_step_history.action_name" => "Final Approval No", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => date("Y-m-d"));
                $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
                $this->session->set_flashdata('msg', 'Final Approval No Set Successfully');
            }
        }
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $_REQUEST['bdprojid'] . "/" . $_REQUEST['designation_id']));
    }

    //if Final Approval Yes. Mail....
    public function finalapprovalyesmail($actID) {
        $data['RecDataForMail'] = $this->Crutrakermodel->GetRecDetailsForMail($actID);
        if ($data['RecDataForMail']->curr_status == '7' and $data['RecDataForMail']->status == '1') {
            $msgDetails_CRU = $this->load->view('email_template/cvtracker_mailtofinalapprv', $data, true);
            $toEmailArr = array("gmhr@cegindia.com", "hr@cegindia.com", "bdcru@cegindia.com", $data['RecDataForMail']->entryby_emailaddress, $data['RecDataForMail']->projcoorduser_emailaddress);
            foreach ($toEmailArr as $rOwrec) {
                $this->sendcvtrackerMail($rOwrec, "CV Formated By BD Team", $msgDetails_CRU);
            }
        }
        return true;
    }

    //Send Mail Master Function By Asheesh..
    function sendcvtrackerMail($to, $subject, $msgDetails) {
        // $to = "ts.admin@cegindia.com";
        $this->load->library('email');
        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.cegindia.com',
            'smtp_user' => 'marketing@cegindia.com',
            'smtp_pass' => 'MARK-2015ceg',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        ));
        $this->email->from('tender@cegindia.com', 'Do_not_reply');
        $this->email->to($to);
        // $cc = array('marketing@cegindia.com', 'jobs@cegindia.com', 'gmcru@cegindia.com');
        //  $CI->email->cc($cc);
        $this->email->bcc('marketing@cegindia.com');
        $this->email->subject($subject);
        $this->email->message($msgDetails);
        $resp = $this->email->send();
        return ($resp) ? $resp : $this->email->print_debugger();
    }

}
