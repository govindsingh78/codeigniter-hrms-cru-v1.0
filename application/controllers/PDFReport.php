<?php

/* Database connection start This Controller Create By Asheesh */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class PDFReport extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Front_model');
        $this->load->model('mastermodel');
        $this->load->model('company_model');
        $this->load->model('website_model');
        $this->load->model('pdfreport_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('session', 'form_validation');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
    }

    //Ceg Exp..
    public function pdfdo() {
        $data['error'] = '';
        $data['title'] = 'PDF List';
        $data['sector'] = $this->Front_model->GetActiveSector();
        $this->load->view('pdfreport/exp_list_view', $data);
    }

    //CEG Exp List data Table..
    public function expListAll() {
        $list = $this->pdfreport_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $comprow) {
            $count = $this->mastermodel->count_rows('ceg_pds_detail', array('ceg_exp_id' => $comprow->fld_id));
            if ($count >= 1) {
                if (!empty($comprow->pds_file_name)) {
                    $chkbox = '<input  name="actchk[]" value="' . $comprow->fld_id . '" type="checkbox">&nbsp&nbsp<input name="' . $comprow->fld_id . '" value="" type="textbox" style="width:100%;">&nbsp&nbsp';
                    $pds = '<span style="cursor:pointer" title="Pds" data-toggle="modal" data-target="#mypsdModel" onclick="pdssector(' . "'" . $comprow->fld_id . "','" . 0 . "'" . ')">PDS</span>';
                } else {
                    $pds = '';
                    $chkbox = '';
                }
            } else {
                $chkbox = '';
                $pds = '';
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->project_name;
            $row[] = $comprow->service;
            $row[] = $comprow->client;
            $row[] = $comprow->start_year;
            $row[] = $comprow->sector;
            $row[] = $comprow->state;
            $row[] = $comprow->funding;
            $row[] = $pds . $chkbox;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->pdfreport_model->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->pdfreport_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function selectedData() {
        $actno = $_POST['actchk'];
        $data = array();
        foreach ($actno as $val) {
            $checkData[$val] = $_REQUEST[$val];
        }
        asort($checkData);
        foreach ($checkData as $key => $value) {
            $projID[] = $key;
        }

        if (!empty($projID)) {
            foreach ($projID as $ids) {
                $this->db->select('*');
                $this->db->from('ceg_exp');
                $this->db->where(array('fld_id' => $ids, 'status' => '1', 'proj_status' => 'won'));
                $data[] = $this->db->get()->result_array();
            }
        }
        $this->load->view('pdfreport/pdf_view', compact('data'));
    }

    public function certificate_download() {
        if (!empty($_REQUEST)) {
            //load download helper
            $this->load->helper('download');
            $id = $_REQUEST ['id'];
            $no = $_REQUEST['no'];
            //get file info from database
            $fileInfo = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $id));
            $ext = explode('.', $fileInfo->certificate_file_name);
            //file path
            $file = './uploads/certificate/' . $fileInfo->certificate_file_name;
            $newfile = 'certificate_0' . $no . '.' . $ext[1];
            //download file from directory
            force_download($newfile, file_get_contents($file));
        }
    }

    public function pds_download() {
        if (!empty($_REQUEST)) {
            //load download helper
            $this->load->helper('download');
            $id = $_REQUEST ['id'];
            $no = $_REQUEST['no'];
            //get file info from database
            $fileInfo = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $id));
            $ext = explode('.', $fileInfo->pds_file_name);

            //file path
            $file = './uploads/pds/' . $fileInfo->pds_file_name;
            $newfile = 'pds_0' . $no . '.' . $ext[1];
            //download file from directory
            //force_download($file, NULL);
            force_download($newfile, file_get_contents($file));
        }
    }

    function savepds() {
        if (!empty($_REQUEST['ceg_pds_id']) && !empty($_REQUEST['service_description'])) {
            $ID = explode(',', $_REQUEST['ceg_pds_id']);
            $expID = array_unique($ID);
            foreach ($expID as $val) {
                $arr = array('service_description' => $_REQUEST['service_description']);
                $respon = $this->mastermodel->UpdateRecords('ceg_pds_detail', array('ceg_exp_id' => $val), $arr);
            }
        }

        if ($respon):

            $this->load->library('m_pdf');
            if (!empty($expID)) {
                foreach ($expID as $val) {
                    $data['cegpds'] = $this->mastermodel->GetTableData('ceg_pds_detail', array('ceg_exp_id' => $val));
                    $data['cegexpRec'] = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $val));
                    $data['cegexpSummery'] = $this->mastermodel->SelectRecordSingle('cegexp_proj_summery', array('cegexp_id' => $val, 'status' => '1'));
                    $data['consortiumArr'] = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('cegexp_id' => $val, 'status' => '1'));
                    $projectID = $data['cegexpRec']->project_id;
                    $data['tqRecArr'] = $this->mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $projectID));

                    $html = $this->load->view('pdfreport/pdf_output', $data, true); //load the pdf_output.php by passing our data and get all data in $html varriable.
                    $pdfFilePath = "pds_" . $projectID . ".pdf";
                    $arr = array('pds_file_name' => $pdfFilePath);
                    $where = array('fld_id' => $val);
                    $respon = $this->mastermodel->UpdateRecords('ceg_exp', $where, $arr);
                    $pdf = $this->m_pdf->load();
                    $pdf->WriteHTML($html);
                    $pdf->Output("./uploads/pds/" . $pdfFilePath, "F");
                }
            }
            $this->session->set_flashdata('msg', 'Record Update successfully');
            redirect(base_url('pdfsort/sorting/'));
        else:
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('pdfsort/sorting/'));
        endif;

        /* if(!empty($_REQUEST['ceg_pds_id']) && !empty($_REQUEST['service_description'])){
          $arr = array('service_description' => $_REQUEST['service_description']);
          $respon = $this->mastermodel->UpdateRecords('ceg_pds_detail', array('ceg_exp_id' => $_REQUEST['ceg_pds_id']),$arr);
          }
          if ($respon):
          $cegexpID = $_REQUEST['ceg_pds_id'];
          //load mPDF library
          $this->load->library('m_pdf');

          if(!empty($cegexpID)){
          $data['cegpds'] = $this->mastermodel->GetTableData('ceg_pds_detail', array('ceg_exp_id' => $cegexpID));
          $data['cegexpRec'] = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $cegexpID));
          $data['cegexpSummery'] = $this->mastermodel->SelectRecordSingle('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
          $data['consortiumArr'] = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('cegexp_id' => $ProjId, 'status' => '1'));
          $projectID = $data['cegexpRec']->project_id;
          $data['tqRecArr'] = $this->mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $projectID));

          $html=$this->load->view('pdfreport/pdf_output',$data, true); //load the pdf_output.php by passing our data and get all data in $html varriable.
          //this the the PDF filename that user will get to download
          $pdfFilePath ="pds_".$projectID.".pdf";

          $arr = array('pds_file_name' => $pdfFilePath);
          $where = array('fld_id' => $cegexpID);
          $respon = $this->mastermodel->UpdateRecords('ceg_exp',$where,$arr);

          //actually, you can pass mPDF parameter on this load() function
          $pdf = $this->m_pdf->load();
          //$pdf->WriteHTML($stylesheet, 1);
          //generate the PDF!
          $pdf->WriteHTML($html);
          //offer it to user via browser download! (The PDF won't be saved on your server HDD)
          $pdf->Output("./uploads/pds/".$pdfFilePath, "F");
          ///$pdf->Output($pdfFilePath, "D");
          }


          $this->session->set_flashdata('msg', 'Competitors Record Added successfully');
          redirect(base_url('pdfreport/pdfdo/'));
          else:
          $this->session->set_flashdata('error_msg', 'Something went gone wrong');
          redirect(base_url('pdfreport/pdfdo/'));
          endif; */
    }

    public function getPDSDescription_ajax() {
        //print_r($_REQUEST); die;
        if (!empty($_REQUEST)) {
            $wheres = array('ceg_exp_id' => $_REQUEST['id']);
            $pdsrec = $this->mastermodel->SelectRecord('ceg_pds_detail', $wheres);
            if (!empty($pdsrec)) {
                echo json_encode(array('service_description' => $pdsrec[0]->service_description));
            } else {
                return false;
            }
            die;
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */