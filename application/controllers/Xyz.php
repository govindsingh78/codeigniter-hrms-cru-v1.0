<?php
/* Database connection start This Controller Create By Asheesh */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Xyz extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
        $this->load->model('mastermodel');
        $this->load->model('company_model');
        $this->load->model('website_model');
        $this->load->model('cegexp_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('session', 'form_validation');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url('welcome'));
        }
    }

    //Dashbord After Login..
    public function add() {
		$data['title'] = 'Add Company';
        $this->form_validation->set_rules('company_name', 'Company Name', 'required|min_length[3]|max_length[100]|is_unique[main_company.company_name]');
        // $this->form_validation->set_rules('projectcode', 'Project Code', 'required|is_unique[cegth_project.projectcode]');
        if ($this->form_validation->run() == FALSE) {
            $data['sectArr_rec'] = $this->mastermodel->SelectRecord('designation_sector', array('status' => '1'), 'sector_name', 'ASC');
            $data['comp_field_Arr'] = $this->mastermodel->SelectRecordFld('comp_field_master', array('status' => '1'), 'field_name', 'ASC');
            //All Country..
            $data['country_Arr'] = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
            $this->load->view('company/addcompany_view', $data);
        } else {
            $_REQUEST['entryby'] = $this->session->userdata('uid');
            $_REQUEST['operating_countries'] = implode(",", $_REQUEST['operating_countries']);
            $_REQUEST['com_sector'] = implode(",", $_REQUEST['com_sector']);
            $_REQUEST['comp_field_id'] = implode(",", $_REQUEST['comp_field_id']);
            $respon = $this->mastermodel->InsertMasterData($_REQUEST, 'main_company');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Company Added successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //Add / Save.. Sector..
    public function addsector() {
        $this->form_validation->set_rules('sector_name', 'Sector Name', 'required|min_length[3]|max_length[100]|is_unique[designation_sector.sector_name]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/add'));
        } else {
            $respon = $this->mastermodel->InsertMasterData($_REQUEST, 'designation_sector');
            if ($respon):
                $this->session->set_flashdata('error_msg', null);
                $this->session->set_flashdata('success_msg', 'Sector Added successfully');
                redirect(base_url('company/add'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/add'));
            endif;
        }
    }

    //Add Field Name or Nature of Company..
    public function addfield_name() {
        $this->form_validation->set_rules('field_name', 'Field Name', 'required|min_length[3]|max_length[100]|is_unique[comp_field_master.field_name]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/add'));
        } else {
            $respon = $this->mastermodel->InsertMasterData($_REQUEST, 'comp_field_master');
            if ($respon):
                $this->session->set_flashdata('error_msg', null);
                $this->session->set_flashdata('success_msg', 'Field Added successfully');
                redirect(base_url('company/add'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/add'));
            endif;
        }
    }

    //Save JV Record Code By Asheesh
    public function savejvdata() {
        if ($_REQUEST['cegexp_id']) {
            $insertedArr = array(
                'lead_companes' => implode(",", $_REQUEST['leadcompany']),
                'joint_ventures' => implode(",", $_REQUEST['joint_venture']),
                'associate_company' => implode(",", $_REQUEST['assoc']),
                'cegexp_id' => $_REQUEST['cegexp_id'],
                'entry_by' => $this->session->userdata('empid'));
            $respon = $this->mastermodel->InsertMasterData($insertedArr, 'jv_companyrecords');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Consortium Added successfully');
                redirect(base_url('company/cegexpedit/' . $_REQUEST['cegexp_id']));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/cegexpedit/' . $_REQUEST['cegexp_id']));
            endif;
        } else {
            $insertedArr = array(
                'lead_companes' => implode(",", $_REQUEST['leadcompany']),
                'joint_ventures' => implode(",", $_REQUEST['joint_venture']),
                'associate_company' => implode(",", $_REQUEST['assoc']),
                'project_id' => $_REQUEST['projid'],
                'entry_by' => $this->session->userdata('empid'));
            $respon = $this->mastermodel->InsertMasterData($insertedArr, 'jv_companyrecords');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Consortium Added successfully');
                redirect(base_url('togoproject'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('togoproject'));
            endif;
        }
    }

    //Ajax For Show If Exist..
    public function showjvrecord() {
        $projId = $_REQUEST['jvprojid'];
        $Rec = $this->Front_model->selectRecord('jv_companyrecords', array('*'), array('status' => '1', 'project_id' => $projId));
        if ($Rec) {
            $recData = $Rec->result();
            $LC = '';
            $jv = '';
            $as = '';
            $lead_companes = explode(",", $recData[0]->lead_companes);
            $joint_ventures = explode(",", $recData[0]->joint_ventures);
            $associate_company = explode(",", $recData[0]->associate_company);
            $entry_by = $recData[0]->entry_by;
            foreach ($joint_ventures as $r11) {
                $jv .= $this->mastermodel->GetCompNameById($r11) . " , ";
            }
            foreach ($lead_companes as $r12) {
                $LC .= $this->mastermodel->GetCompNameById($r12) . " , ";
            }
            foreach ($associate_company as $r13) {
                $as .= $this->mastermodel->GetCompNameById($r13) . " , ";
            }
            ?>
            <table class="table table-striped">
                <thead><tr><th>Lead Company</th></tr></thead>  
                <tbody><tr><td><?= $LC ?></td></tr></tbody>
                <thead><tr><th> JV ( joint venture )</th></tr></thead>
                <tbody><tr><td><?= $jv ?></td></tr></tbody>
                <thead><tr><th>Associate company.</th></tr></thead>
                <tbody><tr><td><?= $as ?></td></tr></tbody>
            </table>
            <?php
        } else {
            echo null;
        }
    }

    //Compitior View By Project Id Ajax
    public function compititorview_ajax() {
        if ($_REQUEST['projid']):
            $Rec = $this->Front_model->selectRecord('competitors_company', array('compt_com_id'), array('status' => '1', 'project_id' => $_REQUEST['projid']));
            if ($Rec) {
                $recData = $Rec->result();
                foreach ($recData as $rroww) {
                    echo "<span class='btn btn-link' >" . ucwords(strtolower($this->mastermodel->GetCompNameById($rroww->compt_com_id))) . "</span>, ";
                }
            }
        endif;
        exit();
    }

    //Ceg Exp..
    public function cegexp() {
        $data['error'] = '';
        $data['title'] = ' CEG Experience List';
        $this->load->view('company/exp_list_view', $data);
    }

    //Save Compititors..
    function savecomptitors() {
		//echo '<pre>'; print_r($_REQUEST); die;
        if (($_REQUEST['comptitors']) and ( $_REQUEST['projid'])):
            foreach ($_REQUEST['comptitors'] as $comptrow) {
				$count = $this->mastermodel->count_rows('competitors_company', array('project_id' => $_REQUEST['projid'], 'compt_com_id' => $comptrow));
                if ($count < 1):
					$IncrtArr = array(
						'project_id' => $_REQUEST['projid'],
						'compt_com_id' => $comptrow,
						'entry_by' => $this->session->userdata('uid'));
					$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'competitors_company');
				endif;
            }
            if ($respon):
                $this->session->set_flashdata('msg', 'Competitors Record Added successfully');
                redirect(base_url('completeproject'));
				
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('completeproject'));
            endif;
        endif;
    }

    // Project Display
    public function companyListAll() {
        $list = $this->company_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $comprow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->company_name;
            $row[] = $comprow->email_id;
            $row[] = $comprow->contact_no;
            $row[] = "<a target='_blank' href='" . $comprow->company_website . "'>" . $comprow->company_website . "</a>";
            $row[] = $this->mastermodel->GetSectNameById($comprow->com_sector);
            $row[] = $this->mastermodel->GetnatureofCompanyById($comprow->comp_field_id);
            $row[] = "<a href='" . base_url("company/addcompany_extra?compnm=" . $comprow->fld_id) . "'>Edit</a>" . "&nbsp&nbsp&nbsp&nbsp<a href='" . base_url("company/contdelete?delid=" . $comprow->fld_id) . "'>Delete</a>";
            // addcompany_extra
            // $row[] = $comprow->comp_field_id;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->company_model->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->company_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    // Project Display
    public function websiteListAll() {
        $list = $this->website_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';

        foreach ($list as $comprow) {
            $actualink = $comprow->website_url;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = "<a title='" . $actualink . "' target='_blank' href='" . $actualink . "'>Website</a>";
            $row[] = $comprow->department;
            $row[] = "<a title='" . $comprow->eproc . "' target='_blank' href='" . $comprow->eproc . "'>Eproc</a>";
            $row[] = $comprow->user_name;
            $row[] = '<i onclick="showpass(' . "'" . $comprow->password_encode . "','" . $comprow->fld_id . "'" . ')" title="' . $comprow->password_encode . '" class="glyphicon glyphicon-eye-open"></i>' . '<span id="dv' . $comprow->fld_id . '"></span>';
            $row[] = $comprow->country;
            $row[] = '<li title="Edit" class="glyphicon glyphicon-edit"></li> &nbsp <li title="Delete"  class="glyphicon glyphicon-trash"></li>';

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->website_model->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->website_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //CEG Exp List data Table..
    public function expListAll() {
        $list = $this->cegexp_model->get_datatables();
		//echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $comprow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->TenderDetails;
            $row[] = $comprow->service_name;
            $row[] = $comprow->client;
            $row[] = $comprow->start_year;
            $row[] = $comprow->sectName;
            $row[] = $comprow->state_name;
            $row[] = $comprow->funding;
            $row[] = '<a href="cegexpedit/' . $comprow->project_id . '"><i title="EDIT" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp&nbsp';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->cegexp_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //Edit CEG Exp.. Code By Asheesh.. 
    public function cegexpedit() {
        $projstatus = '';
        $compnameArr = array();
		$cegexpID = $this->uri->segment(3);
		if ($cegexpID) {
			
			// Find Data
			$data['bddetail'] = $this->mastermodel->SelectRecordSingle('bd_tenderdetail', array('fld_id' => $cegexpID));
			$data['project_status'] = $this->mastermodel->SelectRecordSingle('bdproject_status', array('project_id' => $cegexpID));
			
			//bdcegexp data
			$this->db->select('a.*,c.service_name,d.generated_tenderid');
			$this->db->from('bdceg_exp as a');
			$this->db->join('main_services as c','a.service_id = c.id','left');
			$this->db->join('tender_generated_id as d','a.project_id = d.project_id','left');
			$this->db->where('a.project_id',$cegexpID);
			
			$resultdata  = $this->db->get()->result(); 
			$data['expres']  = $resultdata[0];
			
			$country_id = $data['expres']->countries_id;
			$data['stateArr'] = $this->mastermodel->SelectRecordS('states', array('country_id' => $country_id, 'status' => '1'));
			
			$data['project'] = $this->SecondDB_model->getProject();
			
			$data['cegpds'] = $this->mastermodel->SelectRecordSingle('ceg_pds_detail', array('project_id' =>$cegexpID));
			
			$data['expsummery'] = $this->mastermodel->SelectRecordSingle('bdcegexp_proj_summery', array('project_id' => $cegexpID, 'status' => '1'));
			$data['sectorname'] = $this->mastermodel->SelectRecordSingle('sector', array('fld_id' => $data['expsummery']->sector));	
			$data['projectname'] = $this->SecondDB_model->getProjectBYId($data['expsummery']->project_numberid);
			
			$data['cegcompcomp'] = $this->mastermodel->SelectRecordFld('cegexp_competitor_comp', array('project_id' => $cegexpID, 'status' => '1'));
			$data['tqRecArr'] = $this->mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $cegexpID));
			$data['mailby'] = $this->SecondDB_model->getUserByID($this->session->userdata('uid'));
//			echo '<pre>'; print_r($data['cegpds']); die;
			
			
            //Delete Rec...
            if (isset($_REQUEST['delc'])):
                $respp = $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $_REQUEST['delc']), array('status' => '0'));
                $this->session->set_flashdata('msg', 'Record Deleted Successfully');
                redirect(base_url('company/cegexpedit/' . $cegexpID));
            endif;

            //Summery...
            $respCount = $this->mastermodel->count_allByCond('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
            if ($respCount < 1):
                $this->mastermodel->InsertMasterData(array('cegexp_id' => $cegexpID), 'cegexp_proj_summery');
            endif;
			
            //Comptitors Details..
           /*  $respCompt = $this->mastermodel->count_allByCond('cegexp_competitor_comp', array('cegexp_id' => $cegexpID, 'status' => '1'));
            if ($respCompt < 1):
                //custom Company Add of Ceg..
                $this->mastermodel->InsertMasterData(array('cegexp_id' => $cegexpID, 'projid' => $cegexpID, 'entry_by' => $this->session->userdata('uid'), 'compt_comp_id' => '74',), 'cegexp_competitor_comp');
            endif; */
			
            //Comptitors Company List
            $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
            if ($Rec != '') {
                $data['compnameArr'] = $Rec->result();
            }
			
			
            //Edit Proc..
            
            $data['cegexpSummery'] = $this->mastermodel->SelectRecordSingle('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
            $data['cegexpRec'] = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $cegexpID));
            $data['country_Arr'] = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
            $data['security_Type'] = $this->mastermodel->SelectRecord('bidsecurity_type', array('is_active' => '1'));
            $countID = $data['cegexpRec']->countries_id;
           // $data['stateArr'] = $this->mastermodel->SelectRecordS('states', array('country_id' => $countID, 'status' => '1'));
            $ProjId = $this->mastermodel->ProjIdByGenId($data['cegexpRec']->project_code);
            @$consortiumRecArr = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('project_id' => $ProjId, 'status' => '1'));
            $projectID = $data['cegexpRec']->project_id;
			
			
			$data['ceglane'] = $this->mastermodel->SelectRecord('ceg_lane', array('project_id' =>$cegexpID));
			$data['sector'] = $this->Front_model->GetActiveSector();
			$data['serviceArr'] = $this->Front_model->mainServices();
			
			
			$data['servicename'] = $this->mastermodel->SelectRecordSingle('main_services', array('id' => $data['cegexpRec']->service_id));	
			 
			if ($consortiumRecArr):
                $data['consortiumArr'] = $consortiumRecArr;
            else:
                $data['consortiumArr'] = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('cegexp_id' => $cegexpID, 'status' => '1'));
            endif;
			//echo '<pre>'; print_r($data); die;
            
			
			//Send Mail Code By Asheesh..
            if ($_REQUEST['mailsender']):
				$msgDetails = $this->load->view('email_template/comptitor_list_email', $data, true);
				$emailsAddress = $this->SecondDB_model->GetBdEmailsList();
				//echo '<pre>'; print_r($emailsAddress); die;
                $emailArrStr77 = implode(",", $emailsAddress);
				sendcomptitorMail("$emailArrStr77", 'Comptitor List of Project', $msgDetails);
                //sendcomptitorMail("jitendra00752@gmail.com", 'Comptitor List of Project', $msgDetails);
                $this->session->set_flashdata('msg', "Comptitor List Mail Successfully.");
                redirect(base_url('company/cegexpedit/' . $cegexpID));
            endif;
			
			if ($_REQUEST['mailsendercom']):
				$msgDetails = $this->load->view('email_template/comptitor_list_email8020', $data, true);
				$emailsAddress = $this->SecondDB_model->GetBdEmailsList();
                $emailArrStr77 = implode(",", $emailsAddress);
                
				$cruemailsAddress = $this->Front_model->GetCruEmailsList();
                $cruemailArrStr77 = implode(",", $cruemailsAddress);
				//echo '<pre>'; print_r($cruemailsAddress); die;
				$email =  $emailArrStr77.','.$cruemailArrStr77;
				sendcomptitorProjectMail("$email", 'Project Result', $msgDetails);
				//sendcomptitorProjectMail("$cruemailArrStr77", 'Project Result', $msgDetails);
				$this->session->set_flashdata('msg', "Project Result Mail Successfully.");
                redirect(base_url('company/cegexpedit/' . $cegexpID));
            endif;
			
            $this->load->view('company/cegexp_editproj_view', $data);
        } else {
            redirect(base_url('company'));
        }
    }

    //Update Basic Comp Details..
    /* public function updatecegexp() {
        $recArr = $_REQUEST;
		//echo '<pre>'; print_r($recArr); die;
		array_pop($recArr);
		
		$recArr['state'] = $this->mastermodel->GetStateNameById($recArr['state_id']);
        $respp = $this->mastermodel->UpdateRecords('ceg_exp', array('fld_id' => $this->input->post('projid')), $recArr);
		
		$cegexpRec = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $this->input->post('projid')));
		$proID = $cegexpRec->project_id;
		
		$desc = $this->mastermodel->SelectRecordSingle('project_description', array('project_id' => $proID));
		
		
		$servicename = $this->mastermodel->SelectRecordSingle('main_services', array('id' => $this->input->post('service_id')));	
		$servicearr = array('service'=>$servicename->service_name);
		$this->mastermodel->UpdateRecords('ceg_exp', array('fld_id' => $this->input->post('projid')), $servicearr);
		
		if(empty($desc)){
			$inserArr = array('user_id' => $this->session->userdata('uid'), 'project_id' => $proID,'country_id' => $recArr['countries_id'],'state_id' => $recArr['state_id'],'service_id' => $recArr['service_id'],'submission_date'=>$recArr['submission_date'],'tender_openingdate'=>$recArr['tender_openingdate']);
			$Respon = $this->Front_model->insertRecord('project_description', $inserArr);
		}else{
			$updaterec = array('user_id' => $this->session->userdata('uid'),'country_id' => $recArr['countries_id'],'state_id' => $recArr['state_id'],'service_id' => $recArr['service_id'],'submission_date'=>$recArr['submission_date'],'tender_openingdate'=>$recArr['tender_openingdate']);
			$respp = $this->mastermodel->UpdateRecords('project_description', array('project_id' => $proID), $updaterec);	
		}
		
        if ($respp):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
            redirect(base_url('company/cegexpedit/' . $this->input->post('projid')));
        else:
            redirect(base_url('company/cegexpedit/' . $this->input->post('projid')));
        endif;
    } */
	
	
	public function updatecegexp() {
        if($_REQUEST){
			$recArr = array(
				'submission_date' => $_REQUEST['submission_date'],	
				'tender_openingdate' => $_REQUEST['tender_openingdate'],	
				'service_id' => $_REQUEST['service_id'],	
				'client' => $_REQUEST['client'],	
				'countries_id' => $_REQUEST['countries_id'],	
				'state_id' => $_REQUEST['state_id'],	
				'start_year' => $_REQUEST['start_year'],	
				'funding' => $_REQUEST['funding'],	
				'bid_validity' => $_REQUEST['bid_validity'],	
				'bidsecurity_type' => $_REQUEST['bidsecurity_type'],	
				'bidsecurity_amount' => $_REQUEST['bidsecurity_amount'],	
				'cost_of_rfp' => $_REQUEST['cost_of_rfp'],
				'entry_by' => $this->session->userdata('uid')
				
			);
			$respp = $this->mastermodel->UpdateRecords('bdceg_exp', array('project_id' => $_REQUEST['projectid']), $recArr);
			
			$respp1 = $this->mastermodel->UpdateRecords('bd_tenderdetail', array('fld_id' => $_REQUEST['projectid']), array('TenderDetails' => $_REQUEST['project_name']));

		}
		/* $recArr['state'] = $this->mastermodel->GetStateNameById($recArr['state_id']);
        $respp = $this->mastermodel->UpdateRecords('ceg_exp', array('fld_id' => $this->input->post('projid')), $recArr);
		
		$cegexpRec = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $this->input->post('projid')));
		$proID = $cegexpRec->project_id;
		
		$desc = $this->mastermodel->SelectRecordSingle('project_description', array('project_id' => $proID));
		
		
		$servicename = $this->mastermodel->SelectRecordSingle('main_services', array('id' => $this->input->post('service_id')));	
		$servicearr = array('service'=>$servicename->service_name);
		$this->mastermodel->UpdateRecords('ceg_exp', array('fld_id' => $this->input->post('projid')), $servicearr);
		
		if(empty($desc)){
			$inserArr = array('user_id' => $this->session->userdata('uid'), 'project_id' => $proID,'country_id' => $recArr['countries_id'],'state_id' => $recArr['state_id'],'service_id' => $recArr['service_id'],'submission_date'=>$recArr['submission_date'],'tender_openingdate'=>$recArr['tender_openingdate']);
			$Respon = $this->Front_model->insertRecord('project_description', $inserArr);
		}else{
			$updaterec = array('user_id' => $this->session->userdata('uid'),'country_id' => $recArr['countries_id'],'state_id' => $recArr['state_id'],'service_id' => $recArr['service_id'],'submission_date'=>$recArr['submission_date'],'tender_openingdate'=>$recArr['tender_openingdate']);
			$respp = $this->mastermodel->UpdateRecords('project_description', array('project_id' => $proID), $updaterec);	
		} */
		
        if ($respp):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
            redirect(base_url('company/cegexpedit/' . $_REQUEST['projectid']));
        else:
            redirect(base_url('company/cegexpedit/' . $_REQUEST['projectid']));
        endif;
    }
	
    //Update Weightage Record..
    public function updateweightage() {
        $recArr = $_REQUEST;
		array_pop($recArr);
		$respp = $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $this->input->post('actid')), $recArr);
        if ($respp):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
            redirect(base_url('company/cegexpedit/' . $this->input->post('project_id')));
        else:
            redirect(base_url('company/cegexpedit/' . $this->input->post('project_id')));
        endif;
    }

    //Ajax Edit Score..
    public function ajax_score_byid() {
        $editId = $_REQUEST['editId'];
        $recArr = $this->mastermodel->SelectRecordSingle('cegexp_competitor_comp', array('fld_id' => $editId));
        echo json_encode($recArr);
    }

    //Upadte Weightage
    public function updatefixweightage() {
		$respp = $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('project_id' => $this->input->post('project_id')), $_REQUEST);
        if ($respp):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
            redirect(base_url('company/cegexpedit/' . $this->input->post('project_id')));
        else:
            redirect(base_url('company/cegexpedit/' . $this->input->post('project_id')));
        endif;
    }

    //Update Summery..
    /* public function updatesummery() {
        $Arr = $_REQUEST;
		
        array_pop($Arr);
	
		
		$recArr = array(
					'project_code'=> $Arr['project_code'],
					'project_number'=> $Arr['project_number'],
					'sector'=> $Arr['sector'],
					'start_date'=> $Arr['start_date'],
					'end_date'=> $Arr['end_date'],
					'project_cost'=> $Arr['project_cost'],
					'project_rcv'=> $Arr['project_rcv'],
					'consultancy_fee'=> $Arr['consultancy_fee'],
					'cegshare_val'=> $Arr['cegshare_val'],
					'othershare_val'=> $Arr['othershare_val'],
					'length'=> $Arr['length']
				);
		if(!empty($Arr['lane_chk'][0])){
			$lane = $Arr['lane_chk'];
			$lane_length = $Arr['lane_length'];
			for($i=0; $i<count($lane); $i++){
				echo 'Lane'.$lane[$i].' Length'.$lane_length[$i].'<br/>';
				$count = $this->mastermodel->count_rows('ceg_lane', array('cegexp_id' => $this->input->post('cegexp_id'), 'lane' => $lane[$i]));
                if ($count < 1):
                    $this->mastermodel->InsertMasterData(array('cegexp_id' =>$this->input->post('cegexp_id'), 'lane' => $lane[$i], 'lane_length' => $lane_length[$i]), 'ceg_lane');
                else: 
					$this->mastermodel->UpdateRecords('ceg_lane', array('cegexp_id' => $this->input->post('cegexp_id') , 'lane' => $lane[$i]), array('lane_length' => $lane_length[$i]));
				endif;
			}
		}
		
		$cegexpRec = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $this->input->post('projid')));
		$proID = $cegexpRec->project_id;
		
		$desc = $this->mastermodel->SelectRecordSingle('project_description', array('project_id' => $proID));
		if(empty($desc)){
			$inserArr = array('user_id' => $this->session->userdata('uid'),'sector_id' => $Arr['sector_id']);
			$Respon = $this->Front_model->insertRecord('project_description', $inserArr);
		}else{
			$updaterec = array('user_id' => $this->session->userdata('uid'),'sector_id' => $Arr['sector_id']);
			$respp = $this->mastermodel->UpdateRecords('project_description', array('project_id' => $proID), $updaterec);	
		}
		
        $respp = $this->mastermodel->UpdateRecords('cegexp_proj_summery', array('cegexp_id' => $this->input->post('cegexp_id')), $recArr);
        //Sector Update in Main Table .. C
		$sectorname = $this->mastermodel->SelectRecordSingle('sector', array('fld_id' => $this->input->post('sector_id')));	
		$secResp = $this->mastermodel->UpdateRecords('ceg_exp', array('fld_id' => $this->input->post('cegexp_id')), array('sector' => $sectorname->sectName,'sector_id' => $this->input->post('sector_id')));
        //File Upload code 1..
        if ($_FILES['pds_file']['name']) {
            $configThm = $this->getUploadConfig();
            $this->load->library('upload', $configThm);
            $this->upload->initialize($configThm);
            if ($this->upload->do_upload('pds_file')) {
                $uploadData = $this->upload->data();
                $filename1 = $uploadData['file_name'];
                $this->mastermodel->UpdateRecords('cegexp_proj_summery', array('cegexp_id' => $this->input->post('cegexp_id')), array('pds_file' => $filename1));
            }
        }
        //File Upload code 2..
        if ($_FILES['certificate_file']['name']) {
			$cegexpRec = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $this->input->post('cegexp_id')));
			$projectID = $cegexpRec->project_id;
			
			$folder = './uploads/certificate/';
			$filename = 'certificate_'.$projectID;
			$configThm = $this->getUpload($folder,$filename);
			$this->load->library('upload', $configThm);
            $this->upload->initialize($configThm);
            if ($this->upload->do_upload('certificate_file')) {
				$uploadData = $this->upload->data();
                $filename2 = $uploadData['file_name'];
                $this->mastermodel->UpdateRecords('ceg_exp', array('fld_id' => $this->input->post('cegexp_id')), array('certificate_file_name' => $filename2));
            }
        }
			
        //File Upload code 3..
        if ($_FILES['contract_agreement_file']['name']) {
            $configThm = $this->getUploadConfig();
            $this->load->library('upload', $configThm);
            $this->upload->initialize($configThm);
            if ($this->upload->do_upload('contract_agreement_file')) {
                $uploadData = $this->upload->data();
				$filename3 = $uploadData['file_name'];
                $this->mastermodel->UpdateRecords('cegexp_proj_summery', array('cegexp_id' => $this->input->post('cegexp_id')), array('contract_agreement_file' => $filename3));
            }
        }
        if ($respp):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
            redirect(base_url('company/cegexpedit/' . $this->input->post('projid')));
        else:
            redirect(base_url('company/cegexpedit/' . $this->input->post('projid')));
        endif;
    } */
	
	public function updatesummery() {
        $Arr = $_REQUEST;
		if($Arr){
			$count = $this->mastermodel->count_rows('bdcegexp_proj_summery', array('project_id' => $Arr['project_id']));
			if ($count < 1):
				$recArr = array(
					'project_id' => $Arr['project_id'],
					'project_numberid'=> $Arr['project_number'],
					'sector'=> $Arr['sector_id'],
					'start_date'=> $Arr['start_date'],
					'end_date'=> $Arr['end_date'],
					'project_cost'=> $Arr['project_cost'],
					'project_rcv'=> $Arr['project_rcv'],
					'consultancy_fee'=> $Arr['consultancy_fee'],
					'cegshare_val'=> $Arr['cegshare_val'],
					'othershare_val'=> $Arr['othershare_val'],
					'length'=> $Arr['length'],
					'entry_by'=> $this->session->userdata('uid')
				);
				$respp = $this->mastermodel->InsertMasterData($recArr, 'bdcegexp_proj_summery');
			else: 
				$recArr = array(
					'project_numberid'=> $Arr['project_number'],
					'sector'=> $Arr['sector_id'],
					'start_date'=> $Arr['start_date'],
					'end_date'=> $Arr['end_date'],
					'project_cost'=> $Arr['project_cost'],
					'project_rcv'=> $Arr['project_rcv'],
					'consultancy_fee'=> $Arr['consultancy_fee'],
					'cegshare_val'=> $Arr['cegshare_val'],
					'othershare_val'=> $Arr['othershare_val'],
					'length'=> $Arr['length'],
					'entry_by'=> $this->session->userdata('uid')
				);
				$respp = $this->mastermodel->UpdateRecords('bdcegexp_proj_summery', array('project_id' => $Arr['project_id']), $recArr);
			endif;
			
		
			if(!empty($Arr['lane_chk'][0])){
				$lane = $Arr['lane_chk'];
				$lane_length = $Arr['lane_length'];
				for($i=0; $i<count($lane); $i++){
					$count = $this->mastermodel->count_rows('ceg_lane', array('project_id' => $Arr['project_id'], 'lane' => $lane[$i]));
					if ($count < 1):
						$this->mastermodel->InsertMasterData(array('project_id' => $Arr['project_id'], 'lane' => $lane[$i], 'lane_length' => $lane_length[$i]), 'ceg_lane');
					else: 
						$this->mastermodel->UpdateRecords('ceg_lane', array('project_id' => $Arr['project_id'] , 'lane' => $lane[$i]), array('lane_length' => $lane_length[$i]));
					endif;
				}
			}
			
			
			//File Upload code 1..
			if ($_FILES['pds_file']['name']) {
				$folder = './uploads/pdsfile/';
				$filename = 'pdsfile_'.$Arr['project_id'];
				$configThm = $this->getUpload($folder,$filename);
				$this->load->library('upload', $configThm);
				$this->upload->initialize($configThm);
				if ($this->upload->do_upload('pds_file')) {
					$uploadData = $this->upload->data();
					$filename1 = $uploadData['file_name'];
					$this->mastermodel->UpdateRecords('bdcegexp_proj_summery', array('project_id' => $Arr['project_id']), array('pds_file' => $filename1));
				}
			}
			//File Upload code 2..
			if ($_FILES['certificate_file']['name']) {
				$folder = './uploads/certificate/';
				$filename = 'certificate_'.$Arr['project_id'];
				$configThm = $this->getUpload($folder,$filename);
				$this->load->library('upload', $configThm);
				$this->upload->initialize($configThm);
				if ($this->upload->do_upload('certificate_file')) {
					$uploadData = $this->upload->data();
					$filename2 = $uploadData['file_name'];
					$this->mastermodel->UpdateRecords('bdcegexp_proj_summery', array('project_id' => $Arr['project_id']), array('certificate_file' => $filename2));
				}
			}
				
			//File Upload code 3..
			if ($_FILES['contract_agreement_file']['name']) {
				$folder = './uploads/contract/';
				$filename = 'contract_'.$Arr['project_id'];
				$configThm = $this->getUpload($folder,$filename);
				$this->load->library('upload', $configThm);
				$this->upload->initialize($configThm);
				if ($this->upload->do_upload('contract_agreement_file')) {
					$uploadData = $this->upload->data();
					$filename3 = $uploadData['file_name'];
					$this->mastermodel->UpdateRecords('bdcegexp_proj_summery', array('project_id' => $Arr['project_id']), array('contract_agreement_file' => $filename3));
				}
			}
		}
		
		
		
		/* $cegexpRec = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $this->input->post('projid')));
		$proID = $cegexpRec->project_id;
		
		$desc = $this->mastermodel->SelectRecordSingle('project_description', array('project_id' => $proID));
		if(empty($desc)){
			$inserArr = array('user_id' => $this->session->userdata('uid'),'sector_id' => $Arr['sector_id']);
			$Respon = $this->Front_model->insertRecord('project_description', $inserArr);
		}else{
			$updaterec = array('user_id' => $this->session->userdata('uid'),'sector_id' => $Arr['sector_id']);
			$respp = $this->mastermodel->UpdateRecords('project_description', array('project_id' => $proID), $updaterec);	
		}
		
        $respp = $this->mastermodel->UpdateRecords('cegexp_proj_summery', array('cegexp_id' => $this->input->post('cegexp_id')), $recArr);
        //Sector Update in Main Table .. C
		$sectorname = $this->mastermodel->SelectRecordSingle('sector', array('fld_id' => $this->input->post('sector_id')));	
		$secResp = $this->mastermodel->UpdateRecords('ceg_exp', array('fld_id' => $this->input->post('cegexp_id')), array('sector' => $sectorname->sectName,'sector_id' => $this->input->post('sector_id')));
         */
		 
		
        if ($respp):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
            redirect(base_url('company/cegexpedit/' . $Arr['project_id']));
        else:
            redirect(base_url('company/cegexpedit/' . $Arr['project_id']));
        endif;
    }

    //Add Comptitor Company By Asheesh .. 
    public function addcomptcompn() {
		$projId = $_REQUEST['project_id'];
        if ($_REQUEST['leadcompany']):
            foreach ($_REQUEST['leadcompany'] as $compId) {
                $count = $this->mastermodel->count_rows('cegexp_competitor_comp', array('project_id' => $projId, 'compt_comp_id' => $compId));
                if ($count < 1):
                    $this->mastermodel->InsertMasterData(array('project_id' => $projId, 'entry_by' => $this->session->userdata('uid'), 'compt_comp_id' => $compId), 'cegexp_competitor_comp');
                endif;
            }
        endif;
        $this->session->set_flashdata('msg', 'Record Inserted successfully');
        redirect(base_url('company/cegexpedit/' . $projId));
    }

    //Score Reload And Update Technical..
    public function reloadscore_tech() {
        $projID = $this->uri->segment(3);
        $recArr = $this->mastermodel->SelectRecordFld('cegexp_competitor_comp', array('project_id' => $projID, 'status' => '1'));
        if ($recArr):
            foreach ($recArr as $rowdt) {
                if (($rowdt->techn_weightage) and ( $rowdt->technical_score)) {
                    $reeMulScore = ($rowdt->technical_score * $rowdt->techn_weightage);
                    $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $rowdt->fld_id), array('technical_marks' => $reeMulScore));
                }
            }
        endif;
        $this->session->set_flashdata('msg', 'Technical Score Updated successfully');
        redirect(base_url('company/cegexpedit/' . $projID));
    }

    //Score Reload And Update Financial.. .
    public function reloadscore_finan() {
        $projID = $this->uri->segment(3);
        $recArr = $this->mastermodel->SelectRecordFld('cegexp_competitor_comp', array('project_id' => $projID, 'status' => '1'));
        $getminVal = $this->mastermodel->getMinVal('cegexp_competitor_comp', 'financial_score', array('project_id' => $projID, 'status' => '1'));
        if ($recArr):
            foreach ($recArr as $rowdt) {
                $recUpd = 0;
                $rrrFinal = 0;
                if (($rowdt->finan_weightage) and ( $rowdt->financial_score)) {
                    $recUpd = ($getminVal / $rowdt->financial_score);
                    $rrrFinal = $recUpd * $rowdt->finan_weightage;
                    $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $rowdt->fld_id), array('financial_marks' => $rrrFinal));
                }
            }
        endif;
        $this->session->set_flashdata('msg', 'Financial Score Updated successfully');
        redirect(base_url('company/cegexpedit/' . $projID));
    }

    //Upload Config By Asheesh..
    protected function getUploadConfig() {
        $config = array();
        $config['upload_path'] = './uploads/awardedproject/';
        $config['allowed_types'] = 'pdf|doc|PDF';
        $config['max_size'] = '200000'; //20MB
        $config['file_name'] = "Resume_" . time();
        return $config;
    }
	
	public function getUpload($folder,$filename) {
        $config = array();
        $config['upload_path'] = $folder;
        $config['allowed_types'] = 'pdf|doc|PDF';
        $config['max_size'] = '200000'; //20MB
        //$config['file_name'] = "news_" . time();
        $config['file_name'] = $filename;
        $config['overwrite'] = true;
        return $config;
    }
    //Add Extra About Company.. 
    public function addcompany_extra() {
        $data = array();
        $data['comp_single_rec'] = null;
        $data['title'] = 'Add,Edit ,Update or View Company Details';
        $data['ComprecArr'] = $this->mastermodel->SelectRecordFld('main_company', array('status' => '1'));
        if ($_REQUEST['compnm']):
            $data['comp_single_rec'] = $this->mastermodel->SelectRecordFld('main_company', array('status' => '1', 'fld_id' => $_REQUEST['compnm']));
            $data['sectArr_rec'] = $this->mastermodel->SelectRecord('designation_sector', array('status' => '1'),'sector_name', 'ASC');
            $data['comp_extraRec'] = $this->mastermodel->SelectRecordFld('company_extra_details', array('status' => '1', 'main_comp_id' => $_REQUEST['compnm']));
            $data['selectedcomp'] = $_REQUEST['compnm'];
            $data['country_Arr'] = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
            $countID = $data['comp_single_rec'][0]->country;
            $data['stateArr'] = $this->mastermodel->SelectRecordS('states', array('country_id' => $countID, 'status' => '1'));
            $data['comp_field_Arr'] = $this->mastermodel->SelectRecordFld('comp_field_master', array('status' => '1'), 'field_name', 'ASC');
        endif;
        $this->load->view('company/extradetails_company_view', $data);
    }

    //Save Extra Act..
    public function savecompany_extra() {
        $recInsertArr = $this->input->post();
        $recInsertArr['entry_by'] = $this->session->userdata('uid');
        $respon = $this->mastermodel->InsertMasterData($recInsertArr, 'company_extra_details');
        if ($respon):
            $this->session->set_flashdata('msg', 'Extra Company Detail Added successfully');
            redirect(base_url('company/addcompany_extra?compnm=' . $recInsertArr['main_comp_id']));
        else:
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/addcompany_extra?compnm=' + $recInsertArr['main_comp_id']));
        endif;
    }

    //Update Extra Details..
    public function updatecompany_extra() {
        $UpdArr = $this->input->post();
        array_pop($UpdArr);
        $respp = $this->mastermodel->UpdateRecords('company_extra_details', array('fld_id' => $this->input->post('edtid')), $UpdArr);
        if ($respp):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
            redirect(base_url('company/addcompany_extra?compnm=' . $UpdArr['main_comp_id']));
        else:
            redirect(base_url('company/addcompany_extra?compnm=' . $UpdArr['main_comp_id']));
        endif;
    }

    //Update Main Company Basic..
    public function updatecompany_basic() {
        $UpdArr = $this->input->post();
        array_pop($UpdArr);
        $UpdArr['operating_countries'] = implode(",", $_REQUEST['operating_countries']);
        $UpdArr['com_sector'] = implode(",", $_REQUEST['com_sector']);
        $UpdArr['comp_field_id'] = implode(",", $_REQUEST['comp_field_id']);
        $respp = $this->mastermodel->UpdateRecords('main_company', array('fld_id' => $this->input->post('fld_id')), $UpdArr);
        if ($respp):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
            redirect(base_url('company/addcompany_extra?compnm=' . $this->input->post('fld_id')));
        else:
            redirect(base_url('company/addcompany_extra?compnm=' . $this->input->post('fld_id')));
        endif;
    }

    //Edit Ajax Extra Address By Id..
    public function ajax_getextcomp_byid() {
        $edtid = $_REQUEST['editId'];
        $CextraRec = $this->mastermodel->SelectRecordFld('company_extra_details', array('status' => '1', 'fld_id' => $edtid));
        echo json_encode($CextraRec[0]);
        exit;
    }

    //Add joint venture.. Consortium
    public function addjointventure() {
		$leadCompString = '';
        $joint_venture = '';
        $assoc = '';
        if ($_REQUEST['leadcompany']):
            $leadCompString = implode(",", $_REQUEST['leadcompany']);
        endif;
        if ($_REQUEST['joint_venture']):
            $joint_venture = implode(",", $_REQUEST['joint_venture']);
        endif;
        if ($_REQUEST['assoc']):
            $assoc = implode(",", $_REQUEST['assoc']);
        endif;
		
		$count = $this->mastermodel->count_rows('jv_cegexp', array('project_id' => $_REQUEST['project_id'],'base_comp_id' => $_REQUEST['leadcompanyid']));
		if ($count < 1):
			$inserArr = array(
				'project_id' => $_REQUEST['project_id'],
				'base_comp_id' => $_REQUEST['leadcompanyid'],
				'lead_comp_id' => $leadCompString,
				'joint_venture' => $joint_venture,
				'asso_comp' => $assoc,
				'entry_by' => $this->session->userdata('uid')
			);
			$respon = $this->mastermodel->InsertMasterData($inserArr, 'jv_cegexp');
		else:
			$inserArr1 = array(
				'base_comp_id' => $_REQUEST['leadcompanyid'],
				'lead_comp_id' => $leadCompString,
				'joint_venture' => $joint_venture,
				'asso_comp' => $assoc,
				'entry_by' => $this->session->userdata('uid')
			);
			$respon = $this->mastermodel->UpdateRecords('jv_cegexp', array('project_id' => $_REQUEST['project_id'],'base_comp_id' => $_REQUEST['leadcompanyid']),$inserArr1);
		endif;
        
        $this->session->set_flashdata('msg', 'Insert Jv Details Record successfully');
        redirect(base_url('company/cegexpedit/' . $_REQUEST['project_id']));
    }

    //Ajax Get All Sate By Country I..
    public function ajax_state_getbycid() {
        $counid = $_REQUEST['country'];
        $stateRec = $this->mastermodel->SelectRecordS('states', array('status' => '1', 'country_id' => $counid));
        echo json_encode($stateRec);
        // return $stateRec;
    }

    //Project Status Change By Asheesh..
   /*  public function changeprojstatus() {
        $statusArr = array('1' => 'won', '2' => 'loose', '0' => 'awaiting', '3' => 'cancel');
        $updArr = array('proj_status' => $statusArr[$_REQUEST['proj_sts']]);
        $Resp = $this->mastermodel->UpdateRecords('ceg_exp', array('fld_id' => $_REQUEST['project_idedt']), $updArr);
        
		$cegexpRec = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $_REQUEST['project_idedt']));
		$proID = $cegexpRec->project_id;
		$Resp1 = $this->mastermodel->UpdateRecords('bid_project', array('project_id' => $proID), array('project_status'=>$_REQUEST['proj_sts']));
		
		$this->session->set_flashdata('msg', 'Project Status Changed Successfully');
        redirect(base_url('company/cegexpedit/' . $_REQUEST['project_idedt']));
    } */ 
	
	public function changeprojstatus() {
		if($_REQUEST){
			$Resp = $this->mastermodel->UpdateRecords('bdproject_status', array('project_id' => $_REQUEST['project_id']), array('project_status' => $_REQUEST['project_status']));	
			$this->session->set_flashdata('msg', 'Project Status Changed Successfully');
        }else{
			$this->session->set_flashdata('msg', 'Something Went Wrong !!!');
		}
        redirect(base_url('company/cegexpedit/' . $_REQUEST['project_id']));
    }

	public function getcomptdata(){
		if($_REQUEST){
			$sectorname = $this->mastermodel->SelectRecordSingle('jv_cegexp', array('project_id' => $_REQUEST['projectID'],'base_comp_id' => $_REQUEST['companyid']));	
			echo json_encode($sectorname);
		}
	}
    //Company List Show
    function showlist() {
        $data['error'] = '';
        $data['title'] = 'Company Contact List ' ;
        $data['sectArr_rec'] = $this->mastermodel->SelectRecord('designation_sector', array('status' => '1'));
        $data['country_Arr'] = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
        $data['comp_field_Arr'] = $this->mastermodel->SelectRecordFld('comp_field_master', array('status' => '1'), 'field_name', 'ASC');
        $this->load->view('company/list_view', $data);
    }

    //All Tender Search Website Details Code By Asheesh..
    public function websites() {
        $data['error'] = '';
        $data['title'] = 'Websites';
        $this->load->view('company/list_view_websites', $data);
    }

    //Test Asheesh Pdf Merge..
    public function pdfm() {
        echo "Asheesh Yadav";
        die;
    }

    //Company Contact Delete Code By Asheesh..
    public function contdelete() {
        $delId = $_REQUEST['delid'];
        if ($delId):
            $respp = $this->mastermodel->UpdateRecords('main_company', array('fld_id' => $delId), array('status' => '0'));
            $respp = $this->mastermodel->UpdateRecords('company_extra_details', array('main_comp_id' => $delId), array('status' => '0'));
            $this->session->set_flashdata('msg', 'Company Contact Deleted Successfully');
        endif;
        redirect(base_url('company/showlist'));
    }

    //Upload updcompany_brochure
    public function updcompany_brochure() {
        if (($_FILES['comp_brochure_file']['name']) and ($_REQUEST['main_compn_id'])) {
            $configThm = $this->getUploadConfigcomp();
            $this->load->library('upload', $configThm);
            $this->upload->initialize($configThm);
            if ($this->upload->do_upload('comp_brochure_file')) {
                $uploadData = $this->upload->data();
                $filename3 = $uploadData['file_name'];
                $main_compn_id = $_REQUEST['main_compn_id'];
                $this->session->set_flashdata('msg', 'File Upload Success');
                $this->mastermodel->UpdateRecords('main_company', array('fld_id' => $this->input->post('main_compn_id')), array('brochure' => $filename3));
                redirect(base_url('company/addcompany_extra?compnm=' . $main_compn_id));
            } else {
                $this->session->set_flashdata('errormsg', 'Something went wrong');
                redirect(base_url('company/addcompany_extra?compnm=' . $main_compn_id));
            }
        }
    }

    
    //Upload Config By Asheesh..
    protected function getUploadConfigcomp() {
        $config = array();
        $config['upload_path'] = './uploads/companybrochure/';
        $config['allowed_types'] = 'pdf|doc|DOC|docx|jpg|JPG|jpeg|PDF';
        $config['max_size'] = '200000'; //20MB
        $config['file_name'] = "Compb_" . time();
        return $config;
    }
	
	 //Save Compititors..
    function savepds() {
		//echo '<pre>'; print_r($_REQUEST); die;
		if(!empty($_REQUEST['project_id'])){
			$arr = array(
				'professional_staff_provided' => $_REQUEST['professional_staff_provided'],
				'numberof_man_month' => $_REQUEST['numberof_man_month'],
				'date_of_commencement' => $_REQUEST['date_of_commencement'],
				'stipulated_date_of_completion' => $_REQUEST['stipulated_date_of_completion'],
				'mmby_associated_firm' => $_REQUEST['mmby_associated_firm'],
				'narrative_description' => $_REQUEST['narrative_description'],
				'major_bridges' => $_REQUEST['major_bridges'],
				'urbon_flyovers' => $_REQUEST['urbon_flyovers'],
				'project_id' => $_REQUEST['project_id']
				//'service_description' => $_REQUEST['service_description']
			);
			
			$count = $this->mastermodel->count_rows('ceg_pds_detail', array('project_id' => $_REQUEST['project_id']));
			if ($count < 1):	
				$respon = $this->mastermodel->InsertMasterData($arr, 'ceg_pds_detail');
			else:
				$respon = $this->mastermodel->UpdateRecords('ceg_pds_detail', array('project_id' => $_REQUEST['project_id']),$arr);
			endif;
		}
		if ($respon):
			$this->session->set_flashdata('msg', 'Competitors Record Added successfully');
			redirect(base_url('company/cegexpedit/' . $_REQUEST['project_id']));
		else:
			$this->session->set_flashdata('error_msg', 'Something went gone wrong');
			redirect(base_url('company/cegexpedit/' . $_REQUEST['project_id']));
		endif; 
		
    }
	
	/* public function pdf_output(){
		$this->load->view('company/pdf_output',$data, true);
	} */
	
	public function save_download()
	{ 
		$cegexpID = $this->uri->segment(3);
		//load mPDF library
		$this->load->library('m_pdf');
		//load mPDF library
		if(!empty($cegexpID)){
			$data['cegpds'] = $this->mastermodel->GetTableData('ceg_pds_detail', array('ceg_exp_id' => $cegexpID));
			$data['cegexpRec'] = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $cegexpID));
			$data['cegexpSummery'] = $this->mastermodel->SelectRecordSingle('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
			$data['consortiumArr'] = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('cegexp_id' => $ProjId, 'status' => '1'));
			$projectID = $data['cegexpRec']->project_id;
			$data['tqRecArr'] = $this->mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $projectID));
			
			$html=$this->load->view('company/pdf_output',$data, true); //load the pdf_output.php by passing our data and get all data in $html varriable.
			//this the the PDF filename that user will get to download
			$pdfFilePath ="pds_".$projectID.".pdf";
			
			$arr = array('pds_file_name' => $pdfFilePath);
			$where = array('fld_id' => $cegexpID);
			$respon = $this->mastermodel->UpdateRecords('ceg_exp',$where,$arr);
			
			//actually, you can pass mPDF parameter on this load() function
			$pdf = $this->m_pdf->load();
			//$pdf->WriteHTML($stylesheet, 1);
			//generate the PDF!
			$pdf->WriteHTML($html);
			//offer it to user via browser download! (The PDF won't be saved on your server HDD)
			$pdf->Output("./uploads/pds/".$pdfFilePath, "F");
			$pdf->Output($pdfFilePath, "D");
			
			/* $zip = new ZipArchive;
			if ($zip->open('./uploads/pds/test_overwrite.zip', ZipArchive::OVERWRITE) === TRUE)
			{
				// Add file to the zip file
				$zip->addFile('./uploads/pds/pds_59109.pdf','pds_data/pds_59109.pdf');
				// All files are added, so close the zip file.
				$zip->close();
			} */
		}
		
	}
	
	public function pds_description(){
		$title = 'PDS Description';
		$sector = $this->Front_model->GetActiveSector();
		$service1 = $this->Front_model->GetActiveService1();
		$service2 = $this->Front_model->GetActiveService2();
		$this->load->view('pds_description/tender_view',compact('sector','service1','service2','title'));
	}
	
	public function add_sector(){
		$this->form_validation->set_rules('sector_name', 'Sector Name', 'required|min_length[3]|max_length[100]|is_unique[sector.sectName]');
        if ($this->form_validation->run() == FALSE) {
            redirect(base_url('company/pds_description'));
        } else {
			$arr = array('sectName'=>$_REQUEST['sector_name'],'is_active'=>'1');
            $respon = $this->mastermodel->InsertMasterData($arr,'sector');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Sector Added successfully');
                redirect(base_url('company/pds_description'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/pds_description'));
            endif;
        }
	}
	
	public function add_service1(){
		/* $this->form_validation->set_rules('service1_name', 'Service Name', 'required|min_length[3]|max_length[100]|is_unique[pds_service1.service_name]');
        if ($this->form_validation->run() == FALSE) {
            redirect(base_url('company/pds_description'));
        } else { */
			if(!empty($_REQUEST)){
				$count = $this->mastermodel->count_rows('pds_service1', array('sector_id' => $_REQUEST['sector_id'], 'service_name' => $_REQUEST['service1_name'], 'is_active' => 1));
				if($count<1):
					$arr = array('service_name'=>$_REQUEST['service1_name'],'sector_id'=>$_REQUEST['sector_id'],'is_active'=>'1');
					$respon = $this->mastermodel->InsertMasterData($arr,'pds_service1');
					if ($respon):
						$this->session->set_flashdata('success_msg', 'Service Added successfully');
						redirect(base_url('company/pds_description'));
					else:
						$this->session->set_flashdata('error_msg', 'Something went gone wrong');
						redirect(base_url('company/pds_description'));
					endif;
				else:
					$this->session->set_flashdata('error_msg', 'Something went gone wrong');
					redirect(base_url('company/pds_description'));
				endif;
			}
        //}
	}
	
	public function add_service2(){
		/* $this->form_validation->set_rules('service2_name', 'Service Name', 'required|min_length[3]|max_length[100]|is_unique[pds_service2.service_name]');
        if ($this->form_validation->run() == FALSE) {
            redirect(base_url('company/pds_description'));
        } else { */
			if(!empty($_REQUEST)){
				$count = $this->mastermodel->count_rows('pds_service2', array('sector_id' => $_REQUEST['sector_id1'], 'service1_id' => $_REQUEST['service1_id'], 'service_name' => $_REQUEST['services2_name'], 'is_active' => 1));
				if($count<1):
					$arr = array('sector_id' => $_REQUEST['sector_id1'], 'service1_id' => $_REQUEST['service1_id'],'service_name'=>$_REQUEST['services2_name'],'is_active'=>'1');
					$respon = $this->mastermodel->InsertMasterData($arr,'pds_service2');
					if ($respon):
						$this->session->set_flashdata('success_msg', 'Service Added successfully');
						redirect(base_url('company/pds_description'));
					else:
						$this->session->set_flashdata('error_msg', 'Something went gone wrong');
						redirect(base_url('company/pds_description'));
					endif;
				else:
					$this->session->set_flashdata('error_msg', 'Something went gone wrong');
					redirect(base_url('company/pds_description'));
				endif;
			}
        //}
	}
	
	public function add_pdsDescription(){
		//echo '<pre>'; print_r($_REQUEST); die;
		if(!empty($_REQUEST['sector_name']) && !empty($_REQUEST['service1_name']) && !empty($_REQUEST['service2_name'])){
			$count = $this->mastermodel->count_rows('pds_service_description', array('sector_id' => $_REQUEST['sector_name'], 'service1_id' => $_REQUEST['service1_name'], 'service2_id' => $_REQUEST['service2_name'], 'is_active' => 1));
			if ($count < 1):
				$arr = array('sector_id' => $_REQUEST['sector_name'], 'service1_id' => $_REQUEST['service1_name'], 'service2_id' => $_REQUEST['service2_name'], 'description' => $_REQUEST['service_description'], 'is_active' => 1);
				$respon = $this->mastermodel->InsertMasterData($arr,'pds_service_description');
				redirect(base_url('company/pds_description'));
			else:
				$arr = array('description' => $_REQUEST['service_description']);
				$where = array('sector_id' => $_REQUEST['sector_name'], 'service1_id' => $_REQUEST['service1_name'], 'service2_id' => $_REQUEST['service2_name'], 'is_active' => 1);
				$respon = $this->mastermodel->UpdateRecords('pds_service_description',$where,$arr);
				redirect(base_url('company/pds_description'));
			endif;	
		}else{
			$this->session->set_flashdata('error_msg', 'Something went gone wrong');
			redirect(base_url('company/pds_description'));
		}
	}
	
	public function getPDSDesc_ajax(){
		//print_r($_REQUEST); die;
		if(!empty($_REQUEST)){
			$wheres = array('sector_id' => $_REQUEST['sector_name'], 'service1_id' => $_REQUEST['service1_name'], 'service2_id' => $_REQUEST['service2_name'], 'is_active' => 1);
			//$pdsrec = $this->Front_model->SelectRecord('pds_service_description',$wheres);
			//$wheres = array('sector_id' => $_REQUEST['sector_name'], 'service1_id' =>$_REQUEST['service1_name'],'service2_id' => $_REQUEST['service2_name'],'is_active'=>1);
			$pdsrec = $this->mastermodel->SelectRecord('pds_service_description', $wheres);
			if(!empty($pdsrec)){
				echo json_encode(array('desc'=>$pdsrec[0]->description)); 
			}else{
				return false;
			}
			die;
		}
	}
	
	public function getservice1_ajax(){
		$secID = $_REQUEST['secid'];
		if($secID):
			$wheres = array('sector_id' => $secID,'is_active' => 1);
			$pdsrec = $this->mastermodel->SelectRecord('pds_service1', $wheres);
			if(!empty($pdsrec)){
				echo json_encode($pdsrec); 
			}else{
				return false;
			}
			die;
		endif;
	}
	
	public function getservice2_ajax(){
		$secID = $_REQUEST['secid'];
		$service1_name_id = $_REQUEST['service1_name_id'];
		if($secID):
			$wheres = array('sector_id' => $secID,'service1_id' => $service1_name_id,'is_active' => 1);
			$pdsrec = $this->mastermodel->SelectRecord('pds_service2', $wheres);
			if(!empty($pdsrec)){
				echo json_encode($pdsrec); 
			}else{
				return false;
			}
			die;
		endif;
	}
	
	public function ceg_laneRemove(){
		$id = $_POST['id'];
		$res = $this->db->delete('ceg_lane', array('id' => $id)); 
		echo '1';
	}
	
	/* public function testchk(){
		$this->db->select('fld_id,project_id,proj_status');
		$this->db->from('ceg_exp');
		$res = $this->db->get()->result_object();
		foreach($res as $val){
			$statusArr = array('won' => '1', 'loose' => '2', 'awaiting' => '0', 'cancel' => '3');
			$updArr = array('project_status' => $statusArr[$val->proj_status]);
			$Resp1 = $this->mastermodel->UpdateRecords('bid_project', array('project_id' => $val->project_id), $updArr);
		}
	} */
	
	
	// First Cron 
	public function insertbdscope(){
		$this->db->select('fld_id,visible_scope');
		$this->db->from('bd_tenderdetails');
		$res = $this->db->get()->result_object();
		//echo '<pre>'; print_r($res);
		foreach ($res as $value) {
			$count = $this->mastermodel->count_rows('bdtender_scope', array('project_id' => $value->fld_id));
			if ($count < 1):
				$IncrtArr = array(
					'project_id' => $value->fld_id,
					'visible_scope' => $value->visible_scope,
					'businessunit_id' => 1,
					);
				$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdtender_scope');
				echo $value->fld_id .'<br/>';
			endif;
		}
	}
	
	
	// Second Cron 
	public function insertProjectHistory(){
		$arr = array('Active_project', 'Important_project', 'In_Review_project', 'To_Go_project', 'Bid_project');
		$this->db->select('id,project_id,visible_scope');
		$this->db->from('bdtender_scope');
		$this->db->where_in('visible_scope', $arr);
		$res = $this->db->get()->result_object();
		//echo '<pre>'; print_r($res); die;
		foreach ($res as $value) {
			if($value->visible_scope == 'Active_project'){
				$count = $this->mastermodel->count_rows('bdactive_project_byuser', array('project_id' => $value->id));
				if ($count < 1):
					$IncrtArr = array(
							'project_id' => $value->id,
							'user_id' => $this->session->userdata('uid'),
							'action_date' => date('Y-m-d H:i:s'),
							'actionIP' => get_client_ip(),
							'message' => 'Active Project'
						);
					$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdactive_project_byuser');
					echo $value->fld_id .'<br/>';
				endif; 
			}
			elseif($value->visible_scope == 'Important_project'){
				$count = $this->mastermodel->count_rows('bdimportant_project_byuser', array('project_id' => $value->id));
				if ($count < 1):
					$IncrtArr = array(
							'project_id' => $value->id,
							'user_id' => $this->session->userdata('uid'),
							'action_date' => date('Y-m-d H:i:s'),
							'actionIP' => get_client_ip(),
							'message' => 'Important Project'
						);
					$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdimportant_project_byuser');
					echo $value->fld_id .'<br/>';
				endif; 
			}
			elseif($value->visible_scope == 'In_Review_project'){
				$count = $this->mastermodel->count_rows('bdinreview_project_byuser', array('project_id' => $value->id));
				if ($count < 1):
					$IncrtArr = array(
							'project_id' => $value->id,
							'user_id' => $this->session->userdata('uid'),
							'action_date' => date('Y-m-d H:i:s'),
							'actionIP' => get_client_ip(),
							'message' => 'In Review Project'
						);
					$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdinreview_project_byuser');
					echo $value->fld_id .'<br/>';
				endif; 
			}
			elseif($value->visible_scope == 'To_Go_project'){
				$count = $this->mastermodel->count_rows('bdtogo_project_byuser', array('project_id' => $value->id));
				if ($count < 1):
					$IncrtArr = array(
							'project_id' => $value->id,
							'user_id' => $this->session->userdata('uid'),
							'action_date' => date('Y-m-d H:i:s'),
							'actionIP' => get_client_ip(),
							'message' => 'Ongoing Bidding Project'
						);
					$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdtogo_project_byuser');
					echo $value->fld_id .'<br/>';
				endif; 
			}
			elseif($value->visible_scope == 'Bid_project'){
				$count = $this->mastermodel->count_rows('bdbid_project_byuser', array('project_id' => $value->id));
				if ($count < 1):
					$IncrtArr = array(
							'project_id' => $value->id,
							'user_id' => $this->session->userdata('uid'),
							'action_date' => date('Y-m-d H:i:s'),
							'actionIP' => get_client_ip(),
							'message' => 'Bid Project'
						);
					$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdbid_project_byuser');
					echo $value->fld_id .'<br/>';
				endif; 
			}
		}
	}
	
	
	// Third Cron 
	public function insertdeletebdscope(){
		$this->db->select('parent_tbl_id,visible_scope');
		$this->db->from('tenderdetails_dump');
		$res = $this->db->get()->result_object();
		//echo '<pre>'; print_r($res);
		foreach ($res as $value) {
			$count = $this->mastermodel->count_rows('bdtender_scopedump', array('project_id' => $value->parent_tbl_id));
			if ($count < 1):
				$IncrtArr = array(
					'project_id' => $value->parent_tbl_id,
					'visible_scope' => $value->visible_scope,
					'businessunit_id' => 1,
					);
				$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdtender_scopedump');
				echo $value->parent_tbl_id .'<br/>';
			endif;
		}
	}
	
	// Fourth Cron 
	public function insertdeleteProjectHistory(){
		$arr = array('InActive_project', 'Not_Important_project', 'No_Go_project', 'No_Submit_project');
		$this->db->select('id,project_id,visible_scope');
		$this->db->from('bdtender_scope');
		$this->db->where_in('visible_scope', $arr);
		
		$res = $this->db->get()->result_object();
		foreach ($res as $value) {
			if($value->visible_scope == 'InActive_project'){
				$count = $this->mastermodel->count_rows('bdinactive_project_byuser', array('project_id' => $value->id));
				if ($count < 1):
					$IncrtArr = array(
							'project_id' => $value->id,
							'user_id' => $this->session->userdata('uid'),
							'action_date' => date('Y-m-d H:i:s'),
							'actionIP' => get_client_ip(),
							'message' => 'InActive Project'
						);
					$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdinactive_project_byuser');
					echo $value->fld_id .'<br/>';
				endif; 
			}
			elseif($value->visible_scope == 'Not_Important_project'){
				$count = $this->mastermodel->count_rows('bdnotimp_project_byuser', array('project_id' => $value->id));
				if ($count < 1):
					$IncrtArr = array(
							'project_id' => $value->id,
							'user_id' => $this->session->userdata('uid'),
							'action_date' => date('Y-m-d H:i:s'),
							'actionIP' => get_client_ip(),
							'message' => 'Not Important Project'
						);
					$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdnotimp_project_byuser');
					echo $value->fld_id .'<br/>';
				endif; 
			}
			elseif($value->visible_scope == 'No_Go_project'){
				$count = $this->mastermodel->count_rows('bdnogo_project_byuser', array('project_id' => $value->id));
				if ($count < 1):
					$IncrtArr = array(
							'project_id' => $value->id,
							'user_id' => $this->session->userdata('uid'),
							'action_date' => date('Y-m-d H:i:s'),
							'actionIP' => get_client_ip(),
							'message' => 'Nogo Project'
						);
					$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdnogo_project_byuser');
					echo $value->fld_id .'<br/>';
				endif; 
			}
			elseif($value->visible_scope == 'No_Submit_project'){
				$count = $this->mastermodel->count_rows('bdnotsubmit_project_byuser', array('project_id' => $value->id));
				if ($count < 1):
					$IncrtArr = array(
							'project_id' => $value->id,
							'user_id' => $this->session->userdata('uid'),
							'action_date' => date('Y-m-d H:i:s'),
							'actionIP' => get_client_ip(),
							'message' => 'Not Submit Project'
						);
					$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdnotsubmit_project_byuser');
					echo $value->fld_id .'<br/>';
				endif; 
			}
		}
	}
	
	
	
	
	
	
	
	
	
	// First 
	public function insertDumpData(){
		$arr = array('InActive_project', 'Not_Important_project', 'No_Go_project', 'No_Submit_project');
		$this->db->select('*');
		$this->db->from('bd_tenderdetails');
		$this->db->where_in('visible_scope', $arr);
		
		$res = $this->db->get()->result_object();
		foreach ($res as $value) {
			$count = $this->mastermodel->count_rows('bdtenderdetails_dump', array('parent_tbl_id' => $value->fld_id));
			if ($count < 1):
				$IncrtArr = array(
					'created_By' => $value->created_By,
					'tender24_ID' => $value->tender24_ID,
					'TenderDetails' => $value->TenderDetails,
					'TenderValue' => $value->TenderValue,
					'Location' => $value->Location,
					'Organization' => $value->Organization,
					'Tender_url' => $value->Tender_url,
					'Sector_IDs' => $value->Sector_IDs,
					'Sector_Keyword_IDs' => $value->Sector_Keyword_IDs,
					'Keyword_IDs' => $value->Keyword_IDs,
					'keyword_phase' => $value->keyword_phase,
					'Created_Date' => $value->Created_Date,
					'Expiry_Date' => $value->Expiry_Date,
					'source_file' => $value->source_file,
					'visible_scope' => $value->visible_scope,
					'project_type' => $value->project_type,
					'country' => $value->country,
					'remarks' => $value->remarks,
					'bid_validity' => $value->bid_validity,
					'national_intern' => $value->national_intern,
					'national_status' => $value->national_status,
					'parent_tbl_id' => $value->fld_id
					
				);
				$respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdtenderdetails_dump');
				echo $value->fld_id .'<br/>';
			endif;
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */