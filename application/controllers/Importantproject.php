<?php

/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Importantproject extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Importantproject_model', 'importantproject');
        $this->load->model('Front_model');
        $this->load->model('Mastermodel');
        $this->load->model('SecondDB_model');
        $this->load->model('Projectplanning_model');
        // $this->load->helper(array('form', 'url', 'user_helper'));
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }

        $bduserid = $this->session->userdata('loginid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";

        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):

            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }



     
   
    //Dashbord After Login..
    public function index() {

        $title = 'Important Project';
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';

        $this->load->view('importantproject/tender_view', compact('title', 'secId', 'sectorArr'));
    }

    // Project Display
    public function newProjectAll() {

        $list = $this->importantproject->get_datatables();

        // print_r($this->db->last_query()); echo "test"; die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view = '';
        $action = '';
        foreach ($list as $importantproject) {
            $count = $this->Mastermodel->count_rows('comment_on_project', array('project_id' => $importantproject->fld_id, 'is_active' => '1'));
            if ($count < 1):
                //$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $importantproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp';
                $comment = '';
            else:
                //$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $importantproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp&nbsp';
                $comment = '';
            endif;
            /* if ($bdRole == 1 || $bdRole == 2) {
              // $view = 	'<i id="faction" title="Comment" onclick="gocomment(' . "'" . $importantproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp'.
              // '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('importantproject/projurlview?projID=' . $importantproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp';
              $view = '<i id="faction" title="Go To Review" onclick="setreview(' . "'" . $importantproject->fld_id . "'" . ')" class="glyphicon glyphicon-share-alt"></i>&nbsp&nbsp' .
              '<a href="#"><i title="Details" style="cursor:pointer" onclick="window.open(' . "'" . base_url('importantproject/projectconcerndetails?projID=' . $importantproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')">Details</i></a>&nbsp&nbsp' .
              '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $importantproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp' .
              '<a href="#"><i title="View" style="cursor:pointer" onclick="window.open(' . "'" . base_url('importantproject/projurlview?projID=' . $importantproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i></a>&nbsp&nbsp' .
              '<a  href="javascript:void(0)" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $importantproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i>' . '&nbsp;&nbsp';
              } else { */

                /* '<a href="#"><i style="cursor:pointer" title="To Be Submitted" onclick="togoproj(' . "'" . $importantproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-share-alt"></i></a>&nbsp&nbsp' . */
            // $action = '<i id="faction" title="Go To Review" onclick="setreview(' . "'" . $importantproject->fld_id . "'" . ')" class="glyphicon glyphicon-share-alt"></i>&nbsp&nbsp' .
            //         '<a href="#"><i title="Details" style="cursor:pointer" onclick="window.open(' . "'" . base_url('importantproject/projectconcerndetails?projID=' . $importantproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')">Details</i></a>&nbsp&nbsp' .
            //         '<a href="#"><i id="faction" title="No Go / Not Important" onclick="nogoproj(' . "'" . $importantproject->fld_id . "'" . ')" class="glyphicon glyphicon-ban-circle"></i></a>&nbsp&nbsp' .
            //         $comment .
                    
            //         '<i title="View" style="cursor:pointer" onclick="window.open(' . "'" . base_url('importantproject/projurlview?projID=' . $importantproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp' .
            //         '<a href="#"><a  href="javascript:void(0)" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $importantproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>' . '&nbsp;&nbsp;<input name="actchk[]" value="' . $importantproject->fld_id . '" type="checkbox">';
            
            // <button title="Not Important" class="btn btn-danger btn-sm" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $importantproject->fld_id . "','" . 0 . "'" . ')"><i class="icon-trash"></i></button>. '<button title="Not Important" class="btn btn-danger btn-sm" onclick="setnotimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="icon-trash"></i></button>' . '&nbsp;&nbsp; <label class="fancy-checkbox"><input type="checkbox" name="actchk[]" value="' . $activeproject->fld_id . '"><span></span></label>

            $action =   '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$importantproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
            </button>&nbsp&nbsp<button   class="btn btn-success btn-sm" title="To Go" onclick="togoproj(' . "'" . $importantproject->fld_id . "'" . ')"><i class="fa fa-bookmark" aria-hidden="true"></i></button>&nbsp&nbsp <button   class="btn btn-danger btn-sm" title="No Go / Not Important" onclick="nogoproj(' . "'" . $importantproject->fld_id . "'" . ')"><i class="fa fa-trash" aria-hidden="true"></i></button>&nbsp&nbsp' .
            $comment .
            
                     ' &nbsp;&nbsp;' . '&nbsp;<label class="fancy-checkbox"><input type="checkbox" name="actchk[]" value="' . $importantproject->fld_id . '"><span></span></label>';


            //}

            // <a class="btn btn-warning btn-sm" href="'. base_url('importantproject/projectdetail_view/'.$importantproject->fld_id).'" target="_blank" title=" View Project Detail" alt=" View Project Detail"> <i class="fa fa-list" aria-hidden="true"></i></a>
            $detail = $importantproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($importantproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($importantproject->Expiry_Date));
            $row[] = ucFirst($importantproject->TenderDetails);
            $row[] = $importantproject->Location;
            $row[] = $importantproject->Organization;
            $row[] = $view . $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => $this->importantproject->count_filtered(),
            "recordsFiltered" => $this->importantproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //Link View Details Popup Open Ash..
    public function projurlview() {
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        if ($Rec) {
            $RowDataArr = $Rec->row();
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        $this->load->view('welcome_link', compact('projId', 'hyperlink'));
    }

    //For set To Go Project..
    public function projtogoset() {

          $message =  "Some issues occured !";
            

        if ($_REQUEST['actid']) {
            // $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'], 'actionIP' => get_client_ip());
            // $Respon = $this->Front_model->insertRecord('proj_togo_for_user', $inserArr);

            $inserArr = array(
                'project_id' => $_REQUEST['actid'],
                'user_id' => $this->session->userdata('loginid'),
                'action_date' => date('Y-m-d H:i:s'),
                'actionIP' => get_client_ip(),
                'message' => 'Review Project'
            );

            $Respon = $this->Front_model->insertRecord('bdinreview_project_byuser', $inserArr);
            //print_r($this->db->last_query());
            $respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $_REQUEST['actid']), array('visible_scope' => 'In_Review_project'));
          

           // print_r($this->db->last_query()); die;
        }
        if ($Respon > 0):
            
            ### Need to Discuss ###
            //$this->visibilityStatus('To_Go_project', $_REQUEST['actid']);
            //Notification Status..
            //$this->notified($_REQUEST['actid'], 'Tender Marked as To Go.');
            //$this->session->set_flashdata('msg', "Project To Go Success. ");

                
                $message = "Project To Go Success.";

                


        endif;

       

            $output = array(

            "msg" => $message,
            );
            echo json_encode($output);
    }

    //For No Go Project set..
    public function projnogoset() {
        if ($_REQUEST['actid']) {
            $businessID = $this->session->userdata('businessunit_id');
            $id = $_REQUEST['actid'];
            $data = $this->Front_model->getProjectDetail($id);
            //echo '<pre>'; print_r($data); 
            if ($data) {
                $inserArr = array(
                    'created_By' => $data->created_By,
                    'tender24_ID' => $data->tender24_ID,
                    'TenderDetails' => $data->TenderDetails,
                    'Location' => $data->Location,
                    'Organization' => $data->Organization,
                    'Tender_url' => $data->Tender_url,
                    'Sector_IDs' => $data->Sector_IDs,
                    'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
                    'Keyword_IDs' => $data->Keyword_IDs,
                    'keyword_phase' => $data->keyword_phase,
                    'Created_Date' => $data->Created_Date,
                    'Expiry_Date' => $data->Expiry_Date,
                    'source_file' => $data->source_file,
                    'project_type' => $data->project_type,
                    'country' => $data->country,
                    'remarks' => $data->remarks,
                    'national_intern' => $data->national_intern,
                    'national_status' => $data->national_status,
                    'parent_tbl_id' => $data->project_id,
                );

                $insertnogoArr = array(
                    'project_id' => $data->project_id,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'No Go Project'
                );

                $inserscopedumpArr = array(
                    'project_id' => $data->project_id,
                    'visible_scope' => 'No_Go_project',
                    'businessunit_id' => $data->businessunit_id
                );
            }
            $count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
            if ($count <= 5 AND $count > 1):
                $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                $Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
                $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
            else:
                $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                $Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
                $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
                $res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id));
            endif;

            // if ($Respon > 0):
            //     $this->session->set_flashdata('msg', "Project Status  Changed No Go / Not Important. ");
            // endif;

            //set to json encode

            if ($Respon > 0) {
                $message = "Project Status  Changed No Go / Not Important.";
    
            }else{
                $message =  "Some issues occured !";
            }
                
            $output = array(
        
                "msg" => $message,
            );
            echo json_encode($output);



        }

        /*  if ($_REQUEST['actid']) {
          $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'], 'actionIP' => get_client_ip());
          $Respon = $this->Front_model->insertRecord('proj_nogo_for_user', $inserArr);
          }
          if ($Respon > 0):
          $this->visibilityStatus('No_Go_project', $_REQUEST['actid']);
          $this->notified($_REQUEST['actid'], 'Tender Status Changed as No Go / Not Important.');
          $this->session->set_flashdata('msg', "Project Status  Changed No Go / Not Important. ");
          endif; */
    }

    //Go To Trash..
    public function gototrashbyuser() {
        if ($_REQUEST['delid']) {
            $businessID = $this->session->userdata('businessunit_id');
            $id = $_REQUEST['delid'];
            $data = $this->Front_model->getProjectDetail($id);
            $message =  "Some issues occured !";
            //echo '<pre>'; print_r($data); 
            if ($data) {
                $inserArr = array(
                    'created_By' => $data->created_By,
                    'tender24_ID' => $data->tender24_ID,
                    'TenderDetails' => $data->TenderDetails,
                    'Location' => $data->Location,
                    'Organization' => $data->Organization,
                    'Tender_url' => $data->Tender_url,
                    'Sector_IDs' => $data->Sector_IDs,
                    'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
                    'Keyword_IDs' => $data->Keyword_IDs,
                    'keyword_phase' => $data->keyword_phase,
                    'Created_Date' => $data->Created_Date,
                    'Expiry_Date' => $data->Expiry_Date,
                    'source_file' => $data->source_file,
                    'project_type' => $data->project_type,
                    'country' => $data->country,
                    'remarks' => $data->remarks,
                    'national_intern' => $data->national_intern,
                    'national_status' => $data->national_status,
                    'parent_tbl_id' => $data->project_id,
                );

                $inserinactiveArr = array(
                    'project_id' => $data->project_id,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'InActive Project'
                );

                $inserscopedumpArr = array(
                    'project_id' => $data->project_id,
                    'visible_scope' => 'InActive_project',
                    'businessunit_id' => $data->businessunit_id
                );
            }
            $count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $_REQUEST['delid'], 'businessunit_id' => $businessID));
            if ($count <= 5 AND $count > 1):
                $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
               
                $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                $Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
                $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
            else:
                $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                $Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
                $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
                $res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id));
            endif;

            if ($Respon > 0):
                //$this->session->set_flashdata('msg', "Tender Inactivated.");
                $message = "Tender Inactivated.";
            endif;
        }

        
      
            $output = array(

            "msg" => $message,
            );
            echo json_encode($output);
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

    //Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            $this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

    //Set As Important and Not Important With check box...
    public function importantprojectbycheckbox() {
        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];

        //Set As Important Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "Important")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('import_for_user', $inserArr);
                $this->visibilityStatus('Important_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, "$countvar Tender Marked as Important.");
            $this->session->set_flashdata('msg', "$countvar Tender Mark as a Important Success.");
            redirect(base_url('/activeproject'));
        endif;
        //Set As Not Important Process.. 
        if (count($chkBoxArr) > 0 && ($actTyle == "Notimportant")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('notimport_for_user', $inserArr);
                $this->visibilityStatus('Not_Important_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, " $countvar Tender Marked as Not Important.");

            $this->session->set_flashdata('msg', "$countvar Tender Marked as a Not Important.");
            redirect(base_url('/activeproject'));
        endif;
    }

    //Set As To Go and No Go With check box...
    /* public function togonogobycheckbox() {
      $chkBoxArr = $this->input->post('actchk');
      $actTyle = $this->input->post('act_type');
      $secId = $_REQUEST['secId'];
      //Set As Important Process..
      if (count($chkBoxArr) > 0 && ($actTyle == "Togo")):
      $countvar = 0;
      foreach ($chkBoxArr as $tndrID) {
      $inserArr = array(
      'user_id' => $this->session->userdata('loginid'),
      'project_id' => $tndrID,
      'actionIP' => get_client_ip());
      $Respon = $this->Front_model->insertRecord('proj_togo_for_user', $inserArr);
      $this->visibilityStatus('To_Go_project', $tndrID);
      $countvar ++;
      }
      $this->notified($tndrID, "$countvar Tender Marked as To Go.");
      $this->session->set_flashdata('msg', "$countvar Tender Mark as a To Go Success.");
      redirect(base_url('/importantproject'));
      endif;
      //Set As Not Important Process..
      if (count($chkBoxArr) > 0 && ($actTyle == "Nogo")):
      $countvar = 0;
      foreach ($chkBoxArr as $tndrID) {
      $inserArr = array(
      'user_id' => $this->session->userdata('loginid'),
      'project_id' => $tndrID,
      'actionIP' => get_client_ip());
      $Respon = $this->Front_model->insertRecord('notimport_for_user', $inserArr);
      $this->visibilityStatus('proj_nogo_for_user', $tndrID);
      $countvar ++;
      }
      $this->notified($tndrID, " $countvar Tender Marked as No Go.");
      $this->session->set_flashdata('msg', "$countvar Tender Marked as a No Go.");
      redirect(base_url('/importantproject'));
      endif;
      } */


    public function togonogobycheckbox() {
        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];
        //Set As Important Process..
        $businessID = $this->session->userdata('businessunit_id');
        if (count($chkBoxArr) > 0 && ($actTyle == "Togo")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'project_id' => $tndrID,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'Review Project'
                );

                $Respon = $this->Front_model->insertRecord('bdinreview_project_byuser', $inserArr);
                $respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $tndrID, 'businessunit_id' => $businessID), array('visible_scope' => 'In_Review_project'));
                $countvar ++;
            }
            $this->session->set_flashdata('msg', "$countvar Tender Mark as a To Go Success.");
            redirect(base_url('/importantproject'));
        endif;
        //Set As Not Important Process.. 
        if (count($chkBoxArr) > 0 && ($actTyle == "Nogo")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('notimport_for_user', $inserArr);
                
                $this->visibilityStatus('proj_nogo_for_user', $tndrID);
                 
               

                $countvar ++;


                $id = $tndrID;
                $data = $this->Front_model->getProjectDetail($id);
                if ($data) {
                    $inserArr = array(
                        'created_By' => $data->created_By,
                        'tender24_ID' => $data->tender24_ID,
                        'TenderDetails' => $data->TenderDetails,
                        'Location' => $data->Location,
                        'Organization' => $data->Organization,
                        'Tender_url' => $data->Tender_url,
                        'Sector_IDs' => $data->Sector_IDs,
                        'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
                        'Keyword_IDs' => $data->Keyword_IDs,
                        'keyword_phase' => $data->keyword_phase,
                        'Created_Date' => $data->Created_Date,
                        'Expiry_Date' => $data->Expiry_Date,
                        'source_file' => $data->source_file,
                        'project_type' => $data->project_type,
                        'country' => $data->country,
                        'remarks' => $data->remarks,
                        'national_intern' => $data->national_intern,
                        'national_status' => $data->national_status,
                        'parent_tbl_id' => $data->project_id,
                    );

                    $insertnotimpArr = array(
                        'project_id' => $data->project_id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'No Go Project'
                    );

                    $inserscopedumpArr = array(
                        'project_id' => $data->project_id,
                        'visible_scope' => 'No_Go_project',
                        'businessunit_id' => $data->businessunit_id
                    );
                }
                $count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));

                if ($count <= 5 AND $count > 1):
                    $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                    $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                    $Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnotimpArr);
                    $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
                else:
                    $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                    $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                    $Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnotimpArr);
                    $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
                    $res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id));
                endif;



                $countvar ++;
            }
            $this->notified($tndrID, " $countvar Tender Marked as No Go.");
            $this->session->set_flashdata('msg', "$countvar Tender Marked as a No Go.");
            redirect(base_url('/importantproject'));
        endif;
    }

     

    public function newtoGonoGoProject() {
        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];
        $message =  "Some issues occured !";
        //Set As Important Process..
        $businessID = $this->session->userdata('businessunit_id');

      
       
        if (count($chkBoxArr) > 0 && ($actTyle == "Togo")){
            $countvar = 0;

           
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'project_id' => $tndrID,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'Review Project'
                );

                $Respon = $this->Front_model->insertRecord('bdinreview_project_byuser', $inserArr);
                $respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $tndrID, 'businessunit_id' => $businessID), array('visible_scope' => 'In_Review_project'));
                $countvar ++;

                // print_r($this->db->last_query()); die;
            }

            // print_r($chkBoxArr); 
            // $this->session->set_flashdata('msg', "$countvar Tender Mark as a To Go Success.");
            // redirect(base_url('/importantproject'));

            if ($countvar > 0) {
                $message = $countvar ." Tender Marked as To Go.";
    
            }
        }


        // print_r("test"); die;
        //Set As Not Important Process.. 
        if (count($chkBoxArr) > 0 && ($actTyle == "Nogo")){
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $tndrID,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('notimport_for_user', $inserArr);
                //$this->visibilityStatus('proj_nogo_for_user', $tndrID);
                ### Need to Discuss ###
                //$this->Front_model->updateRecord('proj_nogo_for_user', array('visible_scope' => $copeName), array('fld_id' => $tndrID));
                $countvar ++;


                $id = $tndrID;
                $data = $this->Front_model->getProjectDetail($id);
                if ($data) {
                    $inserArr = array(
                        'created_By' => $data->created_By,
                        'tender24_ID' => $data->tender24_ID,
                        'TenderDetails' => $data->TenderDetails,
                        'Location' => $data->Location,
                        'Organization' => $data->Organization,
                        'Tender_url' => $data->Tender_url,
                        'Sector_IDs' => $data->Sector_IDs,
                        'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
                        'Keyword_IDs' => $data->Keyword_IDs,
                        'keyword_phase' => $data->keyword_phase,
                        'Created_Date' => $data->Created_Date,
                        'Expiry_Date' => $data->Expiry_Date,
                        'source_file' => $data->source_file,
                        'project_type' => $data->project_type,
                        'country' => $data->country,
                        'remarks' => $data->remarks,
                        'national_intern' => $data->national_intern,
                        'national_status' => $data->national_status,
                        'parent_tbl_id' => $data->project_id,
                    );

                    $insertnotimpArr = array(
                        'project_id' => $data->project_id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'No Go Project'
                    );

                    $inserscopedumpArr = array(
                        'project_id' => $data->project_id,
                        'visible_scope' => 'No_Go_project',
                        'businessunit_id' => $data->businessunit_id
                    );
                }
                $count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));

                if ($count <= 5 AND $count > 1):
                    $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                    $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                    $Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnotimpArr);
                    $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
                else:
                    $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                    $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                    $Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnotimpArr);
                    $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
                    $res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id));
                endif;



                $countvar ++;
            }
             

            // $this->notified($tndrID, $countvar." Tender Marked as No Go.");
            // $this->session->set_flashdata('msg', "$countvar Tender Marked as a No Go.");
            // redirect(base_url('/importantproject'));
            if ($countvar > 0) {
                $message = $countvar ." Tender Marked as No Go.";
    
            }
        }

        $output = array(
        
            "msg" => $message,
        );


        echo json_encode($output);
    }

   

    //Review On Project...
    public function reviewproject() {
        if ($_REQUEST['actid']) {
            $businessID = $this->session->userdata('businessunit_id');
            $inserArr = array(
                'project_id' => $_REQUEST['actid'],
                'user_id' => $this->session->userdata('loginid'),
                'action_date' => date('Y-m-d H:i:s'),
                'actionIP' => get_client_ip(),
                'message' => 'Review Project'
            );

            $Respon = $this->Front_model->insertRecord('bdinreview_project_byuser', $inserArr);
            $respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $_REQUEST['actid'], 'businessunit_id' => $businessID), array('visible_scope' => 'In_Review_project'));
        }
        if ($Respon > 0):
            $this->session->set_flashdata('msg', "Tender Submitted into Review.");
        endif;

        /* if ($_REQUEST['actid']) {
          $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'], 'actionIP' => get_client_ip());
          $Respon = $this->Front_model->insertRecord('proj_review_for_user', $inserArr);
          }
          if ($Respon > 0):
          $this->visibilityStatus('In_Review_project', $_REQUEST['actid']);
          $this->notified($_REQUEST['actid'], 'Tender Review Submitted.');
          $this->session->set_flashdata('msg', "Tender Submitted into Review.");
          endif; */
    }

    //Submit Project Concern Details..
    public function projectconcerndetails() {
        //Get Project Id..
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('project_concern_details', array('concern_details'), array('project_id' => $projId));
        if ($Rec != '') {
            $data = $Rec->result();
        } else {
            $data[0] = '';
        }
        $this->load->view('welcome_message', compact('projId', 'data'));
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('loginid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('loginid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

    public function projectdetail_view() {
        $projectID = $this->uri->segment(3);
        $data = '';
        $data = $this->Front_model->getprojectdetailByProjectId($projectID);
        $this->db->select('*');
        $this->db->from('bd_moved_eoi');
        $this->db->where('new_project_id', $projectID);
        $movedEoiStatus = $this->db->get()->result();
        $moved_eoi_status = $movedEoiStatus[0];
         $cegcompcomp = $this->Mastermodel->SelectRecordFldNew('cegexp_competitor_comp', array('project_id' => $projectID, 'status' => '1'));
        $teamdata = $this->Projectplanning_model->getuserDetailByexpidNew($projectID);
        
        $this->load->view('projectdetail/projectdetail_view', compact('teamdata', 'cegcompcomp', 'data', 'moved_eoi_status'));
    }

    public function usercommentonproject() {
        if ($_REQUEST['user_comment']) {
            $recArr = array('user_id' => $this->session->userdata('loginid'),
                'project_id' => $_REQUEST['comment_id'],
                'commentText' => $_REQUEST['user_comment'],
            );
            $this->Mastermodel->InsertMasterData($recArr, 'comment_on_project');
        }
        redirect(base_url('importantproject/projectdetail_view/' . $_REQUEST['comment_id']));
    }

    public function projectdesc() {
        if ($_REQUEST['editor1']) {
            $projId = $_REQUEST['desc_id'];
            $projDetails = $_REQUEST['editor1'];
            $inserArr = array('user_id' => $this->session->userdata('loginid'),
                'project_id' => $projId,
                'concern_details' => $projDetails,
                'link' => '');
            $Respon = $this->Front_model->insertRecord('Project_concern_details', $inserArr);
        }
        redirect(base_url('importantproject/projectdetail_view/' . $_REQUEST['desc_id']));
    }

    public function projectdocumentupload() {
        //echo '<pre>'; print_r($_REQUEST); print_r($_FILES['docfile']); 
        if ($_FILES['docfile']['name']) {
            $filesCount = count($_FILES['docfile']['name']);
            for ($i = 0; $i <= $filesCount; $i++) {
                $_FILES['file']['name'] = time() . '__' . $_FILES['docfile']['name'][$i];
                $_FILES['file']['type'] = $_FILES['docfile']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['docfile']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['docfile']['error'][$i];
                $_FILES['file']['size'] = $_FILES['docfile']['size'][$i];

                // File upload configuration
                $uploadPath = 'uploads/projectdoc/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'doc|DOC|docx|pdf|PDF|xlsx|xls|jpg|jpeg|JPG|JPEG|png|PNG';

                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                // Upload file to server
                if ($this->upload->do_upload('file')) {
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $uploadData[$i]['project_id'] = $_REQUEST['proj_id'];
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['entryby'] = $this->session->userdata('loginid');
                }
            }

            if (!empty($uploadData)) {
                $insert = $this->db->insert_batch('projectdocument', $uploadData);
            }
            redirect(base_url('importantproject/projectdetail_view/' . $_REQUEST['proj_id']));
        }
    }

    public function getUpload($folder, $filename) {
        $config = array();
        $config['upload_path'] = $folder;
        $config['allowed_types'] = 'doc|DOC|docx|pdf|PDF';
        $config['max_size'] = '900000'; //90MB
        //$config['file_name'] = "news_" . time();
        $config['file_name'] = $filename;
        $config['overwrite'] = true;
        return $config;
    }

    public function removeproject() {
        $id = $this->uri->segment(3);
        $projectID = $this->uri->segment(4);
        if ($id) {
            $this->db->select('file_name');
            $this->db->from('projectdocument');
            $this->db->where('id', $id);

            $res = $this->db->get()->result();
            if ($res) {
                unlink(FCPATH . "uploads/projectdoc/" . $res[0]->file_name);
                $this->db->delete('projectdocument', array('id' => $id));
            }
        }
        redirect(base_url('importantproject/projectdetail_view/' . $projectID));
    }

    public function reminderinfo() {
        if ($_REQUEST['reminder_date'] && $_REQUEST['reminder_subject'] && $_REQUEST['reminder_message']) {
            $_REQUEST['entryby'] = $this->session->userdata('loginid');
            $res = $this->Front_model->insertRecord('reminderinfo', $_REQUEST);
        }
        if ($res):
            $data['arr'] = $_REQUEST;
            $data['mailby'] = $this->SecondDB_model->getUserByID($this->session->userdata('loginid'));
            $data['tenderdetail'] = $this->Mastermodel->SelectRecordSingle('bd_tenderdetail', array('fld_id' => $_REQUEST['project_id']));
            $msgDetails = $this->load->view('email_template/reminder_email', $data, true);
            $emailsAddress = $this->Front_model->ReminderEmail();
            //$emailArrStr77 = implode(",", $emailsAddress);
            $emailArrStr77 = 'bdcru@cegindia.com';
            sendMail("$emailArrStr77", $_REQUEST['reminder_subject'], $msgDetails);
            //sendMail("jitendra00752@gmail.com", $_REQUEST['reminder_subject'], $msgDetails);
            $this->session->set_flashdata('msg', "Reminder Successfully Added.");
        else:
            $this->session->set_flashdata('msg', "Something Went Wrong.");
        endif;
        redirect(base_url('importantproject/projectdetail_view/' . $_REQUEST['project_id']));
    }

    public function ajax_reminderinfo() {
        $client_Arr = $this->Mastermodel->SelectRecord('reminderinfo', array('project_id' => $_REQUEST['project_id']));
        if ($client_Arr) {
            echo json_encode($client_Arr);
        } else {
            return false;
        }
    }

    public function ajax_reminderdel() {
        $id = $this->uri->segment(3);
        $projid = $this->uri->segment(4);
        if ($id) {
            $this->db->delete('reminderinfo', array('id' => $id));
        }
        redirect(base_url('importantproject/projectdetail_view/' . $projid));
    }

}
