<?php

/* Database connection start This Controller Create By Asheesh */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Pdfsort extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Front_model');
        $this->load->model('mastermodel');
        $this->load->model('Pdfsort_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('session', 'form_validation');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
    }

    public function pdfsortdata() {
        $this->load->view('pdfsort/list_view');
    }

    //CEG Exp List data Table..
    public function expListAll() {
        $list = $this->Pdfsort_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $comprow) {
            $arr = $this->session->userdata('exp_id');
            if (in_array($comprow->fld_id, $arr)) {
                $checked = 'checked';
            } else {
                $checked = '';
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->project_name;
            $row[] = $comprow->service;
            $row[] = $comprow->client;
            $row[] = $comprow->start_year;
            $row[] = $comprow->sector;
            $row[] = $comprow->state;
            $row[] = '<input id="actchkbox" ' . $checked . ' name="actchk[]" value="' . $comprow->fld_id . '" type="checkbox">&nbsp&nbsp';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Pdfsort_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function selectedData() {
        $ids = array_unique($_REQUEST['actchk']);
        if (!empty($ids)) {
            if ($this->session->userdata('exp_id')) {
                $arr = $this->session->userdata('exp_id');
                $arrid = array_merge($arr, $ids);
                $this->session->set_userdata('exp_id', $arrid);
            } else {
                $this->session->set_userdata('exp_id', $ids);
            }
        }
        redirect(base_url('pdfsort/pdfsortdata/'));
    }

    public function sorting() {
        $arr = $this->session->userdata('exp_id');
        $projID = array_unique($arr);
        if (!empty($projID)) {
            if (!empty($projID)) {
                foreach ($projID as $ids) {
                    $this->db->select('*');
                    $this->db->from('ceg_exp');
                    $this->db->where(array('fld_id' => $ids));
                    $data[] = $this->db->get()->result_array();
                }
            }
            $sector = $this->Front_model->GetActiveSector();
        }
        $this->load->view('pdfsort/pdf_view', compact('data', 'sector'));
    }

    public function sortData() {
        $actno = $_POST['actchk'];
        $data = array();
        foreach ($actno as $val) {
            $checkData[$val] = $_REQUEST[$val];
        }
        asort($checkData);
        foreach ($checkData as $key => $value) {
            $projID[] = $key;
        }

        if (!empty($projID)) {
            foreach ($projID as $ids) {
                $this->db->select('*');
                $this->db->from('ceg_exp');
                $this->db->where(array('fld_id' => $ids, 'status' => '1', 'proj_status' => 'won'));
                $data[] = $this->db->get()->result_array();
            }
            $this->session->unset_userdata('exp_id');
        }
        $this->load->view('pdfreport/pdf_view', compact('data'));
    }

    public function delete_ajax() {
        $arr = $this->session->userdata('exp_id');
        $id = $_REQUEST['favorite1'];
        if ($id) {
            $arr1 = array();
            foreach ($arr as $key => $val) {
                if (in_array($val, $id)) {
                    unset($arr[$key]);
                } else {
                    $arr1[] = $val;
                }
            }
        }
        $this->session->set_userdata('exp_id', $arr1);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */