<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tender extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('tender_model');
        $this->load->model('Front_model');
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
    }

// Active Tenders List
    public function add_tender_new() {
        $title = 'Add Tender';
        $activesectArr = $this->GetActiveSector();
		// print_r($activesectArr); die;
        $this->session->userdata('loginid');
        $this->load->view('tender_add_view', compact('activesectArr', 'title'));
    }

    //code by durgesh (10-12-2019)(For tender manual add form view)
    public function tender_add_manaul() {
        $title = 'Add Tender Manaul';
        $activesectArr = $this->GetActiveSector();
        $this->load->view('tender_manaul_add', compact('activesectArr', 'title'));
    }

    //code by durgesh (10-12-2019)(For tender manual add/insert query into main table of tender add)
    public function tender_added_manaul() {
        $ArrRec = $_POST;
        if (count($ArrRec) > 3) {
            if (count($_REQUEST['tndrphase']) > 0):
                $paseNameString = implode(",", $_REQUEST['tndrphase']);
            else:
                $paseNameString = "";
            endif;
            $SectIdString = implode(",", $_REQUEST['tendersector']);

            $insertArr = Array(
                'created_By' => $this->session->userdata('loginid'),
                'tender24_ID' => "BD" . uniqid(),
                'TenderDetails' => strip_tags($ArrRec['tendername']),
                'TenderValue' => $ArrRec['tendervalue'],
                'Location' => $ArrRec['location'],
                'Organization' => $ArrRec['organisation'],
                'Tender_url' => $ArrRec['linkurl'],
                'Sector_IDs' => $SectIdString,
                'Sector_Keyword_IDs' => '',
                'Keyword_IDs' => '',
                'keyword_phase' => $paseNameString,
                'Expiry_Date' => $ArrRec['deadline'],
                'national_intern' => $ArrRec['national_intern']
            );
            if ($ArrRec['tndrType'] == "Manaul_project") {
                $uniq = $this->tender_model->insertRecord('bd_tenderdetail', $insertArr);
                $insertscopeArr = Array(
                    'project_id' => $uniq,
                    'visible_scope' => $ArrRec['tndrType'],
                    'businessunit_id' => 1,
                );
                $uniq1 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr);
                $insertscopeArr1 = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'Manaul_project',
                    'businessunit_id' => 2,
                );
                $uniq2 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr1);

                $inserArrstatus = array(
                    'project_id' => $uniq,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'status_date'=>date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'project_status' => $ArrRec['tender_project_status'],
                );

                $Respon = $this->Front_model->insertRecord('bdproject_status', $inserArrstatus);


                $inserArrsummary = array(
                    'project_id' => $uniq,
                    'entry_date' => date('Y-m-d H:i:s'),
                    'entry_by' => $this->session->userdata('loginid')
                );

                $Responsum = $this->Front_model->insertRecord('bdcegexp_proj_summery', $inserArrsummary);

                $inserArrcegsum = array(
                    'project_id' => $uniq,
                    'entry_by' => $this->session->userdata('loginid')
                );

                $Responcegsum = $this->Front_model->insertRecord('bdceg_exp', $inserArrcegsum);

                $insertArrgeneratedid = Array('project_id' => $uniq, 'generated_tenderid' => $ArrRec['generated_id'], 'generate_type' => $ArrRec['generated_type'], 'generate_by' => $this->session->userdata('loginid'), 'entrydate' => date('y-m-d h:i:s'));
                $responsegen = $this->tender_model->insertRecord('tender_generated_id', $insertArrgeneratedid);
            }
            $this->session->set_flashdata('success', True);
            redirect(base_url('tender_add_manaul'));
        }
    }

    //Tender 24 Info..
    public function tndrtwentyfourcsv() {
        $title = 'Upload Tender24 Csv File';
        $activesectArr = $this->GetActiveSector();
        $this->load->view('tender_tndrtwentyfourupload', compact('title','activesectArr'));
    }

    //Csv Upload Tender Info..
    public function tndrinfocsv() {
        $title = 'Upload Tender info Csv File';
        $activesectArr = $this->GetActiveSector();
        $this->load->view('tender_tndrinfocsvupload', compact('title','activesectArr'));
    }

    //Tender Info CSV Upload..
    public function uploadtenderinfocsv() {
        $allTendrIdsArr = $this->tender_model->alltenderidArr();
        $extIdsArr = array();

        if ($_FILES['tenderinfocsv']['name']) {
            $configThm = $this->getUploadConfig();
            $this->load->library('upload', $configThm);
            $this->upload->initialize($configThm);
            if ($this->upload->do_upload('tenderinfocsv')) {
                $uploadData = $this->upload->data();
                $filepath = $uploadData['full_path'];
                $file = fopen($filepath, 'r');
                $n = 0;
               
                while (($line = fgetcsv($file)) !== FALSE) {

                     
					
                    if (($n > 0) and ( $line[1])) {

                        

                        //  print_r("test"); die;
                        //Process to sector Update...
                        $tenderDetails = str_replace("_x000D_", "<br>", $line[12]);
                        $responArr = $this->getSectorSectKeywordId($tenderDetails);
                        $phseRespArr = $this->getPhseKeywordById($tenderDetails);

                        $idExistance = in_array($line[0], $allTendrIdsArr);
                        //InsertData Array
                        $insertDataArr = array(
                            'created_By' => $this->session->userdata('loginid'),
                            'tender24_ID' => $line[0],
                            'TenderDetails' => str_replace("_x000D_", "<br>", $line[12]),
                            // 'CEG_TenderID' => date('tinfo') . '_' . uniqid(),
                            'Location' => $line[3],
                            'Organization' => ($line[9]) ? $line[9] : '',
                            'Tender_url' => $line[1],
                            'Sector_IDs' => $responArr['Sector_IDsArr'],
                            'Sector_Keyword_IDs' => $responArr['Sector_Keyword_IDs'],
                            'Keyword_IDs' => $phseRespArr['Keyword_IDs'],
                            'keyword_phase' => $phseRespArr['keyword_phase'],
                            'Expiry_Date' => date("d-m-Y", strtotime($line[14])),
                            'source_file' => '1');


                            #### hnadle date #####


                          

                        if ($idExistance != 1):
                            $Resp = $this->tender_model->insertRecord('bd_tenderdetail', $insertDataArr);
                            //Entry in Scope
                            $insert_id = $this->db->insert_id();
                            
                            $tndrScopeCEG = array('project_id' => $insert_id, 'visible_scope' => 'New_project', 'businessunit_id' => '1');
                            $tndrScopeCEGTH = array('project_id' => $insert_id, 'visible_scope' => 'New_project', 'businessunit_id' => '2');
                            $Resp1 = $this->tender_model->insertRecord('bdtender_scope', $tndrScopeCEG);
                            $Resp2 = $this->tender_model->insertRecord('bdtender_scope', $tndrScopeCEGTH);
                           
                            

                        else:
                            array_push($extIdsArr, $line[1]);
                        endif;

                        $tenderDetails = null;
                        $responArr = null;
                        $phseRespArr = null;
                        $insertDataArr = array('');
                    }
                    $n++;
                }

               
                fclose($file);
                
                $this->session->set_flashdata('successfile', "Your Tender info CSV Uploaded Success." . count($extIdsArr) . " No of Exitstence ID : <br>" . json_encode($extIdsArr));
                redirect(base_url('tender/tndrinfocsv'));
            } else {
                $this->session->set_flashdata('errmsg', $this->upload->display_errors());
                redirect(base_url('tender/tndrinfocsv'));
            }
        }

        
        $this->session->set_flashdata('errmsg', $this->upload->display_errors());
        redirect(base_url('tender/tndrinfocsv'));
    }

    //Check Tender Id Existance..
    public function chkTenderIdExistance($tndrId) {
        $tndr = '1';
        $tndr = $this->tender_model->selectRecord('bd_tenderdetail', array('tender24_ID'), array('is_active' => '1', 'tender24_ID' => $tndrId));
        // return $this->tender_model->chkTenderIdExistance($tndrId);
        if ($tndr):
            return true;
        else:
            return false;
        endif;
    }

    //Tendr 24 Upload CSV Details...
    /*  public function uploadtndrtwentyfourcsv() {

      $allTendrIdsArr = $this->tender_model->alltenderidArr();
      $extIdsArr = array();

      if ($_FILES['tenderinfocsv']['name']) {
      $configThm = $this->getUploadConfigfour();
      $this->load->library('upload', $configThm);
      $this->upload->initialize($configThm);
      if ($this->upload->do_upload('tenderinfocsv')) {
      $uploadData = $this->upload->data();
      $filepath = $uploadData['full_path'];
      $file = fopen($filepath, 'r');
      $n = 0;

      while (($line = fgetcsv($file)) !== FALSE) {
      if (($n > 1) and ( $line[1])) {
      //Process to sector Update...
      $tenderDetails = str_replace("_x000D_", "<br>", $line[2]);
      $responArr = $this->getSectorSectKeywordId($tenderDetails);
      $phseRespArr = $this->getPhseKeywordById($tenderDetails);

      $idExistance = in_array($line[1], $allTendrIdsArr);
      // $idExistance = $this->chkTenderIdExistance($line[1]);
      $insertDataArr = array(
      'created_By' => $this->session->userdata('loginid'),
      'tender24_ID' => $line[1],
      'TenderDetails' => $tenderDetails,
      'CEG_TenderID' => date('t24') . '_' . uniqid(),
      'TenderValue' => $line[3],
      'Location' => $line[5],
      'Organization' => ($line[6]) ? $line[6] : '',
      'Tender_url' => '',
      'Sector_IDs' => $responArr['Sector_IDsArr'],
      'Sector_Keyword_IDs' => $responArr['Sector_Keyword_IDs'],
      'Keyword_IDs' => $phseRespArr['Keyword_IDs'],
      'keyword_phase' => $phseRespArr['keyword_phase'],
      'is_active' => '1',
      'Expiry_Date' => $line[4],
      'source_file' => '2',
      'important' => '0');

      if ($idExistance != 1):
      $Resp = $this->tender_model->insertRecord('bd_tenderdetails', $insertDataArr);
      else:
      array_push($extIdsArr, $line[1]);
      endif;

      $tenderDetails = null;
      $responArr = null;
      $phseRespArr = null;
      $insertDataArr = array('');
      }
      $n++;
      }
      }
      } else {
      $this->session->set_flashdata('errmsg', $this->upload->display_errors());
      redirect(base_url('tender/tndrtwentyfourcsv'));
      }

      //Url CSv  Link / Url ...
      if ($_FILES['tenderlink']['name']) {
      $configThm = $this->getUploadConfigfourlink();
      $this->load->library('upload', $configThm);
      $this->upload->initialize($configThm);
      if ($this->upload->do_upload('tenderlink')) {
      $uploadData = $this->upload->data();
      $filepath = $uploadData['full_path'];
      $file = fopen($filepath, 'r');
      $n = 0;
      while (($line = fgetcsv($file)) !== FALSE) {
      if (($n > 1) and ( $line[1])) {
      //Update Url On Tender..
      $this->tender_model->updateRecord('bd_tenderdetails', array('Tender_url' => $line[2]), array('tender24_ID' => $line[1]));
      }
      $n++;
      }
      }

      $this->session->set_flashdata('successfile', "Your Tender 24 Csv Uploaded Seccess. " . count($extIdsArr) . " No of Exitstence ID : <br>" . json_encode($extIdsArr));
      redirect(base_url('tender/tndrtwentyfourcsv'));
      }
      } */


    public function uploadtndrtwentyfourcsv() {

        $allTendrIdsArr = $this->tender_model->alltenderidArr();
		//  print_r($allTendrIdsArr); die;
        $extIdsArr = array();

        if ($_FILES['tenderinfocsv']['name']) {

           
            $configThm = $this->getUploadConfigfour();
            
            $this->load->library('upload', $configThm);
            $this->upload->initialize($configThm);

           
            if ($this->upload->do_upload('tenderinfocsv')) {
               
                $uploadData = $this->upload->data();
                $filepath = $uploadData['full_path'];
                $file = fopen($filepath, 'r');
                $n = 0;

                while (($line = fgetcsv($file)) !== FALSE) {
					// echo "<pre>"; print_r($line); die;	
                    if (($n > 1) and ( $line[1])) {
                        //Process to sector Update...
                        $tenderDetails = str_replace("_x000D_", "<br>", $line[2]);
                        
						// echo $tenderDetails; die;
						$responArr = $this->getSectorSectKeywordId($tenderDetails);
                        $phseRespArr = $this->getPhseKeywordById($tenderDetails);
						// echo "<pre>"; print_r($line[1]); die; 31538908
                        $idExistance = in_array($line[1], $allTendrIdsArr);
                        // $idExistance = $this->chkTenderIdExistance($line[1]);
                        $insertDataArr = array(
                            'created_By' => $this->session->userdata('loginid'),
                            'tender24_ID' => $line[1],
                            'TenderDetails' => $tenderDetails,
                            //'TenderValue' => $line[3],
                            'Location' => $line[5],
                            'Organization' => ($line[6]) ? $line[6] : '',
                            'Tender_url' => '',
                            'Sector_IDs' => $responArr['Sector_IDsArr'],
                            'Sector_Keyword_IDs' => $responArr['Sector_Keyword_IDs'],
                            'Keyword_IDs' => $phseRespArr['Keyword_IDs'],
                            'keyword_phase' => $phseRespArr['keyword_phase'],
                            'Expiry_Date' =>  date("d-m-Y", strtotime($line[4])),
                            'source_file' => '2');


                            ####### hnadle date #####
                           
                        if ($idExistance != 1):
                          
                            $this->tender_model->insertRecord('bd_tenderdetail', $insertDataArr);

                            $Resp = $this->db->insert_id();

                            $tndrScopeCEG = array('project_id' => $Resp, 'visible_scope' => 'New_project', 'businessunit_id' => '1');
                            $tndrScopeCEGTH = array('project_id' => $Resp, 'visible_scope' => 'New_project', 'businessunit_id' => '2');
                            $Resp1 = $this->tender_model->insertRecord('bdtender_scope', $tndrScopeCEG);
                           // print_r($this->db->last_query());

                            $Resp2 = $this->tender_model->insertRecord('bdtender_scope', $tndrScopeCEGTH);
                           // print_r($this->db->last_query()); die;


                        else:
                           
                            array_push($extIdsArr, $line[1]);
                        endif;

                        $tenderDetails = null;
                        $responArr = null;
                        $phseRespArr = null;
                        $insertDataArr = array('');
                    }
                    $n++;
                }
            }
        } else {
            $this->session->set_flashdata('errmsg', $this->upload->display_errors());
            redirect(base_url('tender/tndrtwentyfourcsv'));
        }

        //Url CSv  Link / Url ...
        if ($_FILES['tenderlink']['name']) {

           
            $configThm = $this->getUploadConfigfourlink();

           
            $this->load->library('upload', $configThm);
           
            $this->upload->initialize($configThm);
           
            if ($this->upload->do_upload('tenderlink')) {
               
                $uploadData = $this->upload->data();
                $filepath = $uploadData['full_path'];
                $file = fopen($filepath, 'r');
                $n = 0;

                
                while (($line = fgetcsv($file)) !== FALSE) {
                    
					// print_r($line); die;
                    if (($n > 1) and ( $line[1])) {

                     
						// echo "teswting "; die;
                        //Update Url On Tender..
                        $test= $this->tender_model->updateRecord('bd_tenderdetail', array('Tender_url' => $line[2]), array('tender24_ID' => $line[1]));
                        
					}
                    $n++;
                }
            }

            $this->session->set_flashdata('successfile', "Your Tender 24 Csv Uploaded Seccess. " . count($extIdsArr) . " No of Exitstence ID : <br>" . json_encode($extIdsArr));
            redirect(base_url('tender/tndrtwentyfourcsv'));
        }
    }

    //Function For Filter Sector And Sector Keyword in Tender ash..
    public function getSectorSectKeywordId($tenderDetails) {
        $Sector_IDsArr = '';
        $Sector_Keyword_IDs = '';
        $SectorKey = $this->tender_model->selectRecord('sectorkey', array('*'), array('is_active' => '1'));
        // echo "<pre>"; print_r($SectorKey); die;
		if ($SectorKey):
            $dataArray = $SectorKey->result();
			// echo "<pre>"; print_r($dataArray); die;
        endif;
        foreach ($dataArray as $rowdata):
            $keyname = " " . $rowdata->keyName;
			// echo $keyname.", ".$tenderDetails; die;
            if (strpos($tenderDetails, $keyname) !== false) {
				// echo "tstststs"; die;
                $sectId = $rowdata->sectID;
                $sectKeyId = $rowdata->fld_id;
                $Sector_IDsArr .= "," . $sectId . ",";
                $Sector_Keyword_IDs .= "," . $sectKeyId;
            }
        endforeach;
        return array('Sector_IDsArr' => $Sector_IDsArr, 'Sector_Keyword_IDs' => $Sector_Keyword_IDs);
        // $test =  array('Sector_IDsArr' => $Sector_IDsArr, 'Sector_Keyword_IDs' => $Sector_Keyword_IDs);
		// echo "<pre>"; print_r($test); die;
    }

    //Custom Function For Phse Filter Code By Ash ..
    public function getPhseKeywordById($tenderDetails) {
        $Keyword_IDs = "";
        $keyword_phase = "";
        $PhaseKey = $this->tender_model->selectRecord('bd_keyword_master', array('*'), array('is_active' => '1'));
        if ($PhaseKey):
            $dataPhaseArray = $PhaseKey->result();
        endif;
        foreach ($dataPhaseArray as $prowdata):
            $phaseNm = " " . $prowdata->name;
            if (strpos($tenderDetails, $phaseNm) !== false) {
                $Keyword_IDs .= "," . $prowdata->fld_id;
                $keyword_phase .= "," . $prowdata->phase;
            }
        endforeach;
        return array('Keyword_IDs' => $Keyword_IDs, 'keyword_phase' => $keyword_phase);
    }

    //Copde By Asheesh For Add..
    public function tender_add() {
        $ArrRec = $_REQUEST;
        //echo '<pre>'; print_r($ArrRec); die;
        if (count($ArrRec) > 3) {
            if (count($_REQUEST['tndrphase']) > 0):
                $paseNameString = implode(",", $_REQUEST['tndrphase']);
				// echo $paseNameString; die;
            else:
                $paseNameString = "";
            endif;
            $SectIdString = implode(",", $_REQUEST['tendersector']);

            $insertArr = Array(
                'created_By' => $this->session->userdata('loginid'),
                'tender24_ID' => "BD" . uniqid(),
                'TenderDetails' => strip_tags($ArrRec['editor1']),
                'TenderValue' => $ArrRec['tendervalue'],
                'Location' => $ArrRec['location'],
                'Organization' => $ArrRec['organisation'],
                'Tender_url' => $ArrRec['linkurl'],
                'Sector_IDs' => $SectIdString,
                'Sector_Keyword_IDs' => '',
                'Keyword_IDs' => '',
                'keyword_phase' => $paseNameString,
                'Expiry_Date' =>  date("d-m-Y", strtotime($ArrRec['deadline'])),
                'national_intern' => $ArrRec['national_intern']
            );

            if ($ArrRec['tndrType'] == "New_project") {
                  $this->tender_model->insertRecord('bd_tenderdetail', $insertArr);
                $uniq = $this->db->insert_id();

                $insertscopeArr = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'New_project',
                    'businessunit_id' => 1,
                );

                $insertscopeArr1 = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'New_project',
                    'businessunit_id' => 2,
                );
                $uniq1 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr);
                $uniq2 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr1);
            }
            //If Tender Status Active,,,
            if ($ArrRec['tndrType'] == "Active_project") {
                $this->tender_model->insertRecord('bd_tenderdetail', $insertArr);
                $uniq = $this->db->insert_id();
                $insertscopeArr = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'Active_project',
                    'businessunit_id' => 1,
                );
                $uniq1 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr);
                $insertscopeArr1 = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'Active_project',
                    'businessunit_id' => 2,
                );
                $uniq2 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr1);

                $inserArr = array(
                    'project_id' => $uniq,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'Active Project'
                );

                $Respon = $this->Front_model->insertRecord('bdactive_project_byuser', $inserArr);
            }

            //Insert into Important..
            if ($ArrRec['tndrType'] == "Important_project") {
                 $this->tender_model->insertRecord('bd_tenderdetail', $insertArr);
                 $uniq = $this->db->insert_id();
                $insertscopeArr = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'Important_project',
                    'businessunit_id' => 1,
                );
                $uniq1 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr);
                $insertscopeArr1 = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'Important_project',
                    'businessunit_id' => 2,
                );
                $uniq2 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr1);

                $inserArr = array(
                    'project_id' => $uniq,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'Important Project'
                );

                $Respon = $this->Front_model->insertRecord('bdimportant_project_byuser', $inserArr);
            }
            //To Go Project Status..
            /* if ($ArrRec['tndrType'] == "To_Go_project") {
              $stpArr2 = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $uniq, 'Sector_ID' => $SectIdString[0], 'actionIP' => get_client_ip());
              $this->tender_model->insertRecord('proj_togo_for_user', $stpArr2);
              }
              //Bid Status..
              if ($ArrRec['tndrType'] == "Bid_project") {
              $stpArr2 = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $uniq, 'Sector_ID' => $SectIdString[0], 'actionIP' => get_client_ip());
              $this->tender_model->insertRecord('bid_project', $stpArr2);
              } */
            if ($ArrRec['tndrType'] == "In_Review_project") {
                 $this->tender_model->insertRecord('bd_tenderdetail', $insertArr);
                 $uniq = $this->db->insert_id();
                $insertscopeArr = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'In_Review_project',
                    'businessunit_id' => 1,
                );
                $uniq1 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr);
                $insertscopeArr1 = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'In_Review_project',
                    'businessunit_id' => 2,
                );
                $uniq2 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr1);

                $inserArr = array(
                    'project_id' => $uniq,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'In Review Project'
                );

                $Respon = $this->Front_model->insertRecord('bdinreview_project_byuser', $inserArr);
            }
            $this->session->set_flashdata('success', True);
            redirect(base_url('add_tender'));
        }
    }

    /* public function tender_add() {
      $ArrRec = $_POST;
      if (count($ArrRec) > 3) {
      if (count($_REQUEST['tndrphase']) > 0):
      $paseNameString = implode(",", $_REQUEST['tndrphase']);
      else:
      $paseNameString = "";
      endif;
      $SectIdString = implode(",", $_REQUEST['tendersector']);
      $insertArr = Array('created_By' => $this->session->userdata('loginid'), 'tender24_ID' => "BD" . uniqid(), 'TenderDetails' => strip_tags($ArrRec['editor1']), 'CEG_TenderID' => "CEG" . uniqid() . time(), 'TenderValue' => $ArrRec['tendervalue'], 'Location' => $ArrRec['location'], 'Organization' => $ArrRec['organisation'], 'Tender_url' => $ArrRec['linkurl'], 'Sector_IDs' => $SectIdString, 'Sector_Keyword_IDs' => '', 'Keyword_IDs' => '', 'keyword_phase' => $paseNameString, 'is_active' => '1', 'Expiry_Date' => $ArrRec['deadline'], 'important' => '0', 'Current_status' => $ArrRec['tendervalue'], 'visible_scope' => $ArrRec['tndrType'], 'national_intern' => $ArrRec['national_intern']);
      $uniq = $this->tender_model->insertRecord('bd_tenderdetails', $insertArr);

      //If Tender Status Active,,,
      if ($ArrRec['tndrType'] == "Active_project") {
      $stpArr2 = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $uniq, 'Sector_ID' => $SectIdString[0], 'actionIP' => get_client_ip());
      $this->tender_model->insertRecord('active_project_byuser', $stpArr2);
      }
      //Insert into Important..
      if ($ArrRec['tndrType'] == "Important_project") {
      $stpArr2 = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $uniq, 'Sector_ID' => $SectIdString[0], 'actionIP' => get_client_ip());
      $this->tender_model->insertRecord('import_for_user', $stpArr2);
      }
      //To Go Project Status..
      if ($ArrRec['tndrType'] == "To_Go_project") {
      $stpArr2 = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $uniq, 'Sector_ID' => $SectIdString[0], 'actionIP' => get_client_ip());
      $this->tender_model->insertRecord('proj_togo_for_user', $stpArr2);
      }
      //Bid Status..
      if ($ArrRec['tndrType'] == "Bid_project") {
      $stpArr2 = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $uniq, 'Sector_ID' => $SectIdString[0], 'actionIP' => get_client_ip());
      $this->tender_model->insertRecord('bid_project', $stpArr2);
      }
      if ($ArrRec['tndrType'] == "In_Review_project") {
      $stpArr2 = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $uniq, 'Sector_ID' => $SectIdString[0], 'actionIP' => get_client_ip());
      $this->tender_model->insertRecord('proj_review_for_user', $stpArr2);
      }
      $this->session->set_flashdata('success', True);
      redirect(base_url('tender'));
      }
      } */

    protected function getUploadConfig() {
        $config = array();
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv|xls';
        $config['max_size'] = '200000'; //20MB
        $config['file_name'] = "TenderInfo" . uniqid('search_') . time() . '.csv';
        return $config;
    }

    protected function getUploadConfigfour() {
        $config = array();
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv|xls';
        $config['max_size'] = '200000'; //20MB
        $config['file_name'] = "T24" . uniqid('tender_') . time() . '.csv';
        return $config;
    }

    protected function getUploadConfigfourlink() {
        $config = array();
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv|xls';
        $config['max_size'] = '200000'; //20MB
        $config['file_name'] = "T24" . uniqid('url_') . time() . '.csv';
        return $config;
    }

//Get Active Sector..
    public function GetActiveSector() {
//$this->load->model('Front_model');
        $Rec = $this->tender_model->selectRecord('sector', array('*'), array('is_active' => '1'));
        if ($Rec) {
            return $Rec->result();
        } else {
            return false;
        }
    }

    // Csv Uploaded BY CSG BD coded by jitendra 07-11-2017

    public function tendercsv() {
        $this->load->view('tender_csvupload');
    }

    public function tenderuploadcsv() {
        /*  $allTendrIdsArr = $this->tender_model->alltenderidArr();
          $extIdsArr = array();
         */
        if ($_FILES['tenderinfocsv']['name']) {
            $configThm = $this->getUploadCSv();
            $this->load->library('upload', $configThm);
            $this->upload->initialize($configThm);
            if ($this->upload->do_upload('tenderinfocsv')) {
                $uploadData = $this->upload->data();
                $filepath = $uploadData['full_path'];
                $file = fopen($filepath, 'r');
                $n = 0;
                echo '<pre>';
                while (($line = fgetcsv($file)) !== FALSE) {
                    //print_r($line); 
                    if ($n >= 1) {
                        if ($line[2] != '' || !empty($line[2])) {
                            $sectorIDs = $this->getsectorID($line[2]);
                        } else {
                            $sectorIDs = '';
                        }

                        //InsertData Array
                        $insertDataArr = array(
                            'created_By' => $this->session->userdata('loginid'),
                            'tender24_ID' => "BD" . uniqid(),
                            'TenderDetails' => ($line[0]) ? $line[0] : '',
                            'CEG_TenderID' => "CEG" . uniqid() . time(),
                            'Location' => ($line[4]) ? $line[4] : '',
                            'Organization' => ($line[3]) ? $line[3] : '',
                            'Tender_url' => '',
                            'Sector_IDs' => $sectorIDs,
                            'Sector_Keyword_IDs' => '',
                            'Keyword_IDs' => '',
                            'Keyword_IDs' => '',
                            'is_active' => '1',
                            'Expiry_Date' => '',
                            'source_file' => '1',
                            'important' => '0',
                            'visible_scope' => ($line[9]) ? $line[9] : '',
                            'project_type' => ($line[1]) ? $line[1] : '',
                            'country' => ($line[5]) ? $line[5] : '',
                            'submission_date' => ($line[7]) ? $line[7] : '',
                            'consortium' => ($line[8]) ? $line[8] : '',
                            'date_of_opening' => ($line[13]) ? $line[13] : '',
                            'remarks' => ($line[14]) ? $line[14] : '',
                            'bid_validity' => ($line[16]) ? $line[16] : ''
                        );

                        $lastID = $this->tender_model->insertRecord('bd_tenderdetail', $insertDataArr);

                        $insertprojectmanager = array(
                            'project_id' => $lastID,
                            'assign_to' => "253",
                            'assign_by' => "211"
                        );
                        $projectmanager = $this->tender_model->insertRecord('assign_project_manager', $insertprojectmanager);


                        $insertcompetitors = array(
                            'project_id' => $lastID,
                            'competitors_name' => ($line[10]) ? $line[10] : '',
                            'marks' => ($line[11]) ? $line[11] : '',
                            'awarded' => ($line[12]) ? $line[12] : '',
                        );
                        $competitors = $this->tender_model->insertRecord('competitors_list', $insertcompetitors);
                        $tableID = 'CEGHO/BD/' . $line[6];
                        $insertgeneratedid = array(
                            'project_id' => $lastID,
                            'generated_tenderid' => ($line[6]) ? $tableID : '',
                            'generate_by' => '260'
                        );
                        $tendergeneratedid = $this->tender_model->insertRecord('tender_generated_id', $insertgeneratedid);

                        if ($line[9] == 'To_Go_project') {
                            $inserttogo = array(
                                'user_id' => '211',
                                'project_id' => $lastID,
                                'projectgenid' => ($line[6]) ? $tableID : '',
                            );
                            $proj_togo = $this->tender_model->insertRecord('proj_togo_for_user', $inserttogo);
                        } else if ($line[9] == 'Bid_project') {
                            $insertbid = array(
                                'user_id' => '211',
                                'project_id' => $lastID,
                                'proposal_manager' => '253',
                            );
                            $bid = $this->tender_model->insertRecord('bid_project', $insertbid);
                        } else if ($line[9] == 'No_Submit_project') {
                            $insertnogo = array(
                                'user_id' => '211',
                                'project_id' => $lastID,
                            );
                            $nogo = $this->tender_model->insertRecord('proj_nogo_for_user', $insertnogo);
                        }
                    }
                    $n++;
                } die;
                fclose($file);
                $this->session->set_flashdata('successfile', "Your Tender info CSV Uploaded Success.");
                redirect(base_url('tender/tndrinfocsv'));
            } else {
                $this->session->set_flashdata('errmsg', $this->upload->display_errors());
                redirect(base_url('tender/tndrinfocsv'));
            }
        }
        $this->session->set_flashdata('errmsg', $this->upload->display_errors());
        redirect(base_url('tender/tndrinfocsv'));
    }

    protected function getUploadCSv() {
        $config = array();
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv|xls';
        $config['max_size'] = '200000'; //20MB
        $config['file_name'] = "TenderDetail" . uniqid('search_') . time() . '.csv';
        return $config;
    }

    //SELECT * FROM `sector` WHERE `sectName` LIKE '%airport%'
    public function getsectorID($id) {
        $query = $this->db->query("select fld_id from sector where sectName LIKE '%$id%'");
        $res = $query->result();
        return $res[0]->fld_id;
    }

    public function tenderchk() {
        $this->load->view('tender_add2');
    }

    public function tender_add2() {
        $ArrRec = $_POST;
        /* if(!empty($ArrRec)){
          $insertArr = Array('created_By' => $this->session->userdata('loginid'), 'tender24_ID' => "BD" . uniqid(), 'TenderDetails' => strip_tags($ArrRec['editor1']), 'CEG_TenderID' => "CEG" . uniqid() . time(), 'TenderValue' => '', 'Location' => '', 'Organization' => $ArrRec['organisation'], 'Tender_url' => '', 'Sector_IDs' => '', 'Sector_Keyword_IDs' => '', 'Keyword_IDs' => '', 'keyword_phase' => '', 'is_active' => '1', 'Expiry_Date' => $ArrRec['expiry_date'], 'important' => '0', 'Current_status' => '', 'visible_scope' => 'To_Go_project', 'national_intern' => '');
          $uniq = $this->tender_model->insertRecord('bd_tenderdetails', $insertArr);

          $insertArr1 = Array('project_id'=>$uniq,'generated_tenderid'=>'CEGHO/BD/'.$ArrRec['generated_id'],'generate_type'=>'P','generate_by' => $this->session->userdata('loginid'), 'entrydate' => $ArrRec['financial_date'] );
          $uniq1 = $this->tender_model->insertRecord('tender_generated_id', $insertArr1);

          $insertArr2 = Array('project_id'=>$uniq,'assign_to'=>'346','assign_by'=>$this->session->userdata('loginid'),'filled_by' => $this->session->userdata('loginid'));
          $uniq2 = $this->tender_model->insertRecord('assign_project_manager', $insertArr2);

          $insertArr3 = Array('user_id'=>$this->session->userdata('loginid'),'project_id'=>$uniq,'projectgenid'=>'CEGHO/BD/'.$ArrRec['generated_id']);
          $uniq2 = $this->tender_model->insertRecord('proj_togo_for_user', $insertArr3);
          } */

        if (!empty($ArrRec)) {
            $insertArr = Array('created_By' => $this->session->userdata('loginid'), 'tender24_ID' => "BD" . uniqid(), 'TenderDetails' => strip_tags($ArrRec['editor1']), 'TenderValue' => '', 'Location' => '', 'Organization' => $ArrRec['organisation'], 'Tender_url' => '', 'Sector_IDs' => '', 'Sector_Keyword_IDs' => '', 'Keyword_IDs' => '', 'keyword_phase' => '', 'is_active' => '1', 'Expiry_Date' => $ArrRec['expiry_date'], 'national_intern' => '');
            $uniq = $this->tender_model->insertRecord('bd_tenderdetail', $insertArr);

            $insertArr5 = Array('project_id' => $uniq, 'visible_scope' => 'To_Go_project', 'businessunit_id' => '1');
            $uniq1 = $this->tender_model->insertRecord('bdtender_scope', $insertArr5);

            $insertArr1 = Array('project_id' => $uniq, 'generated_tenderid' => 'CEGHO/BD/' . $ArrRec['generated_id'], 'generate_type' => 'P', 'generate_by' => $this->session->userdata('loginid'), 'entrydate' => $ArrRec['financial_date']);
            $uniq3 = $this->tender_model->insertRecord('tender_generated_id', $insertArr1);

            $insertArr2 = Array('project_id' => $uniq, 'assign_to' => '346', 'assign_by' => $this->session->userdata('loginid'), 'filled_by' => $this->session->userdata('loginid'));
            $uniq2 = $this->tender_model->insertRecord('assign_project_manager', $insertArr2);

            $insertArr3 = Array('user_id' => $this->session->userdata('loginid'), 'project_id' => $uniq, 'message' => 'ToGo Project');
            $uniq5 = $this->tender_model->insertRecord('bdtogo_project_byuser', $insertArr3);
        }
        redirect(base_url('tender/tenderchk'));
    }

    public function traffic() {
        $this->db->select('a.fld_id,a.TenderDetails');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('bdtender_scope as b', 'a.fld_id = b.project_id', 'left');
        $this->db->where('b.visible_scope', 'New_project');
        $this->db->where('b.businessunit_id', '1');
        $this->db->where('a.traffic_status', '0');
        $res = $this->db->get()->result();

        foreach ($res as $value) {
            $tenderDetails = $value->TenderDetails;
            $response = $this->getTrafficSectorSectKeywordId($tenderDetails);
            if (!empty($response['Sector_IDsArr']) || !empty($response['Sector_Keyword_IDs'])) {
                $project_id = $value->fld_id;
                $arr = array('Sector_IDs' => $response['Sector_IDsArr'], 'Sector_Keyword_IDs' => $response['Sector_Keyword_IDs'], 'traffic_status' => 1);
                $result = $this->tender_model->updateRecord('bd_tenderdetail', $arr, array('fld_id' => $project_id));
            }
        }

        if ($result) {
            $this->session->set_flashdata('successfile', "Your Traffic Tender Success.");
            redirect(base_url('newproject'));
        } else {
            $this->session->set_flashdata('successfile', "Please try again.");
            redirect(base_url('newproject'));
        }
    }

    public function getTrafficSectorSectKeywordId($tenderDetails) {
        $Sector_IDsArr = '';
        $Sector_Keyword_IDs = '';
        $SectorKey = $this->tender_model->selectRecord('sectorkey', array('*'), array('is_active' => '1', 'sectID' => 13));
        if ($SectorKey):
            $dataArray = $SectorKey->result();
        endif;
        foreach ($dataArray as $rowdata):
            $keyname = " " . $rowdata->keyName;
            if (strpos($tenderDetails, $keyname) !== false) {
                $sectId = $rowdata->sectID;
                $sectKeyId = $rowdata->fld_id;
                $Sector_IDsArr .= "," . $sectId . ",";
                $Sector_Keyword_IDs .= "," . $sectKeyId;
            }
        endforeach;
        return array('Sector_IDsArr' => $Sector_IDsArr, 'Sector_Keyword_IDs' => $Sector_Keyword_IDs);
    }

    //24-04-2019 Code By Ash.. Script for Project status coppied bdtender_scope to project_status table..
    public function projectstatuscopy() {
        $this->db->select('a.*');
        $this->db->from('bdtender_scope as a');
        $this->db->where('a.businessunit_id', '1');
        $ProjSres = $this->db->get()->result();
        if ($ProjSres) {
            foreach ($ProjSres as $rOw):
                //Cond 1..
                if (($rOw->visible_scope) and ( $rOw->visible_scope == "Bid_project")):
                    echo $rOw->visible_scope;
                    $numCount = $this->numrowproj($rOw->project_id);
                    if ($numCount < 1) {
                        $insrtArr = array("user_id" => "296", "project_id" => $rOw->project_id, "project_status" => "0", "status_date" => date('Y-m-d'));
                        $this->db->insert("bdproject_status", $insrtArr);
                    }
                    echo '<br>';
                endif;
                //Cond 2..
                if (($rOw->visible_scope) and ( $rOw->visible_scope == "No_Submit_project")):
                    echo $rOw->visible_scope;
                    $numCount = $this->numrowproj($rOw->project_id);
                    if ($numCount < 1) {
                        $insrtArr = array("user_id" => "296", "project_id" => $rOw->project_id, "project_status" => "3", "status_date" => date('Y-m-d'));
                        $this->db->insert("bdproject_status", $insrtArr);
                    }
                    echo '<br>';
                endif;
            endforeach;
        }

        echo "drfegy";
        die;
    }

    public function numrowproj($projID) {
        // $projID = '372444';
        $this->db->select('a.id');
        $this->db->from('bdproject_status as a');
        $this->db->where('a.is_active', '1');
        $this->db->where('a.project_id', $projID);

        $ProjSres = $this->db->get()->num_rows();
        return $ProjSres;
    }

}
