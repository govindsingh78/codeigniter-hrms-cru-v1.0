<?php

/* Database connection start */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Manmonth extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Manmonth_model', 'manmonth');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }

        $bduserid = $this->session->userdata('uid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";

        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):

            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    //Dashbord After Login..
    public function index() {
        // $noofActiveProj = $this->noOfActiveProject();
        // $noOfImportantProject = $this->noOfImportantProject();
        // $noOfInActiveTrashProject = $this->noOfInActiveTrashProject();
        // $noOfBidProject = $this->noOfBidProject();
        // $noOfToGoProject = $this->noOfToGoProject();
        // $noOfBidProject = $this->noOfBidProject();

        $this->load->view('manmonth_view');
    }

    // Project Display
    public function projectdata() {
        // $this->db->select('bd.tmid,bd.project_id, tp.*');
        // $this->db->from('bid_project as bd');
        // $this->db->join('tm_projectdata tp', 'bd.tmid = tp.id', 'left');
        //$this->db->join('tm_clients  tc', 'tp.client_id = tc.id', 'left');
        // $this->db->where('bd.project_status ', '1');
        // $query = $this->db->get();
        // if($query->num_rows() > 0) {
        // $resulstArr = $query->result_array();
        // }
        // echo '<pre>';
        // print_r($resulstArr);
        // $this->load->view('manmonth_view',compact('resulstArr'));


        $list = $this->manmonth->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $manmonth) {

            $no++;
            $row = array();
            $row[] = $no;

            /* $row[] = '<a   data-toggle="modal" data-target="#myModal" href="javascript:void(0)" title="Show Detail" onclick="userAllDetail('. $customers->id. ')">'.$customers->employeeId.'</a>' . '&nbsp;&nbsp;'; */
            $row[] = $manmonth->project_name;
            $row[] = $manmonth->project_code;
            $row[] = $manmonth->services;
            $row[] = $manmonth->start_date;
            $row[] = $manmonth->end_date;
            $row[] = $manmonth->total_mm;
            $row[] = $manmonth->construction_mm;
            $row[] = $manmonth->development_mm;
            $row[] = $manmonth->om_mm;
            $row[] = $manmonth->client;
            $row[] = '<a href=' . base_url("/manmonth/view?pid=" . $manmonth->project_id) . '>Details</a>';


            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->manmonth->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function view() {
        print_r($_REQUEST);
        die;
    }

}

/* End of file manmonth.php */
/* Location: ./application/controllers/manmonth.php */