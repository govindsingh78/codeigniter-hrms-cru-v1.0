<?php

/* Database connection start */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Inactiveproject extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Inactiveproject_model', 'inactiveproject');
        $this->load->model('Front_model');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }

        $bduserid = $this->session->userdata('uid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";

        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):

            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    //Dashbord After Login..
    public function index() {
		$title = 'Inactive Project';
        $sectorArr = $this->GetActiveSector();
        $secId = '0';
        $this->load->view('inactiveproject/tender_view', compact('secId', 'sectorArr','title'));
    }

    // Project Display
    public function newProjectAll() {

        $list = $this->inactiveproject->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view = '';
        $action = '';
        foreach ($list as $inactiveproject) {
            if ($bdRole == 1 || $bdRole == 2) {
                $view = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $inactiveproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp' .
                        '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('inactiveproject/projurlview?projID=' . $inactiveproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp';
            } else {
                $action = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $inactiveproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp' .
                        '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('inactiveproject/projurlview?projID=' . $inactiveproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'
                        . '<a  href="javascript:void(0)" title="Restore to Active" onclick="restore(' . "'" . $inactiveproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-repeat"></i></a>' . '&nbsp;&nbsp';
            }
            $detail = $inactiveproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = ($inactiveproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($inactiveproject->Expiry_Date));
            $row[] = $inactiveproject->TenderDetails;
            $row[] = $inactiveproject->Location;
            $row[] = $inactiveproject->Organization;
            $row[] = $view . $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->inactiveproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //Get Active Sector..
    public function GetActiveSector() {
        //$this->load->model('Front_model');
        $Rec = $this->Front_model->selectRecord('sector', array('*'), array('is_active' => '1'));
        if ($Rec) {
            return $Rec->result();
        } else {
            return false;
        }
    }

    
    //For Active Project .....
    public function activeproject() {
        if ($_REQUEST['actid']) {
            $inserArr = array(
                'user_id' => $this->session->userdata('uid'),
                'project_id' => $_REQUEST['actid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('active_project_byuser', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('Active_project', $_REQUEST['actid']);
            $this->notified($_REQUEST['actid'], 'Tender Activated.');
            $this->session->set_flashdata('msg', "Tender Restore into Activated.");
        endif;
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

    //Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('uid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            $this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('uid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('uid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

}
