<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
        $this->load->helper(array('form', 'url'));

        $this->load->model('crutraker/Crutrakermodel');
        $this->load->model('crutraker/Projectvacantposition_model');
        $this->load->model('crutraker/Projectlist_model');

        date_default_timezone_set("Asia/Kolkata");
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);


        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
    }

    public function index() {
        if ($this->session->userdata('uid')) {
            redirect(base_url('newproject'));
        }
        $this->load->view('login_view');
    }

    public function singleScreenLogin() {
        $userID = $this->uri->segment(3);
        $password = $this->uri->segment(4);
        $result = $this->SecondDB_model->checkSingleScreenLogin($userID, $password);
        if ($result) {
            $uDataSessionArr = array('empid' => $result->employeeId, 'uid' => $result->id, 'businessunit_id' => $result->businessunit_id);
            $this->session->set_userdata($uDataSessionArr);
            if ($result->department_id == 14) {
                redirect(base_url('dashboard/cruDashboard'));
            } else {
                //redirect(base_url('newproject'));
                redirect(base_url('dashboard/user_dashboard'));
            }
        } else {
            $this->session->set_flashdata('errormsg', "Userid or Password is invalid");
            redirect(base_url());
        }
    }

    //Send Email by Punit...
    function Email() {
        parent::Controller();
        $this->load->library('email');
    }

    //mail function by punit...
    function demomail() {
        $rec = $this->Front_model->selectRecord('email_send', array('email', 'fld_id'), array('status' => '0'), '', '20');
        if ($rec) {
            $result = $rec->result();

            $ci = get_instance();
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "mail.cegindia.com";
            $config['smtp_port'] = "25";
            $config['smtp_user'] = "marketing@cegindia.com";
            $config['smtp_pass'] = "MARK-2015ceg";
            $config['charset'] = "UTF-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $config['MIME-Version'] = "1.0";
            $config['Content-Transfer-Encoding'] = "8bit";
            $config['Content-Type'] = "multipart/related";

            $ci->email->initialize($config);
            foreach ($result as $email) {
//        $ci->email->from('marketing@cegindia.com');
                $ci->email->from('pharma@cegtesthouse.com');
//        $list = array('rahulsaxenajpr@gmail.com');
//        $list = array('CSukhlecha@cegtesthouse.com');
//        $list = array('pntsrm26@gmail.com');
//        $list = array('marketing@cegindia.com');
//        $list = array('marketing@cegindia.com');
                $ci->email->to($email->email);
//        $ci->email->cc('Marketing@cegindia.com');
                $ci->email->subject('Offering services for testing of drugs / Cosmetics and allied products.');
                $ci->email->message("<html><head>
<meta charset='UTF-8'>
    </head>
<body>
<div style='padding-left: 5px; line-height:5px;'>
    <table width='70%' cellspacing='10'; style='line-height:1;'> <tr><td>
                Respected Sir, </td></tr>
        <tr>
        <td>

We, CEG Test House and Research Center Pvt Ltd ( CEGTH ), are pleased to
introduce ourselves as GLP
certified testing facility approved by the State Drug Controller,
Rajasthan licensed to test all drugs
and devices like Antibiotics, Vitamins, Parenteral Preparations, Sutures,
Ligatures and Surgical dressings as per
National and International Pharmacopeia Standards
(IP, EP, BP, USP, JP, AP etc.) under Drugs & Cosmetics Act

and Rules thereunder. We are also capable of testing of Herbal,
Ayurveda medicines, Medical Devices,
Disinfectants and Preservatives.<br>
<br>
Some of the Analytical Services
offered are:
</td></tr>
        <tr><td>
<table  cellpadding='10'>
    <tr>
        <td style='border-bottom: 1px solid #ccc; border-right: 1px solid #ccc'>
        Assays – HPLC with DAD and RI Detector,  GC, Micro-biologically
        </td>
        <td style='border-bottom: 1px solid #ccc; border-right: 1px solid #ccc'>
        Stability Study,   Method Validation
        </td>
        <td style='border-bottom: 1px solid #ccc;'>
        Trace Metals – AAS,  ICP MS
        </td>
    </tr>
    <tr>
        <td style='border-bottom: 1px solid #ccc; border-right: 1px solid #ccc'>
        Identification – IR, GC, HPLC, UV,TLC
        </td>
        <td style='border-bottom: 1px solid #ccc; border-right: 1px solid #ccc'>
        Impurities,  BET and Sterility Testing
        </td>
        <td style='border-bottom: 1px solid #ccc;'>
        Residual Solvents
        </td>
    </tr>
    <tr>
        <td style='border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;'>
        Dissolution,Disintegration
        </td>
        <td style='border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;'>
        Hardness, Friability, Water Content
        </td>
        <td style='border-bottom: 1px solid #ccc;'> Melting point
        </td>
    </tr>
    <tr>
        <td style='border-right: 1px solid #ccc'>
        Related Substances – GC, TLC, HPLC
        </td>
        <td style='border-right: 1px solid #ccc'>
        Bioburden, Microbiological Testings
        </td>
        <td>
        Sugar Content
        </td>

    </tr>
</table> </td></tr>
        <tr><td><br><br>

 We have technically qualified Scientists on board who provide specialist
 inputs and unbiased multi-disciplinary
analytical testing, method validation, method development and efficacy
studies services to reputed Indian,
 multinational & Govt Organizations. We are committed to deliver highest
 quality prompt services and continuous Customer Support for quality
control and product certification as per various regulatory requirements
at competitive charges.<br><br>
 It would be apt for us to mention the following about CEGTH :<br>
 we are duly accredited / approved by NABL, Bureau of Indian Standards,
 Ministry of Environment and Forest,
Food safety and Standard Authority of India (FSSAI); and are ISO 9001:2008,
ISO 14001:2004 and ISO
18001:2007 (OHSAS) compliant.    Operational since 1984, we are one of the
largest state of the art ISO 17025 certified testing laboratories in
India having 21 CFR Part 11 compliant sophisticated analytical
instrumentation
\like LC-MS-MS, ICP-MS,
GC-MS-MS, GC, HPLC, AAS, FTIR, ELISA etc, automated robotics, data loggers,
recorders, Class 10K
clean rooms with full operation on LIMS. <br></td></tr>
        <tr><td>

We are a well established name for testing of Water, dairy, beverages,
Food & agri products, Industrial
Products, Construction Material, Environmental monitoring and Geo-technical
and
Geological investigations. <br><br>
We are part of Consulting Engineers Group Ltd which is amongst the top
three
Indian consulting organization
in the country and enjoys support of more than 900 professionals working
across our 35 offices in India. Our
Team has executed more than 2000 projects including 500 bridges, 700
building complexes and many Power
based Projects, including Rail Vikas Nigam Ltd, RITES, NTPC, Simhadri,
Ramagundam and NTPC Bijapur. <br><br>
We are seeking an active association with an esteemed pharmaceutical/
herbal products company like yours and
look forward to hearing from you about your specific requirement to
enable us to submit our offer.<br><br>
We will be pleased to offer an inaugural discount on our scheduled
charges
to initiate a business relation.<br><br>
            </td></tr>
        <tr><td>
With Regards  (J.P. Sinha)  Chief General Manager
Encl. :- <ol><li>Approval Letter</li>
    <li>Schedule List for Testing of Drugs.</li></ol>
            </td></tr></table>
<table><tr><td><img src='http://www.cegindia.com/mail-footer-cegth.png' alt=''/></td></tr></table>
  Note :  For any enquiry / discussion you may contact Ms. Jyoti Jain at
  Mob. No. <strong>08890778901.</strong>
</div>
</body></html>");
                $ci->email->attach('C:\Users\AshishYadav\Desktop\Dr. Charu\attachment\Form 37- Approval.pdf');
                $ci->email->attach('C:\Users\AshishYadav\Desktop\Dr. Charu\attachment\SCHEDULE LIST FOR TESTING OF DRUGS.docx');
//        $result = $ci->email->send();
                if ($ci->email->send()) {
                    $res = $this->Front_model->updateRecord('email_send', array('send_status' => 'success', 'status' => '1'), array('fld_id' => $email->fld_id));
                    echo 'success'; // die;
                } else {
                    $res = $this->Front_model->updateRecord('email_send', array('send_status' => 'fail', 'status' => '1'), array('fld_id' => $email->fld_id));
                    echo 'fail/error'; // die;
                }
            }
        }
    }

    public function login() {
        $postArr = $_POST;
        $result = $this->SecondDB_model->checkLogin($postArr['userID'], $postArr['userPWD']);
        if ($result) {
            
           // echo "Test Asheesh"; die;
            
            $uDataSessionArr = array('empid' => $result->employeeId, 'uid' => $result->id, 'businessunit_id' => $result->businessunit_id);
            $this->session->set_userdata($uDataSessionArr);
            if ($result->department_id == 14) {
                redirect(base_url('dashboard/cruDashboard'));
            } else {
                //redirect(base_url('newproject'));
                redirect(base_url('dashboard/user_dashboard'));
            }
        } else {
            $this->session->set_flashdata('errormsg', "Userid or Password is invalid");
            redirect(base_url());
        }
    }

    public function adminlogin() {
        /* if ($this->session->userdata('uid')) {
          redirect(base_url('newproject'));
          } */
        $this->load->view('admin/login_view');
    }

    public function adminlogincheck() {
        $postArr = $_POST;
        /* echo '<pre>';
          print_r($postArr);die; */
        $result = $this->SecondDB_model->checkLogin($postArr['userID'], $postArr['userPWD']);
        if ($result) {
            if ($result->id == 1) {
                $uDataSessionArr = array('empid' => $result->employeeId, 'uid' => $result->id);
                $this->session->set_userdata($uDataSessionArr);
                redirect(base_url('welcome/adminDashboard'));
            } else {
                $this->session->set_flashdata('errormsg', "Userid or Password is invalid");
                redirect(base_url('admin'));
            }
        } else {
            $this->session->set_flashdata('errormsg', "Userid or Password is invalid");
            redirect(base_url('admin'));
        }
    }

    public function adminDashboard() {
        $this->load->view('admin/admindashboard_view');
    }

    public function adminlogout() {
        $uDataSessionArr = array('empid' => null, 'uid' => null);
        $this->session->set_userdata($uDataSessionArr);
        redirect(base_url('admin'));
    }

    public function logout() {
        $uDataSessionArr = array('empid' => null, 'uid' => null);
        $this->session->set_userdata($uDataSessionArr);
        redirect(base_url());
    }

    public function mailcron() {
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "mail.cegindia.com";
        $config['smtp_port'] = "25";
        $config['smtp_user'] = "marketing@cegindia.com";
        $config['smtp_pass'] = "MARK-2015ceg";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $ci->email->initialize($config);
        //$ci->email->from('ts.admin@cegindia.com');
        $ci->email->from('marketing@cegindia.com', 'Do-Not-Reply');
        $userData = $this->db->query('select * from main_users_new');
        $user = $userData->result();
        foreach ($user as $row) {
            $list = $row->emailaddress;
            //$list =  'jitendra00752@gmail.com';
            //$list = array('hrms.admin@cegindia.com');
            $ci->email->cc('Marketing@cegindia.com');
            $ci->email->subject('Happy Holi');
            $ci->email->to($list);
            $message = '<html>
						<head></head>
						<body>
								<div style="width:100%;">
									<div style="background-color:#eeeeee; width:800px; margin:0 auto; position:relative;">
									<div style="padding:20px 20px 50px 20px;">
										<div style="font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:normal; line-height:30px; margin:0 0 20px 0;">
												<div>
											<img src="http://hrms.cegindia.com/holi.jpg" width="100%"/>
									<div>
									</div>
									</div>
											</div>

											<div style="font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:normal; line-height:30px;">
												Regards,<br />
												TS Team <br />
												<b>Consulting Engineers Group Ltd.</b>
											</div>
									</div>
									</div>
								</div>
						</body>
						</html>';
            $ci->email->message($message);
            $ci->email->send();
        }
    }

    public function reminderCron() {
        $beforedate = date("Y-m-d", strtotime('-2 days'));
        $currdate = date("Y-m-d");
        $this->db->select('*');
        $this->db->from('reminderinfo');
        $where = "(reminder_date >= '$beforedate' AND reminder_date = '$currdate')";
        $this->db->where($where);
        $res = $this->db->get()->result();

        foreach ($res as $val) {
            $data['arr'] = array(
                'reminder_date' => $val->reminder_date,
                'reminder_subject' => $val->reminder_subject,
                'reminder_message' => $val->reminder_message
            );
            $data['mailby'] = $this->SecondDB_model->getUserByID($val->entryby);

            $this->db->select('*');
            $this->db->from('bd_tenderdetail');
            $this->db->where('fld_id', $val->project_id);
            $tenderdetail = $this->db->get()->result();
            $data['tenderdetail'] = isset($tenderdetail) ? $tenderdetail[0] : '';

            $msgDetails = $this->load->view('email_template/reminder_email', $data, true);
            $emailArrStr77 = 'bdcru@cegindia.com';
            sendMail("$emailArrStr77", $val->reminder_subject, $msgDetails);
            //sendMail("jitendra00752@gmail.com", $val->reminder_subject, $msgDetails);
            echo $val->project_id . '<br/>';
        }
    }

    public function bidvalidityCron() {
        $beforedate = date("Y-m-d", strtotime('+7 days'));
        $currdate = date("Y-m-d");
        $this->db->select('a.*,b.security_type');
        $this->db->from('project_description as a');
        $this->db->join('bidsecurity_type as b', 'a.bid_securitytype = b.id', 'left');
        $where = "(a.tender_openingdate >= '$beforedate' AND a.tender_openingdate <= '$currdate')";
        //$where = "(a.tender_openingdate = '$beforedate')";
        $this->db->where($where);
        $this->db->where('a.bid_security', 1);
        $this->db->where('a.is_active', 1);
        $this->db->where('b.is_active', 1);
        $res = $this->db->get()->result();
        foreach ($res as $val) {
            $data['arr'] = array(
                'bidvalidatydate' => $val->tender_openingdate,
                'security_type' => $val->security_type,
                'amount' => $val->amount
            );
            $data['mailby'] = $this->SecondDB_model->getUserByID($val->user_id);
            $this->db->select('*');
            $this->db->from('bd_tenderdetail');
            $this->db->where('fld_id', $val->project_id);
            $tenderdetail = $this->db->get()->result();
            $data['tenderdetail'] = isset($tenderdetail) ? $tenderdetail[0] : '';
            $msgDetails = $this->load->view('email_template/bidvalidaityreminder_email', $data, true);
            $emailArrStr77 = 'bdcru@cegindia.com';
            sendMail("$emailArrStr77", 'Bid Validity Expire Message ', $msgDetails);
            //sendMail("jitendra00752@gmail.com", 'Bid Validity Expire Message ', $msgDetails);
            echo $val->project_id . '<br/>';
        }
    }

    //2019 Crown Mail For BD Project... Coode By Asheesh...
    public function crownmail() {
        $beforedate = date("Y-m-d", strtotime('+7 days'));
        $currdate = date("Y-m-d");
        $this->db->select('a.tender_openingdate,b.TenderDetails');
        $this->db->from('project_description as a');
        $this->db->join('bd_tenderdetail as b', 'a.project_id = b.fld_id', 'LEFT');
        $where = "(a.tender_openingdate >= '$currdate' AND a.tender_openingdate <= '$beforedate')";
        $this->db->where($where);
        $this->db->group_by('a.project_id');
        $this->db->where('a.is_active', '1');
        $resArrRec['recData'] = $this->db->get()->result();
        if (count($resArrRec['recData']) > 0):
            $msgDetails = $this->load->view('email_template/bidvalidaityreminder_email_newasheesh', $resArrRec, true);
        endif;
        if (count($resArrRec['recData']) > 0):
            $emailArrStr77 = 'bdcru@cegindia.com,accounts@gmail.com,arvind.mehta@cegindia.com,asdaga@cegindia.com,billing@cegindia.com';
            echo sendMail("$emailArrStr77", 'Bid Validity Notification', $msgDetails);
        endif;
        redirect("http://bd.cegtechno.com/");
    }

    //11-07-2019 .. Code For Approve By Mail..
    public function apprv_by_pc_mail() {
        $actID = $this->uri->segment(3);
        $CoordnUserID = $this->uri->segment(4);
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($actID) {
            $this->db->select("$db2.main_users.userfullname as cruentryby,$db1.bdcruvacant_crupanel.*,$db2.main_employees_summary.userfullname as userfullname_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.main_employees_summary.emailaddress as emailaddress_ihr,$db2.main_employees_summary.contactnumber as contactnumber_ihr");
            $this->db->from("$db1.bdcruvacant_crupanel");
            $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.user_id=$db2.main_employees_summary.user_id", "LEFT");
            $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
            $this->db->join("$db2.main_users", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_users.id", "LEFT");
            $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.id" => $actID));
            $this->db->where(array("$db1.bdcruvacant_crupanel.curr_status" => "Pending"));
            $RowRecdata = $this->db->get()->row();
        }
        if ($RowRecdata):
            $UpDdata = array("$db1.bdcruvacant_crupanel.curr_status" => "Approved", "$db1.bdcruvacant_crupanel.approval_status_chng_by" => $CoordnUserID, "$db1.bdcruvacant_crupanel.approval_status_chng_date" => date("Y-m-d H:i:s"));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $actID);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            if ($reps):
                $this->sendcruteam_after_projectcoordaction($actID, $CoordnUserID);
            endif;
        endif;
        redirect("http://bd.cegtechno.com/");
    }

    public function reject_by_pc_mail() {
        $actID = $this->uri->segment(3);
        $CoordnUserID = $this->uri->segment(4);
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($actID) {
            $this->db->select("$db2.main_users.userfullname as cruentryby,$db1.bdcruvacant_crupanel.*,$db2.main_employees_summary.userfullname as userfullname_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.main_employees_summary.emailaddress as emailaddress_ihr,$db2.main_employees_summary.contactnumber as contactnumber_ihr");
            $this->db->from("$db1.bdcruvacant_crupanel");
            $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.user_id=$db2.main_employees_summary.user_id", "LEFT");
            $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
            $this->db->join("$db2.main_users", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_users.id", "LEFT");
            $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.id" => $actID));
            $this->db->where(array("$db1.bdcruvacant_crupanel.curr_status" => "Pending"));
            $RowRecdata = $this->db->get()->row();
        }
        if ($RowRecdata):
            $UpDdata = array("$db1.bdcruvacant_crupanel.curr_status" => "Rejected", "$db1.bdcruvacant_crupanel.approval_status_chng_by" => $CoordnUserID, "$db1.bdcruvacant_crupanel.approval_status_chng_date" => date("Y-m-d"));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $actID);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            if ($reps):
                $this->sendcruteam_after_projectcoordaction($actID, $CoordnUserID);
            endif;
        endif;
        redirect("http://bd.cegtechno.com/");
    }

    //Send Mail To CruTeam on the Action of Approve or Reject By P-coordinator.
    public function sendcruteam_after_projectcoordaction($vacntID, $coordUserID) {
        $data['data'] = $this->GetRecDetailByID($vacntID);
        $data['coordUserID'] = $coordUserID;
        $msgDetails = $this->load->view('email_template/cvtracker_tocru_actbyprojcoord_email', $data, true);
        $resp = $this->sendcvtrackerMail($data['data']->entrybyemail, "Vacant Position Credential Status Changed", $msgDetails);
        return ($resp) ? $resp : false;
    }

    public function GetRecDetailByID($bdcruTbl_RowId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcruvacant_crupanel.id,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.resourceuser_inhouse.userfullname as resourceuser_inhouse_userfullname,$db2.resourceuser_inhouse.emailaddress as resourceuser_inhouse_emailaddress,$db2.resourceuser_inhouse.contactnumber as resourceuser_inhouse_contactnumber,$db2.tm_projects.project_name,$db1.designation_master_requisition.designation_name,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.entry_date,$db2.main_employees_summary.userfullname as entryby,$db2.coord.userfullname as coord_userfullname,$db1.bdcruvacant_crupanel.approval_status_chng_by,$db1.bdcruvacant_crupanel.approval_status_chng_date,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.step_by_cru,$db1.bdcruvacant_crupanel.cv_sent_to_client,$db2.cvsentclientby.userfullname as cvsentclientby_userfullname,$db2.clntcvappryesornoentryby.userfullname as clntcvappryesornoentryby_userfullname,$db1.bdcruvacant_crupanel.client_cvappr_yesorno,$db1.bdcruvacant_crupanel.interaction_date,$db1.bdcruvacant_crupanel.final_cvappr_yesorno,$db1.bdcruvacant_crupanel.mobilization_date,$db1.bdcruvacant_crupanel.inhouse_or_other,$db2.main_employees_summary.emailaddress as entrybyemail");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as coord", "$db1.bdcruvacant_crupanel.approval_status_chng_by=$db2.coord.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as cvsentclientby", "$db1.bdcruvacant_crupanel.cv_sent_to_client_by=$db2.cvsentclientby.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as clntcvappryesornoentryby", "$db1.bdcruvacant_crupanel.client_cvappr_entryby=$db2.clntcvappryesornoentryby.user_id", "LEFT");
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.bdcruvacant_crupanel.designation_id", 'LEFT');
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.bdcruvacant_crupanel.project_id", 'LEFT');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
        $this->db->join("$db2.main_employees_summary as resourceuser_inhouse", "$db1.bdcruvacant_crupanel.user_id=$db2.resourceuser_inhouse.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.id" => $bdcruTbl_RowId));
        $recDeptArr = $this->db->get()->row();
        return ($recDeptArr) ? $recDeptArr : null;
    }

    function sendcvtrackerMail($to, $subject, $msgDetails) {
        $this->load->library('email');
        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.cegindia.com',
            'smtp_user' => 'marketing@cegindia.com',
            'smtp_pass' => 'MARK-2015ceg',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        ));

        $this->email->from('tender@cegindia.com', 'Do_not_reply_Mail');
        $this->email->to($to);
        // $cc = array('marketing@cegindia.com', 'jobs@cegindia.com', 'gmcru@cegindia.com');
        // $CI->email->cc($cc);
        $this->email->bcc('marketing@cegindia.com');
        $this->email->subject($subject);
        $this->email->message($msgDetails);
        $resp = $this->email->send();
        return ($resp) ? $resp : $this->email->print_debugger();
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */