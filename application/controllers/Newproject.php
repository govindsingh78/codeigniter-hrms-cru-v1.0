<?php
/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);
class Newproject extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Newproject_model', 'newproject');
        $this->load->model('Newproject_national_model', 'newprojectNational');
        $this->load->model('Newproject_Inter_national_model', 'newprojectInterNational');
        $this->load->model('Newproject_MultiLocation_model', 'newprojectMultiLocation');
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
        // $this->load->helper(array('form', 'url', 'user_helper'));
        $this->load->model('Mastermodel');
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('loginid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
            if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else :
                redirect(base_url('welcome/logout'));
            endif;
        }
    }
 
    //Dashbord After Login..
    public function index()
    {
        // $businessID = $this->session->userdata('businessunit_id');
        // $bduserid = $this->session->userdata('loginid');
        // echo $businessID; die;
        $title = 'New Project';
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $this->load->view('newproject/tender_view', compact('secId', 'sectorArr', 'title'));
    }

    // Project Display
 
    public function bulkUpdateNational(){
        $list = $this->newproject->get_datatables();

        // echo "<pre/>";
        // print_r($this->db->last_query());  
        
        
        foreach ($list as $newproject) {
            $detailCountryName = explode(" ",$newproject->TenderDetails);
            $locationCountryName = preg_split('/[, ;]/', $newproject->Location);//explode(" ",$newproject->Location);


            $id = $newproject->fld_id;
    


            $this->db->select('*');
            $this->db->from('countries');
            $this->db->order_by("country_name", "ASC");
            $this->db->where('status', 1);
            $filterDetail = $this->db->get()->result();
            foreach($filterDetail as $filterData){

                $countryNameExist = $filterData->country_name;


                //  echo "<pre/>";
                // //  print_r($locationCountryName);
                // //  print_r($countryNameExist);
                //  echo in_array($countryNameExist, $locationCountryName); 
              


                if(in_array($countryNameExist, $locationCountryName) == 1){
                    
                   if(in_array("india", $locationCountryName) == 1 || in_array( "India", $locationCountryName) == 1){
                    
                     // update for national
                     $this->db->where('fld_id',$id); 
                     $this->db->where_not_in('national_intern', [1,2]);
                     $this->db->set('national_intern', 1);
                     $this->db->update('bd_tenderdetail');
                     

                    }else{

                            // update for international
                            $this->db->where('fld_id',$id); 
                            $this->db->where_not_in('national_intern', [1,2]);
                            $this->db->set('national_intern', 2);
                            $this->db->update('bd_tenderdetail');
                            

                    }


                } else if(in_array($countryNameExist, $detailCountryName) == 1){
                    
                    if(in_array("india", $detailCountryName) == 1 || in_array( "India", $detailCountryName) == 1){
                    
                     // update for national
                     $this->db->where('fld_id',$id); 
                     $this->db->where_not_in('national_intern', [1,2]);
                     $this->db->set('national_intern', 1);
                     $this->db->update('bd_tenderdetail');
                    


                    }else{

                            // update for international
                            $this->db->where('fld_id',$id); 
                            $this->db->where_not_in('national_intern', [1,2]);
                            $this->db->set('national_intern', 2);
                            $this->db->update('bd_tenderdetail');
                            

                    }


                }  else{
                    //update for others
                    $this->db->where('fld_id',$id); 
                    $this->db->where_not_in('national_intern', [1,2]);
                    $this->db->set('national_intern', 3);
                    $this->db->update('bd_tenderdetail');
                   
                }

                // echo "<pre/>";
                // print_r($this->db->last_query());    


            }
            
            

           
        }



        
          
		$message = "Bulk data has been updated successfully !";
        
 

		$output = array(
        
			"msg" => $message,
		);
		echo json_encode($output);

         
    }

 

 

    public function newProjectAll()
    {
       
        $list = $this->newproject->get_datatables();


       
        // echo "<pre>"; print_r($this->db->last_query()); die;
        // echo "<pre>"; print_r($list); die;
        // $businessID = $this->session->userdata('businessunit_id');
        // echo $businessID;
        // echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $newproject) {
            $count = $this->Mastermodel->count_rows('comment_on_project', array('project_id' => $newproject->fld_id, 'is_active' => '1'));
            if ($count < 1) :
                $comment = '';
            else :
                $comment = '';
            endif;
            $detail = $newproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($newproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($newproject->Expiry_Date));
            //$row[] = "<small>" . str_replace("_x000D_", "<br>", strip_tags($newproject->TenderDetails)) . "</small>";
            // $row[] = $newproject->Expiry_Date;
            $row[] = ucFirst($newproject->TenderDetails);
            $row[] = $newproject->Location;
            $row[] = $newproject->Organization;

            // $row[] = $comment . '<i style="cursor:pointer" title="Active" onclick="activethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-ok"></i>&nbsp&nbsp' .
            //         '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('newproject/projurlview?projID=' . $newproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'
            //         . '<a  href="javascript:void(0)" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>' . '&nbsp;&nbsp;<input name="actchk[]" value="' . $newproject->fld_id . '" type="checkbox">';

            
           
          
            $row[] = $comment . ' <button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$newproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
            </button>&nbsp&nbsp<button   class="btn btn-success btn-sm" title="Active" onclick="activethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-bookmark" aria-hidden="true"></i>
            </button>&nbsp&nbsp' . '<button   class="btn btn-danger btn-sm" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="icon-trash"></i></button>' . '&nbsp;&nbsp;
                        <label class="fancy-checkbox"><input type="checkbox" name="actchk[]" value="' . $newproject->fld_id . '">
                        <span></span></label>';


            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->newproject->count_all(),
            "recordsFiltered" => $this->newproject->count_filtered(),
            "data" => $data,
        );
         
        echo json_encode($output);




    }

    // Project Display National
    public function newProjectNational()
    {
        $list = $this->newproject->get_datatables();


       

        // echo "<pre>"; print_r($list); die;
        // $businessID = $this->session->userdata('businessunit_id');
        // echo $businessID;
        // echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $newproject) {
            $count = $this->Mastermodel->count_rows('comment_on_project', array('project_id' => $newproject->fld_id, 'is_active' => '1'));
            if ($count < 1) :
                $comment = '';
            else :
                $comment = '';
            endif;
            $detail = $newproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($newproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($newproject->Expiry_Date));
            //$row[] = "<small>" . str_replace("_x000D_", "<br>", strip_tags($newproject->TenderDetails)) . "</small>";
            // $row[] = $newproject->Expiry_Date;
            $row[] = ucFirst($newproject->TenderDetails);
            $row[] = $newproject->Location;
            $row[] = $newproject->Organization;

            // $row[] = $comment . '<i style="cursor:pointer" title="Active" onclick="activethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-ok"></i>&nbsp&nbsp' .
            //         '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('newproject/projurlview?projID=' . $newproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'
            //         . '<a  href="javascript:void(0)" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>' . '&nbsp;&nbsp;<input name="actchk[]" value="' . $newproject->fld_id . '" type="checkbox">';




            $row[] = $comment . '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$newproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
            </button>&nbsp&nbsp<button   class="btn btn-success btn-sm" title="Active" onclick="activethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-bookmark" aria-hidden="true"></i>
            </button>&nbsp&nbsp' . '<button   class="btn btn-danger btn-sm" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="icon-trash"></i></button>' . '&nbsp;&nbsp;
                        <label class="fancy-checkbox"><input  type="checkbox" name="actchk[]" value="' . $newproject->fld_id . '">
                        <span></span></label>';


            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => $this->newproject->count_filtered(),
            "recordsFiltered" => $this->newproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);


    }

    // Project Display National
    public function newProjectInterNational()
    {
        $list = $this->newproject->get_datatables();


       

        // echo "<pre>"; print_r($list); die;
        // $businessID = $this->session->userdata('businessunit_id');
        // echo $businessID;
        // echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $newproject) {
            $count = $this->Mastermodel->count_rows('comment_on_project', array('project_id' => $newproject->fld_id, 'is_active' => '1'));
            if ($count < 1) :
                $comment = '';
            else :
                $comment = '';
            endif;
            $detail = $newproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($newproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($newproject->Expiry_Date));
            //$row[] = "<small>" . str_replace("_x000D_", "<br>", strip_tags($newproject->TenderDetails)) . "</small>";
            // $row[] = $newproject->Expiry_Date;
            $row[] = ucFirst($newproject->TenderDetails);
            $row[] = $newproject->Location;
            $row[] = $newproject->Organization;

            // $row[] = $comment . '<i style="cursor:pointer" title="Active" onclick="activethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-ok"></i>&nbsp&nbsp' .
            //         '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('newproject/projurlview?projID=' . $newproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'
            //         . '<a  href="javascript:void(0)" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>' . '&nbsp;&nbsp;<input name="actchk[]" value="' . $newproject->fld_id . '" type="checkbox">';




            $row[] = $comment . '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$newproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
            </button>&nbsp&nbsp<button   class="btn btn-success btn-sm" title="Active" onclick="activethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-bookmark" aria-hidden="true"></i>
            </button>&nbsp&nbsp' . '<button   class="btn btn-danger btn-sm" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="icon-trash"></i></button>' . '&nbsp;&nbsp;
                        <label class="fancy-checkbox"><input  type="checkbox" name="actchk[]" value="' . $newproject->fld_id . '">
                        <span></span></label>';


            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => $this->newproject->count_filtered(),
            "recordsFiltered" => $this->newproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);


    }

    // Project Display National
    public function newProjectMultiLocation()
    {
        $list = $this->newproject->get_datatables();


       

        // echo "<pre>"; print_r($list); die;
        // $businessID = $this->session->userdata('businessunit_id');
        // echo $businessID;
        // echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $newproject) {
            $count = $this->Mastermodel->count_rows('comment_on_project', array('project_id' => $newproject->fld_id, 'is_active' => '1'));
            if ($count < 1) :
                $comment = '';
            else :
                $comment = '';
            endif;
            $detail = $newproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($newproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($newproject->Expiry_Date));
            //$row[] = "<small>" . str_replace("_x000D_", "<br>", strip_tags($newproject->TenderDetails)) . "</small>";
            // $row[] = $newproject->Expiry_Date;
            $row[] = ucFirst($newproject->TenderDetails);
            $row[] = $newproject->Location;
            $row[] = $newproject->Organization;

            // $row[] = $comment . '<i style="cursor:pointer" title="Active" onclick="activethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-ok"></i>&nbsp&nbsp' .
            //         '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('newproject/projurlview?projID=' . $newproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'
            //         . '<a  href="javascript:void(0)" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>' . '&nbsp;&nbsp;<input name="actchk[]" value="' . $newproject->fld_id . '" type="checkbox">';




            $row[] = $comment . '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$newproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
            </button>&nbsp&nbsp<button   class="btn btn-success btn-sm" title="Active" onclick="activethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-bookmark" aria-hidden="true"></i>
            </button>&nbsp&nbsp' . '<button   class="btn btn-danger btn-sm" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $newproject->fld_id . "','" . 0 . "'" . ')"><i class="icon-trash"></i></button>' . '&nbsp;&nbsp;
                    <label class="fancy-checkbox"><input  type="checkbox" name="actchk[]" value="' . $newproject->fld_id . '">
                        <span></span></label>';


            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => $this->newproject->count_filtered(),
            "recordsFiltered" => $this->newproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);


    }


    //Link View Details Popup Open Ash..
    public function projurlview()
    {
        $projId = $_REQUEST['projID'];
        // echo $projId; die;
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        // echo "<pre>"; print_r($Rec);  die;
        if ($Rec) {
            $RowDataArr = $Rec->row();
            // echo "<pre>"; print_r($RowDataArr);  die;
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        // echo $hyperlink; die;
        //$this->load->view('welcome_link', compact('projId', 'hyperlink'));
 
            
        $output = array(
    
            "link" => $hyperlink,
        );
        //output to json format
        echo json_encode($output);
    }

    //InActive Project ,,,
    public function gototrashbyuser()
    {
        if ($_REQUEST['delid']) {
            $businessID = $this->session->userdata('businessunit_id');
            $id = $_REQUEST['delid'];
            $data = $this->Front_model->getProjectDetail($id);
            //echo '<pre>'; print_r($data); 
            if ($data) {
                $inserArr = array(
                    'created_By' => $data->created_By,
                    'tender24_ID' => $data->tender24_ID,
                    'TenderDetails' => $data->TenderDetails,
                    'Location' => $data->Location,
                    'Organization' => $data->Organization,
                    'Tender_url' => $data->Tender_url,
                    'Sector_IDs' => $data->Sector_IDs,
                    'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
                    'Keyword_IDs' => $data->Keyword_IDs,
                    'keyword_phase' => $data->keyword_phase,
                    'Created_Date' => $data->Created_Date,
                    'Expiry_Date' => $data->Expiry_Date,
                    'source_file' => $data->source_file,
                    'project_type' => $data->project_type,
                    'country' => $data->country,
                    'remarks' => $data->remarks,
                    'national_intern' => $data->national_intern,
                    'national_status' => $data->national_status,
                    'parent_tbl_id' => $data->project_id,
                );
                $inserinactiveArr = array(
                    'project_id' => $data->project_id,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'InActive Project'
                );
                $inserscopedumpArr = array(
                    'project_id' => $data->project_id,
                    'visible_scope' => 'InActive_project',
                    'businessunit_id' => $data->businessunit_id
                );
            }
            $count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $_REQUEST['delid'], 'businessunit_id' => $businessID));
            if ($count <= 5 and $count > 1) :
                $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                $Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
                $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
            else :
                $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                $Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
                $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
                $res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id));
            endif;

            // if ($Respon > 0) :
            //     $this->session->set_flashdata('msg', "Tender Inactivated.");
            // endif;

            if ($Respon > 0) {
                $message = "Tender set to Inactive";
    
            }else{
                $message =  "Some issues occured !";
            }
                
            $output = array(
        
                "msg" => $message,
            );
            //output to json format
            echo json_encode($output);


        }
        /* if ($Respon > 0):
          $this->session->set_flashdata('msg', "Tender Inactivated.");
          endif; */
        /* if ($_REQUEST['delid']) {
          $inserArr = array(
          'user_id' => $this->session->userdata('loginid'),
          'project_id' => $_REQUEST['delid'],
          'actionIP' => get_client_ip());
          $Respon = $this->Front_model->insertRecord('trash_for_user', $inserArr);
          }
          if ($Respon > 0):
          //Visible Activities Change..
          $this->visibilityStatus('InActive_project', $_REQUEST['delid']);
          //Notification Status..
          $this->notified($_REQUEST['delid'], 'Tender Deactivated.');
          $this->session->set_flashdata('msg', "Tender Inactivated.");
          endif; */
    }

    //Active Project By User...
    public function activeproject()
    {


        

        // echo "tgststg"; 
        // echo '<pre>'; print_r($_REQUEST['actid']); 
        if ($_REQUEST['actid']) {
            // echo "tsssssssss"; die;
            $businessID = $this->session->userdata('businessunit_id');
            // echo "test".$businessID; 
            // echo "tst"; 
            $inserArr = array(
                'project_id' => $_REQUEST['actid'],
                'user_id' => $this->session->userdata('loginid'),
                'action_date' => date('Y-m-d H:i:s'),
                'actionIP' => get_client_ip(),
                'message' => 'Active Project'
            );
            // print_r($inserArr); 
            // echo "tst1"; die;
            $Respon = $this->Front_model->insertRecord('bdactive_project_byuser', $inserArr);
            // echo "teststst12";
            $respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $_REQUEST['actid'], 'businessunit_id' => $businessID), array('visible_scope' => 'Active_project'));
            // echo "teststst";
            // print_r($respp); 
        }

        if ($Respon > 0) {
            $message = "Tender set to Active";

        }else{
            $message =  "Some issues occured !";
        }
            
        $output = array(
    
            "msg" => $message,
        );
        //output to json format
        echo json_encode($output);
        // if ($Respon > 0) :
        //     //$this->visibilityStatus('Active_project', $_REQUEST['actid']);
        //     //$this->notified($_REQUEST['actid'], 'Tender Activated.');
        //     $this->session->set_flashdata('msg', "Tender Activated.");
        // endif;
        //die;
        /*  if ($_REQUEST['actid']) {
          $inserArr = array(
          'user_id' => $this->session->userdata('loginid'),
          'project_id' => $_REQUEST['actid'],
          'actionIP' => get_client_ip());
          $Respon = $this->Front_model->insertRecord('active_project_byuser', $inserArr);
          }
          if ($Respon > 0):
          $this->visibilityStatus('Active_project', $_REQUEST['actid']);
          $this->notified($_REQUEST['actid'], 'Tender Activated.');
          $this->session->set_flashdata('msg', "Tender Activated.");
          endif; */
        //redirect(base_url('/newproject'));
    }


    public function newActiveProject()
    {


       
 
        if (!empty($_REQUEST['actchk'])) {

           $arrActid = $_REQUEST['actchk'];
           $act_type = ($_REQUEST['act_type'] == "Active" ? "Active_project" : "InActive_project");

           $Respon = 0;
           $counter = 0;
           
           foreach($arrActid as $actid){

            
            $businessID = $this->session->userdata('businessunit_id');
            $inserArr = array(
                'project_id' => $actid,
                'user_id' => $this->session->userdata('loginid'),
                'action_date' => date('Y-m-d H:i:s'),
                'actionIP' => get_client_ip(),
                'message' => 'Active Project'
            );
            $Respon = $this->Front_model->insertRecord('bdactive_project_byuser', $inserArr);
            $respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $actid, 'businessunit_id' => $businessID), array('visible_scope' => $act_type));
            $counter++;
        
        }
        }

        
        if ($Respon > 0) {
            $message = ($_REQUEST['act_type'] == "Active" ? "Tender ".$counter." set to Active" : "Tender ".$counter." set to Inactive");

        }else{
            $message =  "Some issues occured !";
        }
            
        $output = array(
    
            "msg" => $message,
        );
        //output to json format
        echo json_encode($output);
       
    }


    public function newActiveForNationProject()
    {


       // print_r($_REQUEST); die;
 
        if (!empty($_REQUEST['actchk'])) {

           $arrActid = $_REQUEST['actchk'];
           $act_type = ($_REQUEST['act_type'] == "Inactive" ? "InActive_project" : "Active_project");

           $Respon = 0;
           $counter = 0;
           foreach($arrActid as $actid){
            $businessID = $this->session->userdata('businessunit_id');
            $inserArr = array(
                'project_id' => $actid,
                'user_id' => $this->session->userdata('loginid'),
                'action_date' => date('Y-m-d H:i:s'),
                'actionIP' => get_client_ip(),
                'message' => 'Active Project'
            );
            $Respon = $this->Front_model->insertRecord('bdactive_project_byuser', $inserArr);
            $respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $actid, 'businessunit_id' => $businessID), array('visible_scope' => $act_type));

            if($_REQUEST['act_type'] == "ActiveNational"){
                $data=array('national_intern' => 1);
                $this->db->where('fld_id', $actid);
                $this->db->update('bd_tenderdetail', $data);
            }elseif($_REQUEST['act_type'] == "ActiveInternational"){
                $data=array('national_intern' => 2);
                $this->db->where('fld_id', $actid);
                $this->db->update('bd_tenderdetail', $data);
            }
            $counter++;
           
         }
        }

        
        if ($Respon > 0) {
            $message = ($_REQUEST['act_type'] == "Inactive" ? "Tender ".$counter." set to Inactive" : "Tender ".$counter." set to Active");

        }else{
            $message =  "Some issues occured !";
        }
            
        $output = array(
    
            "msg" => $message,
        );
        //output to json format
        echo json_encode($output);
       
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid)
    {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset()
    {
        $tndrid = $_REQUEST['tndrid'];
        // echo "test";
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));

            // echo "<pre>"; print_r($Rec); 
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

    //Comment Submit..
    public function commentsubmit()
    {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt) :
            $insertArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
        //$this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

    //Project Active By Check Box Multi Action..
    /* public function activeprojectbycheckbox() {
      $chkBoxArr = $this->input->post('actchk');
      $actTyle = $this->input->post('act_type');
      $secId = $_REQUEST['secId'];

      //Bulk Active Process..
      if (count($chkBoxArr) > 0 && ($actTyle == "Active")):
      $countvar = 0;
      foreach ($chkBoxArr as $tndrID) {
      $inserArr = array(
      'user_id' => $this->session->userdata('loginid'),
      'project_id' => $tndrID,
      'actionIP' => get_client_ip());
      $Respon = $this->Front_model->insertRecord('active_project_byuser', $inserArr);
      $this->visibilityStatus('Active_project', $tndrID);
      $countvar ++;
      }
      $this->notified($tndrID, " $countvar Tender Activated.");
      $this->session->set_flashdata('msg', "Total $countvar Tender Activated.");
      redirect(base_url('/newproject'));
      endif;

      //Bulk InActive Process..
      if (count($chkBoxArr) > 0 && ($actTyle == "InActive")):
      $countvar = 0;
      foreach ($chkBoxArr as $tndrID) {
      $inserArr = array(
      'user_id' => $this->session->userdata('loginid'),
      'project_id' => $tndrID,
      'actionIP' => get_client_ip());
      $Respon = $this->Front_model->insertRecord('trash_for_user', $inserArr);
      $this->visibilityStatus('InActive_project', $tndrID);
      $countvar ++;
      }
      $this->notified($tndrID, "Total $countvar Tender Deactivated.");
      $this->session->set_flashdata('msg', "Total $countvar Tender Deactivated.");
      redirect(base_url('/newproject'));
      endif;
      redirect(base_url('/newproject'));
      } */

    public function activeprojectbycheckbox()
    {
        // echo "test"; die;
        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];
        $businessID = $this->session->userdata('businessunit_id');
        //Bulk Active Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "Active")) :
            $countvar = 0;

            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'project_id' => $tndrID,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'Active Project'
                );

                $Respon = $this->Front_model->insertRecord('bdactive_project_byuser', $inserArr);
                $respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $tndrID, 'businessunit_id' => $businessID), array('visible_scope' => 'Active_project'));
                $countvar++;
            }
            $this->session->set_flashdata('msg', "Total $countvar Tender Activated.");
            redirect(base_url('/newproject'));
        endif;

        //Bulk InActive Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "InActive")) :
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $data = $this->Front_model->getProjectDetail($tndrID);
                if ($data) {
                    $inserArr = array(
                        'created_By' => $data->created_By,
                        'tender24_ID' => $data->tender24_ID,
                        'TenderDetails' => $data->TenderDetails,
                        'Location' => $data->Location,
                        'Organization' => $data->Organization,
                        'Tender_url' => $data->Tender_url,
                        'Sector_IDs' => $data->Sector_IDs,
                        'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
                        'Keyword_IDs' => $data->Keyword_IDs,
                        'keyword_phase' => $data->keyword_phase,
                        'Created_Date' => $data->Created_Date,
                        'Expiry_Date' => $data->Expiry_Date,
                        'source_file' => $data->source_file,
                        'project_type' => $data->project_type,
                        'country' => $data->country,
                        'remarks' => $data->remarks,
                        'national_intern' => $data->national_intern,
                        'national_status' => $data->national_status,
                        'parent_tbl_id' => $data->project_id,
                    );

                    $inserinactiveArr = array(
                        'project_id' => $data->project_id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'InActive Project'
                    );

                    $inserscopedumpArr = array(
                        'project_id' => $data->project_id,
                        'visible_scope' => 'InActive_project',
                        'businessunit_id' => $data->businessunit_id
                    );

                    $count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $tndrID, 'businessunit_id' => $businessID));
                    if ($count <= 5 and $count > 1) :
                        $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                        $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                        $Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
                        $res = $this->db->delete('bdtender_scope', array('project_id' => $tndrID, 'businessunit_id' => $businessID));
                    else :
                        $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                        $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                        $Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
                        $res = $this->db->delete('bdtender_scope', array('project_id' => $tndrID, 'businessunit_id' => $businessID));
                        $res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $tndrID));
                    endif;
                    $countvar++;
                }
            }
            $this->session->set_flashdata('msg', "Total $countvar Tender Deactivated.");
            redirect(base_url('/newproject'));
        endif;
        redirect(base_url('/newproject'));
    }

    public function notified($tendrId, $notifData)
    {
        $rowArrData = NotifiedUserIDs($this->session->userdata('loginid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array(
                'user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('loginid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData
            );
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }
}
