<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accountdashboard_model extends CI_Model {

    var $table = 'invoice_after_xlsdownload as a';
	var $invoice_table = 'invoicedetail as b';
   //var $column_order = array(null, 'company_name', 'company_details', 'contact_no', 'email_id'); //set column field database for datatable orderable
   //var $column_search = array('company_name', 'company_details', 'contact_no', 'email_id'); //set column field database for datatable searchable 
    //var $order = array('b.id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $date= date('Y-m');
        $db1 = $this->db1->database;
        $this->db->select("$db1.invoicedetail.id,$db1.invoicedetail.project_numberid,$db1.invoicedetail.invoice_name,$db1.invoicedetail.invoice_date,$db1.invoice_after_xlsdownload.*");
        $this->db->from("$db1.invoicedetail");       
        $this->db->join("$db1.invoice_after_xlsdownload","$db1.invoice_after_xlsdownload.invoice_id=$db1.invoicedetail.id","inner");
        $this->db->like("$db1.invoicedetail.invoice_date", $date);
        $this->db->where("$db1.invoice_after_xlsdownload.srpos_no", "sub_total");

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $ResultRec = $query->result();
        $recordArr = array();
        if ($ResultRec) {
          return $ResultRec;
        }   
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->invoice_table);
        return $this->db->count_all_results();
    }
	
	public function GetprojectTenderName($ten_id){
		$this->db->select('*');
		$this->db->from('bd_tenderdetail');
		$this->db->where('fld_id', $ten_id);
	    $result= $this->db->get()->row_array();
		return ($result) ? $result:'';
	}

    public function GetprojectHrmsName($hrm_id){
		$db1 = $this->db1->database;
        $db2 = $this->db2->database;
		$this->db->select("$db2.tm_projects.*");
		$this->db->from("$db2.tm_projects");
		$this->db->where("$db2.tm_projects.id", $hrm_id);
	    $result= $this->db->get()->row_array();
		return ($result) ? $result:'';
	}

}
