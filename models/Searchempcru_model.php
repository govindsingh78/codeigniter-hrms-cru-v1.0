<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Searchempcru_model extends CI_Model {

    var $table = 'team_assign_member as a';
    var $users_table = 'main_users as b';
    var $team_req_table = 'team_req_otheremp as c';
    var $designation_table = 'designation_master_requisition as d';
    var $bid_table = 'bid_project as e';
    var $column_order = array(null, 'a.TenderDetails', 'a.Location', 'a.Organization', 'a.keyword_phase'); //set column field database for datatable orderable
    var $column_search = array('a.marks', 'a.firm', 'b.userfullname', 'b.emailaddress', 'b.contactnumber', 'c.emp_name', 'c.emp_email', 'c.emp_contact', 'd.designation_name'); //set column field database for datatable searchable 

    //var $order = array('a.fld_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        if (!empty($this->input->post('emp_cond')) == '1') {
            $emp_cond = ' AND ';
        } elseif (!empty($this->input->post('emp_cond')) == '2') {
            $emp_cond = ' OR ';
        } else {
            $emp_cond = '';
        }

        if (!empty($this->input->post('email_cond')) == '1') {
            $email_cond = ' AND ';
        } elseif (!empty($this->input->post('email_cond')) == '2') {
            $email_cond = ' OR ';
        } else {
            $email_cond = '';
        }

        if (!empty($this->input->post('designation_cond')) == '1') {
            $designation_cond = ' AND ';
        } elseif (!empty($this->input->post('designation_cond')) == '2') {
            $designation_cond = ' OR ';
        } else {
            $designation_cond = '';
        }

        if (!empty($this->input->post('contact_cond')) == '1') {
            $contact_cond = ' AND ';
        } elseif (!empty($this->input->post('contact_cond')) == '2') {
            $contact_cond = ' OR ';
        } else {
            $contact_cond = '';
        }

        /* if(!empty($this->input->post('country_cond')) == '1'){
          $country_cond = ' AND ';
          }elseif(!empty($this->input->post('country_cond')) == '2'){
          $country_cond = ' OR ';
          }else{
          $country_cond = '';
          } */

        if (!empty($this->input->post('balance_cond')) == '1') {
            $balance_cond = ' AND ';
        } elseif (!empty($this->input->post('balance_cond')) == '2') {
            $balance_cond = ' OR ';
        } else {
            $balance_cond = '';
        }

        if (!empty($this->input->post('emp_name'))) {
            $empname = $this->input->post('emp_name');
            $emp_name = "($db1.a.empname = '$empname' OR $db1.a.empnameother = '$empname')";
        } else {
            $emp_name = '';
        }

        if (!empty($this->input->post('email_id'))) {
            $email1 = "$db2.b.emailaddress LIKE " . '"%' . $this->input->post('email_id') . '%"';
            $email2 = "$db1.c.emp_email LIKE " . '"%' . $this->input->post('email_id') . '%"';
            $email_id = $email1 . ' OR ' . $email2;
        } else {
            $email_id = '';
        }

        if (!empty($this->input->post('designation_id'))) {
            $designation = $this->input->post('designation_id');
            $designationId = implode(",", $designation);
            $designation_id = " $db1.d.fld_id IN(" . $designationId . ")";
        } else {
            $designation_id = '';
        }

        if (!empty($this->input->post('contact_no'))) {
            $contact1 = "$db2.b.contactnumber LIKE " . '"%' . $this->input->post('contact_no') . '%"';
            $contact2 = "$db1.c.emp_contact LIKE " . '"%' . $this->input->post('contact_no') . '%"';
            $contact_no = $contact1 . ' OR ' . $contact2;
        } else {
            $contact_no = '';
        }

        if ($this->input->post('min_marks') or $this->input->post('max_marks')) {
            if (!empty($this->input->post('min_marks'))) {
                $minMarks = $this->input->post('min_marks');
            } else {
                $minMarks = 0;
            }

            if (!empty($this->input->post('max_marks'))) {
                $maxMarks = $this->input->post('max_marks');
            } else {
                $maxMarks = 100;
            }

            $marks = "($db1.a.marks >= '$minMarks' AND $db1.a.marks <= '$maxMarks')";
        } else {
            $marks = '';
        }

        /* if(!empty($this->input->post('status'))){
          $status = " $db1.e.project_status = ". $this->input->post('status');
          }else{
          $status = '';
          } */
        if (!empty($this->input->post('balance_mm'))) {
            $balance_mm = " $db1.a.balancemm <= " . $this->input->post('balance_mm');
        } else {
            $balance_mm = '';
        }


        if (!empty($emp_cond) != '0' || !empty($email_cond) != '0' || !empty($designation_cond) != '0' || !empty($contact_cond) != '0' || !empty($balance_cond) != '0' ||
                !empty($emp_name) || !empty($email_id) || !empty($designation_id) || !empty($contact_no) || !empty($marks) || !empty($balance_mm)
        ) {
            $wheres = $emp_name . $emp_cond . $email_id . $email_cond . $designation_id . $designation_cond . $contact_no . $contact_cond . $marks . $balance_cond . $balance_mm;
            $this->db->where($wheres);
        }

        /*  $this->db->select('a.empname,a.empnameother,a.marks,a.firm,b.userfullname,b.emailaddress,b.contactnumber,c.emp_name,c.emp_email,c.emp_contact,d.designation_name,e.project_status');
          $this->db->from($this->table);
          $this->db->join($this->users_table,'a.empname = b.fld_id','left');
          $this->db->join($this->team_req_table,'a.empnameother = c.fld_id','left');
          $this->db->join($this->designation_table,'a.designation_id = d.fld_id','left');
          $this->db->join($this->bid_table, 'a.project_id = e.project_id', 'left');
          $this->db->where_in('a.status','1');
          $this->db->group_by('a.empname,a.empnameother'); */

        $this->db->select("$db1.a.empname,$db1.a.empnameother,$db1.a.marks,$db1.a.firm,$db2.b.userfullname,$db2.b.emailaddress,$db2.b.contactnumber,$db1.c.emp_name,$db1.c.emp_email,$db1.c.emp_contact,$db1.d.designation_name,$db1.e.project_status");
        $this->db->from("$db1.assign_finalteam as a");
        $this->db->join("$db2.main_users as b", "$db1.a.empname = $db2.b.fld_id", "left");
        $this->db->join("$db1.team_req_otheremp as c", "$db1.a.empnameother = $db1.c.fld_id", "left");
        $this->db->join("$db1.designation_master_requisition as d", "$db1.a.designation_id = $db1.d.fld_id", "left");
        $this->db->join("$db1.bdproject_status as e", "$db1.a.project_id = $db1.e.project_id", "left");
        $this->db->where("$db1.a.status", '1');
        $this->db->group_by("$db1.a.empname,$db1.a.empnameother");





        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $this->db->where("$db1.a.status", '1');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        // $this->db->from($this->table);
        // $this->db->where('a.is_active', '1');
        // $this->db->where('a.visible_scope', 'Bid_project');
        //  return $this->db->count_all_results();
        return '0';
    }

}
