<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account_assigned_designationlist extends CI_Model {

    var $table = 'assign_finalteam as a';
    var $column_order = array(null, 'project_id');
    var $column_search = array('project_id');

    public function __construct() {
        parent::__construct();
        //$this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.assign_finalteam.teamstatus,$db1.assign_finalteam.id,$db1.assign_finalteam.project_id,$db2.tm_projects.project_name,$db1.designationcat_master.ktype_name,$db1.main_company.company_name,$db1.assign_finalteam.status,$db1.designation_master_requisition.designation_name");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id=$db1.assign_finalteam.project_id", "LEFT");
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", "LEFT");
        $this->db->join("$db1.designationcat_master", "$db1.designationcat_master.k_id=$db1.assign_finalteam.designatin_categid", "LEFT");
        $this->db->join("$db1.main_company", "$db1.main_company.fld_id=$db1.assign_finalteam.company_id", "LEFT");
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.assign_finalteam.designation_id", "LEFT");
        $this->db->where("$db1.accountinfo.status", '1');
        $this->db->where("$db1.assign_finalteam.status", '1');

        if ($this->input->post('bdproject_id')) {
            $this->db->where("$db1.assign_finalteam.project_id", $this->input->post('bdproject_id'));
        }
        if ($this->input->post('design_categ_id')) {
            $this->db->where("$db1.assign_finalteam.designatin_categid", $this->input->post('design_categ_id'));
        }
        $this->db->group_by("$db1.assign_finalteam.id");
        $this->db->order_by("$db1.assign_finalteam.id", "DESC");

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    
    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
         $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->_get_datatables_query();
        $query = $this->db->get();
        
        return $query->num_rows();
    }

    public function count_all() {
         $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
